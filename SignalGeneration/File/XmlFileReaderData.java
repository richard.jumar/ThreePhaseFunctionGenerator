package File;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import Controller.DataControl;
import Data.AddSignal;
import Data.AmplitudeModulation;
import Data.Angle;
import Data.ConstSignal;
import Data.Frequency;
import Data.FrequencyModulation;
import Data.Harmonic;
import Data.MultAmplitudeModulation;
import Data.MultSignalAmplitudeModulation;
import Data.NonPeriodicAmplitudeModulationGain;
import Data.NonPeriodicAmplitudeModulationRamp;
import Data.NonPeriodicAmplitudeModulationStep;
import Data.NonPeriodicInterpolation;
import Data.NonPeriodicInterpolationPoint;
import Data.NonPeriodicLinearInterpolation;
import Data.NonPeriodicPartwiseDefinedAmplitudeModulation;
import Data.NonPeriodicPartwiseDefinedSignal;
import Data.NonPeriodicPointInterpolationAmplitudeModulation;
import Data.NonPeriodicPointInterpolationSignal;
import Data.NonPeriodicSignalRecorded;
import Data.NonPeriodicSplineInterpolation;
import Data.PeriodicAmplitudeModulationRamp;
import Data.PeriodicAmplitudeModulationStep;
import Data.PeriodicInterpolation;
import Data.PeriodicLinearInterpolation;
import Data.PeriodicPartwiseDefinedAmplitudeModulation;
import Data.PeriodicPartwiseDefinedSignal;
import Data.PeriodicPointInterpolationAmplitudeModulation;
import Data.PeriodicPointInterpolationSignal;
import Data.PeriodicSplineInterpolation;
import Data.Phasor;
import Data.PolynomSegment;
import Data.Signal;
import Data.Wave;
import GUI.DisplayFunctions;

/**
 * Data structure for read xml files
 */
public class XmlFileReaderData {
	
	private Element root;
	private Signal signal;
	private AmplitudeModulation am;
	private List<Element> phasors;
	private String path;
	private List<Phasor> usedPhasors;
	private List<Phasor> phasorToReplace_oldPhasor;
	private List<String> phasorToReplace_newName;
	private List<XmlFileReaderData> phasorToReplace_subXml;
	private boolean error;
	private List<Phasor> availablePhasors;
	
	public XmlFileReaderData(String path) {
		this.error = false;
		this.path = path;
		this.phasorToReplace_oldPhasor = new ArrayList<Phasor>();
		this.phasorToReplace_newName = new ArrayList<String>();
		this.phasorToReplace_subXml = new ArrayList<XmlFileReaderData>();
		this.usedPhasors = new ArrayList<Phasor>();
		try {
			Document xmlDocument = new SAXBuilder().build(path);
			root = xmlDocument.getRootElement();
			this.phasors = root.getChildren("phasor");
		} catch (JDOMException | IOException e) {
			FileLog.log("file " + path + " doesn't exist or couldn't be opend.");
			this.error = true;
		}
	}
	public XmlFileReaderData(Element root, List<Element> phasorElements, List<Phasor> usedPhasors, List<Phasor> availablePhasors) {
		this.error = false;
		this.path = "";
		this.root = root;
		this.phasorToReplace_oldPhasor = new ArrayList<Phasor>();
		this.phasorToReplace_newName = new ArrayList<String>();
		this.phasorToReplace_subXml = new ArrayList<XmlFileReaderData>();
		if (usedPhasors != null) {
			this.usedPhasors = usedPhasors;
		} else {
			this.usedPhasors = new ArrayList<Phasor>();
		}
		if (phasorElements != null) {
			this.phasors = phasorElements;
		} else {
			this.phasors = new ArrayList<Element>();
		}
		if (availablePhasors == null) {
			this.availablePhasors = new ArrayList<Phasor>();
		} else {
			this.availablePhasors = availablePhasors;
		}
	}
	
	public void setTopLevelPhasors(List<Element> phasorElements, List<Phasor> usedPhasors) {
		this.phasors = phasorElements;
		this.usedPhasors = usedPhasors;
	}
	
	public String getPath() {
		return this.path;
	}
	
	/**
	 * 
	 * @param embedded embeds all linked files
	 */
	public void builtData() {
		if (!error) {
			if (root.getChildren("canvas").size() > 0) {
				if (root.getChild("canvas").getChildren("topleveloutput").size() > 0) {
					if (root.getChild("canvas").getChild("topleveloutput").getChildren("signaloutput").size() > 0) {
						FileLog.log(this.path + ": Baue Signalbaum auf");
						this.signal = readSignalOutput(root.getChild("canvas").getChild("topleveloutput").getChild("signaloutput"));
						if (this.signal == null) {
							FileLog.log(this.path + ": Signalbaumaufbau fehlgeschlagen");
						} else {
							FileLog.log(this.path + ": Signalbaum fertig aufgebaut");

							for (int i = 0; i < this.phasorToReplace_oldPhasor.size(); i++) {
								this.phasorToReplace_subXml.get(i).getSignal().replacePhasor(phasorToReplace_oldPhasor.get(i), getPhasor(this.phasorToReplace_newName.get(i)));
							}
						}
					} else if (root.getChild("canvas").getChild("topleveloutput").getChildren("amplitudemodulationoutput").size() > 0) {
						FileLog.log(this.path + ": Baue AMbaum auf");
						this.am = readAmplitudeModulationOutput(root.getChild("canvas").getChild("topleveloutput").getChild("amplitudemodulationoutput"));
						if (this.am == null) {
							FileLog.log(this.path + ": AMbaumaufbau fehlgeschlagen");
						} else {
							FileLog.log(this.path + ": AMbaum fertig aufgebaut");
						}
					} else {	
						FileLog.log(this.path + ": Kein Signal/AM-Output gefunden");
					}
				} else {
					FileLog.log(this.path + ": topleveloutput-Element fehlt");
				}
			} else {
				FileLog.log(this.path + ": canvas-Element fehlt");
			}
		} else {
			FileLog.log("An error occured before. Unable to build the data");
		}
	}
		
	public Signal getSignal() {
		return this.signal;
	}
	
	public AmplitudeModulation getAmplitudeModulation() {
		return this.am;
	}
	
	private Signal readSignalOutput(Element signalOutputElement) {
		return readChildSignal(signalOutputElement.getChild("child"));
	}
	
	private AmplitudeModulation readAmplitudeModulationOutput(Element amplitudeModulationOutputElement) {
		return readChildAmplitudeModulation(amplitudeModulationOutputElement.getChild("child"));
	}
	
	private boolean isSignal(Element element) {
		if (element.getChildren("harmonic").size() > 0 || element.getChildren("pointinterpolationsignal").size() > 0 || element.getChildren("partwisesignal").size() > 0 || element.getChildren("signalimport").size() > 0 || element.getChildren("const").size() > 0 || element.getChildren("recorded").size() > 0) {
			return true;
		} else if (element.getChildren("addoperator").size() > 0) { 	
			if (element.getChild("addoperator").getChildren("type").size() > 0 && element.getChild("addoperator").getChildText("type").equals("signal")) {
				return true;
			} 
		} else if (element.getChildren("multoperator").size() > 0) {
			if (element.getChild("multoperator").getChildren("type").size() > 0 && element.getChild("multoperator").getChildText("type").equals("signal")) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isAmplitudeModulation(Element element) {
		if (element.getChildren("gain").size() > 0 || element.getChildren("step").size() > 0 || element.getChildren("ramp").size() > 0 || element.getChildren("pointinterpolationamplitudemodulation").size() > 0 || element.getChildren("partwiseamplitudemodulation").size() > 0 || element.getChildren("amplitudemodulationimport").size() > 0) {
			return true;
		} else if (element.getChildren("addoperator").size() > 0) {
			if (element.getChild("addoperator").getChildren("type").size() > 0 && element.getChild("addoperator").getChildText("type").equals("amplitudemodulation")) {
				return true;
			}
		} else if(element.getChildren("multoperator").size() > 0) {
			if (element.getChild("multoperator").getChildren("type").size() > 0 && element.getChild("multoperator").getChildText("type").equals("amplitudemodulation")) {
				return true;
			}
		}
		return false;
	}
	
	private Signal readChildSignal(Element childElement) {
		if (childElement.getChildren("harmonic").size() > 0) {
			return readHarmonic(childElement.getChild("harmonic"));
		} else if (childElement.getChildren("pointinterpolationsignal").size() > 0) {
			return readPointInterpolationSignal(childElement.getChild("pointinterpolationsignal"));
		} else if (childElement.getChildren("partwisesignal").size() > 0) {
			return readSignalFunction(childElement.getChild("partwisesignal"));
		} else if (childElement.getChildren("const").size() > 0) {
			return readConst(childElement.getChild("const"));		
		} else if (childElement.getChildren("addoperator").size() > 0) {
			return readAddOperatorSignal(childElement.getChild("addoperator"));
		} else if (childElement.getChildren("multoperator").size() > 0) {
			return readMultOperatorSignal(childElement.getChild("multoperator"));
		} else if (childElement.getChildren("signalimport").size() > 0) {
			return readSignalImport(childElement.getChild("signalimport"));
		} else if (childElement.getChildren("recorded").size() > 0) {
			return readRecorded(childElement.getChild("recorded"));
		}
		FileLog.log(this.path + ": Signal-Kindelement " + childElement.getChildren().get(0).getName() + " unbekannt oder fehlend");
		return null;
	}
	
	private AmplitudeModulation readChildAmplitudeModulation(Element childElement) {
		if (childElement.getChildren("gain").size() > 0) {
			return readGain(childElement.getChild("gain"));
		} else if (childElement.getChildren("step").size() > 0) {
			return readStep(childElement.getChild("step"));
		} else if (childElement.getChildren("ramp").size() > 0) {
			return readRamp(childElement.getChild("ramp"));
		} else if (childElement.getChildren("multoperator").size() > 0) {
			return readMultOperatorAmplitudeModulation(childElement.getChild("multoperator"));
		} else if (childElement.getChildren("amplitudemodulationimport").size() > 0) {
			return readAmplitudeModulationImport(childElement.getChild("amplitudemodulationimport"));
		} else if (childElement.getChildren("pointinterpolationamplitudemodulation").size() > 0) {
			return readPointInterpolationAmplitudeModulation(childElement.getChild("pointinterpolationamplitudemodulation"));
		} else if (childElement.getChildren("partwiseamplitudemodulation").size() > 0) {
			return readAmplitudeModulationFunction(childElement.getChild("partwiseamplitudemodulation"));
		}
		
		FileLog.log(this.path + ": AM-Kindelement " + childElement.getChildren().get(0).getName() + " unbekannt oder fehlend");
		return null;
	}
	
	private Signal readAddOperatorSignal(Element addOpElement) {
		if (addOpElement.getChild("child0").getChildren().size() > 0 && addOpElement.getChild("child1").getChildren().size() > 0) {
			Signal child0 = readChildSignal(addOpElement.getChild("child0"));
			Signal child1 = readChildSignal(addOpElement.getChild("child1"));
			if (child0 != null && child1 != null) {
				return new AddSignal(child0, child1);
			} else {
				FileLog.log(this.path + ": Ein Kindelement von AddOperator liefert kein Signal");
				return null;
			}
		} else {
			FileLog.log(this.path + ": Addoperator: Kindelemente fehlen");
			return null;
		}
	}
	
	private AmplitudeModulation readMultOperatorAmplitudeModulation(Element multOpElement) {
		if (multOpElement.getChild("child0").getChildren().size() > 0 && multOpElement.getChild("child1").getChildren().size() > 0) {
			AmplitudeModulation child0 = readChildAmplitudeModulation(multOpElement.getChild("child0"));
			AmplitudeModulation child1 = readChildAmplitudeModulation(multOpElement.getChild("child1"));
			if (child0 != null && child1 != null) {
				return new MultAmplitudeModulation(child0, child1);
			} else {
				FileLog.log(this.path + ": Ein Kindelement von der Amplitudenmodulation liefert keine AM");
				return null;
			}
		} else {
			FileLog.log(this.path + ": Multoperator(AM): Kindelemente fehlen");
			return null;
		}
	}

	private Signal readConst(Element constElement) {
		ConstSignal s = new ConstSignal();
		double[] voltage = {Double.parseDouble(constElement.getChildText("voltage0")), Double.parseDouble(constElement.getChildText("voltage1")), Double.parseDouble(constElement.getChildText("voltage2"))};
		s.updateValues(voltage);
		return s;
	}

	private Signal readRecorded(Element recordedElement) {
		NonPeriodicSignalRecorded s = new NonPeriodicSignalRecorded(DataControl.getClock(), recordedElement.getChildText("path"));
		s.setStartTime(Long.parseLong(recordedElement.getChildText("start")));
		if (recordedElement.getChildren("calibrationfile").size() > 0) {
			s.setCalibrationFilePath(recordedElement.getChildText("calibrationfile"));
		}
		if (recordedElement.getChild("path").getAttribute("loadfollowingfiles") != null && recordedElement.getChild("path").getAttributeValue("loadfollowingfiles").equals("true")) {
			s.setLoadNextFile(true);
		} else {
			s.setLoadNextFile(false);
		}
		return s;
	}
	
	private Signal readMultOperatorSignal(Element multOpElement) {
		if (multOpElement.getChildren("child0").size() > 0 && multOpElement.getChildren("child1").size() > 0) {
			if (isSignal(multOpElement.getChild("child0")) && isAmplitudeModulation(multOpElement.getChild("child1"))) {
				Signal child0 = readChildSignal(multOpElement.getChild("child0"));
				AmplitudeModulation child1 = readChildAmplitudeModulation(multOpElement.getChild("child1"));
				if (child0 != null && child1 != null) {
					return new MultSignalAmplitudeModulation(child0, child1);
				} else {
					FileLog.log(this.path + ": Multoperatorsignal: Ein Kindelement liefert kein Signal/AM");
					return null;
				}
			} else if (isAmplitudeModulation(multOpElement.getChild("child0")) && isSignal(multOpElement.getChild("child1"))) {
				AmplitudeModulation child0 = readChildAmplitudeModulation(multOpElement.getChild("child0"));
				Signal child1 = readChildSignal(multOpElement.getChild("child1"));
				if (child0 != null && child1 != null) {
					return new MultSignalAmplitudeModulation(child1, child0);
				} else {
					FileLog.log(this.path + ": Multoperatorsignal: Ein Kindelement liefert keine AM/Signal");
					return null;
				}
			} else {
				FileLog.log(this.path + ": Multoperatorsignal: Falsche Kindtypen");
				return null;
			}
		} else {
			FileLog.log(this.path + ": Multoperatorsignal: Kindelemente fehlen");
			return null;
		}
	}
	
	private Signal readPointInterpolationSignal(Element piElement) {
		try {
			FileLog.log(this.path + ": Reading point interpolation");
			Signal piSignal;
			if (piElement.getChildren("time").size() == 0) { //periodic
				PeriodicPointInterpolationSignal ppiSignal = new PeriodicPointInterpolationSignal(getPhasorFor(piElement.getChildText("phasor")));
				int type = Integer.parseInt(piElement.getChildText("interpolationtype")); 
				PeriodicInterpolation pi;
				if (type == 1) {
					pi = new PeriodicSplineInterpolation();
				} else {
					pi = new PeriodicLinearInterpolation();
				}
				List<Element> pointElements = piElement.getChildren("point");
				List<Point2D> points2D = new ArrayList<Point2D>();
				for (int i = 0; i < pointElements.size(); i++) {
					points2D.add(new Point2D.Double(Double.parseDouble(pointElements.get(i).getChildText("x")), Double.parseDouble(pointElements.get(i).getChildText("y"))));
				}
				if (piElement.getChild("canvaswidth") == null || piElement.getChild("amplitude").getChild("min") == null || piElement.getChild("amplitude").getChild("max") == null || piElement.getChild("amplitude").getChild("canvasmin") == null || piElement.getChild("amplitude").getChild("canvasmax") == null || piElement.getChild("amplitude").getChild("canvasmean") == null) {
					throw new NumberFormatException();
				}
				int canvasWidth = Integer.parseInt(piElement.getChildText("canvaswidth"));
				String min = piElement.getChild("amplitude").getChildText("min");
				String max = piElement.getChild("amplitude").getChildText("max");
				int canvasMin = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmin"));
				int canvasMax = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmax"));
				double canvasMean = Double.parseDouble(piElement.getChild("amplitude").getChildText("canvasmean"));
				ppiSignal.setPointList(DisplayFunctions.canvasValuesToDoubleValues(points2D, min, max, canvasMin, canvasMax, canvasMean, canvasWidth));
				ppiSignal.setInterpolation(pi);
				ppiSignal.interpolate();
				piSignal = ppiSignal;
			} else { //nonperiodic
				long start = Long.parseLong(piElement.getChild("time").getChildText("start"));
				long end = Long.parseLong(piElement.getChild("time").getChildText("end"));
				int type = Integer.parseInt(piElement.getChildText("interpolationtype")); 
				int canvasWidth = Integer.parseInt(piElement.getChildText("canvaswidth"));
				NonPeriodicInterpolation npi;
				if (type == 1) {
					npi = new NonPeriodicSplineInterpolation(start, end);
				} else {
					npi = new NonPeriodicLinearInterpolation(start, end);
				}
				if (piElement.getChild("canvaswidth") == null || piElement.getChild("amplitude").getChild("pitype") == null || piElement.getChild("amplitude").getChild("value") == null) {
					throw new NumberFormatException();
				}
				
				double voltage = 0;
				double voltPerPixel = 0;
				voltage = Double.parseDouble(piElement.getChild("amplitude").getChildText("value"));
				int canvasMin = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmin"));
				int canvasMax = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmax"));
				double canvasMean = Double.parseDouble(piElement.getChild("amplitude").getChildText("canvasmean"));
				if (piElement.getChild("amplitude").getChildText("pitype").equals("zeromeanmax")) {
					voltPerPixel = voltage / (canvasMean - (double) canvasMax);
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("zeromeanmin")) {
					voltPerPixel = voltage / ((double) canvasMean - canvasMin); //neg min voltage => pos voltPerPixel
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("max")) {
					voltPerPixel = voltage / ((double) (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline"))) - canvasMax);
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("min")){ //min
					voltPerPixel = voltage / ((double) (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline")) - canvasMin));
				} else {
					throw new NumberFormatException();
				}
			
				NonPeriodicPointInterpolationSignal npiSignal = new NonPeriodicPointInterpolationSignal(DataControl.getClock(), start, end);
				npiSignal.setInterpolation(npi);
				double microSecondsPerPixel = (double) (end - start) / (canvasWidth + 2); //connection pixels are not displayed (should not be set by the user)
				List<Element> pointElementList = piElement.getChildren("point");
				for (int i = 0; i < pointElementList.size(); i++) {
					if (piElement.getChild("amplitude").getChildText("pitype").equals("min") || piElement.getChild("amplitude").getChildText("pitype").equals("max")) {
						npi.setAbsoluteZeroMode(0);
						npiSignal.addPoint(new NonPeriodicInterpolationPoint(Math.round((Integer.parseInt(pointElementList.get(i).getChildText("x")) + 1) * microSecondsPerPixel), (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline")) - Integer.parseInt(pointElementList.get(i).getChildText("y"))) * voltPerPixel));
					} else {
						npi.setZeroMeanMode();
						npiSignal.addPoint(new NonPeriodicInterpolationPoint(Math.round((Integer.parseInt(pointElementList.get(i).getChildText("x")) + 1) * microSecondsPerPixel), (canvasMean - (double) Integer.parseInt(pointElementList.get(i).getChildText("y"))) * voltPerPixel));
					}
				}
				
				
				npiSignal.interpolate();
				piSignal = npiSignal;
			}
			
			return piSignal;		
		} catch (NumberFormatException e) {
			FileLog.log(this.path + ": missing element in point interpolation signal");
			return null;
		}
	}
	
	private AmplitudeModulation readPointInterpolationAmplitudeModulation(Element piElement) {
		try {
			FileLog.log(this.path + ": Reading point interpolation");
			AmplitudeModulation piAm;
			if (piElement.getChildren("time").size() == 0) { //periodic
				PeriodicPointInterpolationAmplitudeModulation ppiAm = new PeriodicPointInterpolationAmplitudeModulation(getPhasorFor(piElement.getChildText("phasor")));
				int type = Integer.parseInt(piElement.getChildText("interpolationtype")); 
				PeriodicInterpolation pi;
				if (type == 1) {
					pi = new PeriodicSplineInterpolation();
				} else {
					pi = new PeriodicLinearInterpolation();
				}
				List<Element> pointElements = piElement.getChildren("point");
				List<Point2D> points2D = new ArrayList<Point2D>();
				for (int i = 0; i < pointElements.size(); i++) {
					points2D.add(new Point2D.Double(Double.parseDouble(pointElements.get(i).getChildText("x")), Double.parseDouble(pointElements.get(i).getChildText("y"))));
				}
				if (piElement.getChild("canvaswidth") == null || piElement.getChild("amplitude").getChild("min") == null || piElement.getChild("amplitude").getChild("max") == null || piElement.getChild("amplitude").getChild("canvasmin") == null || piElement.getChild("amplitude").getChild("canvasmax") == null || piElement.getChild("amplitude").getChild("canvasmean") == null) {
					throw new NumberFormatException();
				}
				int canvasWidth = Integer.parseInt(piElement.getChildText("canvaswidth"));
				String min = piElement.getChild("amplitude").getChildText("min");
				String max = piElement.getChild("amplitude").getChildText("max");
				int canvasMin = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmin"));
				int canvasMax = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmax"));
				double canvasMean = Double.parseDouble(piElement.getChild("amplitude").getChildText("canvasmean"));
				ppiAm.setPointList(DisplayFunctions.canvasValuesToDoubleValues(points2D, min, max, canvasMin, canvasMax, canvasMean, canvasWidth));
				ppiAm.setInterpolation(pi);
				ppiAm.interpolate();
				piAm = ppiAm;
			} else { //nonperiodic
				long start = Long.parseLong(piElement.getChild("time").getChildText("start"));
				long end = Long.parseLong(piElement.getChild("time").getChildText("end"));
				int type = Integer.parseInt(piElement.getChildText("interpolationtype")); 
				int canvasWidth = Integer.parseInt(piElement.getChildText("canvaswidth"));
				NonPeriodicInterpolation npi;
				if (type == 1) {
					npi = new NonPeriodicSplineInterpolation(start, end);
				} else {
					npi = new NonPeriodicLinearInterpolation(start, end);
				}
				if (piElement.getChild("canvaswidth") == null || piElement.getChild("amplitude").getChild("pitype") == null || piElement.getChild("amplitude").getChild("value") == null) {
					throw new NumberFormatException();
				}
				
				double voltage = 0;
				double voltPerPixel = 0;
				voltage = Double.parseDouble(piElement.getChild("amplitude").getChildText("value"));
				int canvasMin = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmin"));
				int canvasMax = Integer.parseInt(piElement.getChild("amplitude").getChildText("canvasmax"));
				double canvasMean = Double.parseDouble(piElement.getChild("amplitude").getChildText("canvasmean"));
				if (piElement.getChild("amplitude").getChildText("pitype").equals("zeromeanmax")) {
					voltPerPixel = voltage / (canvasMean - (double) canvasMax);
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("zeromeanmin")) {
					voltPerPixel = voltage / ((double) canvasMean - canvasMin); //neg min voltage => pos voltPerPixel
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("max")) {
					voltPerPixel = voltage / ((double) (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline"))) - canvasMax);
				} else if (piElement.getChild("amplitude").getChildText("pitype").equals("min")){ //min
					voltPerPixel = voltage / ((double) (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline")) - canvasMin));
				} else {
					throw new NumberFormatException();
				}
			
				NonPeriodicPointInterpolationAmplitudeModulation npiAm = new NonPeriodicPointInterpolationAmplitudeModulation(DataControl.getClock(), start, end);
				npiAm.setInterpolation(npi);
				double microSecondsPerPixel = (double) (end - start) / (canvasWidth + 2); //connection pixels are not displayed (should not be set by the user)
				List<Element> pointElementList = piElement.getChildren("point");
				for (int i = 0; i < pointElementList.size(); i++) {
					if (piElement.getChild("amplitude").getChildText("pitype").equals("min") || piElement.getChild("amplitude").getChildText("pitype").equals("max")) {
						npi.setAbsoluteZeroMode(0);
						npiAm.addPoint(new NonPeriodicInterpolationPoint(Math.round((Integer.parseInt(pointElementList.get(i).getChildText("x")) + 1) * microSecondsPerPixel), (Double.parseDouble(piElement.getChild("amplitude").getChildText("zeroline")) - Integer.parseInt(pointElementList.get(i).getChildText("y"))) * voltPerPixel));
					} else {
						npi.setZeroMeanMode();
						npiAm.addPoint(new NonPeriodicInterpolationPoint(Math.round((Integer.parseInt(pointElementList.get(i).getChildText("x")) + 1) * microSecondsPerPixel), (canvasMean - (double) Integer.parseInt(pointElementList.get(i).getChildText("y"))) * voltPerPixel));
					}
				}
				
				
				npiAm.interpolate();
				piAm = npiAm;
			}
			
			return piAm;		
		} catch (NumberFormatException e) {
			FileLog.log(this.path + ": missing element in point interpolation amplitude modulation");
			return null;
		}
	}
	
	private Signal readSignalFunction(Element functionElement) {
		List<Element> sectionElements = functionElement.getChildren("section");
		List<PolynomSegment> polynoms = new ArrayList<PolynomSegment>();
		for (int j = 0; j < sectionElements.size(); j++) {
			List<Double> polynom = new ArrayList<Double>();
			List<Element> summandElements = sectionElements.get(j).getChildren("summand");
			for (int k = 0; k < summandElements.size(); k++) {
				while (polynom.size() < Integer.parseInt(summandElements.get(k).getChildText("exponent"))) {
					polynom.add(0d);
				}
				polynom.add(Double.parseDouble(summandElements.get(k).getChildText("coefficient")));
			}
			PolynomSegment section = new PolynomSegment(Long.parseLong(sectionElements.get(j).getChildText("start")), polynom);
			polynoms.add(section);
		}
		Signal signal_return;
		if (functionElement.getChildren("starttime").size() > 0) { //nonperiodic
			NonPeriodicPartwiseDefinedSignal signal = new NonPeriodicPartwiseDefinedSignal(DataControl.getClock());
			signal.setStartTime(Long.parseLong(functionElement.getChildText("starttime")));
			signal.setPolynoms(polynoms);
			signal.setReadyToPlay();
			signal_return = signal;
		} else { //periodic
			PeriodicPartwiseDefinedSignal signal = new PeriodicPartwiseDefinedSignal(getPhasorFor(functionElement.getChildText("phasor")));
			signal.setPolynoms(polynoms);
			signal.setReadyToPlay();
			signal_return = signal;
		}

		
		return signal_return;
	}

	private AmplitudeModulation readAmplitudeModulationFunction(Element functionElement) {
		List<Element> sectionElements = functionElement.getChildren("section");
		List<PolynomSegment> polynoms = new ArrayList<PolynomSegment>();
		for (int j = 0; j < sectionElements.size(); j++) {
			List<Double> polynom = new ArrayList<Double>();
			List<Element> summandElements = sectionElements.get(j).getChildren("summand");
			for (int k = 0; k < summandElements.size(); k++) {
				while (polynom.size() < Integer.parseInt(summandElements.get(k).getChildText("exponent"))) {
					polynom.add(0d);
				}
				polynom.add(Double.parseDouble(summandElements.get(k).getChildText("coefficient")));
			}
			PolynomSegment section = new PolynomSegment(Long.parseLong(sectionElements.get(j).getChildText("start")), polynom);
			polynoms.add(section);
		}
		AmplitudeModulation am; 
		if (functionElement.getChildren("starttime").size() > 0) { //nonperiodic
			am = new NonPeriodicPartwiseDefinedAmplitudeModulation(DataControl.getClock());
			((NonPeriodicPartwiseDefinedAmplitudeModulation)am).setStartTime(Long.parseLong(functionElement.getChildText("starttime")));
			((NonPeriodicPartwiseDefinedAmplitudeModulation)am).setPolynoms(polynoms);
			((NonPeriodicPartwiseDefinedAmplitudeModulation)am).setReadyToPlay();
		} else { //periodic
			am = new PeriodicPartwiseDefinedAmplitudeModulation(getPhasorFor(functionElement.getChildText("phasor")));
			((PeriodicPartwiseDefinedAmplitudeModulation)am).setPolynoms(polynoms);
			((PeriodicPartwiseDefinedAmplitudeModulation)am).setReadyToPlay();
		}
		return am;
	}

	
	private Signal readHarmonic(Element harmonicElement) {
		Harmonic h = new Harmonic(getPhasorFor(harmonicElement.getChildText("phasor")));
		List<Element> waveElements = harmonicElement.getChildren("wave");
		for (int j = 0; j < waveElements.size(); j++) {
			h.addWave(new Wave(Integer.parseInt(waveElements.get(j).getChildText("order")), Double.parseDouble(waveElements.get(j).getChildText("sin")), Double.parseDouble(waveElements.get(j).getChildText("cos"))));
		}
		return h;
	}
	
	private AmplitudeModulation readGain(Element gainElement) {
		double[] gains = {Double.parseDouble(gainElement.getChildText("k0")), Double.parseDouble(gainElement.getChildText("k1")), Double.parseDouble(gainElement.getChildText("k2"))};
		NonPeriodicAmplitudeModulationGain am = new NonPeriodicAmplitudeModulationGain(DataControl.getClock());
		am.setGain(gains);
		return am;
	}
	
	private AmplitudeModulation readStep(Element stepElement) {
		if (stepElement.getChildren("phasor").size() > 0) {
			//periodic
			Phasor phasor = getPhasorFor(stepElement.getChildText("phasor"));
			Angle[] step_ab = new Angle[3];
			Angle[] step_ba = new Angle[3];
			double[] level_a = new double[3];
			double[] level_b = new double[3];
			for (int i = 0; i < 3; i++) {
				step_ab[i] = new Angle(Long.parseLong(stepElement.getChildText("step_ab" + i)));
				step_ba[i] = new Angle(Long.parseLong(stepElement.getChildText("step_ba" + i)));
				level_a[i] = Double.parseDouble(stepElement.getChildText("value_a" + i));
				level_b[i] = Double.parseDouble(stepElement.getChildText("value_b" + i));
			}		
			PeriodicAmplitudeModulationStep step = new PeriodicAmplitudeModulationStep(phasor, level_a, level_b, step_ab, step_ba);
			return step;
		} else {
			//nonPeriodic
			long[] stepTime = new long[3];
			double[] startValue = new double[3];
			double[] endValue = new double[3];
			for (int i = 0; i < 3; i++) {
				stepTime[i] = Long.parseLong(stepElement.getChildText("steptime" + i));
				startValue[i] = Double.parseDouble(stepElement.getChildText("startvalue" + i));
				endValue[i] = Double.parseDouble(stepElement.getChildText("endvalue" + i));
			}
			NonPeriodicAmplitudeModulationStep step = new NonPeriodicAmplitudeModulationStep(DataControl.getClock(), stepTime, startValue, endValue);
			return step;
		}
	}
	
	private AmplitudeModulation readRamp(Element rampElement ) {
		if (rampElement.getChildren("phasor").size() > 0) {
			//periodic
			Phasor phasor = getPhasorFor(rampElement.getChildText("phasor"));
			Angle[] start_ab = new Angle[3];
			Angle[] end_ab = new Angle[3];
			Angle[] start_ba = new Angle[3];
			Angle[] end_ba = new Angle[3];
			double[] value_a = new double[3];
			double[] value_b = new double[3];
			for (int i = 0; i < 3; i++) {
				start_ab[i] = new Angle(Long.parseLong(rampElement.getChildText("start_ab" + i)));
				end_ab[i] = new Angle(Long.parseLong(rampElement.getChildText("end_ab" + i)));
				start_ba[i] = new Angle(Long.parseLong(rampElement.getChildText("start_ba" + i)));
				end_ba[i] = new Angle(Long.parseLong(rampElement.getChildText("end_ba" + i)));
				value_a[i] = Double.parseDouble(rampElement.getChildText("value_a" + i));
				value_b[i] = Double.parseDouble(rampElement.getChildText("value_b" + i));
			}		
			PeriodicAmplitudeModulationRamp ramp = new PeriodicAmplitudeModulationRamp(phasor, start_ab, end_ab, start_ba, end_ba, value_a, value_b);
			return ramp;
		} else {
			//nonPeriodic
			long[] startTime = new long[3];
			long[] endTime = new long[3];
			double[] startValue = new double[3];
			double[] endValue = new double[3];
			for (int i = 0; i < 3; i++) {
				startTime[i] = Long.parseLong(rampElement.getChildText("starttime" + i));
				endTime[i] = Long.parseLong(rampElement.getChildText("endtime" + i));
				startValue[i] = Double.parseDouble(rampElement.getChildText("startvalue" + i));
				endValue[i] = Double.parseDouble(rampElement.getChildText("endvalue" + i));
			}
			NonPeriodicAmplitudeModulationRamp ramp = new NonPeriodicAmplitudeModulationRamp(DataControl.getClock(), startTime, endTime, startValue, endValue);
			return ramp;
		}
	}
	
	private Signal readSignalImport(Element signalImportElement) {
		XmlFileReaderData subXml;
		Element phasorImportElement = null;
		if (signalImportElement.getChildText("mode").equals("embedded")) {
			subXml = new XmlFileReaderData(signalImportElement.getChild("subroot"), this.phasors, this.usedPhasors, this.availablePhasors);
		} else {
			subXml = new XmlFileReaderData(signalImportElement.getChildText("path"));
			phasorImportElement = signalImportElement.getChild("phasorimport");
		}
		subXml.builtData();
		if (phasorImportElement != null) { //not embedded case, phasor import rules are defined.
			List<Element> phasorTransferElements = phasorImportElement.getChildren("phasortransfer");
			for (int i = 0; i < phasorTransferElements.size(); i++) {
				Phasor transferPhasor = subXml.getAndRemoveUsedPhasor(phasorTransferElements.get(i).getText());
				if (transferPhasor == null) {
					FileLog.log("Couldn't import phasor " + phasorTransferElements.get(i).getText() + " from file " + subXml.getPath());
					this.error = true;
				} else {
					this.usedPhasors.add(transferPhasor);
				}
			}
			List<Element> phasorRenameElements = phasorImportElement.getChildren("phasorrename");
			for (int i = 0; i < phasorRenameElements.size(); i++) {
				Phasor p = subXml.getAndRemoveUsedPhasor(phasorRenameElements.get(i).getChildText("oldname"));
				if (p == null) {
					FileLog.log("Couldn't import phasor " + phasorRenameElements.get(i).getText() + " from file " + subXml.getPath());
					this.error = true;
				} else {
					p.setName(phasorRenameElements.get(i).getChildText("newname"));
					this.usedPhasors.add(p);
				}
			}
			List<Element> phasorReplaceElements = phasorImportElement.getChildren("phasorreplace");
			for (int i = 0; i < phasorReplaceElements.size(); i++) {
				Phasor oldPhasor = subXml.getAndRemoveUsedPhasor(phasorReplaceElements.get(i).getChildText("oldphasor"));
				if (oldPhasor == null) {
					FileLog.log("Couldn't import phasor " + phasorReplaceElements.get(i).getText() + " from file " + subXml.getPath());
					this.error = true;
				} else {
					this.phasorToReplace_oldPhasor.add(oldPhasor);
					this.phasorToReplace_newName.add(phasorReplaceElements.get(i).getChildText("newphasor"));
					this.phasorToReplace_subXml.add(subXml);
				}
			}
		}
		return subXml.getSignal();
	}
	
	private AmplitudeModulation readAmplitudeModulationImport(Element amplitudeModulationImportElement) {
		XmlFileReaderData subXml;
		if (amplitudeModulationImportElement.getChildText("mode").equals("embedded")) {
			subXml = new XmlFileReaderData(amplitudeModulationImportElement.getChild("subroot"), this.phasors, this.usedPhasors, this.availablePhasors);
		} else {
			subXml = new XmlFileReaderData(amplitudeModulationImportElement.getChildText("path"));
			subXml.setTopLevelPhasors(this.phasors, this.usedPhasors);
		}
		subXml.builtData();
		return subXml.getAmplitudeModulation();
	}
	
	/**
	 * Gets a used phasor and removes it from the usedPhasor list. 
	 * Removing is necessary for phasor transfer to higher levels to avoid name conflicts.
	 * @param phasorName
	 * @return
	 */
	public Phasor getAndRemoveUsedPhasor(String phasorName) {
		for (int i = 0; i < this.usedPhasors.size(); i++) {
			if (this.usedPhasors.get(i).getName().equals(phasorName)) {
				Phasor returnPhasor = this.usedPhasors.get(i);
				this.usedPhasors.remove(returnPhasor);
				return returnPhasor;
			}
		}
		return null;
	}
	
 	public Phasor getPhasor(String phasorName) {
		//check whether the phasor is used before in this xml
		for (int i = 0; i < this.usedPhasors.size(); i++) {
			if (this.usedPhasors.get(i).getName().equals(phasorName)) {
				return this.usedPhasors.get(i);
			}
		}
		//phasor is unused before in this xml, export the phasor into the phasor list
		for (int i = 0; i < this.phasors.size(); i++) {
			if (this.phasors.get(i).getChildText("name").equals(phasorName)) {
				Element frequencyElement = phasors.get(i).getChild("frequency");
				Frequency f = new Frequency(Integer.parseInt(frequencyElement.getChildText("startfrequency")));
				Element frequencyModulationListElement = frequencyElement.getChild("frequencymodulationlist");
				List<Element> fModElements = frequencyModulationListElement.getChildren("frequencymodulation");
				for (int j = 0; j < fModElements.size(); j++) {
					long[] start = new long[3];
					long[] end = new long[3];
					int[] endFrequency = new int[3];
					for (int k = 0; k < 3; k++) {
						start[k] = Long.parseLong(fModElements.get(j).getChildText("start" + k));
						end[k] = Long.parseLong(fModElements.get(j).getChildText("end" + k));
						endFrequency[k] = Integer.parseInt(fModElements.get(j).getChildText("endfrequency" + k));
					}
					f.addFrequencyModulation(new FrequencyModulation(start[0], end[0], endFrequency[0], start[1], end[1], endFrequency[1], start[2], end[2], endFrequency[2]));
				}
				Phasor p = new Phasor(DataControl.getClock(), f);
				Angle startAngle1 = new Angle(Long.parseLong(phasors.get(i).getChildText("startangle0")));
				Angle startAngle2 = new Angle(Long.parseLong(phasors.get(i).getChildText("startangle1")));
				Angle startAngle3 = new Angle(Long.parseLong(phasors.get(i).getChildText("startangle2")));
				p.setStartAngles(startAngle1, startAngle2, startAngle3);
				p.setName(phasorName);
				this.usedPhasors.add(p);
				return p;
			}			
		}
		for (int i = 0; i < this.availablePhasors.size(); i++) {
			if (this.availablePhasors.get(i).getName().equals(phasorName)) {
				this.usedPhasors.add(this.availablePhasors.get(i));
				return this.availablePhasors.get(i);
			}
		}
		FileLog.log("phasor unknown (" + phasorName + ")");
		return null;
	}
	
	
	public List<Phasor> getUsedPhasors() {
		return this.usedPhasors;
	}
	
	public Element getRoot() {
		return this.root;
	}
	
	private Phasor getPhasorFor(String phasorName) {
		return getPhasor(phasorName);
	}
}
