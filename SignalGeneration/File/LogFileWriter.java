package File;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Writer for the log file
 */
public class LogFileWriter {
	public static boolean logToFile(String fileName, String msg) {
		try {
			FileWriter fw = new FileWriter(fileName, true);
			fw.write("["+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd. MM. yyyy kk:mm:ss")) + "]" + msg);
			fw.write(System.lineSeparator());
			fw.close();
			return true;
		} catch (IOException e) {
			System.out.println("log error: " + e.getMessage() + "\n Tryed to log message: '" + msg + "' to file " + fileName);
			return false;
		}
	}
}
