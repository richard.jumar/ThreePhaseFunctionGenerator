package File;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Reader for the ini file
 */
public class IniReaderWriter {
	private File ini;
	public IniReaderWriter(File ini) {
		this.ini = ini;
	}
	
	public String read(String section, String attribute) {
		try {
			FileReader fr;
			fr = new FileReader(ini);		
			BufferedReader br = new BufferedReader(fr);
			String line;
			String value = "";
			boolean isSection = false;
			while ((line = br.readLine()) != null) {
				if (isSection) {
					if (line.contains(attribute)) {
						value = line.split("=")[1];
					} else if (line.startsWith("[")){
						isSection = false;
					}					
				}
				if (line.equals("[" + section + "]")) {
					isSection = true;
				}
			}
			br.close();
			fr.close();
			return value;
		} catch (IOException e) {
			FileLog.log("Tryed to open the preferences.ini: " + e.getMessage());
		}
		return "";
	}
	
	public void save(String newSection, String newAttribute, String newValue) {
		try {
			FileReader fr;
			fr = new FileReader(ini);
			BufferedReader br = new BufferedReader(fr);
			String line;
			boolean isSection = false;
			File tmpFile = new File("tmp.ini");
			FileWriter fw = new FileWriter(tmpFile);
			boolean finished = false;
			while ((line = br.readLine()) != null) {
				if (isSection) {
					if (line.contains(newAttribute)) {
						fw.write(newAttribute + "=" + newValue + "\n");
						finished = true;
					} else if (line.startsWith("[")){
						isSection = false;
						fw.write(line + "\n");
					} else {
						fw.write(line + "\n");
					}
				} else {
					fw.write(line + "\n");
				}
				if (line.equals("[" + newSection + "]")) {
					isSection = true;
				}
			}
			if (!finished) {
				fw.write("\n[" + newSection + "]\n");
				fw.write(newAttribute + "=" + newValue + "\n");
			}
			fw.close();
			br.close();
			fr.close();
			
			String iniPath = ini.getAbsolutePath();
			tmpFile.renameTo(new File(iniPath));
			tmpFile.renameTo(ini);
			ini = tmpFile;
			new File("tmp.ini").delete();
		} catch (IOException e) {
			FileLog.log("Tryed to save the preferences for " + newAttribute + ": " + e.getMessage());
		}
	}
	
}
