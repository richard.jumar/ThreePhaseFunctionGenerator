package File;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * Reader for recorded EDR files
 */
public class XmlFileReaderRecorded {
	private List<Element> edrDataElements;
	private String path;
	private boolean calibrationFullDefined;
	private double[] scaleFactor;
	private double[] offset;
	
	public XmlFileReaderRecorded(String path) {
		this.path = path;
		this.calibrationFullDefined = false;
		this.scaleFactor = new double[3];
		this.offset = new double[3];
		this.offset[0] = Double.MIN_VALUE;
		this.offset[1] = Double.MIN_VALUE;
		this.offset[2] = Double.MIN_VALUE;
		this.scaleFactor[0] = Double.MIN_VALUE;
		this.scaleFactor[1] = Double.MIN_VALUE;
		this.scaleFactor[2] = Double.MIN_VALUE;
	}
	
	public boolean loadNewFile(String path) {
		Document xmlDocument;
		try {
			//Check file whether the last line is undefined (e. g. NUL symbol) and it in this case.
			File xmlFile = new File(path);
			File fixedXmlFile = new File("fixed.xml");
			if (fixedXmlFile.exists()) {
				fixedXmlFile.delete();
			}
			BufferedReader br = new BufferedReader(new FileReader(xmlFile));
			BufferedWriter bw = new BufferedWriter(new FileWriter(fixedXmlFile));
			String line;
			String lastLine = br.readLine(); //used in the first iteration
			if (lastLine != null) {
				while((line = br.readLine()) != null) {
					bw.write(lastLine + "\n");
					lastLine = line;				
				}
				if (lastLine.startsWith("<")) {
					bw.write(lastLine);
				} else {
					FileLog.log("File " + path + " ended with an undefined line: Skiped the last line");
				}
			}
			bw.close();
			br.close();
			//read xml document without null lines
			xmlDocument = new SAXBuilder().build(fixedXmlFile);			
			this.edrDataElements = xmlDocument.getRootElement().getChildren("EDR_RAW_DATA");
			FileLog.log("XmlFileReaderRecorded loaded " + this.edrDataElements.size() + " EDR_RAW_DATA blocks");
			return true;
		} catch (JDOMException e) {
			FileLog.log("XML problem in " + path + ": " + e.getMessage());
		} catch (IOException e) {
			FileLog.log("IO Exception while reading XML file " + path + ": " + e.getMessage());
		}
		return false;
	}

	/**
	 * extracts the sampling rate from the xml file
	 * @return samples per second or -1 if the xml file istn't read
	 */
	public int readSamplesPerSecond() {
		if (this.edrDataElements == null) {
			return -1;
		}
		return Integer.parseInt(this.edrDataElements.get(0).getChild("acquisition").getChildText("samplesPerSecond"));
	}
	
	/**
	 * Set a calibration file. The parameters from this file are used for playing the recorded data
	 * @param path
	 */
	public void setCalibrationFilePath(String path) {
		readCalibrationFile(path);
	}
	
	/**
	 * Reads the calibration file an extracts parameters for voltage offset and voltage scale
	 * @param calibrationFilePath
	 */
	private void readCalibrationFile(String calibrationFilePath) {
		if (calibrationFilePath == null) {
			this.calibrationFullDefined = false;
			return;
		}
		try {
			FileReader fr = new FileReader(calibrationFilePath);
			BufferedReader br = new BufferedReader(fr);
			String line;
			boolean calibrationSection = false;
			while ((line = br.readLine()) != null) {
				if (calibrationSection) {
					if (line.contains("VoltageScaleCh1")) {
						this.scaleFactor[0] = Double.parseDouble(line.split("=")[1]);
					} else if (line.contains("VoltageOffsetCh1")) {
						this.offset[0] = Double.parseDouble(line.split("=")[1]);
					} else if (line.contains("VoltageScaleCh2")) {
						this.scaleFactor[1] = Double.parseDouble(line.split("=")[1].trim());
					} else if (line.contains("VoltageOffsetCh2")) {
						this.offset[1] = Double.parseDouble(line.split("=")[1].trim());
					} else if (line.contains("VoltageScaleCh3")) {
						this.scaleFactor[2] = Double.parseDouble(line.split("=")[1].trim());
					} else if (line.contains("VoltageOffsetCh3")) {
						this.offset[2] = Double.parseDouble(line.split("=")[1].trim());
					} else if (line.startsWith("[")){
						calibrationSection = false;
					} 					
				} 
				if (line.equals("[Calibration]")) {
					calibrationSection = true;
				}
			}
			br.close();
			this.calibrationFullDefined = true;
			for (int i = 0; i < 3; i++) {
				if (this.scaleFactor[i] == Double.MIN_VALUE || this.offset[i] == Double.MIN_VALUE) {
					this.calibrationFullDefined = false;
				}
			}
			
		} catch (IOException e) {
			FileLog.log("Unable to read file " + calibrationFilePath);
			this.calibrationFullDefined = false;
		}
	}
	
	/**
	 * Returns the number of EDR_RAW_DATA blocks in this file. Each block includes a recording with a duration of one second 
	 * @return number of EDR_RAW_DATA blocks or -1 if there is no file loaded
	 */
	public int getEdrDataSize() {
		if (this.edrDataElements == null) {
			return -1;
		}
		return this.edrDataElements.size();
	}
	
	
	/**
	 * Get the data
	 * @param index
	 * @return
	 */
	public double[][] getData(int index) {
		List<Element> dataElements = edrDataElements.get(index).getChildren("data");
		String[] dataString = new String[3]; 
		int length = 0;
		//check attribute name
		//old version: channel
		//new version: name
		String attributeName = "undef";
		if (dataElements.size() > 0 && dataElements.get(0).getAttribute("name") != null) {
			attributeName = "name";
		} else if (dataElements.size() > 0 && dataElements.get(0).getAttribute("channel") != null) {
			attributeName = "channel";
		}
		if (attributeName.equals("undef")) {
			FileLog.log("Cannot find attribute name or channel in data element in file " + path);
			double[][] return0V = new double[1][3];
			return0V[0][0] = 0;
			return0V[0][1] = 0;
			return0V[0][2] = 0;
			return return0V;
		}
		
		for (int i = 0; i < dataElements.size(); i++) {
			if (dataElements.get(i).getAttribute(attributeName).getValue().equals("La")) {
				dataString[0] = dataElements.get(i).getText();
				if (! this.calibrationFullDefined) {
					scaleFactor[0] = Double.parseDouble(dataElements.get(i).getAttributeValue("scaleFactor"));
					if (dataElements.get(i).getAttribute("offset") != null) {
						this.offset[0] = Double.parseDouble(dataElements.get(i).getAttributeValue("offset"));
					} else {
						this.offset[0] = 10;
					}
				}
				length = Integer.parseInt(dataElements.get(i).getAttributeValue("samples"));
			} else if (dataElements.get(i).getAttribute(attributeName).getValue().equals("Lb")) {
				dataString[1] = dataElements.get(i).getText();
				if (! this.calibrationFullDefined) {
					scaleFactor[1] = Double.parseDouble(dataElements.get(i).getAttributeValue("scaleFactor"));
					if (dataElements.get(i).getAttribute("offset") != null) {
						this.offset[1] = Double.parseDouble(dataElements.get(i).getAttributeValue("offset"));
					} else {
						this.offset[1] = 10;
					}
				}
			} else if (dataElements.get(i).getAttribute(attributeName).getValue().equals("Lc")) {
				dataString[2] = dataElements.get(i).getText();
				if (! this.calibrationFullDefined) {
					scaleFactor[2] = Double.parseDouble(dataElements.get(i).getAttributeValue("scaleFactor"));
					if (dataElements.get(i).getAttribute("offset") != null) {
						this.offset[2] = Double.parseDouble(dataElements.get(i).getAttributeValue("offset"));
					} else {
						this.offset[2] = 10;
					}
				}
			}
		}
		boolean errorA = false;
		boolean errorB = false;
		boolean errorC = false;
		if (dataString[0] == null) {
			FileLog.log(this.path + ": Channel La is missing. Set output on 0V");
			errorA = true;
		}
		if (dataString[1] == null) {
			FileLog.log(this.path + ": Channel Lb is missing. Set output on 0V");
			errorB = true;
		}
		if (dataString[2] == null) {
			FileLog.log(this.path + ": Channel Lc is missing. Set output on 0V");
			errorC = true;
		}
		/* ==================================
		 * Der Decoder kann die Base64 kodierten Rohdaten nicht dekodieren. 
		 * Die Kodierung entspricht nicht RFC 4648. 
		 * Anhand der Anzahl der Samplingwerte wird der Base64-String f�r den Decoder angepasst. 
		 * Die Anzahl der Bytes muss ein ganzzahliges Vielfaches von 4 sein, evtl. Verschnitt wird mit dem Paddingsymbol '=' gef�llt.
		 * ==================================
		 */
		
		byte[][] dataBytes = new byte[3][length * 2];
		if (errorA) {
			for (int i = 0; i < dataBytes[0].length; i++) {
				dataBytes[0][i] = 0;
			}
		} else {
			dataBytes[0] =Base64.getDecoder().decode(fixBase64(dataString[0], length));
		}
		if (errorB) {
			for (int i = 0; i < dataBytes[1].length; i++) {
				dataBytes[1][i] = 0;
			}
		} else {
			dataBytes[1] = Base64.getDecoder().decode(fixBase64(dataString[1], length));
		}
		if (errorC) {
			for (int i = 0; i < dataBytes[2].length; i++) {
				dataBytes[2][i] = 0;
			}
		} else {
			dataBytes[2] = Base64.getDecoder().decode(fixBase64(dataString[2], length));
		}
		double[][] voltageValues = new double[length][3];
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < 3; j++) {
				voltageValues[i][j] = valueFunction(dataBytes[j][(2 * i) + 1], dataBytes[j][2 * i], this.offset[j]) * scaleFactor[j];
			}
		}
		return voltageValues;
	}
	
	/**
	 * fix base64 string from XML files for using the decode method
	 * @param input
	 * @param samples
	 * @return
	 */
	private String fixBase64(String input, int samples) {
		//necessary string length: samples 2 * 4 / 3. 
		// 16 bit = 2 byte; 4 chars per 3 byte
		int base64stringLength = samples * 8;
		int residueClass = base64stringLength % 3;
		if (residueClass == 0) {
			base64stringLength = base64stringLength / 3; 
		} else {
			base64stringLength = (base64stringLength / 3) + 1; 
		}
		String data = input.substring(0, base64stringLength);
		if (residueClass == 1) { //16 bit are missing, 
			data += "=="; //fill 12 bit
		} else if (residueClass == 2) { // 8 bit are missing
			data += "="; // fill 6 bit
		}
		return data;
	}
	
	
	/**
	 * Function for converting the values from the recording-xml to voltage (without scalefactor)
	 * ((double)data[uShort16] / 0xffff) * 20.0 - 10.0
	 * @param high high byte (of the 16bit unsigned value)
	 * @param low low byte (of the 16bit unsigned value)
	 * @return
	 */
	private double valueFunction(byte high, byte low, double offset) {
		int value16bit = ((int) low & 0xFF) + 256 * ((int) high & 0xFF);
		return (((double) value16bit) / ((double) 0xffff) * 20d) - offset;
	}	
}
