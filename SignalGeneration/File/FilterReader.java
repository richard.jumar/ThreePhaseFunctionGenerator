package File;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reader for FIR filters created with MATLAB filterDesigner as ASCII coded coefficient files
 */
public class FilterReader {
	private File filterFile;
	private double[] coefficients;
	public FilterReader(File filterFile) {
		this.filterFile = filterFile;
	}
	
	
	public double[] getCoefficients() {
		return this.coefficients;
	}
	
	/**
	 * 
	 * @return 1: ok, 0: file error, -1: no/wrong filter type (Discrete-Time FIR Filter expected), -2: no/wrong filter structure (expected Direct-Form FIR), -3: Filter Length is missing, -4: wrong coefficient format (expected Decimal), -5: Missing "Numerator" Line, -6: Not enough/invalid values   
	 */
	public int readFilter() {
		try {
			FileReader fr;
			fr = new FileReader(this.filterFile);		
			BufferedReader br = new BufferedReader(fr);
			String line;
			int step = 0; //step 0: looking for "Discrete-Time FIR Filter (real)", step 1: Looking for "Filter Structure: Direct-Form FIR"
			String coeffFormat = "";
			boolean typeOk = false;
			String structure = "";
			String length = "";
			double[] coefficients = null;
			int coeffIndex = 0;
			while ((line = br.readLine()) != null) {
				if (step == 0) {
					if (line.contains("Coefficient Format")) {
						coeffFormat = line;
					} else if (line.contains("Discrete-Time FIR Filter (real)")) {
						typeOk = true;
					} else if (line.contains("Filter Structure")) {
						structure = line;
					} else if (line.contains("Filter Length")) {
						length = line;
					} else if (line.contains("Numerator")) {
						step = 1;
					}
				} else if (step == 1) {
					if (typeOk) {
						if (structure.contains("Direct-Form FIR")) {
							if (length.split(":").length < 2) {
								br.close();
								fr.close();
								return -3;
							} else {
								try {
									int length_int = Integer.parseInt(length.split(":")[1].trim());	
									coefficients = new double[length_int];
								} catch (NumberFormatException e) {
									br.close();
									fr.close();
									return -3;
								}
								if (coeffFormat.contains("Decimal")) {
									step = 2;
								} else {
									br.close();
									fr.close();
									return -4;
								}
							}
						} else {
							br.close();
							fr.close();
							return -2;
						}
					} else {
						br.close();
						fr.close();
						return -1;
					}
				}
				if (step == 2) { //no "else if"!
					try {
						coefficients[coeffIndex] = Double.parseDouble(line.trim());
						coeffIndex++;
						if (coeffIndex == coefficients.length) {
							step = 3;
						}
					} catch (NumberFormatException e) {
						FileLog.log("Reading filter, problem in line " + line);
						br.close();
						fr.close();
						return -6;
					}
				}				
			}
			br.close();
			fr.close();
			if (step < 2) {
				return -5;
			} else if (coeffIndex != coefficients.length) {
				return -6;
			}
			this.coefficients = coefficients;
			return 1;
			
		} catch (IOException e) {
			FileLog.log("Tryed to open the filter file: " + e.getMessage());
			return 0;
		}
	}
}
