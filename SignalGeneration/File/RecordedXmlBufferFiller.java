package File;

import java.io.File;

import Player.QueueElement;
import Player.RecordedSignalBuffer;

public class RecordedXmlBufferFiller implements Runnable {
	
	private XmlFileReaderRecorded xml;
	private String path;
	private RecordedSignalBuffer buffer;
	private boolean finished;
	private boolean loadNextFile;
	
	/**
	 * Buffer filler for recorded samples (from xml file)
	 * activate auto loading following files by setLoadNextFile()
	 * @param path first xml file
	 * @param buffer
	 */
	public RecordedXmlBufferFiller(String path, RecordedSignalBuffer buffer, boolean loadNextFile) {
		this.loadNextFile = true;
		this.path = path;
		this.buffer = buffer;
		this.xml = new XmlFileReaderRecorded(this.path);
		this.loadNextFile = loadNextFile;
	}
	
	/**
	 * Number of Samples per second in the recording
	 * @return
	 */
	public int getSamplesPerSecond() {
		return this.xml.readSamplesPerSecond();
	}
	
	/**
	 * Set a calibration file for using
	 * @param path calibration file path
	 */
	public void setCalibrationFilePath(String path) {
		this.xml.setCalibrationFilePath(path);
	}
	
	/**
	 * run buffer filling
	 */
	public void run() {
		finished = false;
		String tmpPath = this.path;
		this.buffer.removeAll();
		this.xml.loadNewFile(tmpPath);
		while (! finished) {		
			FileLog.log("Reading EDR XML file" + tmpPath + "(" + xml.getEdrDataSize() + " rows)");
			if (xml.getEdrDataSize() < 0) {
				FileLog.log("Cannot read EDR XML file " + tmpPath);
			}
			for (int j = 0; j < xml.getEdrDataSize() ; j++) { //all 1s rows in a file
				double[][] values = xml.getData(j);

				for (int i = 0; i < values.length; i++) {
					buffer.push(new QueueElement(values[i]));
				}
				while (buffer.getElementCount() > 100000 && !finished) { //advance should not be greater than 100 000
					try {
						synchronized (this) {
							wait();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} 				
			}
			FileLog.log("Finished reading of file" + tmpPath);
			tmpPath = nextFilePath(tmpPath);
			File nextFile = new File(tmpPath);
			if ((!loadNextFile) || tmpPath.isEmpty() || (! nextFile.exists())) { //finish now
				finished = true;				
			} else { //load next file
				this.xml.loadNewFile(tmpPath);
			}
		}
		buffer.setFinished();
	}
	
	/**
	 * activate stop running of the thread
	 */
	public void activateStop() {
		this.finished = true;
	}
	
	/**
	 * compute the path of the next file by incrementing datetime
	 * @param path
	 * @return
	 */
	public static String nextFilePath(String path) {
		try {
			String[] dateTime = path.substring(path.length() - 17, path.length() - 4).split("-");
			String prefix = path.substring(0, path.length() - 17);
			int date = Integer.parseInt(dateTime[0]);
			int time = Integer.parseInt(dateTime[1]);
			if (time % 100 == 59) {
				if ((time / 100) % 24 == 23) {
					return prefix + dateTimeFormat(incDate(date), 0) + ".xml";
				} else {
					return prefix + dateTimeFormat(date, time + 41) + ".xml";
				}
			} else {
				return prefix + dateTimeFormat(date, time + 1) + ".xml";
			}
		} catch (Exception e) {
			//wrong format
			return "";
		}
	}
	
	/**
	 * format date and time for file name
	 * @param date
	 * @param time
	 * @return
	 */
	private static String dateTimeFormat(int date, int time) {
		String timeString = time + "";
		while(timeString.length() < 4) {
			timeString = "0" + timeString;
		}
		return date + "-" + timeString;
	}
	
	/**
	 * increment a date
	 * @param date
	 * @return
	 */
	private static int incDate(int date) {
		int year = date / 10000;
		int month = (date % 10000) / 100;
		int day = date % 100;
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
			//31 days per month
			if (day < 31) {
				return (date + 1);
			} else {
				if (month < 12) {
					return (date + 70);
				} else {
					return (date + 8870);
				}
			}
		} else if (month == 2) {
			//february
			if (year % 4 == 0) { //Schaltjahr /leap year
				if (day < 29) {
					return (date + 1);
				} else {
					return (date + 72);
				}
			} else {
				if (day < 28) {
					return (date + 1);
				} else {
					return (date + 73);
				}
			}
		} else {
			//30 days
			if (day < 30) {
				return (date + 1);
			} else {
				return (date + 71);
			}
		}
			
	}
}
