package File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import Controller.DataControl;
import Data.Harmonic;
import Data.NonPeriodicPartwiseDefinedAmplitudeModulation;
import Data.NonPeriodicPartwiseDefinedSignal;
import Data.NonPeriodicPointInterpolationAmplitudeModulation;
import Data.NonPeriodicPointInterpolationSignal;
import GUI.Canvas;
import GUI.CanvasBox;
import GUI.CanvasBoxAmplitudeModulationFunction;
import GUI.CanvasBoxAmplitudeModulationImport;
import GUI.CanvasBoxAmplitudeModulationOutput;
import GUI.CanvasBoxAmplitudeModulationPointInterpolation;
import GUI.CanvasBoxConst;
import GUI.CanvasBoxSignalFunction;
import GUI.CanvasBoxGain;
import GUI.CanvasBoxHarmonic;
import GUI.CanvasBoxPointInterpolation;
import GUI.CanvasBoxRamp;
import GUI.CanvasBoxSignalImport;
import GUI.CanvasBoxSignalInputRecorded;
import GUI.CanvasBoxSignalOutput;
import GUI.CanvasBoxSignalPointInterpolation;
import GUI.CanvasBoxStep;
import GUI.CanvasOperator;
import GUI.CanvasOperatorAdd;
import GUI.CanvasTreeElement;
import GUI.ConnectorType;

/**
 * Writer for saving in xml format
 */
public class XmlFileWriter {
	
	private Element rootElement;
	private Element canvasElement;
	
	private Document xmlDoc;
	private String path;
	private Canvas canvas;
	
	public XmlFileWriter(String path, Canvas canvas) {
		
		this.path = path;
		this.canvas = canvas;
		this.rootElement = new Element("root");
		this.canvasElement = new Element("canvas");
		this.rootElement.addContent(canvasElement);
		
		xmlDoc = new Document(rootElement);
	
	}
	
	/**
	 * saves the xml document
	 * @return successfull
	 */
	public boolean save() {
		buildPhasors();
		try {
			buildXmlCanvas();
			XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileOutputStream(path));
			return true;
		} catch (FileNotFoundException e) {
			File.FileLog.log("ERROR: Couldn't create the file " + path);
			return false;
		} catch (IOException e) {
			File.FileLog.log("ERROR: IOException while writing the file " + path + ": " + e.getMessage());
			return false;
		} catch (JDOMException e) {
			File.FileLog.log("ERROR: JDOM Exception while creating the xml file " + path + ": " + e.getMessage());
			return false;
		}
	}
	
	/**
	 * creates the xml elements for the phasors
	 */
	private void buildPhasors() {
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			Element phasorElement = new Element("phasor");
			Element frequencyElement = new Element("frequency");
			frequencyElement.addContent(new Element("startfrequency").addContent(DataControl.getPhasors().get(i).getFrequency().getF0_phase1() + ""));
			phasorElement.addContent(new Element("name").addContent(DataControl.getPhasors().get(i).getName()));
			phasorElement.addContent(new Element("startangle0").addContent(DataControl.getPhasors().get(i).getPhase1Start().getAngle() + ""));
			phasorElement.addContent(new Element("startangle1").addContent(DataControl.getPhasors().get(i).getPhase2Start().getAngle() + ""));
			phasorElement.addContent(new Element("startangle2").addContent(DataControl.getPhasors().get(i).getPhase3Start().getAngle() + ""));
			Element frequencyModulationListElement = new Element("frequencymodulationlist");
			for (int j = 0; j < DataControl.getPhasors().get(i).getFrequency().getFrequencyModulationList().size(); j++) {
				Element frequencyModulationElement = new Element("frequencymodulation");
				for (int k = 0; k < 3; k++) {
					frequencyModulationElement.addContent(new Element("start" + k).addContent(DataControl.getPhasors().get(i).getFrequency().getFrequencyModulationList().get(j).getStart()[k] + ""));
					frequencyModulationElement.addContent(new Element("end" + k).addContent(DataControl.getPhasors().get(i).getFrequency().getFrequencyModulationList().get(j).getEnd()[k] + ""));
					frequencyModulationElement.addContent(new Element("endfrequency" + k).addContent(DataControl.getPhasors().get(i).getFrequency().getFrequencyModulationList().get(j).getFEnd()[k] + ""));
				}
				frequencyModulationListElement.addContent(frequencyModulationElement);
			}
			frequencyElement.addContent(frequencyModulationListElement);
			phasorElement.addContent(frequencyElement);
			this.rootElement.addContent(phasorElement);
		}
	}
	
	
	/**
	 * creates the canvas xml element
	 * @throws JDOMException
	 * @throws IOException
	 */
	private void buildXmlCanvas() throws JDOMException, IOException {
		for (int i = 0; i < canvas.getTreeElements().size(); i++) {
			//if this "canvas component" has a parent it will be considered over the parent
			if (!canvas.getTreeElements().get(i).hasParent()) {
				canvasElement.addContent(buildElement(canvas.getTreeElements().get(i))); 
			} 
		}
	}
	
	/**
	 * creates the xml element for a canvas operator
	 * @param co
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private Element buildElement(CanvasOperator co) throws JDOMException, IOException {
		Element canvasOperatorElement = null;	
		if (co instanceof CanvasOperatorAdd) {
			canvasOperatorElement = new Element("addoperator");
		} else { //if (co instanceof CanvasOperatorMult) {
			canvasOperatorElement = new Element("multoperator");
		}
		canvasOperatorElement.addContent(gui(co));
		if (co.getOutputConnector().getAlternativeType() == ConnectorType.nothing) { //type is defined
			if (co.getOutputConnector().getType() == ConnectorType.SignalOutput) {
				canvasOperatorElement.addContent(new Element("type").addContent("signal"));
			} else if (co.getOutputConnector().getType() == ConnectorType.AMOutput) {
				canvasOperatorElement.addContent(new Element("type").addContent("amplitudemodulation"));
			}
		}
		if (co.getConnector0().isConnected()) {
			Element child0Element = new Element("child0");
			child0Element.addContent(buildElement(co.getConnector0().getConnectedOutputConnector().getTreeElement()));
			canvasOperatorElement.addContent(child0Element);
		}
		if (co.getConnector1().isConnected()) {
			Element child1Element = new Element("child1");
			child1Element.addContent(buildElement(co.getConnector1().getConnectedOutputConnector().getTreeElement()));
			canvasOperatorElement.addContent(child1Element);
		}
		return canvasOperatorElement;
	}
	
	/**
	 * creates the xml element for a tree element
	 * @param te
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private Element buildElement(CanvasTreeElement te) throws JDOMException, IOException {
		if (te instanceof CanvasBox) {
			return buildElement((CanvasBox) te);

		} else if (te instanceof CanvasOperator) {
			return buildElement((CanvasOperator) te);

		} else {
			Canvas.canvasLog("XmlFileWriter: Unknown tree element " + te.toString());
		}
		return null;
	}
	
	
	/**
	 * creates the xml element for a canvas box
	 * @param cb
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private Element buildElement(CanvasBox cb) throws JDOMException, IOException {
		Element cbElement;
		if (cb instanceof CanvasBoxSignalOutput) {
			cbElement = new Element("signaloutput");
		} else if (cb instanceof CanvasBoxAmplitudeModulationOutput) {
			cbElement = new Element("amplitudemodulationoutput");
		} else if (cb instanceof CanvasBoxSignalFunction) {
			cbElement = new Element("partwisesignal");
		} else if (cb instanceof CanvasBoxAmplitudeModulationFunction) {
			cbElement = new Element("partwiseamplitudemodulation");
		} else if (cb instanceof CanvasBoxHarmonic) {
			cbElement = new Element("harmonic");
		} else if (cb instanceof CanvasBoxSignalPointInterpolation) {
			cbElement = new Element("pointinterpolationsignal");
		} else if (cb instanceof CanvasBoxAmplitudeModulationPointInterpolation) {
			cbElement = new Element("pointinterpolationamplitudemodulation");
		} else if (cb instanceof CanvasBoxGain) {
			cbElement = new Element("gain");	
		} else if (cb instanceof CanvasBoxStep) {	
			cbElement = new Element("step");
		} else if (cb instanceof CanvasBoxRamp) {
			cbElement = new Element("ramp");
		} else if (cb instanceof CanvasBoxSignalImport) {
			cbElement = new Element("signalimport");	
		} else if (cb instanceof CanvasBoxAmplitudeModulationImport) {
			cbElement = new Element("amplitudemodulationimport");
		} else if (cb instanceof CanvasBoxConst) {
			cbElement = new Element("const");		
		} else if (cb instanceof CanvasBoxSignalInputRecorded) {
			cbElement = new Element("recorded");
		} else {
			cbElement = new Element("undefinedcanvasbox");
		}
		cbElement.addContent(gui(cb));
		if (cb instanceof CanvasBoxSignalOutput || cb instanceof CanvasBoxAmplitudeModulationOutput) {
			if (cb.hasChild()) {
				Element childElement = new Element("child");
				childElement.addContent(buildElement(cb.getInputConnector().getConnectedOutputConnector().getTreeElement()));
				cbElement.addContent(childElement);
			}
			//special case for toplevel output elements
			Element topLevelOutputElement = new Element("topleveloutput");
			topLevelOutputElement.addContent(cbElement);
			return topLevelOutputElement;
		} else if (cb instanceof CanvasBoxHarmonic) {
			Harmonic h = ((CanvasBoxHarmonic) cb).getHarmonic();
			if (h.getPhasor() != null) {
				cbElement.addContent(new Element("phasor").addContent(h.getPhasor().getName()));
			}
			for (int i = 0; i < h.getWaves().size(); i++) {
				Element waveElement = new Element("wave");
				waveElement.addContent(new Element("order").addContent(h.getWaves().get(i).getOrder() + ""));
				waveElement.addContent(new Element("sin").addContent(h.getWaves().get(i).getSinAmplitude() + ""));
				waveElement.addContent(new Element("cos").addContent(h.getWaves().get(i).getCosAmplitude() + ""));
				cbElement.addContent(waveElement);
			}
		} else if (cb instanceof CanvasBoxSignalFunction) {
			CanvasBoxSignalFunction cbf = (CanvasBoxSignalFunction) cb;
			if (cbf.isPeriodic()) {
				cbElement.addContent(new Element("phasor").addContent(cbf.getSelectedPhasor().getName()));
			} else {
				cbElement.addContent(new Element("starttime").addContent(((NonPeriodicPartwiseDefinedSignal) cbf.getSignal()).getStartTime() + ""));
			}
			for (int i = 0; i < cbf.getPolynoms().size(); i++) {
				Element sectionElement = new Element("section");
				sectionElement.addContent(new Element("start").addContent(cbf.getPolynoms().get(i).getStart() + ""));
				for (int j = 0; j < cbf.getPolynoms().get(i).getParameters().size(); j++) {
					Element summandElement = new Element("summand");
					summandElement.addContent(new Element("coefficient").addContent(cbf.getPolynoms().get(i).getParameters().get(j) + ""));
					summandElement.addContent(new Element("exponent").addContent(j + ""));
					sectionElement.addContent(summandElement);
				}
				cbElement.addContent(sectionElement);
			}
		} else if (cb instanceof CanvasBoxAmplitudeModulationFunction) {
			CanvasBoxAmplitudeModulationFunction cbf = (CanvasBoxAmplitudeModulationFunction) cb;
			if (cbf.isPeriodic()) {
				cbElement.addContent(new Element("phasor").addContent(cbf.getSelectedPhasor().getName()));
			} else {
				cbElement.addContent(new Element("starttime").addContent(( (NonPeriodicPartwiseDefinedAmplitudeModulation) cbf.getAmplitudeModulation()).getStartTime() + ""));
			}
			for (int i = 0; i < cbf.getPolynoms().size(); i++) {
				Element sectionElement = new Element("section");
				sectionElement.addContent(new Element("start").addContent(cbf.getPolynoms().get(i).getStart() + ""));
				for (int j = 0; j < cbf.getPolynoms().get(i).getParameters().size(); j++) {
					Element summandElement = new Element("summand");
					summandElement.addContent(new Element("coefficient").addContent(cbf.getPolynoms().get(i).getParameters().get(j) + ""));
					summandElement.addContent(new Element("exponent").addContent(j + ""));
					sectionElement.addContent(summandElement);
				}
				cbElement.addContent(sectionElement);
			}
			
		} else if (cb instanceof CanvasBoxPointInterpolation) { //for CanvasBoxSignalPointInterpolation and CanvasBoxAmplitudeModulationInterpolation
			if (((CanvasBoxPointInterpolation)cb).isPeriodicSelected()) {
				if (((CanvasBoxPointInterpolation)cb).getSelectedPhasor() != null) {
					cbElement.addContent(new Element("phasor").addContent(((CanvasBoxPointInterpolation)cb).getSelectedPhasor().getName()));
				}

				Element amplitudeElement = new Element("amplitude");
				amplitudeElement.addContent(new Element("max").addContent(((CanvasBoxPointInterpolation)cb).getEdt_maxVoltageText()));
				amplitudeElement.addContent(new Element("min").addContent(((CanvasBoxPointInterpolation)cb).getEdt_minVoltageText()));
				amplitudeElement.addContent(new Element("canvasmin").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMin() + ""));
				amplitudeElement.addContent(new Element("canvasmax").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMax() + ""));
				amplitudeElement.addContent(new Element("canvasmean").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMean() + ""));
				cbElement.addContent(amplitudeElement);
				cbElement.addContent(new Element("canvaswidth").addContent(((CanvasBoxPointInterpolation)cb).getCanvasWidth() + ""));
			} else { //nonperiodic
				Element amplitudeElement = new Element("amplitude");
				amplitudeElement.addContent(new Element("pitype").addContent(((CanvasBoxPointInterpolation)cb).getAmplitudeDefinitionType()));
				amplitudeElement.addContent(new Element("value").addContent(((CanvasBoxPointInterpolation)cb).getEdt_NPVoltageText()));
				cbElement.addContent(amplitudeElement);
				Element timeElement = new Element("time");
				if (cb instanceof CanvasBoxSignalPointInterpolation) {
					timeElement.addContent(new Element("start").addContent(((NonPeriodicPointInterpolationSignal) (((CanvasBoxSignalPointInterpolation)cb).getSignal())).getStartTime() + ""));
					timeElement.addContent(new Element("end").addContent(((NonPeriodicPointInterpolationSignal) (((CanvasBoxSignalPointInterpolation)cb).getSignal())).getEndTime() + ""));
				} else if (cb instanceof CanvasBoxAmplitudeModulationPointInterpolation) {
					timeElement.addContent(new Element("start").addContent(((NonPeriodicPointInterpolationAmplitudeModulation) (((CanvasBoxAmplitudeModulationPointInterpolation)cb).getAmplitudeModulation())).getStartTime() + ""));
					timeElement.addContent(new Element("end").addContent(((NonPeriodicPointInterpolationAmplitudeModulation) (((CanvasBoxAmplitudeModulationPointInterpolation)cb).getAmplitudeModulation())).getEndTime() + ""));	
				}
				cbElement.addContent(timeElement);
				cbElement.addContent(new Element("canvaswidth").addContent(((CanvasBoxPointInterpolation)cb).getCanvasWidth() + ""));
				amplitudeElement.addContent(new Element("canvasmin").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMin() + ""));
				amplitudeElement.addContent(new Element("canvasmax").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMax() + ""));
				amplitudeElement.addContent(new Element("canvasmean").addContent(((CanvasBoxPointInterpolation)cb).getCanvasMean() + ""));
				amplitudeElement.addContent(new Element("zeroline").addContent(((CanvasBoxPointInterpolation)cb).getZeroLinePosition() + ""));
			}
			for (int i = 0; i < ((CanvasBoxPointInterpolation)cb).getPointInterpolationCanvasPointList().size(); i++) {
				Element pointElement = new Element("point");
				pointElement.addContent(new Element("x").addContent(Math.round(((CanvasBoxPointInterpolation)cb).getPointInterpolationCanvasPointList().get(i).getX()) + ""));
				pointElement.addContent(new Element("y").addContent(Math.round(((CanvasBoxPointInterpolation)cb).getPointInterpolationCanvasPointList().get(i).getY()) + ""));
				cbElement.addContent(pointElement);
			}
			cbElement.addContent(new Element("interpolationtype").addContent(((CanvasBoxPointInterpolation)cb).getPeriodicInterpolationNumber() + ""));
		} else if (cb instanceof CanvasBoxGain) {
			for (int i = 0; i < 3; i++) {
				Element kElement = new Element("k" + i).addContent(((CanvasBoxGain) cb).getGain().getGain()[i] + "");
				cbElement.addContent(kElement);
			}
		} else if (cb instanceof CanvasBoxStep) {
			CanvasBoxStep cbs = (CanvasBoxStep) cb;
			if(cbs.getAmStepPeriodic() != null) {
				cbElement.addContent(new Element("phasor").addContent(cbs.getAmStepPeriodic().getPhasor().getName()));	
				for (int i = 0; i < 3; i++) {
					cbElement.addContent(new Element("step_ab" + i).addContent(cbs.getAmStepPeriodic().getStepAB()[i].getAngle() + ""));
					cbElement.addContent(new Element("step_ba" + i).addContent(cbs.getAmStepPeriodic().getStepBA()[i].getAngle() + ""));
					cbElement.addContent(new Element("value_a" + i).addContent(cbs.getAmStepPeriodic().getLevelA()[i] + ""));
					cbElement.addContent(new Element("value_b" + i).addContent(cbs.getAmStepPeriodic().getLevelB()[i] + ""));
				}
			} else if (cbs.getAmStepNonPeriodic() != null) {
				for (int i = 0; i < 3; i++) {
					cbElement.addContent(new Element("steptime" + i).addContent(cbs.getAmStepNonPeriodic().getStepTime()[i] + ""));
					cbElement.addContent(new Element("startvalue" + i).addContent(cbs.getAmStepNonPeriodic().getStartValue()[i] + ""));
					cbElement.addContent(new Element("endvalue" + i).addContent(cbs.getAmStepNonPeriodic().getEndValue()[i] + ""));
				}
			}
			
		} else if (cb instanceof CanvasBoxRamp) {
			CanvasBoxRamp cbr = (CanvasBoxRamp) cb;
			if (cbr.getAmRampPeriodic() != null) {
				cbElement.addContent(new Element("phasor").addContent(cbr.getAmRampPeriodic().getPhasor().getName()));
				for (int i = 0; i < 3; i++) {
					cbElement.addContent(new Element("start_ab" + i).addContent(cbr.getAmRampPeriodic().getStartAngle0()[i].getAngle() + ""));
					cbElement.addContent(new Element("end_ab" + i).addContent(cbr.getAmRampPeriodic().getEndAngle0()[i].getAngle() + ""));
					cbElement.addContent(new Element("start_ba" + i).addContent(cbr.getAmRampPeriodic().getStartAngle1()[i].getAngle() + ""));
					cbElement.addContent(new Element("end_ba" + i).addContent(cbr.getAmRampPeriodic().getEndAngle1()[i].getAngle() + ""));
					cbElement.addContent(new Element("value_a" + i).addContent(cbr.getAmRampPeriodic().getValue0()[i] + ""));
					cbElement.addContent(new Element("value_b" + i).addContent(cbr.getAmRampPeriodic().getValue1()[i] + ""));
				}
			} else if (cbr.getAmRampNonPeriodic() != null) {
				for (int i = 0; i < 3; i++) {
					cbElement.addContent(new Element("starttime" + i).addContent(cbr.getAmRampNonPeriodic().getRampStartTime()[i] + ""));
					cbElement.addContent(new Element("endtime" + i).addContent(cbr.getAmRampNonPeriodic().getRampEndTime()[i] + ""));
					cbElement.addContent(new Element("startvalue" + i).addContent(cbr.getAmRampNonPeriodic().getStartValue()[i] + ""));
					cbElement.addContent(new Element("endvalue" + i).addContent(cbr.getAmRampNonPeriodic().getEndValue()[i] + ""));
				}
			}
		} else if (cb instanceof CanvasBoxSignalImport) {
			CanvasBoxSignalImport cbsi = (CanvasBoxSignalImport) cb;
			cbElement.getChild("gui").addContent(new Element("title").addContent(cbsi.getTitle()));
			if (cbsi.isEmbedded()) {
				cbElement.addContent(new Element("mode").addContent("embedded"));
				Element subRootElement = new Element("subroot");
				//all lower level imported linked imports have to be embedded
				Element subRootContent = cbsi.getSubRootElement().getChild("canvas").clone();
				for (int i = 0; i < subRootContent.getChildren().size(); i++) {
					embedLinkedImports(subRootContent.getChildren().get(i));
					embedPhasors(subRootContent.getChildren().get(i), cbsi.getPhasorImportElement());
				}
				subRootElement.addContent(subRootContent);
				cbElement.addContent(subRootElement);
			} else {
				cbElement.addContent(new Element("mode").addContent("linked"));
				cbElement.addContent(new Element("path").addContent(cbsi.getPath()));
				cbElement.addContent(cbsi.getPhasorImportElement());
			}

		} else if (cb instanceof CanvasBoxAmplitudeModulationImport) {
			CanvasBoxAmplitudeModulationImport cbami = (CanvasBoxAmplitudeModulationImport) cb;
			if (cbami.isEmbedded()) {
				cbElement.addContent(new Element("mode").addContent("embedded"));
				canvasElement.addContent(cbami.getSubRootElement().getChild("canvas").clone());
				cbElement.addContent(canvasElement);
			} else {
				cbElement.addContent(new Element("mode").addContent("linked"));
				cbElement.addContent(new Element("path").addContent(cbami.getPath()));
			}
		} else if (cb instanceof CanvasBoxConst) {
			CanvasBoxConst cbc = (CanvasBoxConst) cb;
			for (int i = 0; i < 3; i++) {
				Element voltageElement = new Element("voltage" + i).addContent(cbc.getVoltages()[i] + "");
				cbElement.addContent(voltageElement);
			}
		} else if (cb instanceof CanvasBoxSignalInputRecorded) {
			CanvasBoxSignalInputRecorded cbr = (CanvasBoxSignalInputRecorded) cb;
			Element pathElement = new Element("path").addContent(cbr.getPath());
			if (cbr.isLoadFollowingFiles()) {
				pathElement.setAttribute("loadfollowingfiles", "true");
			}
			cbElement.addContent(pathElement);
			if (cbr.getCalibrationFilePath() != null) {
				cbElement.addContent(new Element("calibrationfile").addContent(cbr.getCalibrationFilePath()));
			}
			cbElement.addContent(new Element("start").addContent(cbr.getStart() + ""));
		}

		return cbElement;
	}
	
	/**
	 * creates the xml element for an embedded import
	 * @param element
	 * @throws JDOMException
	 * @throws IOException
	 */
	private void embedLinkedImports(Element element) throws JDOMException, IOException {
		if (element.getName().equals("topleveloutput")) {
			for (int i = 0; i < element.getChildren().size(); i++) {
				embedLinkedImports(element.getChildren().get(i));
			}
		} else if (element.getName().equals("signaloutput") || element.getName().equals("amplitudemodulationoutput")) {
			if (element.getChild("child").getChildren().size() > 0) {
				embedLinkedImports(element.getChild("child").getChildren().get(0));
			}
		} else if (element.getName().equals("multoperator") || element.getName().equals("addoperator")) {
			if (element.getChild("child0").getChildren().size() > 0) {
				embedLinkedImports(element.getChild("child0").getChildren().get(0));
			}
			if (element.getChild("child1").getChildren().size() > 0) {
				embedLinkedImports(element.getChild("child1").getChildren().get(0));
			}
		} else if (element.getName().equals("signalimport") || element.getName().equals("amplitudemodulationimport")) {
			if (element.getChildText("mode").equals("embedded")) {
				for (int i = 0; i < element.getChild("subroot").getChild("canvas").getChildren().size(); i++) {
					embedLinkedImports(element.getChild("subroot").getChild("canvas").getChildren().get(i));
				}
			} else {
				element.getChild("mode").setText("embedded");
				String path = element.getChildText("path");
				element.removeChild("path");
				Element subrootElement = new Element("subroot"); 
				try {
					Document xmlDocument = new SAXBuilder().build(path);
					Element canvasElement = xmlDocument.getRootElement().getChild("canvas").clone();
					subrootElement.addContent(canvasElement);
					element.addContent(subrootElement);
					for (int i = 0; i < canvasElement.getChildren().size(); i++) {
						embedLinkedImports(canvasElement.getChildren().get(i));
						embedPhasors(canvasElement.getChildren().get(i), element.getChild("phasorimport"));
					}
					element.removeChild("phasorimport");
				} catch (IOException | JDOMException e) {
					FileLog.log("Tried to open " + path + " for embedding, but an error occured");
					throw e;
				}
			}
		}
	}

	/**
	 * embeds the phasors (transfer, rename or replace)
	 * @param element
	 * @param phasorImportElement
	 */
	private void embedPhasors(Element element, Element phasorImportElement) {
		if (element.getName().equals("topleveloutput")) {
			for (int i = 0; i < element.getChildren().size(); i++) {
				embedPhasors(element.getChildren().get(i), phasorImportElement);
			}
		} else if (element.getName().equals("signaloutput") || element.getName().equals("amplitudemodulationoutput")) {
			if (element.getChild("child").getChildren().size() > 0) {
				embedPhasors(element.getChild("child").getChildren().get(0), phasorImportElement);
			}
		} else if (element.getName().equals("multoperator") || element.getName().equals("addoperator")) {
			if (element.getChild("child0").getChildren().size() > 0) {
				embedPhasors(element.getChild("child0").getChildren().get(0), phasorImportElement);
			}
			if (element.getChild("child1").getChildren().size() > 0) {
				embedPhasors(element.getChild("child1").getChildren().get(0), phasorImportElement);
			}
		} else if (element.getChildren("phasor").size() > 0) {
			//transfer => no problem
			//rename
			for (int i = 0; i < phasorImportElement.getChildren("phasorrename").size(); i++) {
				if (element.getChildText("phasor").equals(phasorImportElement.getChildren("phasorrename").get(i).getChildText("oldname")) ) {
					element.getChild("phasor").setText(phasorImportElement.getChildren("phasorrename").get(i).getChildText("newname"));
				}
			}
			//replace
			for (int i = 0; i < phasorImportElement.getChildren("phasorreplace").size(); i++) {
				if (element.getChildText("phasor").equals(phasorImportElement.getChildren("phasorreplace").get(i).getChildText("oldphasor")) ) {
					element.getChild("phasor").setText(phasorImportElement.getChildren("phasorreplace").get(i).getChildText("newphasor"));
				}
			}
		} else if (element.getName().equals("signalimport") || element.getName().equals("amplitudemodulationimport")) {
			for (int i = 0; i < element.getChild("subroot").getChild("canvas").getChildren().size(); i++) {
				embedPhasors(element.getChild("subroot").getChild("canvas").getChildren().get(i), phasorImportElement);
			}
		}
	}


	/**
	 * returns the gui parameters of a canvas box
	 * @param cb
	 * @return
	 */
	private Element gui(CanvasBox cb) {
		Element guiElement = new Element("gui");
		guiElement.addContent(new Element("x").addContent(cb.getX() + ""));
		guiElement.addContent(new Element("y").addContent(cb.getY() + ""));	
		return guiElement;
	}
	
	
	/**
	 * returns the gui parameters of a canvas operator
	 * @param co
	 * @return
	 */
	private Element gui(CanvasOperator co) {
		Element guiElement = new Element("gui");
		guiElement.addContent(new Element("id").addContent(co.id + ""));
		guiElement.addContent(new Element("x").addContent(co.getX() + ""));
		guiElement.addContent(new Element("y").addContent(co.getY() + ""));	
		return guiElement;
	}
}
