package File;

/**
 * File log on the console
 */
public class FileLog {
	public static void log(String text) {
		if (!LogFileWriter.logToFile("file.log", text)) {
			System.out.println("Cannot write to file log file");
		}
		System.out.println("[FileLog] " + text);
	}
}
