package File;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import Controller.DataControl;
import Data.Angle;
import Data.Frequency;
import Data.FrequencyModulation;
import Data.Harmonic;
import Data.NonPeriodicAmplitudeModulationRamp;
import Data.NonPeriodicAmplitudeModulationStep;
import Data.PeriodicAmplitudeModulationRamp;
import Data.PeriodicAmplitudeModulationStep;
import Data.Phasor;
import Data.PolynomSegment;
import Data.Wave;
import GUI.Canvas;
import GUI.CanvasBoxAmplitudeModulationFunction;
import GUI.CanvasBoxAmplitudeModulationOutput;
import GUI.CanvasBoxAmplitudeModulationPointInterpolation;
import GUI.CanvasBoxConst;
import GUI.CanvasBoxSignalFunction;
import GUI.CanvasBoxGain;
import GUI.CanvasBoxHarmonic;
import GUI.CanvasBoxPointInterpolation;
import GUI.CanvasBoxRamp;
import GUI.CanvasBoxSignalImport;
import GUI.CanvasBoxSignalInputRecorded;
import GUI.CanvasBoxSignalOutput;
import GUI.CanvasBoxSignalPointInterpolation;
import GUI.CanvasBoxStep;
import GUI.CanvasConnector;
import GUI.CanvasOperatorAdd;
import GUI.CanvasOperatorMult;
import GUI.CanvasTreeElement;
import GUI.Connection;
import GUI.GUIMath;

/**
 * Reader for xml files for opening a signal or an amplitude modulation
 */
public class XmlFileReader {
	private Element root;
	private Element canvasElement;
	public XmlFileReader(String path) {
		try {
			Document xmlDocument = new SAXBuilder().build(path);
			root = xmlDocument.getRootElement();
			this.canvasElement = root.getChild("canvas");
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<Phasor> getPhasors() {
		List<Phasor> phasors = new ArrayList<Phasor>();
		if (root != null) {
			List<Element> phasorElements = root.getChildren("phasor");
			for (int i = 0; i < phasorElements.size(); i++) {
				Element frequencyElement = phasorElements.get(i).getChild("frequency");
				Frequency f = new Frequency(Long.parseLong(frequencyElement.getChildText("startfrequency")));

				
				Element frequencyModulationListElement = frequencyElement.getChild("frequencymodulationlist");
				List<Element> fModElements = frequencyModulationListElement.getChildren("frequencymodulation");
				for (int j = 0; j < fModElements.size(); j++) {
					long[] start = new long[3];
					long[] end = new long[3];
					long[] endFrequency = new long[3];
					for (int k = 0; k < 3; k++) {
						start[k] = Long.parseLong(fModElements.get(j).getChildText("start" + k));
						end[k] = Long.parseLong(fModElements.get(j).getChildText("end" + k));
						endFrequency[k] = Long.parseLong(fModElements.get(j).getChildText("endfrequency" + k));
					}
					f.addFrequencyModulation(new FrequencyModulation(start[0], end[0], endFrequency[0], start[1], end[1], endFrequency[1], start[2], end[2], endFrequency[2]));
				}
				Phasor newPhasor = new Phasor(DataControl.getClock(), f);
				newPhasor.setName(phasorElements.get(i).getChildText("name"));
				Angle startAngle1 = new Angle(Long.parseLong(phasorElements.get(i).getChildText("startangle0")));
				Angle startAngle2 = new Angle(Long.parseLong(phasorElements.get(i).getChildText("startangle1")));
				Angle startAngle3 = new Angle(Long.parseLong(phasorElements.get(i).getChildText("startangle2")));
				newPhasor.setStartAngles(startAngle1, startAngle2, startAngle3);
				phasors.add(newPhasor);
			}
		}
		return phasors;
	}
	
	private CanvasOperatorAdd readOpAdd(Element opAdd, Canvas canvas) {
		Element gui = opAdd.getChild("gui");
		CanvasOperatorAdd coa = new CanvasOperatorAdd(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		coa.id = Integer.parseInt(gui.getChildText("id"));
		canvas.addTreeElement(coa);
		canvas.add(coa);
		if (opAdd.getChildren("child0").size() > 0) {
			CanvasConnector output = readChildGetConnector(opAdd.getChild("child0"), canvas);
			if (output != null) {
				canvas.addConnection(new Connection(output, coa.getConnector0()));
			}
		}
		if (opAdd.getChildren("child1").size() > 0) {
			CanvasConnector output = readChildGetConnector(opAdd.getChild("child1"), canvas);
			if (output != null) {
				canvas.addConnection(new Connection(output, coa.getConnector1()));
			}
		}
		return coa;
	}
	
	private CanvasOperatorMult readOpMult(Element opMult, Canvas canvas) {
		Element gui = opMult.getChild("gui");
		CanvasOperatorMult com = new CanvasOperatorMult(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		com.id = Integer.parseInt(gui.getChildText("id"));
		canvas.addTreeElement(com);
		canvas.add(com);
		if (opMult.getChildren("child0").size() > 0) {
			CanvasConnector output = readChildGetConnector(opMult.getChild("child0"), canvas);
			if (output != null) {
				canvas.addConnection(new Connection(output, com.getConnector0()));
			}
		}
		if (opMult.getChildren("child1").size() > 0) {
			CanvasConnector output = readChildGetConnector(opMult.getChild("child1"), canvas);
			if (output != null) {
				canvas.addConnection(new Connection(output, com.getConnector1()));
			}
		}
		return com;
	}
	
	
	private CanvasConnector readChildGetConnector(Element inputElement, Canvas canvas) {
		if (inputElement.getChildren("addoperator").size() > 0) {
			CanvasTreeElement child = readOpAdd(inputElement.getChild("addoperator"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("multoperator").size() > 0) {
			CanvasTreeElement child = readOpMult(inputElement.getChild("multoperator"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("harmonic").size() > 0) {
			CanvasTreeElement child = readHarmonic(inputElement.getChild("harmonic"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("pointinterpolationsignal").size() > 0) {
			CanvasTreeElement child = (CanvasBoxSignalPointInterpolation) readPointInterpolation(inputElement.getChild("pointinterpolationsignal"), canvas, true);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("partwisesignal").size() > 0) {
			CanvasTreeElement child = readSignalFunction(inputElement.getChild("partwisesignal"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("gain").size() > 0) {
			CanvasTreeElement child = readGain(inputElement.getChild("gain"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("step").size() > 0) {
			CanvasTreeElement child = readStep(inputElement.getChild("step"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("ramp").size() > 0) {
			CanvasTreeElement child = readRamp(inputElement.getChild("ramp"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("pointinterpolationamplitudemodulation").size() > 0) {
			CanvasTreeElement child = (CanvasBoxAmplitudeModulationPointInterpolation) readPointInterpolation(inputElement.getChild("pointinterpolationamplitudemodulation"), canvas, false);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("partwiseamplitudemodulation").size() > 0) {
			CanvasTreeElement child = readAmplitudeModulationFunction(inputElement.getChild("partwiseamplitudemodulation"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("signalimport").size() > 0) {
			CanvasTreeElement child = readSignalImport(inputElement.getChild("signalimport"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("const").size() > 0) {
			CanvasTreeElement child = readConst(inputElement.getChild("const"), canvas);
			return child.getOutputConnector();
		} else if (inputElement.getChildren("recorded").size() > 0) {
			CanvasTreeElement child = readRecorded(inputElement.getChild("recorded"), canvas);
			return child.getOutputConnector();
		}
		return null;
	}
	
	private CanvasBoxHarmonic readHarmonic(Element harmonicElement, Canvas canvas) {
		Element gui = harmonicElement.getChild("gui");
		
		List<Element> waveElements = harmonicElement.getChildren("wave");
		Harmonic h = new Harmonic(null);
		if (harmonicElement.getChild("phasor") != null) {
			h.setPhasor(DataControl.getClock().getPhasor(harmonicElement.getChildText("phasor")));
		}
		for (int j = 0; j < waveElements.size(); j++) {
			h.addWave(new Wave(Integer.parseInt(waveElements.get(j).getChildText("order")), Double.parseDouble(waveElements.get(j).getChildText("sin")), Double.parseDouble(waveElements.get(j).getChildText("cos"))));
		}
		CanvasBoxHarmonic box = new CanvasBoxHarmonic(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")), h);
		canvas.addTreeElement(box);
		canvas.add(box);
		box.fillGuiFromData();
		return box;
	}
	
	private CanvasBoxPointInterpolation readPointInterpolation(Element pointInterpolationElement, Canvas canvas, boolean signal) {
		Element gui = pointInterpolationElement.getChild("gui");
		CanvasBoxPointInterpolation box;
		if (signal) {
			box = new CanvasBoxSignalPointInterpolation(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		} else { //am
			box = new CanvasBoxAmplitudeModulationPointInterpolation(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		}
		box.chooseInterpolationType(Integer.parseInt(pointInterpolationElement.getChildText("interpolationtype")));
		List<Element> pointElements = pointInterpolationElement.getChildren("point");
		for (int j = 0; j < pointElements.size(); j++) {
			box.addPointToPiCanvas(new Point(Integer.parseInt(pointElements.get(j).getChildText("x")), Integer.parseInt(pointElements.get(j).getChildText("y")))); 
		}
		if (pointInterpolationElement.getChildren("time").size() > 0) { //nonPeriodic
			box.setNonPeriodicMode();
			Element amplitudeElement = pointInterpolationElement.getChildren("amplitude").get(0);
			if (signal) {
				((CanvasBoxSignalPointInterpolation) box).setEdt_NPVoltageText(amplitudeElement.getChildText("value"));
			} else {
				((CanvasBoxAmplitudeModulationPointInterpolation) box).setEdt_NPValueText(amplitudeElement.getChildText("value"));
			}
			box.setLineParams(Integer.parseInt(amplitudeElement.getChildText("canvasmin")), Integer.parseInt(amplitudeElement.getChildText("canvasmax")), Integer.parseInt(amplitudeElement.getChildText("zeroline")));
			box.setAmplitudeDefinitionType(amplitudeElement.getChildText("pitype"));
			Element timeElement = pointInterpolationElement.getChildren("time").get(0);
			box.setTimeEdits(GUIMath.roundDecimals((((double) Long.parseLong(timeElement.getChildText("start"))) / 1000000), 6),  GUIMath.roundDecimals((((double) Long.parseLong(timeElement.getChildText("end"))) / 1000000), 6));
			box.btn_ok_nonperiodic_click();
		} else {
			box.setPeriodicMode();
			if (pointInterpolationElement.getChild("phasor") != null) {
				box.setPhasor(DataControl.getClock().getPhasor(pointInterpolationElement.getChildText("phasor")));
				box.setAmplitudeEdits(pointInterpolationElement.getChild("amplitude").getChildText("max"), pointInterpolationElement.getChild("amplitude").getChildText("min"));
				box.btn_ok_periodic_click();
			}
		}


		if (signal) {
			canvas.addTreeElement((CanvasBoxSignalPointInterpolation) box);
			canvas.add((CanvasBoxSignalPointInterpolation)box);
		} else {
			canvas.addTreeElement((CanvasBoxAmplitudeModulationPointInterpolation) box);
			canvas.add((CanvasBoxAmplitudeModulationPointInterpolation) box);
		}

		return box;
	}
	
	private CanvasBoxSignalFunction readSignalFunction(Element functionElement, Canvas canvas) {
		Element gui = functionElement.getChild("gui"); 
		CanvasBoxSignalFunction box = new CanvasBoxSignalFunction(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		if (functionElement.getChild("phasor") != null) {
			box.setPhasor(DataControl.getClock().getPhasor(functionElement.getChildText("phasor")));
		} else if (functionElement.getChild("starttime") != null) {
			box.setStartTime(Long.parseLong(functionElement.getChildText("starttime")));
		}
		List<Element> sectionElements = functionElement.getChildren("section");
		for (int j = 0; j < sectionElements.size(); j++) {
			List<Double> polynom = new ArrayList<Double>();
			List<Element> summandElements = sectionElements.get(j).getChildren("summand");
			for (int k = 0; k < summandElements.size(); k++) {
				while (polynom.size() < Integer.parseInt(summandElements.get(k).getChildText("exponent"))) {
					polynom.add(0d);
				}
				polynom.add(Double.parseDouble(summandElements.get(k).getChildText("coefficient")));
			}
			PolynomSegment section = new PolynomSegment(Long.parseLong(sectionElements.get(j).getChildText("start")), polynom);
			box.addPolynomSegment(section, true);
		}

		box.updateGUI();
		box.click_ok();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxAmplitudeModulationFunction readAmplitudeModulationFunction(Element functionElement, Canvas canvas) {
		Element gui = functionElement.getChild("gui"); 
		CanvasBoxAmplitudeModulationFunction box = new CanvasBoxAmplitudeModulationFunction(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		if (functionElement.getChild("phasor") != null) {
			box.setPhasor(DataControl.getClock().getPhasor(functionElement.getChildText("phasor")));
		} else if (functionElement.getChild("starttime") != null) {
			box.setStartTime(Long.parseLong(functionElement.getChildText("starttime")));
		}
		List<Element> sectionElements = functionElement.getChildren("section");
		for (int j = 0; j < sectionElements.size(); j++) {
			List<Double> polynom = new ArrayList<Double>();
			List<Element> summandElements = sectionElements.get(j).getChildren("summand");
			for (int k = 0; k < summandElements.size(); k++) {
				while (polynom.size() < Integer.parseInt(summandElements.get(k).getChildText("exponent"))) {
					polynom.add(0d);
				}
				polynom.add(Double.parseDouble(summandElements.get(k).getChildText("coefficient")));
			}
			PolynomSegment section = new PolynomSegment(Long.parseLong(sectionElements.get(j).getChildText("start")), polynom);
			box.addPolynomSegment(section, true);
		}

		box.updateGUI();
		box.click_ok();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxSignalImport readSignalImport(Element signalImportElement, Canvas canvas) {
		Element guiElement = signalImportElement.getChild("gui");
		boolean embedded = true;
		String path = "";
		if (signalImportElement.getChildText("mode").equals("linked")) {
			embedded = false;
			path = signalImportElement.getChildText("path");
		}
		List<Phasor> availablePhasors = null;
		if (embedded) {
			availablePhasors = DataControl.getPhasors();
		}
		CanvasBoxSignalImport box = new CanvasBoxSignalImport(canvas, Integer.parseInt(guiElement.getChildText("x")), Integer.parseInt(guiElement.getChildText("y")), embedded, path, signalImportElement.getChild("subroot"), signalImportElement.getChild("phasorimport"), availablePhasors);
		box.setTitle(guiElement.getChildText("title"));
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxGain readGain(Element gainElement, Canvas canvas) {
		Element gui = gainElement.getChild("gui");
		CanvasBoxGain box = new CanvasBoxGain(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		double[] gains = {Double.parseDouble(gainElement.getChildText("k0")), Double.parseDouble(gainElement.getChildText("k1")), Double.parseDouble(gainElement.getChildText("k2"))};
		box.setGain(gains);
		box.btn_okClick();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxConst readConst(Element constElement, Canvas canvas) {
		Element gui = constElement.getChild("gui");
		CanvasBoxConst box = new CanvasBoxConst(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		double[] voltages = {Double.parseDouble(constElement.getChildText("voltage0")), Double.parseDouble(constElement.getChildText("voltage1")), Double.parseDouble(constElement.getChildText("voltage2"))};
		box.setVoltage(voltages);
		box.btn_okClick();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxSignalInputRecorded readRecorded(Element recordedElement, Canvas canvas) {
		Element gui = recordedElement.getChild("gui");
		CanvasBoxSignalInputRecorded box = new CanvasBoxSignalInputRecorded(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")), recordedElement.getChildText("path"));
		if (recordedElement.getChildren("calibrationfile").size() > 0) {
			box.setCalibrationFilePath(recordedElement.getChildText("calibrationfile"));
		}
		box.setStart(Long.parseLong(recordedElement.getChildText("start")));
		if (recordedElement.getChild("path").getAttribute("loadfollowingfiles") != null && recordedElement.getChild("path").getAttributeValue("loadfollowingfiles").equals("true")) {
			box.setLoadFollowingFiles(true);
		} else {
			box.setLoadFollowingFiles(false);
		}
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxStep readStep(Element stepElement, Canvas canvas) {
		Element gui = stepElement.getChild("gui");
		CanvasBoxStep box = new CanvasBoxStep(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		if (stepElement.getChildren("phasor").size() > 0) {
			//periodic
			Phasor phasor = DataControl.getClock().getPhasor(stepElement.getChildText("phasor"));
			Angle[] step_ab = new Angle[3];
			Angle[] step_ba = new Angle[3];
			double[] level_a = new double[3];
			double[] level_b = new double[3];
			for (int i = 0; i < 3; i++) {
				step_ab[i] = new Angle(Long.parseLong(stepElement.getChildText("step_ab" + i)));
				step_ba[i] = new Angle(Long.parseLong(stepElement.getChildText("step_ba" + i)));
				level_a[i] = Double.parseDouble(stepElement.getChildText("value_a" + i));
				level_b[i] = Double.parseDouble(stepElement.getChildText("value_b" + i));
			}		
			PeriodicAmplitudeModulationStep step = new PeriodicAmplitudeModulationStep(phasor, level_a, level_b, step_ab, step_ba);
			box.setPeriodicAmStep(step);
		} else if (stepElement.getChildren("steptime0").size() > 0){
			//nonPeriodic
			long[] stepTime = new long[3];
			double[] startValue = new double[3];
			double[] endValue = new double[3];
			for (int i = 0; i < 3; i++) {
				stepTime[i] = Long.parseLong(stepElement.getChildText("steptime" + i));
				startValue[i] = Double.parseDouble(stepElement.getChildText("startvalue" + i));
				endValue[i] = Double.parseDouble(stepElement.getChildText("endvalue" + i));
			}
			NonPeriodicAmplitudeModulationStep step = new NonPeriodicAmplitudeModulationStep(DataControl.getClock(), stepTime, startValue, endValue);
			box.setNonPeriodicAmStep(step);
		}
		box.updateBoxElements();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	private CanvasBoxRamp readRamp(Element rampElement, Canvas canvas) {
		Element gui = rampElement.getChild("gui");
		CanvasBoxRamp box = new CanvasBoxRamp(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
		if (rampElement.getChildren("phasor").size() > 0) {
			//periodic
			Phasor phasor = DataControl.getClock().getPhasor(rampElement.getChildText("phasor"));
			Angle[] start_ab = new Angle[3];
			Angle[] end_ab = new Angle[3];
			Angle[] start_ba = new Angle[3];
			Angle[] end_ba = new Angle[3];
			double[] value_a = new double[3];
			double[] value_b = new double[3];
			for (int i = 0; i < 3; i++) {
				start_ab[i] = new Angle(Long.parseLong(rampElement.getChildText("start_ab" + i)));
				end_ab[i] = new Angle(Long.parseLong(rampElement.getChildText("end_ab" + i)));
				start_ba[i] = new Angle(Long.parseLong(rampElement.getChildText("start_ba" + i)));
				end_ba[i] = new Angle(Long.parseLong(rampElement.getChildText("end_ba" + i)));
				value_a[i] = Double.parseDouble(rampElement.getChildText("value_a" + i));
				value_b[i] = Double.parseDouble(rampElement.getChildText("value_b" + i));
			}		
			PeriodicAmplitudeModulationRamp ramp = new PeriodicAmplitudeModulationRamp(phasor, start_ab, end_ab, start_ba, end_ba, value_a, value_b);
			box.setPeriodicAmRamp(ramp);
		} else if (rampElement.getChildren("starttime0").size() > 0){
			//nonPeriodic
			long[] startTime = new long[3];
			long[] endTime = new long[3];
			double[] startValue = new double[3];
			double[] endValue = new double[3];
			for (int i = 0; i < 3; i++) {
				startTime[i] = Long.parseLong(rampElement.getChildText("starttime" + i));
				endTime[i] = Long.parseLong(rampElement.getChildText("endtime" + i));
				startValue[i] = Double.parseDouble(rampElement.getChildText("startvalue" + i));
				endValue[i] = Double.parseDouble(rampElement.getChildText("endvalue" + i));
			}
			NonPeriodicAmplitudeModulationRamp ramp = new NonPeriodicAmplitudeModulationRamp(DataControl.getClock(), startTime, endTime, startValue, endValue);
			box.setNonPeriodicAmRamp(ramp);
		}
		box.updateBoxElements();
		canvas.addTreeElement(box);
		canvas.add(box);
		return box;
	}
	
	/**
	 * Creates the canvas boxes from the xml.
	 * The phasors from the xml must exist before.
	 * @param canvas
	 * @return
	 */
	public void readTopLevelElements(Canvas canvas) {
		if (canvasElement != null) {
			Canvas.canvasLog("Start loading of canvas elements from the xml file");
			//add operator
			List<Element> opAddElements = canvasElement.getChildren("addoperator");
			for (int i = 0; i < opAddElements.size(); i++) {
				readOpAdd(opAddElements.get(i), canvas);
			}
			
			//mult operator
			List<Element> opMultElements = canvasElement.getChildren("multoperator");
			for (int i = 0; i < opMultElements.size(); i++) {
				readOpMult(opMultElements.get(i), canvas);
			}
			
			//harmonic
			List<Element> harmonics = canvasElement.getChildren("harmonic");
			for (int i = 0; i < harmonics.size(); i++) {
				readHarmonic(harmonics.get(i), canvas);
			}
			
			//pointinterpolation signal
			List<Element> piElements_s = canvasElement.getChildren("pointinterpolationsignal");
			for (int i = 0; i < piElements_s.size(); i++) {
				readPointInterpolation(piElements_s.get(i), canvas, true);
			}
			
			//pointinterpolation am
			List<Element> piElements_am = canvasElement.getChildren("pointinterpolationamplitudemodulation");
			for (int i = 0; i < piElements_am.size(); i++) {
				readPointInterpolation(piElements_am.get(i), canvas, false);
			}
			
			//piecewise defined signal functions
			List<Element> pdsfElements = canvasElement.getChildren("partwisesignal");
			for (int i = 0; i < pdsfElements.size(); i++) {
				readSignalFunction(pdsfElements.get(i), canvas);
			}
			
			//partwise defined functions
			List<Element> pdamfElements = canvasElement.getChildren("partwiseamplitudemodulation");
			for (int i = 0; i < pdamfElements.size(); i++) {
				readAmplitudeModulationFunction(pdamfElements.get(i), canvas);
			}
			
			//gain
			List<Element> gainElements = canvasElement.getChildren("gain");
			for (int i = 0; i < gainElements.size(); i++) {
				readGain(gainElements.get(i), canvas);
			}
			
			List<Element> stepElements = canvasElement.getChildren("step");
			for (int i = 0; i < stepElements.size(); i++) {
				readStep(stepElements.get(i), canvas);
			}
			
			List<Element> rampElements = canvasElement.getChildren("ramp");
			for (int i = 0; i < rampElements.size(); i++) {
				readRamp(rampElements.get(i), canvas);
			}
			
			List<Element> signalImportElements = canvasElement.getChildren("signalimport");
			for (int i = 0; i < signalImportElements.size(); i++) {
				readSignalImport(signalImportElements.get(i), canvas);
			}
			
			List<Element> amplitudeModulationImportElements = canvasElement.getChildren("amplitudemodulationimport");
			for (int i = 0; i < amplitudeModulationImportElements.size(); i++) {
				readSignalImport(amplitudeModulationImportElements.get(i), canvas);
			}
			
			List<Element> constElements = canvasElement.getChildren("const");
			for (int i = 0; i < constElements.size(); i++) {
				readConst(constElements.get(i), canvas);
			}
			
			List<Element> recordedElements = canvasElement.getChildren("recorded");
			for (int i = 0; i < recordedElements.size(); i++) {
				readRecorded(recordedElements.get(i), canvas);
			}
			
			//topleveloutput

			if(canvasElement.getChildren("topleveloutput").size() > 0) {
				Element toplevel = canvasElement.getChild("topleveloutput");
				if (toplevel.getChildren("signaloutput").size() > 0) {
					Element signalOutputElement = toplevel.getChild("signaloutput");
					Element gui = signalOutputElement.getChild("gui"); 
					CanvasBoxSignalOutput box = new CanvasBoxSignalOutput(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
					canvas.add(box);
					canvas.addTreeElement(box);
					canvas.setTopLevelElement(box);
					if (signalOutputElement.getChildren("child").size() > 0) {
						CanvasConnector output = readChildGetConnector(signalOutputElement.getChild("child"), canvas);
						canvas.addConnection(new Connection(output, box.getInputConnector()));
					}
				} else if (toplevel.getChildren("amplitudemodulationoutput").size() > 0) {
					Element amplitudeModulationOutputElement = toplevel.getChild("amplitudemodulationoutput");
					Element gui = amplitudeModulationOutputElement.getChild("gui");
					CanvasBoxAmplitudeModulationOutput box = new CanvasBoxAmplitudeModulationOutput(canvas, Integer.parseInt(gui.getChildText("x")), Integer.parseInt(gui.getChildText("y")));
					canvas.add(box);
					canvas.addTreeElement(box);
					canvas.setTopLevelElement(box);
					if (amplitudeModulationOutputElement.getChildren("child").size() > 0) {
						CanvasConnector output = readChildGetConnector(amplitudeModulationOutputElement.getChild("child"), canvas);
						canvas.addConnection(new Connection(output, box.getInputConnector()));
					}
				}
			} 
		}
	}
	
	
}
