package Monitor;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.util.Date;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileWriter;

public class CpuFrequencyPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private List<Integer[]> data;
	private JLabel display;
	private List<String> table;
	private String time;
	private boolean isM3 = false; //do not set it here! Set it in MonitorWindow

	public CpuFrequencyPanel() {
		super();
		this.setLayout(null);
		this.data = new ArrayList<Integer[]>();
		this.setBackground(Color.WHITE);
		this.display = new JLabel();

		this.add(this.display);
		this.table = new ArrayList<String>();
		this.table.add("Time CpuFrequency");
		this.time = "";
	}
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		this.display.setBounds(5, height - 15, 900, 15);
	}

	public void addData(Integer[] frequency) {
		this.data.add(frequency);
		if (this.data.size() > this.getWidth()) {
			this.data.remove(0);
		}
		if (frequency.length == 8) {
			this.isM3 = true;
			this.display.setText("CPU 1: " + (frequency[0] / 1000) + " MHz, CPU 2: " + (frequency[1] / 1000) + " MHz, CPU 3: " + (frequency[2] / 1000) + " MHz, CPU 4: " + (frequency[3] / 1000) + " MHz, CPU 5: " + (frequency[4] / 1000) + " MHz, CPU 6: " + (frequency[5] / 1000) + " MHz, CPU 7: " + (frequency[6] / 1000) + " MHz, CPU 8: " + (frequency[7] / 1000) + " MHz");
		} else {
			this.isM3 = false;
			this.display.setText("CPU 1: " + (frequency[0] / 1000) + " MHz, CPU 2: " + (frequency[1] / 1000) + " MHz, CPU 3: " + (frequency[2] / 1000) + " MHz, CPU 4: " + (frequency[3] / 1000) + " MHz");
		}
		this.updateUI();
		String newTime = ("" + new Date()).split(" ")[3];
		if (!time.equals(newTime)) {
			table.add(newTime + " " + frequency[0]);
			time = newTime;
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < data.size() - 1; i++) {
			g.drawLine(i, this.getHeight() - (data.get(i)[0]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[0] / 10000) - 1);
			g.setColor(Color.BLUE);
			g.drawLine(i, this.getHeight() - (data.get(i)[1]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[1] / 10000) - 1);
			g.setColor(Color.RED);
			g.drawLine(i, this.getHeight() - (data.get(i)[2]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[2] / 10000) - 1);
			g.setColor(Color.GREEN);
			g.drawLine(i, this.getHeight() - (data.get(i)[3]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[3] / 10000) - 1);
			if (this.isM3) {
				g.setColor(Color.ORANGE);
				g.drawLine(i, this.getHeight() - (data.get(i)[0]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[0] / 10000) - 1);
				g.setColor(Color.CYAN);
				g.drawLine(i, this.getHeight() - (data.get(i)[1]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[1] / 10000) - 1);
				g.setColor(Color.MAGENTA);
				g.drawLine(i, this.getHeight() - (data.get(i)[2]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[2] / 10000) - 1);
				g.setColor(Color.GRAY);
				g.drawLine(i, this.getHeight() - (data.get(i)[3]/ 10000) - 1, i + 1, this.getHeight() - (data.get(i + 1)[3] / 10000) - 1);
			}
		}
	}

	public void writeTable() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("frequenz.txt"));
			for (int i = 0; i < table.size(); i++) {
				bw.write(table.get(i) + "\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
