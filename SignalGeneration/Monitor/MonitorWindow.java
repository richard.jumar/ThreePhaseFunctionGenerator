package Monitor;

import javax.swing.JFrame;
import javax.swing.JLabel;

import GUI.LanguageTranslator;

import javax.swing.JButton;
import java.util.Scanner;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MonitorWindow extends JFrame implements Runnable {
	private static final long serialVersionUID = 3L;
	public static int writer;
	public static int reader;
	public static int sound; //position of the playback. 
	private TemperaturePanel panel_temperature;
	private CpuFrequencyPanel panel_cpuFrequency;
	private ProcessorLoadPanel panel_load;
	private MemoryPanel panel_memory;
	private boolean run;
	private boolean isM3 = true; //false: Banana Pi M2+, true: Banana Pi M3
	
	public MonitorWindow() {
		int m3Diff = 0;
		if (isM3) {
			m3Diff = 60;
		}
		this.setSize(1000, 800 + m3Diff);
		this.setLayout(null);
		JLabel lbl_temp = new JLabel(LanguageTranslator.getTranslation("Temperatur in �C"));
		lbl_temp.setBounds(5, 5, 150, 15);
		this.add(lbl_temp);
		panel_temperature = new TemperaturePanel();
		panel_temperature.setBounds(5, 25, 980, 100);
		this.add(panel_temperature);
		JLabel lbl_cpuFrequency = new JLabel(LanguageTranslator.getTranslation("CPU-Takt"));
		lbl_cpuFrequency.setBounds(5, 140, 100, 15);
		this.add(lbl_cpuFrequency);
		this.panel_cpuFrequency = new CpuFrequencyPanel();
		this.panel_cpuFrequency.setBounds(5, 160, 980, 121 + m3Diff);
		this.add(this.panel_cpuFrequency);
		JLabel lbl_cpuLoad = new JLabel(LanguageTranslator.getTranslation("CPU-Auslastung"));
		lbl_cpuLoad.setBounds(5, 296 + m3Diff, 120, 15);
		this.add(lbl_cpuLoad);
		this.panel_load = new ProcessorLoadPanel();
		this.panel_load.setBounds(5, 316 + m3Diff, 980, 101);
		this.add(panel_load);
		JLabel lbl_memory = new JLabel(LanguageTranslator.getTranslation("Speicher"));
		lbl_memory.setBounds(5, 432 + m3Diff, 980, 15);
		this.add(lbl_memory);
		this.panel_memory = new MemoryPanel();
		this.panel_memory.setBounds(5, 452 + m3Diff, 980, 256);
		this.add(panel_memory);
		JButton btn_save = new JButton(LanguageTranslator.getTranslation("Speichern"));
		btn_save.setBounds(5, 713 + m3Diff, 100, 20);
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_memory.writeTable();
				panel_temperature.writeTable();
				panel_cpuFrequency.writeTable();
			}
		});
		this.add(btn_save);
	}
	@Override
	public void run() {
		this.setVisible(true);
		this.run = true;
		while (run) {
			
			try {
				ProcessBuilder pb;
				Process p;
				Integer[] temperature = new Integer[2];
				for (int i = 0; i < 2; i++) {
					pb = new ProcessBuilder("cat", "/sys/class/thermal/thermal_zone" + i + "/temp");
					p = pb.start();
					temperature[i] = Integer.parseInt(new Scanner(p.getInputStream()).nextLine());
				}
				panel_temperature.addData(temperature);
				int cores = 4;
				if (isM3) {
					cores = 8;
				}
				Integer[] cpuFreq = new Integer[cores];
				for (int i = 0; i < cores; i++) {
					pb = new ProcessBuilder("cat", "/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_cur_freq");
					p = pb.start();
					try {
						Scanner s = new Scanner(p.getInputStream());
						cpuFreq[i] = Integer.parseInt(s.nextLine());
						s.close();
					} catch (NoSuchElementException e) {
						cpuFreq[i] = 0;
					}
				}
				panel_cpuFrequency.addData(cpuFreq);
				pb = new ProcessBuilder("cat", "/proc/stat");
				p = pb.start();
				Scanner s = new Scanner(p.getInputStream());
				s.nextLine(); //Line is not important (sum of CPUs)
				String[] cpus = new String[cores];
				for (int i = 0; i < cores; i++) {
					cpus[i] = s.nextLine();
				}
				s.close();
				panel_load.addData(cpus);
				panel_memory.addData(reader, writer);
				
			} catch (IOException e) {
				System.out.println("Monitor: IOException");
			}
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void activateStop() {
		this.run = false;
		this.dispose();
	}
}
