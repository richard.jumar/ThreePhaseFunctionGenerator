package Monitor;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.util.Date;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileWriter;

public class TemperaturePanel extends JPanel {
	private static final long serialVersionUID = 5L;
	private List<Integer[]> data;
	private JLabel display;
	private List<String> table;
	private String time;

	public TemperaturePanel() {
		super();
		this.setLayout(null);
		this.data = new ArrayList<Integer[]>();
		this.setBackground(Color.WHITE);
		this.display = new JLabel();
		this.display.setBounds(5, 85, 200, 15);
		this.add(this.display);
		this.table = new ArrayList<String>();
		this.table.add("Time Temperature");
		this.time = "";
	}
	
	public void addData(Integer[] temperature) {
		this.data.add(temperature);
		if (this.data.size() > this.getWidth()) {
			this.data.remove(0);
		}
		this.display.setText("Zone 0: " + temperature[0] + "\u00b0C, Zone 1: " + temperature[1] + "\u00b0C");
		this.updateUI();
		String newTime = ("" + new Date()).split(" ")[3];
		if (!time.equals(newTime)) {
			table.add(newTime + " " + temperature[0]);
			time = newTime;
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < data.size() - 1; i++) {
			g.drawLine(i, this.getHeight() - data.get(i)[0] - 1, i + 1, this.getHeight() - data.get(i + 1)[0] - 1);
			g.setColor(Color.RED);
			g.drawLine(i, this.getHeight() - data.get(i)[1] - 1, i + 1, this.getHeight() - data.get(i + 1)[1] - 1);
		}
	}

	public void writeTable() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("temperatur.txt"));
			for (int i = 0; i < table.size(); i++) {
				bw.write(table.get(i) + "\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
