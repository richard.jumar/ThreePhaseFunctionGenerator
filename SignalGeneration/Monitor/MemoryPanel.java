package Monitor;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileWriter;
import javax.swing.JLabel;
import javax.swing.JPanel;

import GUI.LanguageTranslator;

import java.util.Date;

public class MemoryPanel extends JPanel {
	private static final long serialVersionUID = 2L;
	private List<Integer[]> data;
	private JLabel display;
	private String time;
	private List<String> table;
	
	public MemoryPanel() {
		super();
		this.setLayout(null);
		this.data = new ArrayList<Integer[]>();
		this.setBackground(Color.WHITE);
		this.display = new JLabel();
		this.display.setBounds(5, 241, 600, 15);
		this.add(this.display);
		this.table = new ArrayList<String>();
		this.table.add("Time ReadpointerH WritepointerH");
		this.time = "";
	}

	public void writeTable() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("zeiger.txt"));
			for (int i = 0; i < table.size(); i++) {
				bw.write(table.get(i) + "\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addData(int reader, int writer) {

		Integer[] newData = {reader / 65536, writer / 65536};
		this.data.add(newData);
		if (this.data.size() > this.getWidth()) {
			this.data.remove(0);
		}
		this.display.setText(LanguageTranslator.getTranslation("Lesezeiger") + " " + Integer.toHexString(reader) + "h, " + LanguageTranslator.getTranslation("Schreibzeiger") + ": " + Integer.toHexString(writer) + "h");
		this.updateUI();
		String newTime = ("" + new Date()).split(" ")[3];
		if (!time.equals(newTime)) {
			table.add(newTime + " " + newData[0] + " " + newData[1]);
			time = newTime;
		}
	}
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < data.size() - 1; i++) {
			g.setColor(Color.GREEN); 
			g.drawLine(i, this.getHeight() - data.get(i)[1] - 1, i + 1, this.getHeight() - data.get(i + 1)[1] - 1); //Writer
			g.setColor(Color.RED);
			g.drawLine(i, this.getHeight() - data.get(i)[0] - 1, i + 1, this.getHeight() - data.get(i + 1)[0] - 1); //Reader
		}
	}
}
