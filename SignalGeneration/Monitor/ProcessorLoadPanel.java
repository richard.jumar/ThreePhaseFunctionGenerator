package Monitor;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ProcessorLoadPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	private List<Integer[]> data;
	private JLabel display;
	private Integer[] lastIdle;
	private Integer[] lastSum;
	public ProcessorLoadPanel() {
		super();
		this.setLayout(null);
		this.data = new ArrayList<Integer[]>();
		this.setBackground(Color.WHITE);
		this.display = new JLabel();
		this.display.setBounds(5, 81, 900, 15);
		this.add(this.display);
		this.lastIdle = new Integer[8];
		this.lastIdle[0] = 0;
		this.lastIdle[1] = 0;
		this.lastIdle[2] = 0;
		this.lastIdle[3] = 0;
		this.lastIdle[4] = 0; //unused in case BPI M2+
		this.lastIdle[5] = 0; //unused in case BPI M2+
		this.lastIdle[6] = 0; //unused in case BPI M2+
		this.lastIdle[7] = 0; //unused in case BPI M2+
		this.lastSum = new Integer[8];
		this.lastSum[0] = 0;
		this.lastSum[1] = 0;
		this.lastSum[2] = 0;
		this.lastSum[3] = 0;
		this.lastSum[4] = 0; //unused in case BPI M2+
		this.lastSum[5] = 0; //unused in case BPI M2+
		this.lastSum[6] = 0; //unused in case BPI M2+
		this.lastSum[7] = 0; //unused in case BPI M2+
	}
	
	public void addData(String[] cpus) {
		Integer[] idleRatio = new Integer[cpus.length];
		for (int i = 0; i < cpus.length; i++) {
			String[] parts = cpus[i].split(" ");
			if (parts[0].equals("cpu" + i)) {
				Integer idle = Integer.parseInt(parts[4]);
				Integer sum = Integer.parseInt(parts[1]) + Integer.parseInt(parts[2]) + Integer.parseInt(parts[3]) + idle;
				int idleDiff = idle - lastIdle[i];
				int sumDiff = sum - lastSum[i];
				if (sumDiff == 0) {
					idleRatio[i] = 0; //workarround div 0
				} else {
					idleRatio[i] = ((100 * idleDiff) / sumDiff); 
				}
				this.lastIdle[i] = idle;
				this.lastSum[i] = sum;
			} else {
				idleRatio[i] = 100;
			}
		}

		this.data.add(idleRatio);
		if (this.data.size() > this.getWidth()) {
			this.data.remove(0);
		}
		String displayString = "CPU 1: " + (100 - idleRatio[0]) + "%, CPU 2: " + (100 - idleRatio[1]) + "%, CPU 3: " + (100 - idleRatio[2]) + "%, CPU 4: " + (100 - idleRatio[3]) + "%,";
		if (cpus.length == 8) {
			displayString += " CPU 5: " + (100 - idleRatio[4]) + "%, CPU 6: " + (100 - idleRatio[5]) + "%, CPU 7: " + (100 - idleRatio[6]) + "%, CPU 8: " + (100 - idleRatio[7]) + "%,";
		}
		displayString += " Durchschnitt " + (100 - ((idleRatio[0] + idleRatio[1] + idleRatio[2] + idleRatio[3]) / 4)) + "%";
		this.display.setText(displayString);
		this.updateUI();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < data.size() - 1; i++) {
			g.drawLine(i, data.get(i)[0], i + 1, data.get(i + 1)[0]);
			g.setColor(Color.BLUE);
			g.drawLine(i, data.get(i)[1], i + 1, data.get(i + 1)[1]);
			g.setColor(Color.RED);
			g.drawLine(i, data.get(i)[2], i + 1, data.get(i + 1)[2]);
			g.setColor(Color.GREEN);
			g.drawLine(i, data.get(i)[3], i + 1, data.get(i + 1)[3]);
			if (data.get(i).length == 8) {
				g.setColor(Color.ORANGE);
				g.drawLine(i, data.get(i)[4], i + 1, data.get(i + 1)[4]);	
				g.setColor(Color.CYAN);
				g.drawLine(i, data.get(i)[5], i + 1, data.get(i + 1)[5]);
				g.setColor(Color.MAGENTA);
				g.drawLine(i, data.get(i)[6], i + 1, data.get(i + 1)[6]);		
				g.setColor(Color.GRAY);
				g.drawLine(i, data.get(i)[7], i + 1, data.get(i + 1)[7]);			
			}
		}
	}
}
