package Data;

import java.util.ArrayList;
import java.util.Collections;

public class NonPeriodicLinearInterpolation extends NonPeriodicInterpolation {
	private double average = 0;


	public NonPeriodicLinearInterpolation(long startTime, long endTime) {
		super(startTime, endTime);
		this.interpolationSegments = new ArrayList<PolynomSegment>();
	}

	public boolean interpolate() {
		if (this.sortedList.size() == 0) {
			return false;
		}
		long leftBorder_time;
		long rightBorder_time;
		if (this.startTime == -1) {
			leftBorder_time = this.startTime;
			rightBorder_time = this.endTime;
		} else {
			leftBorder_time = 0;
			rightBorder_time = this.endTime - this.startTime;
		}
		this.interpolationSegments = new ArrayList<PolynomSegment>();

		//calculate interpolation between the points
		double area = 0;
		for (int i = 1; i < sortedList.size(); i++) {
			double a = (sortedList.get(i).getValue() - sortedList.get(i - 1).getValue()) / (sortedList.get(i).getTime() - sortedList.get(i - 1).getTime());
			double b = sortedList.get(i).getValue() - (a * (double) sortedList.get(i).getTime());
			interpolationSegments.add(new PolynomSegment(sortedList.get(i - 1).getTime(), b, a));
			area += (sortedList.get(i).getValue() / 2 + sortedList.get(i - 1).getValue() / 2) * (sortedList.get(i).getTime() - sortedList.get(i - 1).getTime());
		}
		if (!this.zeroMean) {
			double a0 = ((double) sortedList.get(0).getValue() - this.zeroLevel) / (sortedList.get(0).getTime() - leftBorder_time);
			double b0 = this.zeroLevel - (a0 * (double) leftBorder_time);
			interpolationSegments.add(new PolynomSegment(leftBorder_time, b0, a0));
			double a1 = (this.zeroLevel - (double) sortedList.get(sortedList.size() - 1).getValue()) / (rightBorder_time - sortedList.get(sortedList.size() - 1).getTime());
			double b1 = sortedList.get(sortedList.size() - 1).getValue() - (a1 * (double) sortedList.get(sortedList.size() - 1).getTime());
			interpolationSegments.add(new PolynomSegment(sortedList.get(sortedList.size() - 1).getTime(), b1, a1));
		} else {
			double width = (endTime - startTime);
			double y_border = (area / width + ((sortedList.get(0).getTime()) * sortedList.get(0).getValue()) / (width * 2) + ((width - sortedList.get(sortedList.size() - 1).getTime()) * sortedList.get(sortedList.size() - 1).getValue())/(width * 2)) / (1 - (sortedList.get(0).getTime())/(2 * width) - (width - sortedList.get(sortedList.size() - 1).getTime())/(2 * width));
			double a = (sortedList.get(0).getValue() - y_border) / (sortedList.get(0).getTime() - leftBorder_time);
			interpolationSegments.add(new PolynomSegment(startTime, y_border - (a * (double) leftBorder_time), a));
			a = (y_border - sortedList.get(sortedList.size() - 1).getValue()) / (rightBorder_time - sortedList.get(sortedList.size() - 1).getTime());
			interpolationSegments.add(new PolynomSegment(sortedList.get(sortedList.size() - 1).getTime(), sortedList.get(sortedList.size() - 1).getValue() - (a * (double) (sortedList.get(sortedList.size() - 1).getTime())), a));
			this.average = y_border;		}
		for (int i = 0; i < interpolationSegments.size(); i++) {
			interpolationSegments.get(i).setSigned(true);
		}
		Collections.sort(interpolationSegments);	
		return true;
	}




	@Override
	public int getIndex() {
		return 1; //linear interpolation
	}


	@Override
	public double getAverage() {
		return this.average;
	}

}
