package Data;

/**
 * interface for double value functions (signal and am)
 */
public interface DoubleValueFunction {
	double[] getValue();
	double[] getCachedValue(int index);
	void swapCaches();
}
