package Data;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 *  periodic amplitude modulation ramp function
 */
public class PeriodicAmplitudeModulationRamp extends PeriodicAmplitudeModulation implements BasicFunction {
	private Angle[] startAngle0;
	private Angle[] endAngle0;
	private Angle[] startAngle1;
	private Angle[] endAngle1;
	private double[] value0;
	private double[] value1;
	private double[] gradient0;
	private double[] gradient1;
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	private boolean run;

	public PeriodicAmplitudeModulationRamp(Phasor p, Angle[] startAngle0, Angle[] endAngle0, Angle[] startAngle1, Angle[] endAngle1, double[] value0, double[] value1) {
		super(p);
		this.startAngle0 = startAngle0;
		this.endAngle0 = endAngle0;
		this.startAngle1 = startAngle1;
		this.endAngle1 = endAngle1;
		this.value0 = value0;
		this.value1 = value1;
		this.constructorInit();
	}

	public PeriodicAmplitudeModulationRamp(Phasor p, Angle startAngle0, Angle endAngle0, Angle startAngle1, Angle endAngle1, double value0, double value1) {
		super(p);
		Angle[] startAngle0Array = {startAngle0, startAngle0, startAngle0};
		Angle[] endAngle0Array = {endAngle0, endAngle0, endAngle0};
		Angle[] startAngle1Array = {startAngle1, startAngle1, startAngle1};
		Angle[] endAngle1Array = {endAngle1, endAngle1, endAngle1};
		double[] value0Array = {value0, value0, value0};
		double[] value1Array = {value1, value1, value1};
		this.startAngle0 = startAngle0Array;
		this.endAngle0 = endAngle0Array;
		this.startAngle1 = startAngle1Array;
		this.endAngle1 = endAngle1Array;
		this.value0 = value0Array;
		this.value1 = value1Array;
		this.constructorInit();
	}
		
	private void constructorInit() {
		this.gradient0 = new double[3];
		this.gradient1 = new double[3];
		for(int i = 0; i < 3; i++) {
			if (endAngle0[i].getAngle() > startAngle0[i].getAngle()) {
				gradient0[i] = (value1[i] - value0[i]) / (endAngle0[i].getAngle() - startAngle0[i].getAngle());
			} else if (endAngle0[i].getAngle() == startAngle0[i].getAngle()) {
				gradient0[i] = 0; //never used, because there is no sample in the ramp. Just to avoid division by zero. 
			} else {
				gradient0[i] = (value1[i] - value0[i]) / (((double) endAngle0[i].getAngle() - (double) Long.MIN_VALUE) + 1 + ((double) Long.MAX_VALUE - (double) startAngle0[i].getAngle()));
			}
			if (endAngle1[i].getAngle() > startAngle1[i].getAngle()) {
				gradient1[i] = (value0[i] - value1[i]) / (endAngle1[i].getAngle() - startAngle1[i].getAngle());
			} else if (endAngle1[i].getAngle() == startAngle1[i].getAngle()) {
				gradient1[i] = 0; //never used, because there is no sample in the ramp. Just to avoid division by zero. 
			} else {
				gradient1[i] = (value0[i] - value1[i]) / (((double) endAngle1[i].getAngle() - (double) Long.MIN_VALUE) + 1 + ((double) Long.MAX_VALUE - (double) startAngle1[i].getAngle()));
			}
		}
	}


	@Override
	public double[] getValue() {
		long angle[] = new long[3];
		angle[0] = this.phasor.getPhase1().getAngle();
		angle[1] = this.phasor.getPhase2().getAngle();
		angle[2] = this.phasor.getPhase3().getAngle();
		return computeValue(angle);
		
	}

	@Override
	public void setPhasor(Phasor phasor) {
		this.phasor = phasor;
		
	}

	public double[] getValue0() {
		return value0;
	}

	public double[] getValue1() {
		return value1;
	}
	
	public Angle[] getStartAngle0() {
		return this.startAngle0;
	}

	public Angle[] getEndAngle0() {
		return this.endAngle0;
	}
	
	public Angle[] getStartAngle1() {
		return this.startAngle1;
	}
	
	public Angle[] getEndAngle1() {
		return this.endAngle1;
	}
	
	public boolean isSymmetric() {
		if (startAngle0[0].equals(startAngle0[1]) && startAngle0[0].equals(startAngle0[2]) && endAngle0[0].equals(endAngle0[1]) && endAngle0[0].equals(endAngle0[2]) && startAngle1[0].equals(startAngle1[1]) && startAngle1[0].equals(startAngle1[2]) && endAngle1[0].equals(endAngle1[1]) && endAngle1[0].equals(endAngle1[2]) && value0[0] == value0[1] && value0[0] == value0[2] && value1[0] == value1[1] && value1[0] == value1[2]) {
			return true;			
		} else {
			return false;
		}
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		if (this.getPhasor() == oldPhasor) {
			setPhasor(newPhasor);
		}		
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;		
	}

	public double[] computeValue(long[] angle) {
		double[] value = new double[3];
		for (int i = 0; i < 3; i ++) {
			if (angle[i] >= startAngle0[i].getAngle() && angle[i] < endAngle0[i].getAngle()) { //ramp between value0 and value1 (not over 180�/-180�)
				value[i] = value0[i] + (gradient0[i] * ((double) angle[i] - (double) startAngle0[i].getAngle()));
			} else if (angle[i] >= endAngle0[i].getAngle() && angle[i] < startAngle1[i].getAngle()) { //after the ramp0, not over -180�/180�
				value[i] = value1[i];
			} else if (angle[i] >= startAngle1[i].getAngle() && angle[i] < endAngle1[i].getAngle()) { //ramp between value1 and value0 (not over 180�/-180�)
				value[i] = value1[i] + gradient1[i] * ((double) angle[i] - (double) startAngle1[i].getAngle());
			} else if (angle[i] >= endAngle1[i].getAngle() && angle[i] < startAngle0[i].getAngle()) { //after the ramp0, not over -180�/180�
				value[i] = value0[i];
			} else if (startAngle1[i].getAngle() < endAngle0[i].getAngle()) { //break between ramp0 and ramp 1
				value[i] = value1[i];
			} else if (startAngle0[i].getAngle() < endAngle1[i].getAngle()) { //break between ramp1 and ramp 0
				value[i] = value0[i];
			} else if (endAngle0[i].getAngle() < startAngle0[i].getAngle()) { //break in ramp0
				if (angle[i] >= startAngle0[i].getAngle()) {
					value[i] = value0[i] + gradient0[i] * ((double) angle[i] - (double) startAngle0[i].getAngle());
				} else {
					value[i] = value1[i] - gradient0[i] * ((double) endAngle0[i].getAngle() - (double) angle[i]);
				}
			} else if (endAngle1[i].getAngle() < startAngle1[i].getAngle()) { //break in ramp0
				if (angle[i] >= startAngle1[i].getAngle()) {
					value[i] = value1[i] + gradient1[i] * ((double) angle[i] - (double) startAngle1[i].getAngle());
				} else {
					value[i] = value0[i] - gradient1[i] * ((double) endAngle1[i].getAngle() - (double) angle[i]);
				}
			} else {
				//case: startangle is asked, where startangle = endangle
				value[i] = value1[i];
			}
		}
		return value;
	}
	
	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = this.phasor.getAngleCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				this.cacheInProcess[cacheIndex] = computeValue(this.phasor.getAngleCache()[cacheIndex]);
			}
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public void activateStop() {
		this.run = false;
	}
}
