package Data;

import java.math.BigInteger;

/**
 * rotation over all values of a long. 
 * 0 is no rotation or 360�, 180� is 2^-63, 90� is 2^62
 */
public class Angle {
	private long angle;
	
	public Angle(long angle) {
		this.angle = angle;
	}
	
	/**
	 * rotate around the given angle 
	 * @param rotationAngle
	 */
	public void rotate(long rotationAngle) {
		angle += rotationAngle;
	}
	
	/**
	 * long value of this 64 bit angle
	 * @return
	 */
	public long getAngle() {
		return this.angle;
	}
	
	/**
	 * set this angle to a given value
	 * @param newAngle
	 */
	public void setAngleLike(Angle newAngle) {
		this.angle = newAngle.getAngle();
	}
	
	/**
	 * return a angle string in degrees with the degree symbol
	 * @param decimals
	 * @return
	 */
	public String getDegreeWithDecimals(int decimals) {
		return getDegreeWithDecimalsWithoutUnit(decimals) +  "�";
	}
	
	/**
	 * get the angle as string with comma in degree between 0� and 360� without the degree symbol
	 * @param decimals
	 * @return
	 */
	public String getDegreeWithDecimalsWithoutUnit(int decimals) {
		double degree = ((double) this.angle) * 360d / Math.pow(2, 64);
		if (degree < 0) {
			degree += 360;
		}
		int withoutDot = (int) Math.round(degree * Math.pow(10, decimals));
		int afterDot = withoutDot % (int) Math.pow(10, decimals);
		int beforeDot = withoutDot / (int) Math.pow(10, decimals);
		return beforeDot + "," + afterDot;
	}
	
	/**
	 * returns the 64 bit value of the angle to the given degree-angle
	 * @param degree
	 * @return
	 */
	public static long DegreeToAngle(int degree) {
		while(degree >= 360 || degree < 0) {
			if (degree < 0) {
				degree += 360;
			} else {
				degree -= 360;
			}
		}
		BigInteger degree360 = BigInteger.valueOf(2).pow(64);
		BigInteger product = degree360.multiply(BigInteger.valueOf(degree));
		BigInteger result = product.divide(BigInteger.valueOf(360));
		return result.longValue();
	}
	
	/**
	 * convert a angle from degree to 64bit representation
	 * @param degree
	 * @return
	 */
	public static long DegreeToAngle(double degree) {
		while (degree < -180) {
			degree += 360;
		}
		while (degree >= 180) {
			degree -= 360;
		}
		return Math.round(degree * Math.pow(2, 64) / 360d);
	}
	 
	
	/**
	 * returns the degree value of the given 64 bit angle
	 * @param angle
	 * @return
	 */
	public static int AngleToDegree(long angle) {
		BigInteger bigAngle = BigInteger.valueOf(angle);
		BigInteger product = bigAngle.multiply(BigInteger.valueOf(360));
		return product.divide(BigInteger.valueOf(2).pow(64)).intValue();
	}
	
	public String toString() {
		return this.angle + " (" + AngleToDegree(angle) + ")";
	}
	
	/**
	 * compares this angle with another angle between 0� and 360�
	 * @param comp compared angle
	 * @return true, if this angle is greater than the given angle in a range from [0�, 360�).
	 */
	public boolean isGreaterThan(Angle comp) {
		if (angle >= 0 && comp.getAngle() < 0) {
			return false;
		} else if (angle >= 0 && comp.getAngle() >= 0) {
			return angle > comp.getAngle();
		} else if (angle < 0 && comp.getAngle() >= 0) {
			return true;
		} else if (angle < 0 && comp.getAngle() < 0) {
			return angle > comp.getAngle();
		}
		return false; //unreachable
	}
	
	/**
	 * compares this angle with another angle between 0� and 360�
	 * @param comp compared angle
	 * @return true, if this angle is greater or equal to the given angle in a range from [0�, 360�).
	 */
	public boolean isGreaterEqual(Angle comp) {
		if (angle == comp.getAngle()) {
			return true;
		} else {
			return this.isGreaterThan(comp);
		}
	}
	
	/**
	 * compares this angle with another angle between 0� and 360�
	 * @param comp compared angle
	 * @return true, if this angle is lower than or equal to the given angle in a range from [0�, 360�).
	 */
	public boolean isLowerEqual(Angle comp) {
		return (!isGreaterThan(comp));
	}
	
	public boolean equals(Angle arg0) {
		return this.angle == arg0.getAngle();
	}
}
