package Data;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import Controller.DataControl;
import File.RecordedXmlBufferFiller;
import Player.RecordedSignalBuffer;
import Player.SignalPlayer;

/**
 * Recorded signal from a EDR XML file. 
 */
public class NonPeriodicSignalRecorded extends NonPeriodicSignal implements BasicFunction {
	private long startTime;
	private double[] quiet;
	private RecordedSignalBuffer buffer; 
	private RecordedXmlBufferFiller bufferFiller;
	private RecordingSampleCounter rsc;
	private boolean active;
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	private boolean run;
	private Thread thread_bufferFiller;
	private String path;
	private boolean loadNextFile;
	private String calibrationFilePath;
	
	/**
	 * constructor
	 * @param clock
	 * @param path
	 */
	public NonPeriodicSignalRecorded(Clock clock, String path) {
		super(clock);
		this.quiet = new double[3];
		this.quiet[0] = 0;
		this.quiet[1] = 0;
		this.quiet[2] = 0;
		this.startTime = 0;
		this.calibrationFilePath = null;
		this.path = path;		
		this.loadNextFile = true;

		
	}

	/**
	 * activate auto loading of the following files 
	 * @param loadNextFile
	 */
	public void setLoadNextFile(boolean loadNextFile) {
		this.loadNextFile = loadNextFile;
	}
	
	/**
	 * set the start time
	 * @param startTime
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	/**
	 * get the start time in microseconds
	 * @return start time
	 */
	public long getStartTime() {
		return this.startTime;
	}
	
	
	public double[] getValue() {
		//unused! 
		return quiet; 
	}



	/**
	 * replace a phasor 
	 * no phasor => nothing to do
	 */
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		// no phasor => do nothing		
	}



	@Override
	/**
	 * get a value from the cache
	 */
	public double[] getCachedValue(int index) {
		return cache[index];
	}



	/**
	 * swap cache for the next pipeline step
	 */
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	/**
	 * set a calibration file
	 * @param calibrationFilePath
	 */
	public void setCalibrationFilePath(String calibrationFilePath) {
		this.calibrationFilePath = calibrationFilePath;
	}

	/**
	 * run reading samples from buffer, interpolating and writing into the pipeline cache. 
	 */
	public void run() {
		this.active = false;
		this.buffer = new RecordedSignalBuffer();
		this.bufferFiller = new RecordedXmlBufferFiller(this.path, this.buffer, this.loadNextFile); 
		if (this.calibrationFilePath != null) {
			this.bufferFiller.setCalibrationFilePath(calibrationFilePath);
		}
		this.run = true;
		this.thread_bufferFiller = new Thread(bufferFiller);
		this.thread_bufferFiller.start();
		int samplingRateEDR = this.bufferFiller.getSamplesPerSecond();
		while (samplingRateEDR < 0) {
			SignalPlayer.log("Waiting for information about the sampling rate from the EDR XML file");
			try {			
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				samplingRateEDR = this.bufferFiller.getSamplesPerSecond();
		}
		this.rsc = new RecordingSampleCounter(samplingRateEDR, clock.getSamplingRate().samplingRate_int, buffer);
		while (run) {
			int cacheSize = DataControl.getClock().getCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			//fill block
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				if (DataControl.getClock().getCache()[cacheIndex] < startTime) { 
					//waiting for start time
					this.cacheInProcess[cacheIndex] = quiet;
				} else if (!(buffer.hasFinished() && buffer.getElementCount() < rsc.getMaxSamplesPerClockIncrement())) {
					//while run time of the recording
					while (buffer.getElementCount() < rsc.getMaxSamplesPerClockIncrement()) {
						try {
							SignalPlayer.log("Waiting for new recording samples in buffer");
							if (buffer.hasFinished()) {
								System.out.println("Race condition problem, please restart");
							}
							Thread.sleep(1000); //wait...
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					double[] bufferValue = new double[3];
					if (buffer.getElementCount() > 1) {
						for (int i = 0; i < 3; i++) {
							/* 
							============================================
							An dieser Stelle kann ggf. auch eine andere Interpolation eingefügt werden!							
							============================================
							*/
							//linear interpolation between the sampling points of the recorded signal. 
							if (buffer.getFirstElement() == null) {
								System.out.println("buffer.getFirstElement() is null");
							}
							if (rsc == null) {
								System.out.println("rsc.getOffset() is null");
							}
							if (buffer.getFirstElement().getNext() == null) {
								System.out.println("buffer.getFirstElement().getNext() is null");
							}
							bufferValue[i] = buffer.getFirstElement().getValue()[i] + rsc.getOffset() * (buffer.getFirstElement().getNext().getValue()[i] - buffer.getFirstElement().getValue()[i]);
						}
					}
					if (!active) {
						rsc.setActive(true);
						this.active = true;
					}
					this.cacheInProcess[cacheIndex] = bufferValue;
				} else {
					//finished playing the recorded signal
					rsc.setActive(false);
					this.active = false;
					this.cacheInProcess[cacheIndex] = quiet;
				}
				this.rsc.clockImpulse();
			}
			synchronized (this.bufferFiller) {
				this.bufferFiller.notifyAll();
			}
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
		this.rsc.setActive(false);
		this.bufferFiller.activateStop();
		synchronized (this.bufferFiller) {
			this.bufferFiller.notifyAll(); //wake bufferFiller up if it is waiting
		}
	}

	/**
	 * set the barrier
	 */
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;		
	}

	/**
	 * activate stop running
	 */
	public void activateStop() {
		this.run = false;
	}
}
