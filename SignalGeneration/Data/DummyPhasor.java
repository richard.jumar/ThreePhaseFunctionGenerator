package Data;

/**
 * Dummy phasor for thumbnail computation
 */
public class DummyPhasor extends Phasor {
	private int steps;
	private long angle_step;
	public DummyPhasor(int steps) {
		super(null, null);
		this.steps = steps;
		this.angle_step = (long) (Math.pow(2, 64) / ((double) this.steps));
	}
	
	public void nextStep() {
		this.phase1.rotate(angle_step);
		this.phase2.rotate(angle_step);
		this.phase3.rotate(angle_step);
	}
}
