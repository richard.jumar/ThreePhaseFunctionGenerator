package Data;

import java.io.File;
import File.FilterReader;


/**
 * FIR Filter for three channels
 * Coefficients are imported from a textfile exported by matlab filterDesigner
 */
public class Filter {
	private File filterFile;
	private double[][] memory;
	private int length;
	private int memoryPointer;
	private double[] gain; //[z^-(n-1), z^-(n-2), ..., z^-1, z^0]
	
	/**
	 * Constructor
	 * @param filterFile coefficient file
	 */
	public Filter(File filterFile) {
		this.filterFile = filterFile;
	}
	
	
	/**
	 * no filter
	 */
	public Filter() {
		double[] gain1 = {1d};
		createFilter(gain1);
	}
	
	/**
	 * reads the coefficient file and creates the filter
	 * @return 1: ok, 0: file error, -1: no/wrong filter type (Discrete-Time FIR Filter expected), -2: no/wrong filter structure (expected Direct-Form FIR), -3: Filter Length is missing, -4: wrong coefficient format (expected Decimal), -5: Missing "Numerator" Line, -6: Not enough/invalid values
	 */
	public int readFile() {
		FilterReader fr = new FilterReader(this.filterFile);
		int status = fr.readFilter();
		if (status == 1) {
			this.createFilter(fr.getCoefficients());
		}
		return status;
	}
	
	/**
	 * 
	 * @param gain [z^0, z^-1, z^-2, ... , z^-(n-1)]
	 */
	private void createFilter(double[] gain) {
		this.length = gain.length;
		this.gain = new double[this.length];
		this.memory = new double[this.length][3];
		for (int i = 0; i < this.length; i++) {
			for (int j = 0; j < 3; j++) {
				this.memory[i][j] = 0;
			}
		}
		this.memoryPointer = 0;
		for (int i = 0; i < this.length; i++) {
			this.gain[i] = gain[this.length - 1 - i];
		}
	}
	
	/**
	 * the filter is feeded with a new value and computes the filtered value 
	 * @param value new value
	 * @return filtered value
	 */
	public double[] getFilteredValue(double[] value) {
		this.memory[memoryPointer] = value;
		this.memoryPointer = (this.memoryPointer + 1) % this.length;
		double[] newValue = {0d, 0d, 0d};
		for (int i = 0; i < this.length; i++) {
			for (int j = 0; j < 3; j++) {
				newValue[j] += gain[i] * this.memory[(this.memoryPointer + i) % this.length][j];
			}
		}
		return newValue;
	}
}
