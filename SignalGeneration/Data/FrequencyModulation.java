package Data;

/**
 * Frequency modulation for three phases with a constant rate of change of frequency.
 */
public class FrequencyModulation {
	private long[] tModStart; //�s
	private long[] tModEnd;
	private long[] fEnd;
	
	/**
	 * get the start times of the frequency modulation in three phases
	 * @return
	 */
	public long[] getStart() {
		return this.tModStart;
	}
	
	/**
	 * get the end times of the frequency modulation in three phases
	 * @return
	 */
	public long[] getEnd() {
		return this.tModEnd;
	}
	
	/**
	 * get the end frequencys of the frequency modulation in three phases
	 * @return
	 */
	public long[] getFEnd() {
		return fEnd;
	}
	
	public long getF1End() {
		return fEnd[0];
	}

	public long getF2End() {
		return fEnd[1];
	}

	public long getF3End() {
		return fEnd[2];
	}
	
	public long gettMod1Start() {
		return tModStart[0];
	}

	public void settMod1Start(long tMod1Start) {
		this.tModStart[0] = tMod1Start;
	}

	public long gettMod1End() {
		return tModEnd[0];
	}

	public void settMod1End(long tMod1End) {
		this.tModEnd[0] = tMod1End;
	}

	public long gettMod2Start() {
		return tModStart[1];
	}

	public void settMod2Start(long tMod2Start) {
		this.tModStart[1] = tMod2Start;
	}

	public long gettMod2End() {
		return tModEnd[1];
	}

	public void settMod2End(long tMod2End) {
		this.tModEnd[1] = tMod2End;
	}

	public long gettMod3Start() {
		return tModStart[2];
	}

	public void settMod3Start(long tMod3Start) {
		this.tModStart[2] = tMod3Start;
	}

	public long gettMod3End() {
		return tModEnd[2];
	}

	public void settMod3End(long tMod3End) {
		this.tModEnd[2] = tMod3End;
	}

	/**
	 * Constructor for synchronous (linear) frequency modulation
	 * start frequency results from the frequency modulation before
	 * @param tModStart start time of frequency modulation
	 * @param tModEnd end time of frequency modulation
	 * @param fEnd end frequency
	 */
	public FrequencyModulation(long tModStart, long tModEnd, long fEnd) {
		this.tModStart = new long[3];
		this.tModEnd = new long[3];
		this.fEnd = new long[3];
		this.tModStart[0] = tModStart;
		this.tModStart[1] = tModStart;
		this.tModStart[2] = tModStart;
		this.tModEnd[0] = tModEnd;
		this.tModEnd[1] = tModEnd;
		this.tModEnd[2] = tModEnd;
		this.fEnd[0] = fEnd;
		this.fEnd[1] = fEnd;
		this.fEnd[2] = fEnd;
	}
	
	/**
	 * Constructor for asynchronous (linear) frequency modulation
	 * start frequency results from the frequency modulation before
	 * @param tMod1Start start time phase 1
	 * @param tMod1End end time phase 1
	 * @param f1End end frequency phase 1
	 * @param tMod2Start start time phase 2
	 * @param tMod2End end time phase 2
	 * @param f2End end frequency phase 2
	 * @param tMod3Start start time phase 3
	 * @param tMod3End end time phase 3
	 * @param f3End end frequency phase 3
	 */
	public FrequencyModulation(long tMod1Start, long tMod1End, long f1End, long tMod2Start, long tMod2End, long f2End, long tMod3Start,  long tMod3End, long f3End) {
		this.tModStart = new long[3];
		this.tModEnd = new long[3];
		this.fEnd = new long[3];
		this.tModStart[0] = tMod1Start;
		this.tModStart[1] = tMod2Start;
		this.tModStart[2] = tMod3Start;
		this.tModEnd[0] = tMod1End;
		this.tModEnd[1] = tMod2End;
		this.tModEnd[2] = tMod3End;
		this.fEnd[0] = f1End;
		this.fEnd[1] = f2End;
		this.fEnd[2] = f3End;
	}
}
