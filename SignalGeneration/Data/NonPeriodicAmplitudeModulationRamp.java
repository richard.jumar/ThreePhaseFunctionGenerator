package Data;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class NonPeriodicAmplitudeModulationRamp extends NonPeriodicAmplitudeModulation implements BasicFunction {

	private long[] startTime;
	private long[] endTime;
	private double[] startValue;
	private double[] endValue;
	private double[] gradient;
	private boolean run;
	
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	
	/**
	 * Amplitude modulation with start and end value and a linear ramp between them. 
	 * The ramp could have different properies in the three phases
	 * @param clock
	 * @param startTime start time of the ramp in �s (three-dimensional)
	 * @param endTime end time of the ramp in �s (three-dimensional)
	 * @param startValue value before the ramp (three-dimensional)
	 * @param endValue value after the ramp (three-dimensional)
	 */
	public NonPeriodicAmplitudeModulationRamp(Clock clock, long startTime[], long endTime[], double startValue[], double endValue[]) {
		super(clock);
		this.run = true;
		this.startTime = startTime;
		this.endTime = endTime;
		this.startValue = startValue;
		this.endValue = endValue;
		this.gradient = new double[3];
		//TODO Fall startTime=endTime -> Abfangen!
		for(int i = 0; i < 3; i++) {
			gradient[i] = (endValue[i] - startValue[i]) / (endTime[i] - startTime[i]);
		}
	}
	
	/**
	 * Amplitude modulation with start and end value and a linear ramp between them. 
	 * The ramps in all phases are synchronous 
	 * @param clock
	 * @param startTime start time of the ramp in �s
	 * @param endTime end time of the ramp in �s
	 * @param startValue value before the ramp
	 * @param endValue value after the ramp
	 */
	public NonPeriodicAmplitudeModulationRamp(Clock clock, long startTime, long endTime, double startValue, double endValue) {
		super(clock);
		this.startTime = new long[3];
		this.startTime[0] = startTime;
		this.startTime[1] = startTime;
		this.startTime[2] = startTime;
		this.endTime = new long[3];
		this.endTime[0] = endTime;
		this.endTime[1] = endTime;
		this.endTime[2] = endTime;
		this.startValue = new double[3];
		this.startValue[0] = startValue;
		this.startValue[1] = startValue;
		this.startValue[2] = startValue;
		this.endValue = new double[3];
		this.endValue[0] = endValue;
		this.endValue[1] = endValue;
		this.endValue[2] = endValue;
		this.gradient = new double[3];
		for(int i = 0; i < 3; i++) {
			gradient[i] = (endValue - startValue) / (endTime - startTime);
		}
	}

	/**
	 * values at the current time
	 */
	public double[] getValue() {
		double value[] = new double[3];
		for (int i = 0; i < 3; i ++) {
			if (this.clock.getMicroSeconds() < this.startTime[i]) { //before the ramp
				value[i] = startValue[i];
			} else if (this.clock.getMicroSeconds() >= endTime[i]) { //after the ramp
				value[i] = endValue[i];
			} else { //ramp
				value[i] = startValue[i] + gradient[i] * (this.clock.getMicroSeconds() - this.startTime[i]);
			}
		}
		return value;
	}
	
	public boolean isSymmetric() {
		return (startTime[0] == startTime[1] && startTime[0] == startTime[2] && endTime[0] == endTime[1] && endTime[0] == endTime[2] && startValue[0] == startValue[1] && startValue[0] == startValue[2] && endValue[0] == endValue[1] && endValue[0] == endValue[2]);
	}
	
	public long[] getRampStartTime() {
		return this.startTime;
	}
	
	public long[] getRampEndTime() {
		return this.endTime;
	}
	
	public double[] getStartValue() {
		return this.startValue;
	}
	
	public double[] getEndValue() {
		return this.endValue;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		//no phasor => do nothing
		
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = 0;
			if (this.clock.getCache() == null) {
				cacheSize = 0;
			} else {
				cacheSize = this.clock.getCache().length;
			}
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				double value[] = new double[3];
				for (int i = 0; i < 3; i++) {
					if (this.clock.getCache()[cacheIndex] < this.startTime[i]) { //before the ramp
						value[i] = startValue[i];
					} else if (this.clock.getCache()[cacheIndex] >= endTime[i]) { //after the ramp
						value[i] = endValue[i];
					} else { //ramp
						value[i] = startValue[i] + gradient[i] * (this.clock.getCache()[cacheIndex] - this.startTime[i]);
					}
					this.cacheInProcess[cacheIndex] = value;
				}				
			}
			
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public void activateStop() {
		this.run = false;
	}
}
