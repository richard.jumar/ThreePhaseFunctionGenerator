package Data;

/**
 * Amplitude modulation created by addition of two amplitude modulations
 */
public class AddAmplitudeModulation extends AmplitudeModulation {
	private AmplitudeModulation am0;
	private AmplitudeModulation am1;
	
	/**
	 * Constructor
	 * @param signal0
	 * @param signal1
	 */
	public AddAmplitudeModulation(AmplitudeModulation am0, AmplitudeModulation am1) {
		this.am0 = am0;
		this.am1 = am1;
	}

	/**
	 * set the amplitude modulation 0
	 * @param am0 new amplitude modulation 0
	 */
	public void updateAmplitudeModulation0(AmplitudeModulation am0) {
		this.am0 = am0;
	}
	
	/**
	 * set the amplitude modulation 1
	 * @param am1 new amplitude modulation 1
	 */
	public void updateAmplitudeModulation1(AmplitudeModulation am1) {
		this.am1 = am1;
	}
	
	/**
	 * 3 phase values at the current time
	 */
	public double[] getValue() {
		double[] value = {am0.getValue()[0] + am1.getValue()[0], am0.getValue()[1] + am1.getValue()[1], am0.getValue()[2] + am1.getValue()[2]}; 
		return value;
	}

	/**
	 * replace a phasor (tell it to the children)
	 * @param oldPhasor
	 * @param newPhasor
	 */
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		this.am0.replacePhasor(oldPhasor, newPhasor);
		this.am1.replacePhasor(oldPhasor, newPhasor);		
	}
	
	/**
	 * get child 0
	 * @return amplitude modulation 0
	 */
	public AmplitudeModulation getChild0() {
		return this.am0;
	}
	
	/**
	 * get child 1
	 * @return amplitude modulation 1
	 */
	public AmplitudeModulation getChild1() {
		return this.am1;
	}

	
	public double[] getCachedValue(int index) {
		double[] value = {am0.getCachedValue(index)[0] + am1.getCachedValue(index)[0], am0.getCachedValue(index)[1] + am1.getCachedValue(index)[1], am0.getCachedValue(index)[2] + am1.getCachedValue(index)[2]}; 
		return value;
	}


	public void swapCaches() {
		this.am0.swapCaches();
		this.am1.swapCaches();
	}
}
