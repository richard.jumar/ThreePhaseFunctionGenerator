package Data;

/**
 * direct voltage signal
 */
public class ConstSignal extends NonPeriodicSignal {
	private double[] value;
	
	/**
	 * returns the value at the current time
	 */
	public double[] getValue() {
		return this.value;
	}

	
	/**
	 * Constructor
	 * @param amplitude in first order in V
	 * @param phasor
	 */
	public ConstSignal() {
		super(null);
		double[] value = {0d, 0d, 0d};
		this.value = value;
	}

	/**
	 * update the dc voltage values
	 * @param values
	 */
	public void updateValues(double[] values) {
		this.value = values;
	}

	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		//nothing
	}


	/**
	 * get the cached value
	 */
	public double[] getCachedValue(int index) {
		return this.value; //no change of values => return the same value everytime
	}


	@Override
	public void swapCaches() {
		//no cache => nothing to do
		
	}

}
