package Data;

public abstract class NonPeriodicAmplitudeModulation extends AmplitudeModulation {
	protected Clock clock;
	
	protected NonPeriodicAmplitudeModulation(Clock clock) {
		this.clock = clock;
	}
	
}
