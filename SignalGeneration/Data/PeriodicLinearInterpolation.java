package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * periodic interpolation with linear connected points
 */
public class PeriodicLinearInterpolation extends PeriodicInterpolation {
	
	private List<PolynomSegment> interpolationSegments;

	public PeriodicLinearInterpolation() {
		this.interpolationSegments = new ArrayList<PolynomSegment>();
	}
	
	/**
	 * computes the interpolation
	 */
	public boolean interpolate() {
		if (this.sortedList.size() == 0) {
			return false;
		}
		this.interpolationSegments = new ArrayList<PolynomSegment>();
		for (int i = 0; i < sortedList.size(); i++) {
			sortedList.get(i).setSignedSort(true); 
		}
		Collections.sort(sortedList);
		
		int lastIndex = sortedList.size() - 1;

		double overlapping_a = 0;
		if (lastIndex > 0) { //lastIndex=0 -> division by 0.
			overlapping_a = (sortedList.get(0).getValue() - sortedList.get(lastIndex).getValue()) / (((double) sortedList.get(0).getAngle().getAngle() - (double) Long.MIN_VALUE) + 1 + ((double) Long.MAX_VALUE - (double) sortedList.get(lastIndex).getAngle().getAngle()));//size is greater than 0
		}
		double start_b = sortedList.get(0).getValue() - (overlapping_a * (double) sortedList.get(0).getAngle().getAngle());
		interpolationSegments.add(new PolynomSegment(Long.MIN_VALUE, start_b, overlapping_a));
		//calculate interpolation between the points in -180� to 180�
		for (int i = 1; i < sortedList.size(); i++) {
			double a = (sortedList.get(i).getValue() - sortedList.get(i - 1).getValue()) / ((double) sortedList.get(i).getAngle().getAngle() - (double) sortedList.get(i - 1).getAngle().getAngle());
			double b = sortedList.get(i).getValue() - (a * (double) sortedList.get(i).getAngle().getAngle());
			interpolationSegments.add(new PolynomSegment(sortedList.get(i - 1).getAngle().getAngle(), b, a));
		}
		if (lastIndex > 0) {
			double end_b = sortedList.get(lastIndex).getValue() - (overlapping_a * (double) sortedList.get(lastIndex).getAngle().getAngle());
			interpolationSegments.add(new PolynomSegment(sortedList.get(lastIndex).getAngle().getAngle(), end_b, overlapping_a));
		}
		return true;
	}


	public double getValue(long angle) {
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i != interpolationSegments.size() - 1) {
				if (angle >= interpolationSegments.get(i).getStart() && angle < interpolationSegments.get(i + 1).getStart()) {
					return polynomFunctionValue(interpolationSegments.get(i), angle);
				}
			} else {
				// last segment
				return polynomFunctionValue(interpolationSegments.get(interpolationSegments.size() - 1), angle);
			}
		}
		return 0;
	}

	@Override
	public double getAverage() {
		double area = 0;
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i < interpolationSegments.size() - 1) {
				area += interpolationSegments.get(i).getIntegralWithOffsetIs0(interpolationSegments.get(i + 1).getStart());
			} else {
				area += interpolationSegments.get(i).getIntegralWithOffsetIs0(Long.MAX_VALUE); // Little difference; better would be Long.MAX_Value + 1 = 2^63
			}
			area -= interpolationSegments.get(i).getIntegralWithOffsetIs0(interpolationSegments.get(i).getStart()); 
		}
		
		return (area / Math.pow(2, 64));
	}

	public int getIndex() {
		//0 is linear and periodic
		return 0;
	}
}
