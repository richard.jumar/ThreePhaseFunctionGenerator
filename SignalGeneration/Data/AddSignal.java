package Data;

/**
 * Signal created by addition of two Signals
 */
public class AddSignal extends Signal {
	private Signal s0;
	private Signal s1;
	/**
	 * Constructor
	 * @param signal0
	 * @param signal1
	 */
	public AddSignal(Signal signal0, Signal signal1) {
		this.s0 = signal0;
		this.s1 = signal1;
	}

	public void updateSignal0(Signal signal0) {
		this.s0 = signal0;
	}
	
	public void updateSignal1(Signal signal1) {
		this.s1 = signal1;
	}
	
	/**
	 * 3 phase values at the current time
	 */
	public double[] getValue() {
		double[] value = {s0.getValue()[0] + s1.getValue()[0], s0.getValue()[1] + s1.getValue()[1], s0.getValue()[2] + s1.getValue()[2]}; 
		return value;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		this.s0.replacePhasor(oldPhasor, newPhasor);
		this.s1.replacePhasor(oldPhasor, newPhasor);		
	}
	
	public Signal getChild0() {
		return this.s0;
	}
	
	public Signal getChild1() {
		return this.s1;
	}

	@Override
	public double[] getCachedValue(int index) {
		double[] value = {s0.getCachedValue(index)[0] + s1.getCachedValue(index)[0], s0.getCachedValue(index)[1] + s1.getCachedValue(index)[1], s0.getCachedValue(index)[2] + s1.getCachedValue(index)[2]}; 
		return value;
	}

	@Override
	public void swapCaches() {
		this.s0.swapCaches();
		this.s1.swapCaches();
	}
}
