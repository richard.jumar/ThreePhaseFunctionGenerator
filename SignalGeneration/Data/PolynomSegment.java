package Data;
import java.util.List;
import java.util.ArrayList;

/**
 * segment consisting out of one polynomial
 */
public class PolynomSegment implements Comparable<PolynomSegment> {
	
	private List<Double> parameters;
	private long start;
	private boolean signed;
	
	public PolynomSegment(long start, List<Double> parameters) {
		this.start = start;
		this.parameters = parameters;
	}
	
	public PolynomSegment(long start, double order0, double order1) {
		this.parameters = new ArrayList<Double>();
		this.start = start;
		parameters.add(order0);
		parameters.add(order1);
	}
	
	public PolynomSegment(long start, double[] parameters) {
		this.parameters = new ArrayList<Double>();
		this.start = start;
		for (int i = 0; i < parameters.length; i++) {
			this.parameters.add(parameters[i]);
		}
	}

	public void setSigned(boolean signed) {
		this.signed = signed;
	}
	
	public List<Double> getParameters() {
		return this.parameters;
	}
	
	public long getStart() {
		return this.start;
	}
	
	
	public void setStart(long start) {
		this.start = start;
	}
	
	/**
	 * integral over the polynom from start to the given upper limit
	 * @param to upper limit
	 * @return
	 */
	public synchronized double getIntegralFromStartTo(long to) {
		double area = 0;
		for (int i = 0; i < this.parameters.size(); i++) {
			area += (1d / ((double) i + 1d)) * this.parameters.get(i) * Math.pow(to, i + 1);
			area -= 1d / ((double) i + 1d) * this.parameters.get(i) * Math.pow(this.start, i + 1);
		}
		return area;
	}
	
	public boolean addParameter(int exponent, double factor) {
		if (exponent >= parameters.size()) {
			while (parameters.size() != exponent) { //start index is 0, so highest index in case size == exponent is exponent - 1.
				parameters.add(0d);
			}
			parameters.add(factor);
			return true;
		} else {
			if (parameters.get(exponent) == 0) {
				parameters.set(exponent, factor);
				return true;
			} else {
				return false;
			}
		}
	}
	
	public double getIntegralWithOffsetIs0(long position) {
		double position_double = (double) position;
		double value = 0;
		for (int i = 0; i < parameters.size(); i++) {
			value += parameters.get(i) * Math.pow(position_double, i + 1) / (i + 1);
		}
		return value;
	}
	public int compareTo(PolynomSegment ps) {
		if (signed) {
			//version: From -180� to 180�
			if (this.start > ps.getStart()) {
				return 1;
			} else if (this.start == ps.getStart()) {
				return 0;
			} else {
				return -1;
			}
		} else {
			//version: From 0� to 360�
			if (this.start == ps.getStart()) {
				return 0;
			}
			if (this.start < 0 && ps.getStart() >= 0) {
				return 1;
			} else if (this.start >= 0 && ps.getStart() < 0) {
				return -1;
			} else {
				if (this.start > ps.getStart()) {
					return 1;
				} else {
					return -1; // case '==' is not possible, this case has been caught before.
				}
			}
		}
	}
	
	public void shiftThis(double rotation) {
		this.parameters = shift(rotation);
	}
	
	public List<Double> shift (double rotation) {
		List<Double> newPolynom = new ArrayList<Double>();
		for (int i = 0; i < this.parameters.size(); i++) {
			newPolynom.add(0d);
		}
		for (int i = 0; i < this.parameters.size(); i++) {
			int[] pascal = pascal(i);
			for (int j = 0; j < pascal.length; j++) {
				double paramJ = this.parameters.get(i) * pascal[j] * Math.pow(-rotation, i - j);
				newPolynom.set(j, paramJ + newPolynom.get(j));
			}
		}
		return newPolynom;
	}
	
	public static int[] pascal (int exponent) {
		if (exponent < 0) {
			return null;
		}
		int[] pascal = new int[exponent + 1];
		for (int i = 0; i <= exponent; i++) {
			pascal[i] = 0;
		}
		for (int i = 0; i <= exponent; i++) {
			pascal[0] = 1;
			int before = 1;
			for (int j = 1; j <= i; j++) {
				int old = pascal[j];
				pascal[j] = pascal[j] + before;
				before = old;
			}
		}
		return pascal;
	}
}
