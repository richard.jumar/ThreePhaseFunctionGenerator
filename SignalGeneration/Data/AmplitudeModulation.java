package Data;

/**
 * Abstract class for all amplitude modulations
 */
public abstract class AmplitudeModulation implements DoubleValueFunction {
	public abstract double[] getValue();
	public abstract void replacePhasor(Phasor oldPhasor, Phasor newPhasor);
}
