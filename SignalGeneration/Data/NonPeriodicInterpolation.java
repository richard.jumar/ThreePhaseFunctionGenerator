package Data;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class NonPeriodicInterpolation extends Interpolation {
	protected List<NonPeriodicInterpolationPoint> sortedList;
	protected List<PolynomSegment> interpolationSegments;
	protected long startTime;
	protected long endTime;
	protected boolean zeroMean;
	protected double zeroLevel;
	public abstract int getIndex();
	
	public NonPeriodicInterpolation(long startTime, long endTime) {
		this.sortedList = new ArrayList<NonPeriodicInterpolationPoint>();
		this.zeroMean = true;
		this.startTime = startTime; 
		this.endTime = endTime;
	}
	
	public void addPoint(NonPeriodicInterpolationPoint p) {
		this.sortedList.add(p);
		Collections.sort(sortedList);
	}
	
	public void setZeroMeanMode() {
		this.zeroMean = true;
	}
	
	public void setAbsoluteZeroMode(double zeroLevel) {
		this.zeroMean = false;
		this.zeroLevel = zeroLevel;
	}
	
	
	public void setList(List<Point2D> points, int width) {
		this.sortedList = new ArrayList<NonPeriodicInterpolationPoint>();
		for (int i = 0; i < points.size(); i++) {
			this.sortedList.add(new NonPeriodicInterpolationPoint(Math.round(points.get(i).getX()), points.get(i).getY()));
		}

		Collections.sort(sortedList);
	}
	
	public double getValue(long microSeconds) {
		long functionTime = microSeconds - startTime;

		if (interpolationSegments.size() > 0 && (microSeconds < this.startTime || microSeconds > this.endTime)) { //before start or after end
			return 0;	
		}
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i != interpolationSegments.size() - 1) {
				if (functionTime >= interpolationSegments.get(i).getStart() && functionTime < interpolationSegments.get(i + 1).getStart()) {
					return polynomFunctionValue(interpolationSegments.get(i), functionTime);
				}
			} else {
				return polynomFunctionValue(interpolationSegments.get(interpolationSegments.size() - 1), functionTime);
			}
		}
		return 0; //case: No elements in list
	}
	
	public long getStartTime() {
		return this.startTime;
	}
	
	public long getEndTime() {
		return this.endTime;
	}
	
}
