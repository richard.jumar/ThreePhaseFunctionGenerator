package Data;

public class NonPeriodicAmplitudeModulationGain extends NonPeriodicAmplitudeModulation {

	private double[] gain;
	
	public NonPeriodicAmplitudeModulationGain(Clock clock) {
		super(clock);
		this.gain = new double[3];
		this.gain[0] = 1d;
		this.gain[1] = 1d;
		this.gain[2] = 1d;
	}

	public void setGain(double[] gain) {
		this.gain = gain;
	}
	
	public double[] getGain() {
		return this.gain;
	}
	
	public double[] getValue() {
		return gain;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		// no phasor => do nothing
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.gain;
	}

	@Override
	public void swapCaches() {
		//nothing to do
	}

}
