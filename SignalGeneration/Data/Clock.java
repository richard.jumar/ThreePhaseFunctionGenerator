package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Clock {
	private long time;
	private SamplingRate samplingRate;
	private List<Phasor> phasors;
	
	private int lastPipelineIterationLength;
	
	private long[] cache;
	private long[] cacheInProcess;
	private int beforeLastPipelineIterationLength;
	
	public Clock(SamplingRate samplingRate) {
		this.samplingRate = samplingRate;
		this.phasors = new ArrayList<Phasor>();
		this.reset();

	}
	
	/**
	 * reset the clock and all phasors to the initial state
	 */
	public void reset() {
		this.time = 0;
		for (int i = 0; i < phasors.size(); i++) {
			phasors.get(i).reset();
		}
		this.cache = new long[0];
		this.lastPipelineIterationLength = 0;
		this.beforeLastPipelineIterationLength = 0;
	}
	/**
	 * get a list with all phasors
	 * @return
	 */
	public List<Phasor> getPhasors() {
		return this.phasors;
	}
	
	/**
	 * get the cached time array
	 * @param index
	 * @return
	 */
	public long getCachedTime(int index) {
		return this.cache[index];
	}
	
	/**
	 * set the sampling rate of the soundcard
	 * @param samplingRate
	 */
	public void setSamplingRate(SamplingRate samplingRate) {
		this.samplingRate = samplingRate;
		for (int i = 0; i < this.phasors.size(); i++) {
			phasors.get(i).samplingRateUpdated();
		}
	}
	
	/**
	 * Get phasor by name
	 * @param name
	 * @return
	 */
	public Phasor getPhasor(String name) {
		for (int i = 0; i < phasors.size(); i++) {
			if (phasors.get(i).getName().equals(name)) {
				return phasors.get(i);
			}
		}
		return null;
	}
	
	/**
	 * increments the current time by one step. 
	 * The step size results from the sampling frequency
	 */
	public void incTime() {
		this.time++;
		for (int i = 0; i < phasors.size(); i++) {

			phasors.get(i).nextSample();

		}
	}
	
	/**
	 * increment clock without caching (only for the graphs)
	 */
	public void incTimeWithoutCaching() {
		this.time++;
		for (int i = 0; i < phasors.size(); i++) {

			phasors.get(i).nextSampleWithoutCaching();

		}
	}
	
	/**
	 * Do the time and phasor computations for one block 
	 * @param blockSize
	 */
	public void incBlock(int blockSize) {
		for (int i = 0; i < phasors.size(); i++) {
			phasors.get(i).nextCacheBlock(blockSize);
		}
		this.cacheInProcess = new long[blockSize];
		for (int i = 0; i < blockSize; i++) {
			this.cacheInProcess[i] = this.getMicroSeconds();
			incTime();
		}
	}
	
	/**
	 * get the time cache array
	 * @return
	 */
	public long[] getCache() {
		return this.cache;
	}
	
	/**
	 * get the block size of the last iteration
	 * @return
	 */
	public int getLastIterationLength() {
		return this.lastPipelineIterationLength;
	}
	
	/**
	 * get the block size of the iteration before the last iteration
	 * @return
	 */
	public int getNextToLastIterationLength() {
		return this.beforeLastPipelineIterationLength;
	}
	
	public void swapCaches() {
		this.beforeLastPipelineIterationLength = this.lastPipelineIterationLength;
		this.lastPipelineIterationLength = this.cache.length; //before cache has been overwritten. 
		this.cache = this.cacheInProcess;
		for (int i = 0; i < phasors.size(); i++) {
			phasors.get(i).swapCaches();
		}
		
	}
	
	/**
	 * Jump to the given time
	 * @param timeMicroSeconds
	 */
	public void goToTime(long timeMicroSeconds) {
		if (timeMicroSeconds >= 0) {
			if (getMicroSeconds() > timeMicroSeconds) {
				this.reset();
			}
			while (this.getMicroSeconds() < timeMicroSeconds) {
				incTime();
			}
		}
	}
	
	/**
	 * go to a start time without phasors (only for non periodic thumbnail computation)
	 * @param timeMicroSeconds
	 */
	public void goToTimeWithoutPhasorWithoutCaching(long timeMicroSeconds) {
		this.time = timeMicroSeconds * this.samplingRate.samplingRate_int / 1000000;
	}
	

	
	/**
	 * add a phasor to the clock. 
	 * The phasor will rotate with every clock increment
	 * The name of the phasor must not exist in the phasor list
	 * @param p
	 * @return phasor added successful (no name conflict)
	 */
	public boolean addPhasor(Phasor p) {
		if (p.getName().trim().equals("")) {
			return false; //empty string is not allowed
		}
		for (int i = 0; i < phasors.size(); i++) {
			if (phasors.get(i).getName().equals(p.getName())) {
				return false;
			}
		}
		if (this.samplingRate != null) {
			p.samplingRateUpdated();
		}
		phasors.add(p);
		Collections.sort(phasors);
		return true;
	}
	
	/**
	 * remove a phasor
	 * @param p
	 */
	public void removePhasor(Phasor p) {
		phasors.remove(p);
	}
	
	/**
	 * returns the time in format
	 * [0] hours
	 * [1] minutes
	 * [2] seconds
	 * [3] milliseconds
	 * @return
	 */
	public int[] getTimeArray() {
		int[] timeArray = new int[4];
		timeArray[3] = (int) (((time % samplingRate.samplingRate_int) * 1000) / samplingRate.samplingRate_int);
		timeArray[2] = (int) ((time / samplingRate.samplingRate_int) % 60);
		timeArray[1] = (int) ((time / (samplingRate.samplingRate_int * 60)) % 60);
		timeArray[0] = (int) ((time / (samplingRate.samplingRate_int * 3600)) % 60);
		return timeArray;
	}
	
	/**
	 * Get the current time in �s
	 * @return
	 */
	public long getMicroSeconds() {
		return ((this.time * 1000000) / this.samplingRate.samplingRate_int);
	}
	
	/**
	 * Print the current time in h, min, s, ms
	 */
	public void printTime() {
		int timeArray[] = getTimeArray();
		System.out.println(timeArray[0] + "h " + timeArray[1] + "min " + timeArray[2] + "s " + timeArray[3] + "ms");
	}
	
	/**
	 * Get the current time in steps
	 * @return
	 */
	public long getTime() {
		return this.time;
	}
	
	/**
	 * get the sampling rate
	 * @return
	 */
	public SamplingRate getSamplingRate() {
		return this.samplingRate;
	}
}
