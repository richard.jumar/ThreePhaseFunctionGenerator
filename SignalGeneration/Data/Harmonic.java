package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Harmonic wave
 */
public class Harmonic extends PeriodicSignal implements BasicFunction {
	private List<Wave> waves;
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	private double divisor = Math.pow(2, 63); //2^64 / 2 because pi is used instead of 2 pi. 
	private boolean run;
	
	
	
	/**
	 * Constructor
	 * @param amplitude in first order in V
	 * @param phasor
	 */
	public Harmonic(Phasor phasor) {
		super(phasor);
		waves = new ArrayList<Wave>();
	}
	
	/**
	 * run the computation in the pipeline
	 */
	public void run() {
		this.run = true;
		while (run) {			
			int cacheSize = this.phasor.getAngleCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				this.cacheInProcess[cacheIndex] = computeValue(phasor.getAngleCache()[cacheIndex]);
			}

			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * calculates the values for three given phase angles
	 * @param angle_long
	 * @return
	 */
	private double[] computeValue(long[] angle_long) {
		double[] value = new double[3];
		for (int i = 0; i < 3; i++) {
			value[i] = 0d;
			double angleRad = (((double) angle_long[i]) / divisor) * Math.PI;
			for (int j = 0; j < waves.size(); j++) {
				value[i] += Math.sin(waves.get(j).getOrder() * angleRad) * waves.get(j).getSinAmplitude();
				value[i] += Math.cos(waves.get(j).getOrder() * angleRad) * waves.get(j).getCosAmplitude();
			}
		}
		return value; 
	}
	
	/**
	 * returns the value at the current time
	 */
	public double[] getValue() {
		long[] angle_long = new long[3];
		angle_long[0] = this.phasor.getPhase1().getAngle();
		angle_long[1] = this.phasor.getPhase2().getAngle();
		angle_long[2] = this.phasor.getPhase3().getAngle();
		return computeValue(angle_long);
	}

	/**
	 * returns the phasor of this harmonic wave
	 */
	public Phasor getPhasor() {
		return this.phasor;
	}


	
	/**
	 * add a wave composed of a sinus and a cosinus component
	 * @param wave
	 * @return true if successful
	 */
	public boolean addWave(Wave wave) {
		if (wave.getOrder() > 0) {
			for (int i = 0; i < waves.size(); i++) {
				if (waves.get(i).getOrder() == wave.getOrder()) {
					return false;
				}
			}
			waves.add(wave);
			Collections.sort(waves);
			return true;
		}
		return false;
	}
	
	public void removeWave(int index) {
		waves.remove(index);
	}
	
	
	/**
	 * Get the list of waves
	 * @return
	 */
	public List<Wave> getWaves() {
		return this.waves;
	}

	/**
	 * replace a phasor
	 */
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		if (this.getPhasor() == oldPhasor) {
			setPhasor(newPhasor);
		}
		
	}

	@Override
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier= barrier;
	}

	@Override
	public double[] getCachedValue(int index) {
		return cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	@Override
	public void activateStop() {
		this.run = false;
	}
}
