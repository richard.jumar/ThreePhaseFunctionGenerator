package Data;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * Abstract class for point interpolations
 */
public abstract class Interpolation {
	public abstract boolean interpolate();
	public abstract void setList(List<Point2D> points, int width);
	
	/**
	 * Calculates the value of the given polynomial segment at the given position
	 * @param polynom polynomial segment
	 * @param x position
	 * @return
	 */
	protected double polynomFunctionValue(PolynomSegment polynom, long x) {
		double value = 0;
		for (int i = 0; i < polynom.getParameters().size(); i++) {
			value += polynom.getParameters().get(i).doubleValue() * Math.pow((double) x, i);
		}
		return value;
	}
	public abstract double getAverage();
	public abstract double getValue(long position);
}
