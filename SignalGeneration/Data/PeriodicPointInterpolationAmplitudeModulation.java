package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * amplitude modulation defined by a periodic point interpolation
 */
public class PeriodicPointInterpolationAmplitudeModulation extends PeriodicAmplitudeModulation implements BasicFunction {
	private List<PeriodicInterpolationPoint> points;
	private PeriodicInterpolation interpolation;
	private CyclicBarrier barrier;
	private boolean run;

	private double[][] cache;
	private double[][] cacheInProcess;
	
	public PeriodicPointInterpolationAmplitudeModulation(Phasor p) {
		super(p);
		points = new ArrayList<PeriodicInterpolationPoint>();
		interpolation = new PeriodicLinearInterpolation(); //change it with setInterpolation(...);
	}

	public void clearPoints() {
		while (points.size() > 0) {
			points.remove(0);
		}
	}
	
	public void setPointList(List<PeriodicInterpolationPoint> points) {
		this.points = points;
		Collections.sort(this.points);
	}
	
	public void addPoint(PeriodicInterpolationPoint point) {
		points.add(point);
		Collections.sort(points);
	}
	
	public boolean interpolate() {
		interpolation.sortedList = points;
		return interpolation.interpolate();
	}
	
	public void setInterpolation(PeriodicInterpolation interpolation) {
		this.interpolation = interpolation;
	}
	
	public int getInterpolytionTypeNumber() {
		return this.interpolation.getIndex();
	}
	
	
	
	public double[] getValue() {
		double[] value = new double[3];
		value[0] = interpolation.getValue(phasor.getPhase1().getAngle());
		value[1] = interpolation.getValue(phasor.getPhase2().getAngle());
		value[2] = interpolation.getValue(phasor.getPhase3().getAngle());
		return value;
	}

	public List<PeriodicInterpolationPoint> getPointList() {
		return this.points;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		if (this.getPhasor() == oldPhasor) {
			setPhasor(newPhasor);
		}
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = this.phasor.getAngleCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				this.cacheInProcess[cacheIndex][0] = interpolation.getValue(phasor.getAngleCache()[cacheIndex][0]);
				this.cacheInProcess[cacheIndex][1] = interpolation.getValue(phasor.getAngleCache()[cacheIndex][1]);
				this.cacheInProcess[cacheIndex][2] = interpolation.getValue(phasor.getAngleCache()[cacheIndex][2]);
			}
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	public void setBarrier(CyclicBarrier barrier) {
		this.barrier= barrier;
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
		
	}

	@Override
	public void activateStop() {
		this.run = false;
	}

}
