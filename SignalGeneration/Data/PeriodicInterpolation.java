package Data;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import GUI.DisplayFunctions;

/**
 * periodic interpolation
 */
public abstract class PeriodicInterpolation extends Interpolation {
	protected List<PeriodicInterpolationPoint> sortedList;
	public abstract int getIndex();

	public abstract double getAverage();
	
	public PeriodicInterpolation() {
		this.sortedList = new ArrayList<PeriodicInterpolationPoint>();
	}
	
	/**
	 * Adds a point to the list of points for the interpolation
	 * @param p 
	 */
	public void addPoint(PeriodicInterpolationPoint p) {
		this.sortedList.add(p);
		Collections.sort(sortedList);
	}
	
	public void setList(List<Point2D> points, int width) {
		this.sortedList = new ArrayList<PeriodicInterpolationPoint>();
		for (int i = 0; i < points.size(); i++) {
			this.sortedList.add(generatePeriodicInterpolationPoint(points.get(i), width));
		}

		Collections.sort(sortedList);
	}
	
	public PeriodicInterpolationPoint generatePeriodicInterpolationPoint(Point2D p, int width) {
		Angle angle = DisplayFunctions.periodicDisplayXToAngle((int) p.getX(), width);
		PeriodicInterpolationPoint pip = new PeriodicInterpolationPoint(angle, p.getY());
		return pip;
	}

}
