package Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Periodic interpolation with cubic splines
 */
public class PeriodicSplineInterpolation extends PeriodicInterpolation {
	private List<PolynomSegment> interpolationSegments;

	public PeriodicSplineInterpolation() {
		this.interpolationSegments = new ArrayList<PolynomSegment>();
	}
	
	/**
	 * returns the mean value of the interpolation function
	 */
	public double getAverage() {
		double area = 0;
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i < interpolationSegments.size() - 1) {
				area += interpolationSegments.get(i).getIntegralWithOffsetIs0(interpolationSegments.get(i + 1).getStart());
			} else {
				area += interpolationSegments.get(i).getIntegralWithOffsetIs0(Long.MAX_VALUE); // Little difference; better would be Long.MAX_Value + 1 = 2^63
			}
			area -= interpolationSegments.get(i).getIntegralWithOffsetIs0(interpolationSegments.get(i).getStart()); 
		}
		
		return (area / Math.pow(2, 64));
	}
	
	/**
	 * create a cubic spline interpolation
	 */
	public boolean interpolate() {
		int size = sortedList.size();
		if (size < 3) {
			//not enough elements
			return false;
		} else {
			//distance vector h. Distance h_n is the distance in x-direction between the points n and n-1.			
			double[] h = new double[size];
			h[0] = ((double) sortedList.get(0).getAngle().getAngle() - (double) Long.MIN_VALUE) + 1d + ((double) Long.MAX_VALUE - (double) sortedList.get(size - 1).getAngle().getAngle());
			for (int i = 1; i < sortedList.size(); i++) {
				h[i] = (double) sortedList.get(i).getAngle().getAngle() - (double) sortedList.get(i - 1).getAngle().getAngle();
			}

			double[][] matrix = new double[size][size];
			double[] vector = new double[size]; 
			double[] moments = new double[size];
			//init matrix
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					matrix[i][j] = 0;
				}
				
			}
			//fill matrix
			for (int i = 0; i < size; i++) {
				matrix[i][i] = 2 * (h[i] + h[mod(i + 1, size)]); //modulo: h[0] in the last iteration
				matrix[i][mod((i - 1), size)] = h[i];
				matrix[i][mod((i + 1), size)] = h[mod(i + 1, size)];
			}
			//fill vector
			for (int i = 0; i < size; i++) {
				vector[i] = 6 * (sortedList.get(mod(i+1, size)).getValue() - sortedList.get(i).getValue()) / h[mod(i + 1, size)] - 6 * (sortedList.get(i).getValue() - sortedList.get(mod(i-1,size)).getValue()) / h[i];
			}
			
			moments = MatrixMath.solve(matrix, vector);

			//spline parameters
			double[][] parameters = new double[size + 1][4]; 
			parameters[0][0] = 0;
			parameters[0][1] = 0;
			parameters[0][2] = 0;
			parameters[0][3] = 0;

			for (int i = 0; i < size + 1; i++) {
				//construct equations
				double[][] em = new double[4][4];
				double[] res = new double[4];
				//s(x) = d +cx + bx^2 + ax^3
				//s''(x) = 2b +6ax
				double leftAngle;

				if (i != 0) {
					leftAngle = sortedList.get(i - 1).getAngle().getAngle();
				} else {
					leftAngle = ((double) Long.MIN_VALUE) - 1 - (((double) Long.MAX_VALUE) - sortedList.get(size - 1).getAngle().getAngle()); 
				}
				double rightAngle;
				if (i < size) {
					rightAngle = sortedList.get(i).getAngle().getAngle();
				} else {
					rightAngle = ((double) sortedList.get(0).getAngle().getAngle() - (double) Long.MIN_VALUE) + 1 + (double) Long.MAX_VALUE;
				}
			
				//s(left point)
				em[0][0] = 1; //1*d
				em[0][1] = leftAngle; //x * c
				em[0][2] = Math.pow(leftAngle, 2); //x^2 * b
				em[0][3] = Math.pow(leftAngle, 3); //x^3 * a
				res[0] = sortedList.get(mod(i - 1, size)).getValue();

				//s(right point)
				em[1][0] = 1; //1*d
				em[1][1] = rightAngle; //x * c
				em[1][2] = Math.pow(rightAngle, 2); //x^2 * b
				em[1][3] = Math.pow(rightAngle, 3); //x^3 * a
				res[1] = sortedList.get(mod(i, size)).getValue();
				
				//s''(left point)
				em[2][0] = 0; //0 * d
				em[2][1] = 0; //0 * c
				em[2][2] = 2; //2 *  b
				em[2][3] = 6 * leftAngle; //6 * x * a
				res[2] = moments[mod(i - 1, size)];
				
				//s''(right point)
				em[3][0] = 0; //0 * d
				em[3][1] = 0; //0 * c
				em[3][2] = 2; //2 * b
				em[3][3] = 6 * rightAngle; //6 * x * a
				res[3] = moments[mod(i, size)];
				
				parameters[i] = MatrixMath.solve(em, res);
			}
			
			interpolationSegments.add(new PolynomSegment(Long.MIN_VALUE, parameters[0]));
			for (int i = 1; i <= size; i++) {
				interpolationSegments.add(new PolynomSegment(sortedList.get(i - 1).getAngle().getAngle(), parameters[i]));
			}

			return true;
		}
	}

	/**
	 * modulo for negative numbers: mod(-1, 5) = 4
	 * @param number
	 * @param modulo
	 * @return
	 */
	private int mod(int number, int modulo) {
		while (number < 0) {
			number += modulo;
		}
		return (number%modulo);
	}
	@Override
	public double getValue(long angle) {
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i < interpolationSegments.size() - 1) {
				if (angle >= interpolationSegments.get(i).getStart() && angle < interpolationSegments.get(i + 1).getStart()) {
					return polynomFunctionValue(interpolationSegments.get(i), angle);
				}
			} else {
				// last segment
				return polynomFunctionValue(interpolationSegments.get(interpolationSegments.size() - 1), angle);

			}
		}
		return 0;
	}
	

	public int getIndex() {
		// 1 is cubic spline and periodic
		return 1;
	}



}
