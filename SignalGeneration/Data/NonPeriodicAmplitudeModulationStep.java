package Data;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class NonPeriodicAmplitudeModulationStep extends NonPeriodicAmplitudeModulation implements BasicFunction{

	private long[] up; //step time in �s
	private double[] start; //start values
	private double[] end; //end values
	private boolean run;
	
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	
	/**
	 * Amplitude modulation with start and end value and a step between, synchronous in all phases
	 * @param clock
	 * @param up time of the step in �s
	 * @param start start value
	 * @param end end value
	 */
	public NonPeriodicAmplitudeModulationStep(Clock clock, long up, double start, double end) {
		super(clock);
		this.up = new long[3];
		this.up[0] = up;
		this.up[1] = up;
		this.up[2] = up;
		this.start = new double[3];
		this.start[0] = start;
		this.start[1] = start;
		this.start[2] = start;
		this.end = new double[3];
		this.end[0] = end;
		this.end[1] = end;
		this.end[2] = end;
	}

	/**
	 * Amplitude modulation with start and end value and a step between, different in all phases
	 * @param clock
	 * @param up time of the step in �s (three-dimensional)
	 * @param start start value (three-dimensional)
	 * @param end end value (three-dimensional)
	 */
	public NonPeriodicAmplitudeModulationStep(Clock clock, long[] up, double[] start, double[] end) {
		super(clock);
		this.up = up; 
		this.start = start;
		this.end = end;
	}
	
	/**
	 * values at the current time.
	 */
	public double[] getValue() {
		double value[] = new double[3];
		if (this.clock.getMicroSeconds() < this.up[0]) {
			value[0] = start[0];
		} else {
			value[0] = end[0];
		}
		if (this.clock.getMicroSeconds() < this.up[1]) {
			value[1] = start[1];
		} else {
			value[1] = end[1];
		}
		if (this.clock.getMicroSeconds() < this.up[2]) {
			value[2] = start[2];
		} else {
			value[2] = end[2];
		}
		return value;
	}

	
	public boolean isSymmetric() {
		return (up[0] == up[1] && up[0] == up[2] && start[0] == start[1] && start[0] == start[2] && end[0] == end[1] && end[0] == end[2]);
	}
	
	public long[] getStepTime() {
		return this.up;
	}
	
	public double[] getStartValue() {
		return this.start;
	}
	
	public double[] getEndValue() {
		return this.end;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		//no phasor => do nothing
	}

	@Override
	public double[] getCachedValue(int index) {
		return cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = cacheInProcess;
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = 0;
			if (this.clock.getCache() == null) {
				cacheSize = 0;
			} else {
				cacheSize = this.clock.getCache().length;
			}
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				for (int i = 0; i < 3; i++) {
					if (clock.getCachedTime(cacheIndex) >= up[i]) {
						cacheInProcess[cacheIndex][i] = end[i];
					} else {
						cacheInProcess[cacheIndex][i] = start[i];
					}
				}				
			}
			
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public void activateStop() {
		this.run = false;
	}
}
