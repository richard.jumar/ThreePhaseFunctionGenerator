package Data;

public class NonPeriodicInterpolationPoint implements Comparable<NonPeriodicInterpolationPoint>{
	private long time; //in �s
	private double value;
	
	public NonPeriodicInterpolationPoint(long microseconds, double value) {
		this.time = microseconds;
		this.value = value;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int compareTo(NonPeriodicInterpolationPoint arg0) {
		return Long.compare(this.time, arg0.getTime());
	}
}
