package Data;

public interface Periodic {
	public Phasor getPhasor();
	public void setPhasor(Phasor phasor);
}
