package Data;
import java.util.ArrayList;
import java.util.List;

/**
 * Frequency
 */
public class Frequency {
	private long f0_phase1; //�Hz
	private long f0_phase2; //�Hz
	private long f0_phase3; //�Hz
	private List<FrequencyModulation> fMod;

	/**
	 * Constructor; same frequencies on every phase.
	 * @param f0 start frequency in �Hz
	 */
	public Frequency(long f0) {
		if (f0 < 0) {
			System.out.println("Warning: Frequency < 0 Hz");
			//TODO check effects
		}
		this.f0_phase1 = f0;
		this.f0_phase2 = f0;
		this.f0_phase3 = f0;
		fMod = new ArrayList<FrequencyModulation>();
	}
	
	/**
	 * Constructor; different frequencies on the phases
	 * @param f0_phase1
	 * @param f0_phase2
	 * @param f0_phase3
	 */
	public Frequency(long f0_phase1, long f0_phase2, long f0_phase3) {
		this.f0_phase1 = f0_phase1;
		this.f0_phase2 = f0_phase2;
		this.f0_phase3 = f0_phase3;
		fMod = new ArrayList<FrequencyModulation>();		
	}

	
	/**
	 * adds a frequency modulation to the list of frequency modulations
	 * the frequency modulation 
	 * @param fm frequency modulation (3 phases)
	 * @return 
	 */
	public boolean addFrequencyModulation(FrequencyModulation fm) {
		if (fm.gettMod1End() <= fm.gettMod1Start() || fm.gettMod2End() <= fm.gettMod2Start() || fm.gettMod3End() <= fm.gettMod3Start()) { //end <= start
			return false;
		}
		if (this.fMod.size() == 0) { //empty list
			this.fMod.add(fm);
			return true;
		} else {
			if (fm.gettMod1End() <= fMod.get(0).gettMod1Start()) { //new modulation comes first
				if (fm.gettMod2End() <= fMod.get(0).gettMod2Start() && fm.gettMod3End() <= fMod.get(0).gettMod3Start()) { //check phase 2 and 3
					this.fMod.add(0, fm); //insert at the first position
					return true;
				}
				return false; //overlap happend
			}
				
		}
		for (int i = 0; i < fMod.size(); i++) { //find the position to insert
			if (fm.gettMod1Start() >= fMod.get(i).gettMod1End()) { 
				if ((i + 1) < fMod.size()) { //...not with the last element in the list
					if (fm.gettMod1End() <= fMod.get(i + 1).gettMod1Start()) { //position found for phase 1
						if (fm.gettMod2Start() >= fMod.get(i).gettMod2End() && fm.gettMod2End() <= fMod.get(i + 1).gettMod2Start() && fm.gettMod3Start() >= fMod.get(i).gettMod3End() && fm.gettMod3End() <= fMod.get(i + 1).gettMod3Start()) {
							fMod.add(i + 1, fm);
							return true;
						} else {
							return false; //overlap happend
						}
					}					
				} else { //end of the list
					if (fm.gettMod2Start() >= fMod.get(i).gettMod2End() && fm.gettMod3Start() >= fMod.get(i).gettMod3End()) {
						fMod.add(fm);
						return true;
					} else { //overlap phase 2 or 3
						return false;
					}
				}
			}
		}
		return false;
	}
	
	public List<FrequencyModulation> getFrequencyModulationList() {
		return fMod;
	}
	
	/**
	 * Returns the average frequency between the given time and the time step before
	 * @param time time step
	 * @return
	 */
	public long[] getSampleFrequency(long time) {
		if (time < 0) {
			long[] ret = {0, 0, 0};
			return ret;
		}
		//find position in the list
		int position1 = -1;
		int position2 = -1;
		int position3 = -1;
		boolean constant1 = true;
		boolean constant2 = true;
		boolean constant3 = true;
		long f1;
		long f2;
		long f3;
		for (int i = 0; i < fMod.size(); i++) {
			if (time > fMod.get(i).gettMod1Start()) { //time is after beginning of this modulation (index of the last conform iteration saved). 
				position1 = i; //last started modulation
				if (time < fMod.get(position1).gettMod1End()) { //modulation is "active"
					constant1 = false;
				}
			}
			if (time > fMod.get(i).gettMod2Start()) { //time is after beginning of this modulation (index of the last conform iteration saved). 
				position2 = i; //last started modulation
				if (time < fMod.get(position2).gettMod2End()) { //modulation is "active"
					constant2 = false;
				}
			}
			if (time > fMod.get(i).gettMod3Start()) { //time is after beginning of this modulation (index of the last conform iteration saved). 
				position3 = i; //last started modulation
				if (time < fMod.get(position3).gettMod3End()) { //modulation is "active"
					constant3 = false;
				}
			}
		}
		if (constant1) { //constant case:
			if (position1 >= 0) {
				f1 = fMod.get(position1).getF1End(); 
			} else {
				f1 = this.f0_phase1;
			}
		} else {
			if (position1 > 0) {
				//"start" means "start of modulation", "end" end of modulation
				//frequency = f_start + rocof * (t - t_start)
				//= f_start + (f_end - f_start) * (t - t_start) / (t_end - t_start)
				f1 = fMod.get(position1 - 1).getF1End() + (long) ((((long) (time - fMod.get(position1).gettMod1Start())) * (long) (fMod.get(position1).getF1End()- fMod.get(position1 - 1).getF1End())) / (fMod.get(position1).gettMod1End() - fMod.get(position1).gettMod1Start()));
			} else {
				f1 = this.f0_phase1 + (long) ((((long) (time - fMod.get(position1).gettMod1Start())) * (long) (fMod.get(position1).getF1End() - this.f0_phase1)) / (fMod.get(position1).gettMod1End() - fMod.get(position1).gettMod1Start()));
			}
		}
		if (constant2) { //constant case:
			if (position2 >= 0) {
				f2 = fMod.get(position2).getF2End(); 
			} else {
				f2 = this.f0_phase2;
			}
		} else {
			if (position2 > 0) {
				//"start" means "start of modulation", "end" end of modulation
				//frequency = f_start + rocof * (t - t_start)
				//= f_start + (f_end - f_start) * (t - t_start) / (t_end - t_start)
				f2 = fMod.get(position2 - 1).getF2End() + (long) ((((long) (time - fMod.get(position2).gettMod2Start())) * (long) (fMod.get(position2).getF2End()- fMod.get(position2 - 1).getF2End())) / (fMod.get(position2).gettMod2End() - fMod.get(position2).gettMod2Start()));
			} else {
				f2 = this.f0_phase2 + (long) ((((long) (time - fMod.get(position2).gettMod2Start())) * (long) (fMod.get(position2).getF2End() - this.f0_phase2)) / (fMod.get(position2).gettMod2End() - fMod.get(position2).gettMod2Start()));
			}
		}
		if (constant3) { //constant case:
			if (position3 >= 0) {
				f3 = fMod.get(position3).getF3End(); 
			} else {
				f3 = this.f0_phase3;
			}
		} else {
			if (position3 > 0) {
				//"start" means "start of modulation", "end" end of modulation
				//frequency = f_start + rocof * (t - t_start)
				//= f_start + (f_end - f_start) * (t - t_start) / (t_end - t_start)
				f3 = fMod.get(position3 - 1).getF3End() + (long) ((((long) (time - fMod.get(position3).gettMod3Start())) * (long) (fMod.get(position3).getF3End()- fMod.get(position3 - 1).getF3End())) / (fMod.get(position3).gettMod3End() - fMod.get(position3).gettMod3Start()));
			} else {
				f3 = this.f0_phase3 + (long) ((((long) (time - fMod.get(position3).gettMod3Start())) * (long) (fMod.get(position3).getF3End() - this.f0_phase3)) / (fMod.get(position3).gettMod3End() - fMod.get(position3).gettMod3Start()));
			}
		}
		long frequencies[] = new long[3];
		frequencies[0] = f1;
		frequencies[1] = f2;
		frequencies[2] = f3;
		return frequencies;
	}
	
	/**
	 * Returns the remaining time to the next frequency modulation
	 * @param time current time in �s
	 * @return remaining time in �s
	 */
	public long remainingConstantMicroSeconds(long time) {
		long min = -1; //-1 means no modulation follows
		for (int i = 0; i < fMod.size(); i++) {
			long diff1 = fMod.get(i).gettMod1Start() - time;
			long diff2 = fMod.get(i).gettMod2Start() - time;
			long diff3 = fMod.get(i).gettMod3Start() - time;
			if (diff1 > 0) { //next modulation in phase 1 will come sometime
				if (diff1 < min || min < 0) { //smaller time to next modulation found
					if (i > 0 && fMod.get(i - 1).gettMod1End() > time) { //end of last modulation has been passed
						min = 0;
					} else {
						min = diff1;
					}
				}
			}
			if (diff2 > 0) { //next modulation in phase 2 will come sometime
				if (diff2 < min || min < 0) {
					if (i > 0 && fMod.get(i - 1).gettMod2End() > time) {
						min = 0;
					} else {
						min = diff2;
					}
				}
			}
			if (diff3 > 0) { //next modulation in phase 3 will come sometime
				if (diff3 < min || min < 0) {
					if (i > 0 && fMod.get(i - 1).gettMod3End() > time) {
						min = 0;
					} else {					
						min = diff3;
					}
				}
			} 
			if (i == fMod.size() - 1) { //last modulation has started...
				if (min == -1 && (fMod.get(i).gettMod1End() > time || fMod.get(i).gettMod2End() > time || fMod.get(i).gettMod3End() > time)) { //...but it has not finished yet
					min = 0;	
				}
			}
		}
		return min;
	}
	
	/**
	 * Converts the frequency in Hz from a string to a long in micro hertz
	 * @param arg0
	 * @return
	 */
	public static long stringHzTomicroHzLong(String arg0) {
		String[] parts = arg0.split(",");
		if (parts.length > 2) {
			return Long.MIN_VALUE;
		}
		long microHz;
		try {
			microHz = Long.parseLong(parts[0]) * 1000000;
		} catch (NumberFormatException e) {
			return Long.MIN_VALUE;
		}
		if (parts.length == 2) {
			try {
				int maxDecimals = parts[1].length();
				if (maxDecimals > 6) {
					maxDecimals = 6;
				}
				microHz += (Long.parseLong(parts[1].substring(0, maxDecimals)) * Math.round(Math.pow(10, (6 - maxDecimals)))); //cut after 6 decimals
			} catch (NumberFormatException e) {
				return Integer.MIN_VALUE;
			}
		}
		return microHz;
	}
	
	/**
	 * get the start frequency of phase 1
	 * @return
	 */
	public long getF0_phase1() {
		return this.f0_phase1;
	}
	
	/**
	 * Convert the frequency from a long in micro hertz to a string in hertz
	 * @param microHz
	 * @param decimals
	 * @return
	 */
	public static String microHz_valueToHzString(long microHz, int decimals) {		
		String f_string = microHz + "";
		if (f_string.length() < 7) {
			while(f_string.length() < 6) {
				f_string = "0" + f_string;
			}
			f_string = "0," + f_string.substring(0, decimals);
		} else {
			f_string = f_string.substring(0, f_string.length() - 6) + "," + f_string.substring(f_string.length() - 6, f_string.length() - 6 + decimals);
		}
		f_string += " Hz";
		return f_string;
	}
}
