package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * amplitude modulation of a non periodic point interpolation
 */
public class NonPeriodicPointInterpolationAmplitudeModulation extends NonPeriodicAmplitudeModulation implements BasicFunction {
	private List<NonPeriodicInterpolationPoint> points;
	private NonPeriodicInterpolation interpolation;
	private CyclicBarrier barrier;
	private boolean run;

	private double[][] cache;
	private double[][] cacheInProcess;
	
	/**
	 * constructor
	 * @param clock
	 * @param startTime
	 * @param endTime
	 */
	public NonPeriodicPointInterpolationAmplitudeModulation(Clock clock, long startTime, long endTime) {
		super(clock);
		this.points = new ArrayList<NonPeriodicInterpolationPoint>();
		this.interpolation = new NonPeriodicLinearInterpolation(startTime, endTime);
	}

	/**
	 * get the start time
	 * @return
	 */
	public long getStartTime() {
		return this.interpolation.getStartTime();
	}
	
	/**
	 * get the end time
	 * @return
	 */
	public long getEndTime() {
		return this.interpolation.getEndTime();
	}
	
	/**
	 * remove all points
	 */
	public void clearPoints() {
		while (points.size() > 0) {
			points.remove(0);
		}
	}
	
	/**
	 * set the list of points for the interpolation
	 * @param points
	 */
	public void setPointList(List<NonPeriodicInterpolationPoint> points) {
		this.points = points;
		Collections.sort(this.points);
	}
	
	/**
	 * get the point list
	 * @return
	 */
	public List<NonPeriodicInterpolationPoint> getPointList() {
		return this.points;
	}
	
	/**
	 * get the value for the current time
	 */
	public double[] getValue() {
		double value =  interpolation.getValue(this.clock.getMicroSeconds());
		double[] ret = {value, value, value};
		return ret;
	}
	
	/**
	 * add a point for the interpolation
	 * @param p point
	 */
	public void addPoint(NonPeriodicInterpolationPoint p) {
		points.add(p);
		Collections.sort(points);
	}
	
	/**
	 * compute the interpolation
	 * @return
	 */
	public boolean interpolate() {
		interpolation.sortedList = points;
		return interpolation.interpolate();
	}
	
	/**
	 * set the interpolation
	 * @param interpolation
	 */
	public void setInterpolation(NonPeriodicInterpolation interpolation) {
		this.interpolation = interpolation;
	}
	
	/**
	 * get the number of interpolation type
	 * 0 is linear
	 * 1 is cubic spline
	 * @return
	 */
	public int getInterpolytionTypeNumber() {
		return this.interpolation.getIndex();
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		// no phasor => do nothing		
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	/**
	 * computing am while run time
	 */
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize;
			if (this.clock.getCache() == null) {
				cacheSize = 0;
			} else {
				cacheSize = this.clock.getCache().length;
			}
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				double value =  interpolation.getValue(this.clock.getCache()[cacheIndex]);
				this.cacheInProcess[cacheIndex][0] = value;
				this.cacheInProcess[cacheIndex][1] = value;
				this.cacheInProcess[cacheIndex][2] = value;
			}
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * set the barrier
	 */
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
		
	}

	/**
	 * stop running after this iteration
	 */
	public void activateStop() {
		this.run = false; 
	}

}
