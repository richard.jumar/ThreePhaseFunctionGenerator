package Data;
import java.math.BigInteger;
public class Phasor implements Comparable<Phasor> {
	protected Angle phase1; //angle phase 1
	protected Angle phase2; //angle phase 2
	protected Angle phase3; //angle phase 3
	private Angle phase1Start; //angle phase 1
	private Angle phase2Start; //angle phase 2
	private Angle phase3Start; //angle phase 3
	private long[] increment;
	private Frequency frequency;
	private long remainingSamples; //remaining samples until increment changes
	private Clock clock;
	private String name;
	private BigInteger numberOfValues;
	private BigInteger divisor;
	
	private int cacheIndex; 
	private long[][] angleCache;
	private long[][] angleCacheInProcess;

	
	public Phasor(Clock clock, Frequency f) {
		this.clock = clock;
		phase1 = new Angle(0);
		phase2 = new Angle(Angle.DegreeToAngle(120));
		phase3 = new Angle(Angle.DegreeToAngle(240));
		phase1Start = new Angle(0);
		phase2Start = new Angle(Angle.DegreeToAngle(120));
		phase3Start = new Angle(Angle.DegreeToAngle(240));
		this.increment = new long[3];
		this.frequency = f;
		this.angleCache = new long[0][3];
		this.numberOfValues = BigInteger.valueOf(2).pow(64);
		reset();	
	}
	
	public void nextCacheBlock(int size) {
		this.angleCacheInProcess = new long[size][3];
		this.cacheIndex = 0;
	}
	
	public void swapCaches() {
		this.angleCache = angleCacheInProcess;
	}
	

	
	public long[][] getAngleCache() {
		return this.angleCache;
	}
	
	private void stateToCache() {
		this.angleCacheInProcess[this.cacheIndex][0] = phase1.getAngle();
		this.angleCacheInProcess[this.cacheIndex][1] = phase2.getAngle();
		this.angleCacheInProcess[this.cacheIndex][2] = phase3.getAngle();

		this.cacheIndex++;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}

	public Frequency getFrequency() {
		return this.frequency;
	}
	
	public void setFrequency(Frequency f) {
		this.frequency = f;
	}

	/**
	 * increments the phasor
	 * @param samplingRate
	 * @param frequency in �Hz
	 */
	public void setIncrement(long[] frequency) {
		for (int i = 0; i < 3; i++) {
			BigInteger f = BigInteger.valueOf(frequency[i]);
			BigInteger product = f.multiply(this.numberOfValues);
			BigInteger inc = product.divide(this.divisor);	
			this.increment[i] = inc.longValue();
			if (i == 0 && frequency[0] == frequency[1] && frequency[0] == frequency[2]) {
				this.increment[1] = this.increment[0];
				this.increment[2] = this.increment[0];
				i = 3;
			}
		}
	}
	
	public void setIncrement(long frequency) {
		long array[] = {frequency, frequency, frequency};
		setIncrement(array);
	}
	
	public void nextSample() {	
		
		stateToCache();
		nextSampleWithoutCaching();

	}
	
	public void samplingRateUpdated() {
		this.divisor = new BigInteger(clock.getSamplingRate().samplingRate_String).multiply(BigInteger.valueOf(1000000)); //1000000 because, parameter is in �Hz
	}

	public void nextSampleWithoutCaching() {

	
		//checkFrequency
		if (remainingSamples == 0) {
			//new incrementSize, frequency is in �Hz. 
			setIncrement(averageFrequency());
			long remainingTime = frequency.remainingConstantMicroSeconds(clock.getMicroSeconds());
			if (remainingTime > 0) {
				//calculate necessary sample number
				remainingSamples = (((long) clock.getSamplingRate().samplingRate_int) * remainingTime) / 1000000; 
			} else if (remainingTime == -1) {
				remainingSamples = -1; //forever constant frequency
			}
		} else if (remainingSamples > 0){
			remainingSamples--;
		}

		phase1.rotate(increment[0]);
		phase2.rotate(increment[1]);
		phase3.rotate(increment[2]);
	}

	
	public void setStartAngles(Angle phase1Start, Angle phase2Start, Angle phase3Start) {
		this.phase1Start = phase1Start;
		this.phase2Start = phase2Start;
		this.phase3Start = phase3Start;
	}
	
	public void reset() {
		phase1.setAngleLike(phase1Start);
		phase2.setAngleLike(phase2Start);
		phase3.setAngleLike(phase3Start);
		this.remainingSamples = 0;
	}
	
	
	public Angle getPhase1Start() {
		return phase1Start;
	}

	public Angle getPhase2Start() {
		return phase2Start;
	}

	public Angle getPhase3Start() {
		return phase3Start;
	}

	/**
	 * Returns the angle of phase 1
	 * @return
	 */
	public Angle getPhase1() {
		return phase1;
	}
	
	/**
	 * Returns the angle of phase 2
	 * @return
	 */
	public Angle getPhase2() {
		return phase2;
	}
	
	/**
	 * Returns the angle of phase 3
	 * @return
	 */
	public Angle getPhase3() {
		return phase3;
	}
	
	public Angle[] getPhases() {
		Angle[] phases = new Angle[3];
		phases[0] = phase1;
		phases[1] = phase2;
		phases[2] = phase3;
		return phases;
	}
	
	/**
	 * returns the average frequency from the last sampling point to the current sampling point
	 * @return
	 */
	private long[] averageFrequency() {
		return frequency.getSampleFrequency(clock.getMicroSeconds());
	}

	@Override
	public int compareTo(Phasor arg0) {
		return this.getName().compareTo(arg0.getName());
	}

}
