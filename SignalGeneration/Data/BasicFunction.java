package Data;

import java.util.concurrent.CyclicBarrier;

/**
 * interface for basic functions (signal or amplitude modulation). 
 * Basic functions are synchronized by a barrier
 */
public interface BasicFunction extends Runnable {
	public void setBarrier(CyclicBarrier barrier);
	/**
	 * stop this thread after reaching barrier. Call only from barrier!
	 */
	public void activateStop();
}
