package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * signal out of a piecewise defined function
 */
public class PeriodicPartwiseDefinedSignal extends PeriodicSignal implements BasicFunction {

	private List<PolynomSegment> polynoms;
	private PolynomSegment startPolynom;
	private List<Angle> startAngles;
	private boolean run;
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	
	public PeriodicPartwiseDefinedSignal(Phasor p) {
		super(p);
		this.polynoms = new ArrayList<PolynomSegment>();
	}
	
	/**
	 * Prepare for play
	 * sort polynomials and do the necessary computation for the overlapping polynomials
	 */
	public void setReadyToPlay() {
		for (int i = 0; i < polynoms.size(); i++) {
			this.polynoms.get(i).setSigned(true);
		}
		Collections.sort(this.polynoms);
		this.startAngles = new ArrayList<Angle>();
		for (int i = 0; i < polynoms.size(); i++) {
			this.startAngles.add(new Angle(polynoms.get(i).getStart()));
		}
		//generate startPolynom overlapping from the end
		if (this.polynoms.size() > 0 && this.polynoms.get(0).getStart() > Long.MIN_VALUE) {
			this.startPolynom = new PolynomSegment(Long.MIN_VALUE, this.polynoms.get(this.polynoms.size() - 1).shift(-Math.pow(2, 64)));
		}
	}
	
	/**
	 * Number of polynomial segments
	 * @return
	 */
	public int getNumberOfSegments() {
		return this.polynoms.size();
	}
	
	/**
	 * Removes a section 
	 * @param index in order from 0� to 360�!
	 */
	public void removePart0to360(int index) {
		if (index >= 0) {
			for (int i = 0; i < polynoms.size(); i++) {
				this.polynoms.get(i).setSigned(false);
			}
			Collections.sort(this.polynoms);
			polynoms.remove(index);
			this.setReadyToPlay();
		}
	}

	/**
	 * get the three phase value of this signal 
 	*/
	public double[] getValue() {
		return this.computeValue(this.phasor.getPhases());
		
	}
	
	/**
	 * compute a value for a given angle
	 * @param angle
	 * @return
	 */
	private double[] computeValue(Angle[] angle) {
		double[] value = new double[3];
		//choose segment
		for (int i = 0; i < 3; i++) {
			if (this.polynoms.size() > 0) {
				if (this.startAngles.get(0).getAngle() > angle[i].getAngle()) {
					double valueHere = 0;
					for (int k = 0; k < this.startPolynom.getParameters().size(); k++) {
						valueHere += this.startPolynom.getParameters().get(k) * Math.pow(this.phasor.getPhases()[i].getAngle(), k);
					}
					value[i] = valueHere;
				} else {
					for (int j = 0; j < polynoms.size() - 1; j++) {
						if (this.polynoms.get(j).getStart() <= angle[i].getAngle() && this.polynoms.get(j + 1).getStart() >= angle[i].getAngle()) {
							double valueHere = 0;
							for (int k = 0; k < this.polynoms.get(j).getParameters().size(); k++) {
								valueHere += this.polynoms.get(j).getParameters().get(k) * Math.pow(angle[i].getAngle(), k);
							}
							value[i] = valueHere;
						}
					}
					if (this.polynoms.get(polynoms.size() - 1).getStart() <= angle[i].getAngle()) {
						double valueHere = 0;
						for (int k = 0; k < this.polynoms.get(polynoms.size() - 1).getParameters().size(); k++) {
							valueHere += this.polynoms.get(polynoms.size() - 1).getParameters().get(k) * Math.pow(angle[i].getAngle(), k);
						}
						value[i] = valueHere;
					}
				}
			} else {
				value[i] = 0;
			}
		}
		return value;
	}

	/**
	 * set all polynomials of the piecewise defined function
	 * @param polynoms
	 */
	public void setPolynoms(List<PolynomSegment> polynoms) {
		this.polynoms = polynoms;

	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		if (this.getPhasor() == oldPhasor) {
			setPhasor(newPhasor);
		}
	}

	@Override
	public double[] getCachedValue(int index) {
		return cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {			
			int cacheSize = this.phasor.getAngleCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				Angle[] angle= new Angle[3];
				angle[0] = new Angle(phasor.getAngleCache()[cacheIndex][0]);
				angle[1] = new Angle(phasor.getAngleCache()[cacheIndex][1]);
				angle[2] = new Angle(phasor.getAngleCache()[cacheIndex][2]);
				this.cacheInProcess[cacheIndex] = computeValue(angle);
			}

			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * set the barrier
	 */
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
		
	}

	/**
	 * activate stop after this iteration
	 */
	public void activateStop() {
		this.run = false;
	}


}
