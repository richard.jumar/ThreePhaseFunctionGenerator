package Data;

/**
 * point for periodic interpolations
 */
public class PeriodicInterpolationPoint implements Comparable<PeriodicInterpolationPoint> {
	private Angle angle;
	private double value;
	private boolean signed; //false: Sort from 0� to 360�; true: Sort from -180� to 180�.
	
	public PeriodicInterpolationPoint(Angle angle, double value) {
		this.angle = angle;
		this.value = value;
		this.signed = true;
	}

	public Angle getAngle() {
		return angle;
	}

	public void setAngle(Angle angle) {
		this.angle = angle;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public void setSignedSort(boolean signed) {
		this.signed = signed;
	}
	
	
	
	public int compareTo(PeriodicInterpolationPoint o) {
		if (signed) {
			//version: From -180� to 180�
			if (this.angle.getAngle() > o.getAngle().getAngle()) {
				return 1;
			} else if (this.angle.getAngle() == o.getAngle().getAngle()) {
				return 0;
			} else {
				return -1;
			}
		} else {
			//version: From 0� to 360�
			if (this.angle.isGreaterThan(o.getAngle())) {
				return 1;
			} else if (this.angle.getAngle() == o.getAngle().getAngle()) {
				return 0;
			} else {
				return -1;
			}
		}
	}
	
}
