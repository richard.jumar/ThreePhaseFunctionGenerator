package Data;

public enum SamplingRate {
	samplingRate8000("8000", 8000, (byte) 1), samplingRate11025("11025", 11025, (byte) 2), samplingRate12000("12000", 12000, (byte) 3), samplingRate16000("16000", 16000, (byte) 4), samplingRate22050("22050", 22050, (byte) 5), samplingRate24000("24000", 24000, (byte) 6), samplingRate32000("32000", 32000, (byte) 7), samplingRate44100("44100", 44100, (byte) 8), samplingRate48000("48000", 48000, (byte) 9), samplingRate88200("88200", 88200, (byte) 10), samplingRate96000("96000", 96000, (byte) 11), samplingRate176400("176400", 176400, (byte) 12), samplingRate192000("192000", 192000, (byte) 13);
	public String samplingRate_String;
	public int samplingRate_int;
	public byte code;
	SamplingRate(String samplingRate_String, int samplingRate_int, byte code) {
		this.samplingRate_String = samplingRate_String;
		this.samplingRate_int = samplingRate_int;
		this.code = code; 
	}
}
