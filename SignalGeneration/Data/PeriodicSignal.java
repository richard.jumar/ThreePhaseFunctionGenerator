package Data;

/**
 * periodic signal
 */
public abstract class PeriodicSignal extends Signal implements Periodic {
	protected Phasor phasor;
	public Phasor getPhasor() {
		return this.phasor;
	}
	
	public void setPhasor(Phasor phasor) {
		this.phasor = phasor;
	}
	protected PeriodicSignal(Phasor p) {
		this.phasor = p;
	}
}
