package Data;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class PeriodicAmplitudeModulationStep extends PeriodicAmplitudeModulation implements BasicFunction {
	private double[] levelA;
	private double[] levelB;
	private Angle[] stepAB;
	private Angle[] stepBA;
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;
	private boolean run;

	public PeriodicAmplitudeModulationStep(Phasor p, double[] levelA, double[] levelB, Angle[] stepAB, Angle[] stepBA) {
		super(p);
		this.levelA = levelA;
		this.levelB = levelB;
		this.stepAB = stepAB;
		this.stepBA = stepBA;
		this.cache = new double[0][3];
	}
	
	public PeriodicAmplitudeModulationStep(Phasor p, double levelA, double levelB, Angle stepAB, Angle stepBA) {
		super(p);
		this.levelA = new double[3];
		this.levelA[0] = levelA;
		this.levelA[1] = levelA;
		this.levelA[2] = levelA;
		this.levelB = new double[3];
		this.levelB[0] = levelB;
		this.levelB[1] = levelB;
		this.levelB[2] = levelB;
		this.stepAB = new Angle[3];
		this.stepAB[0] = stepAB;
		this.stepAB[1] = stepAB;
		this.stepAB[2] = stepAB;
		this.stepBA = new Angle[3];
		this.stepBA[0] = stepBA;
		this.stepBA[1] = stepBA;
		this.stepBA[2] = stepBA;
		this.cache = new double[0][3];
	}

	
	@Override
	public double[] getValue() {
		long[] angles = new long[3];
		angles[0] = phasor.getPhase1().getAngle();
		angles[1] = phasor.getPhase2().getAngle();
		angles[2] = phasor.getPhase3().getAngle();
		return computeValue(angles);
	}


	/**
	 * calculate the value for the given angle
	 * @param angle
	 * @return
	 */
	public double[] computeValue(long[] angle) {
		double[] value = new double[3];
		for (int i = 0; i < 3; i++) {
			Angle thisAngle = new Angle(angle[i]);
			if (stepBA[i].isGreaterEqual(stepAB[i])) {
				if (stepBA[i].isGreaterThan(thisAngle) && stepAB[i].isLowerEqual(thisAngle)) {
					value[i] = levelB[i];
				} else {
					value[i] = levelA[i];
				}
			} else {
				if (stepAB[i].isGreaterThan(thisAngle) && stepBA[i].isLowerEqual(thisAngle)) {
					value[i] = levelA[i];
				} else {
					value[i] = levelB[i];
				}
			}
		}
		return value;
	}
	
	@Override
	public void setPhasor(Phasor phasor) {
		this.phasor = phasor;
	}
	
	public double[] getLevelA() {
		return this.levelA;
	}
	
	public double[] getLevelB() {
		return this.levelB;
	}
	
	public Angle[] getStepAB() {
		return this.stepAB;
	}
	
	public Angle[] getStepBA() {
		return this.stepBA;
	}

	public boolean isSymmetric() {
		return (levelA[0] == levelA[1] && levelA[0] == levelA[2] && levelB[0] == levelB[1] && levelB[0] == levelB[2] && stepAB[0].equals(stepAB[1]) && stepAB[0].equals(stepAB[2]) && stepBA[0].equals(stepBA[1]) && stepBA[0].equals(stepBA[2]));
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		if (this.getPhasor() == oldPhasor) {
			setPhasor(newPhasor);
		}
	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = this.phasor.getAngleCache().length;
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				cacheInProcess[cacheIndex] = computeValue(phasor.getAngleCache()[cacheIndex]);				
			}
			
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public void activateStop() {
		this.run = false;
	}
}
