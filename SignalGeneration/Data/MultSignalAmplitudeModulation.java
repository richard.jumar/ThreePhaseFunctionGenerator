package Data;

/**
 * Signal calculated by multiplying a signal with an amplitude modulation funciton
 */
public class MultSignalAmplitudeModulation extends Signal {

	private Signal s;
	private AmplitudeModulation am;
	/**
	 * Signal consisting of a signal and a multiplied amplitude modulation
	 * @param signal
	 * @param amplitudeModulation
	 */
	public MultSignalAmplitudeModulation(Signal signal, AmplitudeModulation amplitudeModulation) {
		this.s = signal;
		this.am = amplitudeModulation;
	}

	public void update(Signal s, AmplitudeModulation am) {
		this.s = s;
		this.am = am; 
	}
	
	/**
	 * 3 phase value at current time
	 */
	public double[] getValue() {
		double[] value = {s.getValue()[0] * am.getValue()[0], s.getValue()[1] * am.getValue()[1], s.getValue()[2] * am.getValue()[2]}; 
		return value;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		this.s.replacePhasor(oldPhasor, newPhasor);
		this.am.replacePhasor(oldPhasor, newPhasor);		
	}
	
	public Signal getSignalChild() {
		return s;
	}
	
	public AmplitudeModulation getAmplitudeModulationChild() {
		return am;
	}

	@Override
	public double[] getCachedValue(int index) {		
		double[] value = {s.getCachedValue(index)[0] * am.getCachedValue(index)[0], s.getCachedValue(index)[1] * am.getCachedValue(index)[1], s.getCachedValue(index)[2] * am.getCachedValue(index)[2]}; 
		return value;
	}

	@Override
	public void swapCaches() {
		this.s.swapCaches();
		this.am.swapCaches();		
	}

}
