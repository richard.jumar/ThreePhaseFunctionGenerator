package Data;

/**
 * abstract class for all signals
 */
public abstract class Signal implements DoubleValueFunction {
	protected Clock clock;
	public abstract double[] getValue();

	public abstract void replacePhasor(Phasor oldPhasor, Phasor newPhasor);
}
