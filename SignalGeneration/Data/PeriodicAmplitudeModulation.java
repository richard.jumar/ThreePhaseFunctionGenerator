package Data;

public abstract class PeriodicAmplitudeModulation extends AmplitudeModulation implements Periodic {
	protected Phasor phasor;
	@Override
	public Phasor getPhasor() {
		return this.phasor;
	}
	
	public void setPhasor(Phasor phasor) {
		this.phasor = phasor;
	}
	
	protected PeriodicAmplitudeModulation(Phasor p) {
		this.phasor = p;
	}
}
