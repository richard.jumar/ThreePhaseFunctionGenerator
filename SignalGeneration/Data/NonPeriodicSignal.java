package Data;

public abstract class NonPeriodicSignal extends Signal {
	protected Clock clock;
	
	protected NonPeriodicSignal(Clock clock) {
		this.clock = clock;
	}
}
