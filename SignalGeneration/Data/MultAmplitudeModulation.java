package Data;

public class MultAmplitudeModulation extends AmplitudeModulation {

	private AmplitudeModulation am0;
	private AmplitudeModulation am1;
	public MultAmplitudeModulation(AmplitudeModulation am0, AmplitudeModulation am1) {
		this.am0 = am0;
		this.am1 = am1;
	}

	/**
	 * returns the value at the current time
	 */
	public double[] getValue() {
		double[] value = {am0.getValue()[0] * am1.getValue()[0], am0.getValue()[1] * am1.getValue()[1], am0.getValue()[2] * am1.getValue()[2]}; 
		return value;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		this.am0.replacePhasor(oldPhasor, newPhasor);
		this.am1.replacePhasor(oldPhasor, newPhasor);		
	}

	public AmplitudeModulation getChild0() {
		return am0;
	}
	
	public AmplitudeModulation getChild1() {
		return am1;
	}

	@Override
	public double[] getCachedValue(int index) {
		double[] value = {am0.getCachedValue(index)[0] * am1.getCachedValue(index)[0], am0.getCachedValue(index)[1] * am1.getCachedValue(index)[1], am0.getCachedValue(index)[2] * am1.getCachedValue(index)[2]}; 
		return value;
	}

	@Override
	public void swapCaches() {
		this.am0.swapCaches();
		this.am1.swapCaches();
	}
}
