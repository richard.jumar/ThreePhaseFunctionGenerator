package Data;

import java.util.ArrayList;
import java.util.Collections;
import GUI.Canvas;

/**
 * Non periodic interpolation with cubic splines
 */

public class NonPeriodicSplineInterpolation extends NonPeriodicInterpolation {

	public NonPeriodicSplineInterpolation(long startTime, long endTime) {
		super(startTime, endTime);
		this.interpolationSegments = new ArrayList<PolynomSegment>();
	}

	/**
	 * spline calculation
	 */
	@Override
	public boolean interpolate() {
		int size = sortedList.size();
		if (size < 4) {
			Canvas.canvasLog("Not enough points for a non periodic cubic spline interpolation (" + size + ")");
			return false;
		} else {
	
			//distance vector h. Distance h_n is the distance in x-direction between the points n and n+1 (!).
			
			double[] h = new double[size - 1];
			for (int i = 0; i < size - 1; i++) {
				h[i] = (double) sortedList.get(i + 1).getTime() - (double) sortedList.get(i).getTime();
			}
			
			double[][] matrix = new double[size - 2][size - 2];
			double[] vector = new double[size - 2]; 
			double[] moments = new double[size - 2];
			//init matrix
			for (int i = 0; i < size - 2; i++) {
				for (int j = 0; j < size - 2; j++) {
					matrix[i][j] = 0;
				}
				
			}
			//fill matrix
			for (int i = 0; i < size - 2; i++) {
				matrix[i][i] = 2 * (h[i] + h[i + 1]); //modulo: h[0] in the last iteration
				if (i > 0) {
					matrix[i][i - 1] = h[i];
					matrix[i - 1][i] = h[i];
				}
			}
			//fill vector
			for (int i = 0; i < size - 2; i++) {
				vector[i] = 6 * (sortedList.get(i + 2).getValue() - sortedList.get(i + 1).getValue()) / h[i + 1] - 6 * (sortedList.get(i + 1).getValue() - sortedList.get(i).getValue()) / h[i];
			}
			
			moments = MatrixMath.solve(matrix, vector);

			//spline parameters
			double[][] parameters = new double[size - 3][4]; 
			

			for (int i = 0; i < size - 3; i++) {
				//construct equations
				double[][] em = new double[4][4];
				double[] res = new double[4];
				//s(x) = d +cx + bx^2 + ax^3
				//s''(x) = 2b +6ax
				double left = sortedList.get(i + 1).getTime();
				double right = sortedList.get(i + 2).getTime();
				
				//s(left point)
				em[0][0] = 1; //1*d
				em[0][1] = left; //x * c
				em[0][2] = Math.pow(left, 2); //x^2 * b
				em[0][3] = Math.pow(left, 3); //x^3 * a
				res[0] = sortedList.get(i + 1).getValue();

				//s(right point)
				em[1][0] = 1; //1*d
				em[1][1] = right; //x * c
				em[1][2] = Math.pow(right, 2); //x^2 * b
				em[1][3] = Math.pow(right, 3); //x^3 * a
				res[1] = sortedList.get(i + 2).getValue();
				
				//s''(left point)
				em[2][0] = 0; //0 * d
				em[2][1] = 0; //0 * c
				em[2][2] = 2; //2 *  b
				em[2][3] = 6 * left; //6 * x * a
				res[2] = moments[i];
				
				//s''(right point)
				em[3][0] = 0; //0 * d
				em[3][1] = 0; //0 * c
				em[3][2] = 2; //2 * b
				em[3][3] = 6 * right; //6 * x * a
				res[3] = moments[i + 1];
				
				parameters[i] = MatrixMath.solve(em, res); //0: d, 1: c, 2: b, 3: a
			}
			
			
			for (int i = 0; i < size - 3; i++) {
				interpolationSegments.add(new PolynomSegment(sortedList.get(i + 1).getTime(), parameters[i]));
			}
			Collections.sort(interpolationSegments);
			long leftBorder_time;
			long rightBorder_time;
			if (this.startTime == -1) {
				leftBorder_time = this.startTime;
				rightBorder_time = this.endTime;
			} else {
				leftBorder_time = 0;
				rightBorder_time = this.endTime - this.startTime;
			}
			double derivation1left = 3 * parameters[0][3] * Math.pow(sortedList.get(1).getTime(), 2) + 2 * parameters[0][2] * sortedList.get(1).getTime() + parameters[0][1]; 
			double derivation1right = 3 * parameters[size - 4][3] * Math.pow(sortedList.get(size - 2).getTime(), 2) + 2 * parameters[size - 4][2] * sortedList.get(size - 2).getTime() + parameters[size - 4][1];
			/*
			 * calculate outer splines (2 on the left side and 2 right side) for a mean of zero
			 * form Ax=b
			 * x vector: a_0, b_0, c_0, d_0, a_1, b_1, c_1, d_1, a_{n-2}, b_{n-2}, c_{n-2}, d_{n-2}, a_{n-1}, b_{n-1}, c_{n-1}, d_{n-1}			
			*/
			
			matrix = new double[16][16];
			double[] res = new double[16];
			for (int i = 0; i < 16; i++) {
				for (int j = 0; j < 16; j++) {
					matrix[i][j] = 0;
				}
			}
			//I
			matrix[0][4] = Math.pow(sortedList.get(1).getTime(), 3); //x_2^3 * a_1
			matrix[0][5] = Math.pow(sortedList.get(1).getTime(), 2); //x_2^2 * b_1
			matrix[0][6] = sortedList.get(1).getTime(); //x_2 * c_1
			matrix[0][7] = 1; //d_1
			res[0] = sortedList.get(1).getValue();
			
			//II
			matrix[1][4] = 3 * Math.pow(sortedList.get(1).getTime(), 2); //3 * x_2^2 * a_1
			matrix[1][5] = 2 * sortedList.get(1).getTime(); //2 * x_2 * b_1
			matrix[1][6] = 1; //c_1
			res[1] = derivation1left;
			
			//III
			matrix[2][4] = 6 * sortedList.get(1).getTime(); //6 * x_2 * a_1
			matrix[2][5] = 2; //2 * b_1
			res[2] = moments[0];
			
			//IV
			matrix[3][4] = Math.pow(sortedList.get(0).getTime(), 3); //x_1^3 * a_1
			matrix[3][5] = Math.pow(sortedList.get(0).getTime(), 2); //x_1^2 * b_1
			matrix[3][6] = sortedList.get(0).getTime(); //x_1 * c_1
			matrix[3][7] = 1; //d_1
			res[3] = sortedList.get(0).getValue();
			
			//V			
			matrix[4][0] = Math.pow(sortedList.get(0).getTime(), 3); //x_1^3 * a_0
			matrix[4][1] = Math.pow(sortedList.get(0).getTime(), 2); //x_1^2 * b_0
			matrix[4][2] = sortedList.get(0).getTime(); //x_1 * c_0
			matrix[4][3] = 1; //d_0
			res[4] = sortedList.get(0).getValue();
			
			
			//VI
			matrix[5][0] = 3 * Math.pow(sortedList.get(0).getTime(), 2); //3 * x_1^2 * a_0;
			matrix[5][1] = 2 * sortedList.get(0).getTime(); // 2 * x_1 * b_0
			matrix[5][2] = 1; //c_0
			matrix[5][4] = -3 * Math.pow(sortedList.get(0).getTime(), 2); //-3 * x_1^2 * a_1;
			matrix[5][5] = - 2 * sortedList.get(0).getTime(); //-2 * x_1 * b_1
			matrix[5][6] = -1; //-c_0
			res[5] = 0;
			
			//VII
			matrix[6][0] = 6 * sortedList.get(0).getTime(); //6 * x_1 * a_0
			matrix[6][1] = 2; // 2 * b_0
			matrix[6][4] = -6 * sortedList.get(0).getTime(); // -6 * x_1 * a_1
			matrix[6][5] = -2; //-2 * b_1
			res[6] = 0;
			
			//VIII
			matrix[7][8] = Math.pow(sortedList.get(size - 2).getTime(), 3); //x_{n-2}^3 * a_{n-2}
			matrix[7][9] = Math.pow(sortedList.get(size - 2).getTime(), 2); //x_{n-2}^2 * b_{n-2}
			matrix[7][10] = sortedList.get(size -2).getTime(); //x_{n-2} * c_{n-2}
			matrix[7][11] = 1; //d_{n-2}
			res[7] = sortedList.get(size - 2).getValue();
			
			//IX
			matrix[8][8] = 3 * Math.pow(sortedList.get(size - 2).getTime(), 2); //3 * x_{n-2}^2 * a_{n-2}
			matrix[8][9] = 2 * sortedList.get(size - 2).getTime(); // 2 * x_{n-2} * b_{n-2}
			matrix[8][10] = 1; //c_{n-2}
			res[8] = derivation1right;
			
			//X
			matrix[9][8] = 6 * sortedList.get(size - 2).getTime(); // 6 * x_{n-2} * a_{n-2}
			matrix[9][9] = 2; // 2 * b_{n-2}
			res[9] = moments[size - 3];
			
			//XI
			matrix[10][8] = Math.pow(sortedList.get(size - 1).getTime(), 3); //x_{n-1}^3 * a_{n-2}
			matrix[10][9] = Math.pow(sortedList.get(size - 1).getTime(), 2); //x_{n-1}^2 * b_{n-2}
			matrix[10][10] = sortedList.get(size - 1).getTime(); //x_{n-1} * c_{n-2}
			matrix[10][11] = 1; //d_{n-2}
			res[10] = sortedList.get(size - 1).getValue();
			
			//XII
			matrix[11][12] = Math.pow(sortedList.get(size - 1).getTime(), 3); //x_{n-1}^3 * a_{n-1}
			matrix[11][13] = Math.pow(sortedList.get(size - 1).getTime(), 2); //x_{n-1}^2 * b_{n-1}
			matrix[11][14] = sortedList.get(size - 1).getTime(); //x_{n-1} * c_{n-1}
			matrix[11][15] = 1; //d_{n-1}
			res[11] = sortedList.get(size - 1).getValue();
			
			//XIII
			matrix[12][8] = -3 * Math.pow(sortedList.get(size - 1).getTime(), 2); //-3 * x_{n-1}^2 * a_{n-2}
			matrix[12][9] = -2 * sortedList.get(size - 1).getTime(); //-2 * x_{n-1} * b_{n-2}
			matrix[12][10] = -1; //-c_{n-2}
			matrix[12][12] = 3 * Math.pow(sortedList.get(size - 1).getTime(), 2); //3 * x_{n-1}^2 * a_{n-1}
			matrix[12][13] = 2 * sortedList.get(size - 1).getTime(); //2 * x_{n-1} * b_{n-2}
			matrix[12][14] = 1; //c_{n-1}
			res[12] = 0;
			
			//XIV
			matrix[13][8] = -6 * sortedList.get(size - 1).getTime(); //-6 * x_{n-1} * a_{n-2}
			matrix[13][9] = -2; //-2 * b_{n-2}
			matrix[13][12] = 6 * sortedList.get(size - 1).getTime(); //6 * x_{n-1} * a_{n-1}
			matrix[13][13] = 2; // 2 * b_{n-1}
			res[13] = 0;
			
			//XV
			matrix[14][0] = Math.pow(leftBorder_time, 3); //x_0^3 * a_0
			matrix[14][1] = Math.pow(leftBorder_time, 2); //x_0^2 * b_0
			matrix[14][2] = leftBorder_time; //x_0 * c_0
			matrix[14][3] = 1; //d_0
			matrix[14][12] = -Math.pow(rightBorder_time, 3); //x_n^3 * a_{n-1}
			matrix[14][13] = -Math.pow(rightBorder_time, 2); //x_n^2 * b_{n-1}
			matrix[14][14] = -rightBorder_time; //x_n * c_{n-1}
			matrix[14][15] = -1; //d_{n-1}
			res[14] = 0;
			
			//XVI
			if (zeroMean) {
				long l = rightBorder_time - leftBorder_time;
				double area_existing_interpolationSegments = 0;
				for (int i = 0; i < interpolationSegments.size(); i++) {
					area_existing_interpolationSegments += interpolationSegments.get(i).getIntegralFromStartTo(sortedList.get(i + 2).getTime());
				}

				
				matrix[15][0] = 1d / (4d * l) * Math.pow(sortedList.get(0).getTime(), 4) - 1d / (4d * l) * Math.pow(leftBorder_time, 4) - Math.pow(leftBorder_time, 3); //(1/(4l) * x_1^4 - 1/(4l) * x_0^4 - x_0^3) * a_0
				matrix[15][1] = 1d / (3d * l) * Math.pow(sortedList.get(0).getTime(), 3) - 1d / (3d * l) * Math.pow(leftBorder_time, 3) - Math.pow(leftBorder_time, 2); //(1/(3l) * x_1^3 - 1/(3l) * x_0^3- x_0^2) * b_0 
				matrix[15][2] = 1d / (2d * l) * Math.pow(sortedList.get(0).getTime(), 2) - 1d / (2d * l) * Math.pow(leftBorder_time, 2) - leftBorder_time; //(1/(2l) * x_1^2 - 1/(2l) * x_0^2 - x_0) * c_0
				matrix[15][3] = 1d / l * sortedList.get(0).getTime() - 1d / l * leftBorder_time - 1; //(1/l * x_1 - 1/l * x_0 - 1) * d_0
				matrix[15][4] = 1d / (4d * l) * Math.pow(sortedList.get(1).getTime(), 4) - 1d / (4d * l) * Math.pow(sortedList.get(0).getTime(), 4); //(1/(4l) * x_2^4 - 1/(4l) * x_1^4) * a_1
				matrix[15][5] = 1d / (3d * l) * Math.pow(sortedList.get(1).getTime(), 3) - 1d / (3d * l) * Math.pow(sortedList.get(0).getTime(), 3); //(1/(3l) * x_2^3 - 1/(3l) * x_1^3) * b_1 
				matrix[15][6] = 1d / (2d * l) * Math.pow(sortedList.get(1).getTime(), 2) - 1d / (2d * l) * Math.pow(sortedList.get(0).getTime(), 2); //(1/(2l) * x_2^2 - 1/(2l) * x_1^2) * c_1
				matrix[15][7] = 1d / l * sortedList.get(1).getTime() - 1d / l * sortedList.get(0).getTime(); //(1/l * x_2 - 1/l * x_1) * d_1
				matrix[15][8] = 1d / (4d * l) * Math.pow(sortedList.get(size - 1).getTime(), 4) - 1d / (4d * l) * Math.pow(sortedList.get(size - 2).getTime(), 4); //(1/(4l) * x_{n-1}^4 - 1/(4l) * x_{n-2}^4) * a_{n-2}
				matrix[15][9] = 1d / (3d * l) * Math.pow(sortedList.get(size - 1).getTime(), 3) - 1d / (3d * l) * Math.pow(sortedList.get(size - 2).getTime(), 3); //(1/(3l) * x_{n-1}^3 - 1/(3l) * x_{n-2}^3) * b_{n-2}
				matrix[15][10] = 1d / (2d * l) * Math.pow(sortedList.get(size - 1).getTime(), 2) - 1d / (2d * l) * Math.pow(sortedList.get(size - 2).getTime(), 2); //(1/(2l) * x_{n-1}^2 - 1/(2l) * x_{n-2}^2) * c_{n-2}
				matrix[15][11] = 1d / l * sortedList.get(size - 1).getTime() - 1d / l * sortedList.get(size - 2).getTime(); // (1/l * x_{n-1} - 1/l * x_{n-2}) * d_{n-2}
				matrix[15][12] = 1d / (4d * l) * Math.pow(rightBorder_time, 4) - 1d / (4d * l) * Math.pow(sortedList.get(size - 1).getTime(), 4); //(1/(4l) * x_n^4 - 1/(4l) * x_{n-1}^4) * a_{n-1}
				matrix[15][13] = 1d / (3d * l) * Math.pow(rightBorder_time, 3) - 1d / (3d * l) * Math.pow(sortedList.get(size - 1).getTime(), 3); //(1/(3l) * x_n^3 - 1/(3l) * x_{n-1}^3) * b_{n-1}
				matrix[15][14] = 1d / (2d * l) * Math.pow(rightBorder_time, 2) - 1d / (2d * l) * Math.pow(sortedList.get(size - 1).getTime(), 2); //(1/(2l) * x_n^2 - 1/(2l) * x_{n-1}^2) * c_{n-1}
				matrix[15][15] = 1d / l * rightBorder_time - 1d / l * sortedList.get(size - 1).getTime(); // (1/l * x_n - 1/l * x_{n-1}) * d_{n-1}
				res[15] = - area_existing_interpolationSegments / l;
			} else {
				matrix[15][0] = Math.pow(leftBorder_time, 3);
				matrix[15][1] = Math.pow(leftBorder_time, 2);
				matrix[15][2] = leftBorder_time;
				matrix[15][3] = 1;
				res[15] = zeroLevel;
			}
			double[] connectionSplinesParameters = new double[16];
			connectionSplinesParameters = MatrixMath.solve(matrix, res);
			
			for (int i = 0; i < 4; i++) {
				long startTime; 
				if (i == 0) {
					startTime = leftBorder_time;
				} else if (i == 1){
					startTime = sortedList.get(0).getTime();
				} else if (i == 2) {
					startTime = sortedList.get(size - 2).getTime();
				} else { //i==3
					startTime = sortedList.get(size - 1).getTime(); 
				}
				double[] params = new double[4];
				for (int j = i * 4; j < (i + 1) * 4; j++) {
					params[3 - (j % 4)] = connectionSplinesParameters[j];
				}
				interpolationSegments.add(new PolynomSegment(startTime, params));
			}
			
			
			
			for (int i = 0; i < interpolationSegments.size(); i++) {
				interpolationSegments.get(i).setSigned(true);
			}
			Collections.sort(interpolationSegments);
			
			return true;
		}
		
	}

	@Override
	public int getIndex() {
		return 2;
	}



	@Override
	public double getAverage() {
		double area = 0;
		if (interpolationSegments.size() == 0) {
			return 0;
		}
		for (int i = 0; i < interpolationSegments.size(); i++) {
			if (i < interpolationSegments.size() - 1) {
				area += interpolationSegments.get(i).getIntegralFromStartTo(interpolationSegments.get(i + 1).getStart());
			} else {
				area += interpolationSegments.get(i).getIntegralFromStartTo(this.endTime);
			}
		}
		return area / (this.endTime - this.interpolationSegments.get(0).getStart());
	}

}
