package Data;

public class Wave implements Comparable<Wave> {
	private int order;
	private double cosAmplitude;
	private double sinAmplitude;
	
	public Wave(int order, double sinAmplitude, double cosAmplitude) {
		this.order = order;
		this.cosAmplitude = cosAmplitude;
		this.sinAmplitude = sinAmplitude;
	}

	public int getOrder() {
		return this.order;
	}

	@Override
	public int compareTo(Wave arg0) {
		return Integer.compare(this.getOrder(), arg0.getOrder());
	}

	public double getCosAmplitude() {
		return this.cosAmplitude;
	}
	
	public double getSinAmplitude() {
		return this.sinAmplitude;
	}
	
	public void updateWave(int order, double sinAmplitude, double cosAmplitude) {
		this.order = order;
		this.cosAmplitude = cosAmplitude;
		this.sinAmplitude = sinAmplitude;
	}
}
