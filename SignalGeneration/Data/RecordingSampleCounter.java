package Data;

import Player.RecordedSignalBuffer;

public class RecordingSampleCounter {
	private double steps;
	private double offset;
	private RecordedSignalBuffer buffer;
	private boolean active;
	private int maxSamplesPerClockIncrement;
	
	public RecordingSampleCounter(int frequency, int clockFrequency, RecordedSignalBuffer buffer) {
		this.steps = ((double) frequency) / ((double) clockFrequency);
		this.offset = 0;
		this.buffer = buffer;
		this.maxSamplesPerClockIncrement = (int) Math.ceil(((double)frequency) / ((double)clockFrequency));
	}
	
	public void clockImpulse() {
		if (active) {
			this.offset += steps;
			while (this.offset >= 1) {
				this.offset -= 1;
				buffer.pull();
			}
		}
	}
	
	public double getOffset() {
		return this.offset;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}

	public int getMaxSamplesPerClockIncrement() {
		return this.maxSamplesPerClockIncrement;
	}
}
