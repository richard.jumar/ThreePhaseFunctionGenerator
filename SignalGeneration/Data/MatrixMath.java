package Data;

/**
 * Matrix operations including multiplying and solving of Ax=b equations 
 */
public class MatrixMath {
	private static int findPivot(double[][] matrix, int col) {
		double maxAbs = 0; 
		int pivot = col;
		for (int i = col; i < matrix.length; i++) {
			if (matrix[i][col] > 0) {
				if (matrix[i][col] > maxAbs) {
					maxAbs = matrix[i][col];
					pivot = i;
				}
			} else {
				if ((0 - matrix[i][col]) > maxAbs) {
					maxAbs = 0 - matrix[i][col];
					pivot = i;
				}
			}
		}
		return pivot;
	}
	
	//matrix-object is not cloned!
	private static double[][] swapLines(double[][] matrix, int line0, int line1) {
		for (int i = 0; i < matrix[0].length; i++) {
			double tmp = matrix[line0][i];
			matrix[line0][i] = matrix[line1][i];
			matrix[line1][i] = tmp;
		}
		return matrix;
	}
	
	/**
	 * Solve Ax=b with L R decomposition (including pivot procedure)
	 * @param A matrix
	 * @param b solution vector
	 * @return x
	 */
	public static double[] solveWithPivot(double[][] A, double[] b) {
		if (A.length == A[0].length) {
			double[][] R = A.clone();
			double[][] L = new double[R.length][R.length];
			double[][] P = new double[R.length][R.length];
			
			for (int i = 0; i < L.length; i++) {
				for (int j = 0; j < L.length; j++) {
					if (i == j) {
						P[i][j] = 1;
					} else {
						P[i][j] = 0;
					}
					L[i][j] = 0;
				}
			}

			for (int col = 0; col < L.length - 1; col++) {
				int pivot = findPivot(R, col);
				if (pivot > col) {
					R = swapLines(R, col, pivot);
					L = swapLines(L, col, pivot);
					P = swapLines(P, col, pivot);
				}
				for (int row = col + 1; row < L.length; row++) {
					double factor = (-1) * R[row][col] / R[col][col]; 
					for (int col2 = 0; col2 < L.length; col2++) {
						if (col2 < col) {
							R[row][col2] = 0;
						} else {
							R[row][col2] = R[row][col2] + factor * R[col][col2];  
						}
					}
					L[row][col] = factor * (-1);
				}
			}
			
			for (int i = 0; i < L.length; i++) {
				L[i][i] = 1;
			}

			
			//solve Ly = Pb
			double[] Pb = mult(P, b);
			double[] y = new double[L.length];
			for (int row = 0; row < L.length; row++) {
				y[row] = Pb[row];				
				for (int col = 0; col < row; col++) {
					y[row] -= L[row][col] * y[col];
				}
			}
			
			//solve Rx = y
			double[] x = new double[R.length];
			for (int row = R.length - 1; row >= 0; row--) {
				x[row] = y[row];
				for (int col = row + 1; col < R.length; col++) {
					x[row] -= R[row][col] * x[col];
				}
				x[row] = x[row] / R[row][row];
			}
			return x;
			
		}
		return null;	
	}
	
	private static double[] mult(double[][] matrix, double[] vector) {
		double[] res = new double[matrix.length];
		for (int i = 0; i < res.length; i++) {
			double value = 0;
			for (int index = 0; index < matrix[0].length; index ++) {
				value += matrix[i][index] * vector[index];
			}
			res[i] = value;
		}
		return res;
	}

	/**
	 * Solve Ax=b with L R decomposition (including pivot procedure)
	 * @param A matrix
	 * @param b solution vector
	 * @return x
	 */
	public static double[] solve(double[][] A, double[] b) {
		return solveWithPivot(A, b);
	}
	
	public static double[][] mult(double[][] matrix0, double[][] matrix1) {
		double[][] res = new double[matrix0.length][matrix1[0].length];
		for (int i = 0; i < res.length; i++) {
			for (int j = 0; j < res[0].length; j++) {
				double value = 0;
				for (int index = 0; index < matrix0[0].length; index ++) {
					value += matrix0[i][index] * matrix1[index][j];
				}
				res[i][j] = value;
			}
		}
		return res;
	}
	
	public static void printMatrix(double[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			String line = "";
			for (int j = 0; j < matrix[0].length; j++) {
				line += matrix[i][j] + " ";
			}
			System.out.println(line);
		}
	}
}
