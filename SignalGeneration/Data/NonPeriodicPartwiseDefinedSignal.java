package Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * signal out of a piecewise defined function (nonperiodic)
 */
public class NonPeriodicPartwiseDefinedSignal extends NonPeriodicSignal implements BasicFunction {

	private List<PolynomSegment> polynoms;
	private long startTime;
	
	private CyclicBarrier barrier;
	private double[][] cache;
	private double[][] cacheInProcess;	
	
	private boolean run;
	
	/**
	 * constructor
	 * @param c
	 */
	public NonPeriodicPartwiseDefinedSignal(Clock c) {
		super(c);
		this.polynoms = new ArrayList<PolynomSegment>();
		this.startTime = 0;
	}

	/**
	 * sorts the polynomials before playing
	 */
	public void setReadyToPlay() {
		for (int i = 0; i < polynoms.size(); i++) {
			this.polynoms.get(i).setSigned(true);
		}
		Collections.sort(this.polynoms);
	}
	
	/**
	 * Number of polynomial segments
	 * @return
	 */
	public int getNumberOfSegments() {
		return this.polynoms.size();
	}
	
	/**
	 * Removes a section 
	 * @param index in order from 0� to 360�!
	 */
	public void removePart(int index) {
		if (index >= 0) {
			for (int i = 0; i < polynoms.size(); i++) {
				this.polynoms.get(i).setSigned(true);
			}
			Collections.sort(this.polynoms);
			polynoms.remove(index);
			this.setReadyToPlay();
		}
	}
	
	/**
	 * Set the startTime (0 reference for the polygons)
	 * @param startTime
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	/**
	 * returns the start time of the piecewise defined signal (0-reference for the polygons)
	 * @return
	 */
	public long getStartTime() {
		return this.startTime;
	}
	
	/**
	 * computes the value for a given time
	 * @param time
	 * @return
	 */
	private double[] computeValue(long time) {
		double[] value = new double[3];
		//choose segment
		if (this.polynoms.size() > 0) {
			if (this.polynoms.get(0).getStart() > time) { //time is before the first segment started
				value[0] = 0;
				value[1] = 0;
				value[2] = 0;
			} else {
				for (int j = 0; j < polynoms.size() - 1; j++) {
					if (this.polynoms.get(j).getStart() <= time && this.polynoms.get(j + 1).getStart() >= time) {
						double valueHere = 0;
						for (int k = 0; k < this.polynoms.get(j).getParameters().size(); k++) {
							valueHere += this.polynoms.get(j).getParameters().get(k) * Math.pow(time - this.polynoms.get(j).getStart(), k);
						}
						value[0] = valueHere;
						value[1] = valueHere;
						value[2] = valueHere;
					}
				}
				if (this.polynoms.get(polynoms.size() - 1).getStart() <= time) { //last segment
					double valueHere = 0;
					for (int k = 0; k < this.polynoms.get(polynoms.size() - 1).getParameters().size(); k++) {
						valueHere += this.polynoms.get(polynoms.size() - 1).getParameters().get(k) * Math.pow(time - this.polynoms.get(polynoms.size() - 1).getStart(), k);
					}
					value[0] = valueHere;
					value[1] = valueHere;
					value[2] = valueHere;
				}					
			}
		} else {
			value[0] = 0;
			value[1] = 0;
			value[2] = 0;
		}
		return value;
	}
	
	/**
	 * get the current value
	 */
	public double[] getValue() {
		return computeValue(this.clock.getMicroSeconds());
	}

	/**
	 * set all polynomials
	 * @param polynoms polynomial list
	 */
	public void setPolynoms(List<PolynomSegment> polynoms) {
		this.polynoms = polynoms;

	}

	@Override
	public double[] getCachedValue(int index) {
		return this.cache[index];
	}

	@Override
	public void swapCaches() {
		this.cache = this.cacheInProcess;		
	}

	@Override
	public void run() {
		this.run = true;
		while (run) {
			int cacheSize = 0;
			if (this.clock.getCache() == null) {
				cacheSize = 0;
			} else {
				cacheSize = this.clock.getCache().length;
			}
			this.cacheInProcess = new double[cacheSize][3];
			for (int cacheIndex = 0; cacheIndex < cacheSize; cacheIndex++) {
				double value[] = computeValue(this.clock.getCache()[cacheIndex] - this.startTime);
				this.cacheInProcess[cacheIndex] = value;				
			}
			
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setBarrier(CyclicBarrier barrier) {
		this.barrier = barrier;
		
	}

	@Override
	public void activateStop() {
		this.run = false;
	}

	/**
	 * get the start time of the first segment
	 * @return
	 */
	public long getFirstSegmentStart() {
		if (this.polynoms.size() > 0) {
			return this.polynoms.get(0).getStart();
		} 
		return -1;
	}
	
	/**
	 * get the start time of the last segment
	 * @return
	 */
	public long getLastSegmentStart() {
		if (this.polynoms.size() > 0) {
			return this.polynoms.get(this.polynoms.size() - 1).getStart();
		}
		return -1;
	}

	@Override
	public void replacePhasor(Phasor oldPhasor, Phasor newPhasor) {
		//no phasor		
	}

}
