package GUI;

import java.io.File;

import Controller.FileControl;
import File.IniReaderWriter;

public class Main {
	private static File workspaceDir;
	private static MainWindow mainWindow;
	public static void main(String[] args) {
		LanguageTranslator.generateTranslations();
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-l") && args.length > i + 1) {
				Language l = Language.getLanguage(args[i + 1]);
				if (l != null) {
					LanguageTranslator.currentLanguage = l; 
				}
			}
		}
		FileControl.ini = new IniReaderWriter(new File("preferences.ini"));
		mainWindow = new MainWindow();
		mainWindow.setVisible(true);
	}
	
	public static void setWorkspaceDir(File workspaceDir) {
		Main.workspaceDir = workspaceDir;
	}
	
	public static File getWorkspaceDir() {
		return Main.workspaceDir;
	}

	public static MainWindow getMainwindow() {
		return mainWindow;
	}
	
}
