package GUI;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.BorderFactory;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Phasor;
import Data.Signal;

/**
 * Canvas box of the root element (type amplitude modulation)
 *
 */
public class CanvasBoxAmplitudeModulationOutput extends CanvasBox {
	private static final long serialVersionUID = 11L;
	private JLabel title;
	
	/**
	 * constructor
	 * @param canvas
	 * @param x x position
	 * @param y y position
	 */
	public CanvasBoxAmplitudeModulationOutput(Canvas canvas, int x, int y) {
		super(canvas);
		setInput(new CanvasConnector(canvas, this, ConnectorType.AMInput));
		this.output = null;
		this.setBounds(x, y, 120, 50);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		title = new JLabel(LanguageTranslator.getTranslation("Am.-Ausgabe"));
		title.setBounds(5, 10, 90, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(100, 1, 19, 19);
		this.add(status);
	}

	/**
	 * get the resulting amplitude modulation 
	 */
	public AmplitudeModulation getAmplitudeModulation() {
		return this.connectedWith.getTreeElement().getAmplitudeModulation();
	}
	
	/**
	 * get the parent node
	 */
	@Override
	public CanvasTreeElement getParentNode() {
		return null;
	}

	@Override
	public CanvasTreeElement getToplevelElementInTreeSegment() {
		return this;
	}


	/**
	 * number of free inputs
	 * @param type connector type
	 * @return
	 */
	public int getFreeInputs(ConnectorType type) {
		if (type == this.input.getType() && !this.input.isConnected()) {
			return 1;
		}
		return 0;
	}

	@Override
	public void resetSubTree(boolean startHere) {
		
	}

	@Override
	public boolean isVariable() { //only operators could be of an variable type
		return false;
	}

	/**
	 * get the connector which is connected with the given tree element
	 * @param te tree element
	 */
	public CanvasConnector getConnectorFor(CanvasTreeElement te) {
		return this.input;
	}

	/**
	 * amplitude modulation is full defined
	 */
	public boolean fullDefined() {
		if (this.connectedWith != null) {
			return this.connectedWith.getTreeElement().fullDefined();
		}
		return false;
	}

	@Override
	public Signal getSignal() {
		return null; //am is no signal
	}

	@Override
	public CanvasConnector getOutputConnector() {
		return null;
	}

	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true; //no phasor here.
	}
	
	public void updateSignalOrAmplitudeModulation() {
		DataControl.setTopLevelAmplitudeModulation(this.getAmplitudeModulation());
	}
}
