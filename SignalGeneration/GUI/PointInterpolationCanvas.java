package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import Data.Interpolation;
import Data.NonPeriodicLinearInterpolation;
import Data.NonPeriodicSplineInterpolation;
import Data.PeriodicLinearInterpolation;
import Data.PeriodicSplineInterpolation;

public class PointInterpolationCanvas extends JPanel {
	private static final long serialVersionUID = 46L;
	private List<Point2D> points;
	private Interpolation interpolation;
	private int LineMaxY;
	private int LineMinY;
	private int line0Y;
	private double meanValue;
	private CanvasBoxPointInterpolation box;
	private int interpolationType; // 0 linear, 1 cubic spline
	private boolean isPeriodic;
	private boolean isZeroMeanMode;
	private int zeroY;
	
	public PointInterpolationCanvas(CanvasBoxPointInterpolation box) {
		super();
		mouseListener();
		points = new ArrayList<Point2D>();
		this.box = box;
		this.interpolationType = 0;
		this.isPeriodic = true;
		this.isZeroMeanMode = true;
		this.zeroY = -1;
	}
	
	/**
	 * set periodic mode
	 * true -- periodic
	 * false -- nonperiodic 
	 * @param periodic 
	 */
	public void setPeriodic(boolean periodic) {
		this.isPeriodic = periodic;
	}
	
	/**
	 * get the list of the points from canvas
	 * @return
	 */
	public List<Point2D> getPoints() {
		return this.points;
	}
	
	/**
	 * add a point to the point interpolation canvas
	 * @param point
	 * @return
	 */
	public boolean addPoint(Point2D point) {
		for (int i = 0; i < points.size(); i++) {
			if (equals(points.get(i).getX(), point.getX())) {
				Canvas.canvasLog(LanguageTranslator.getTranslation("Ein Punkt mit diesem x-Wert ist bereits vorhanden."));
				return false;
			} 
		}
		this.points.add(point);
		reinterpolate();
		box.setLbl_pointsText(points.size() + " " + LanguageTranslator.getTranslation("Stützpunkte"));
		return true;
	}
	
	private boolean equals(double d0, double d1) {
		double diff = d0 - d1;
		if (diff < 0.00000001 && diff > -0.00000001) {
			return true;
		}
		return false;
	}
	
	/**
	 * set the interpolation type
	 * 0 linear
	 * 1 cubic splines
	 * @param type
	 */
	public void setInterpolationType(int type) {
		this.interpolationType = type;
	}
	
	/**
	 * get the interpolation type
	 * 0 linear
	 * 1 cubic splines
	 * @return
	 */
	public int getInterpolationType() {
		return this.interpolationType;
	}
	
	
	private void mouseListener() {
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				addPoint(arg0.getPoint());
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				
			}
			
		});
	}
	
	/**
	 * reinterpolate the points on the point interpolation canvas
	 */
	public void reinterpolate() {
		if (points.size() >= 3) {
			if (this.isPeriodic) {
				if (interpolationType == 0) {
					interpolation = new PeriodicLinearInterpolation();
				} else if (interpolationType == 1) {
					interpolation = new PeriodicSplineInterpolation();
				} 
			} else {
				if (interpolationType == 0) {
					interpolation = new NonPeriodicLinearInterpolation(-1, this.getWidth());
					if (!isZeroMeanMode) {
						((NonPeriodicLinearInterpolation) interpolation).setAbsoluteZeroMode(getZeroLinePosition());
					}
				} else if (interpolationType == 1) {
					interpolation = new NonPeriodicSplineInterpolation(-1, this.getWidth());
					if (!isZeroMeanMode) {
						((NonPeriodicSplineInterpolation) interpolation).setAbsoluteZeroMode(getZeroLinePosition());
					}
				} 
			}
			interpolation.setList(points, this.getWidth());
			interpolation.interpolate();			
			this.meanValue = interpolation.getAverage();
			line0Y = (int) Math.round(this.meanValue);
		}
		updateUI();
	}
	
	/**
	 * get the mean value from the point interpolation canvas
	 * @return
	 */
	public double getMeanValue() {
		return this.meanValue;
	}
	
	/**
	 * set the min max and zero lines
	 * @param min y poition min line
	 * @param max y position max line
	 * @param zeroLine y position zero line
	 */
	public void setLineParams(int min, int max, int zeroLine) {
		this.LineMinY = min;
		this.LineMaxY = max; 
		this.zeroY = zeroLine;
	}
	
	/**
	 * redraw the interpolation
	 */
	public void paintComponent(Graphics g) {		
		super.paintComponent(g);
		
		//grid
		int gridWidth = this.getWidth() / 36;
		g.setColor(Color.lightGray);
		for (int i = 0; i < this.getWidth() / gridWidth; i++) {
			g.drawLine(i * gridWidth, 0, i * gridWidth, this.getHeight() - 1);
		}
		for (int i = 0; i < this.getHeight() / gridWidth; i++) {
			g.drawLine(0, i * gridWidth, this.getWidth() - 1, i * gridWidth);
		}
		this.zeroY = (((this.getHeight() / gridWidth) + 1) / 2) * gridWidth;
		//cross symbols
		for (int i = 0; i < points.size(); i++) {
			g.setColor(Color.RED);
			g.drawLine((int) (points.get(i).getX() - 5), ((int) points.get(i).getY()- 5), (int) (points.get(i).getX() + 5), (int) (points.get(i).getY() + 5));
			g.drawLine((int) (points.get(i).getX() + 5), ((int) points.get(i).getY()- 5), (int) (points.get(i).getX() - 5), (int) (points.get(i).getY() + 5));
		}
		if (interpolation != null) {
			g.setColor(Color.BLACK);			
			int lastValue;
			if (this.isPeriodic ) {
				lastValue = (int) interpolation.getValue(DisplayFunctions.periodicDisplayXToAngle(0, this.getWidth()).getAngle());
			} else {
				lastValue = (int) interpolation.getValue(0);
			}
			int min = lastValue; 
			int max = lastValue;
			for (int i = 1; i < this.getWidth() - 1; i++) {
				int value;
				if (this.isPeriodic) {
					value = (int) interpolation.getValue(DisplayFunctions.periodicDisplayXToAngle(i, this.getWidth()).getAngle());
				} else {
					value = (int) interpolation.getValue(i);
				}
				g.drawLine(i, lastValue, i + 1, value);
				lastValue = value;
				if (value < max) { //the y-axis is upside down
					max = value;
				}
				if (value > min) {
					min = value;
				}
			}


			this.LineMaxY = max;
			this.LineMinY = min;
			g.setColor(Color.BLUE);
			if (max >= 0 && max < this.getHeight()) {
				g.drawLine(0, max, this.getWidth() - 1, max);
			}
			if (min >= 0 && min < this.getHeight()) {
				g.drawLine(0, min, this.getWidth() - 1, min);
			}
			g.setColor(Color.RED);
			if (this.isZeroMeanMode) {
				if (this.line0Y >= 0 && this.line0Y < this.getHeight()) {
					g.drawLine(0, this.line0Y, this.getWidth() - 1 , this.line0Y);
				}
			} else {
				g.drawLine(0, this.zeroY, this.getWidth() - 1, this.zeroY);
			}
			box.updateMarkers();
			box.updateUI();
		} 
	}
	
	/**
	 * set the zero mean mode
	 */
	public void setModeZeroMean() {
		this.isZeroMeanMode = true;
	}
	
	/**
	 * set the zero line mode
	 */
	public void setModeZeroLine() {
		this.isZeroMeanMode = false;
	}
	
	/**
	 * workaround, computes the minimum and maximum values before the first usage of paintComponent().
	 */
	public void computeLineMinMax() {
		if (interpolation != null) {		
			int lastValue = (int) interpolation.getValue(DisplayFunctions.periodicDisplayXToAngle(0, this.getWidth()).getAngle());
			int min = lastValue; 
			int max = lastValue;
			for (int i = 1; i < this.getWidth() - 1; i++) {
				int value = (int) interpolation.getValue(DisplayFunctions.periodicDisplayXToAngle(i, this.getWidth()).getAngle());
				lastValue = value;
				if (value < max) { //the y-axis is upside down
					max = value;
				}
				if (value > min) {
					min = value;
				}
			}
			this.LineMaxY = max;
			this.LineMinY = min;
		}
	}
	
	/**
	 * get the y position of the zero or mean line
	 * @return
	 */
	public int getZeroLinePosition() {
		if (this.isZeroMeanMode) {
			return this.line0Y;
		} else {
			return this.zeroY;
		}
	}
	
	/**
	 * get the minimum line position
	 * @return
	 */
	public int getMin() {
		if (this.LineMinY >= 0 && this.LineMinY < this.getHeight()) {
			return this.LineMinY;
		} 
		return -1;
	}
	
	/**
	 * get the maximum line position
	 * @return
	 */
	public int getMax() {
		if (this.LineMaxY >= 0 && this.LineMaxY < this.getHeight()) {
			return this.LineMaxY;
		} 
		return -1;
	}
}
