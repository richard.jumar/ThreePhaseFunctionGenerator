package GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class SymbolStatus extends JPanel {

	private static final long serialVersionUID = 49L;
	private int status = 0;
	
	public void setStatusUndefined() {
		this.status = 0;
	}
	
	public void setStatusDefined() {
		this.status = 1;
	}
	
	public void setStatusDefindedAndConnected() {
		this.status = 2;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.drawOval(0, 0, 18, 18);
		if (this.status == 0) {
			g.setColor(Color.RED);
		} else if (this.status == 1) {
			g.setColor(Color.ORANGE);
		} else {
			g.setColor(new Color(0, 150, 125));
		}
		g.fillOval(1, 1, 16, 16);
	}
}
