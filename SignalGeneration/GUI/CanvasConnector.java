package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class CanvasConnector extends JPanel implements MouseMotionListener, MouseListener {
	private static final long serialVersionUID = 25L;
	private boolean mouseOver;
	private Canvas canvas;
	private ConnectorType type; 
	private ConnectorType alternativeType;
	private boolean connected;
	private boolean active; //start point while building a connection
	private int connPointOffsetX;
	private int connPointOffsetY;
	private CanvasTreeElement treeElement;
	private CanvasConnector highlightFor;
	public CanvasConnector(Canvas canvas, CanvasTreeElement treeElement, ConnectorType type) {
		super();
		this.canvas = canvas;
		this.treeElement = treeElement;
		this.mouseOver = false; 
		this.type = type;
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.connected = false;
		this.setOpaque(false);
		this.connPointOffsetX = 0; 
		this.connPointOffsetY = 0;
		this.alternativeType = ConnectorType.nothing;
	}
	
	public void removeConnection() {
		this.treeElement.removeConnection(this);
		this.connected = false;
		this.setVisible(true);
	}
	
	public boolean canConnect(CanvasConnector c) {
		if (c == null) {
			return false;
		}
		//check connectorType
		if (type.opposite() == c.getType() || type.opposite() == c.getAlternativeType() || (alternativeType != ConnectorType.nothing && (alternativeType.opposite() == c.getType() || alternativeType.opposite() == c.getAlternativeType()) )) {
			//different nodes?
			if (c.getTreeElement() != this.treeElement ) {
				//no loop
				if ((this.type.isInput() && this.treeElement.getToplevelElementInTreeSegment() != c.getTreeElement()) || (c.getType().isInput() && c.getTreeElement().getToplevelElementInTreeSegment() != this.treeElement)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public CanvasTreeElement getTreeElement() {
		return this.treeElement;
	}
	
	public ConnectorType getAlternativeType() {
		return alternativeType;
	}

	public void setAlternativeType(ConnectorType alternativeType) {
		this.alternativeType = alternativeType;
	}
	
	public void setType(ConnectorType type) {
		this.type = type;
	}

	public boolean isConnected() {
		return connected;
	}
	
	public CanvasConnector getConnectedOutputConnector() {
		if (this.type.isInput()) {
			return canvas.getConnectionOutputConnector(this);
		}
		return null;
	}
	
	public void setConnectionPointOffset(int x, int y) {
		this.connPointOffsetX = x;
		this.connPointOffsetY = y;
	}
	
	public ConnectorType getType() {
		return this.type;
	}
	
	public void setConnected(boolean connected, CanvasConnector connectedWith) {
		this.connected = connected;
		this.setVisible(!connected);
		treeElement.setConnected(this, connectedWith);
	}
	
	public void paintComponent(Graphics g) {
		if (!connected && canvas.getMode() == CanvasMode.MOUSE) {
			super.paintComponent(g);
			if (canConnect(this.highlightFor) || active) {
				g.setColor(Color.GREEN);
			} else if (canvas.isLineActive()) { 	
				g.setColor(Color.GRAY);
			} else {
				g.setColor(Color.BLACK);
			}
			g.drawOval(0, 0, 8, 8);
			if (this.mouseOver || active) {
				g.fillOval(0, 0, 8, 8);
			}
		}
	}
	
	public int getXonCanvas() {
		return (this.getParent().getLocation().x + this.getLocation().x + this.connPointOffsetX);
	}
	
	public int getYonCanvas() {
		return (this.getParent().getLocation().y + this.getLocation().y + this.connPointOffsetY);
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		canvas.setDiff(e.getX(), e.getY());
		canvas.updateUI();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		mouseOver = true;
		updateUI();
		if (!connected) {
			canvas.setTmpConnAim(this);
		}
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		if (canvas.getMode() == CanvasMode.MOUSE) {
			mouseOver = false;
			updateUI();
			if (!connected) {
				canvas.setTmpConnAim(null);
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (canvas.getMode() == CanvasMode.MOUSE) {
			this.active = true;
			treeElement.setMousePressedActive(true);
			canvas.setLineActive(this.getXonCanvas(), this.getYonCanvas());	
			canvas.highlightFor(this);
			canvas.startConnection(this);
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		this.active = false;
		treeElement.setMousePressedActive(false);
		canvas.setLineInactive();
		canvas.stopHighlight();
		canvas.checkConnection();
	}

	public void highlightFor(CanvasConnector cc) {
		this.highlightFor = cc;		
	}
	          
	public void stopHighlight() {
		this.highlightFor = null;
	}


}
