package GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class SymbolPhasorMini extends JPanel {
	private static final long serialVersionUID = 48L;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.drawOval(0, 0, 12, 12);
		g.drawLine(6, 6, 5, 0);
		g.drawLine(6, 6, 12, 8);
		g.drawLine(6, 6, 2, 10);
	}
}
