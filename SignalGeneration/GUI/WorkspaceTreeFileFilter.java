package GUI;

import java.io.File;
import java.io.FileFilter;

public class WorkspaceTreeFileFilter implements FileFilter {

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if (file.getName().endsWith(".xml")) { //recording
			return true;
		}
		if (file.getName().endsWith(".s")) { //signal
			return true;
		}
		if (file.getName().endsWith(".am")) { //amplitude modulation
			return true;
		}
		return false;
	}

}
