package GUI;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Data.ConstSignal;
import Data.Phasor;
import Data.Signal;

/**
 * canvas box for a constant voltage
 */
public class CanvasBoxConst extends CanvasBoxSignalInput {
	private static final long serialVersionUID = 13L;
	private ConstSignal signal;
	private JLabel lbl_cb_u1;
	private JLabel lbl_cb_u2;
	private JLabel lbl_cb_u3;
	private JButton btn_edit;
	private JDialog dialog;
	private JRadioButton radio_symmetric;
	private JRadioButton radio_asymmetric;
	private JTextField edt_u1;
	private JTextField edt_u2;
	private JTextField edt_u3;
	private JLabel lbl_u1;
	private JLabel lbl_u2;
	private JLabel lbl_u3;
	private JLabel lbl_v1;
	private JLabel lbl_v2;
	private JLabel lbl_v3;
	
	/**
	 * constructor
	 * @param canvas canvas
	 * @param x x position
	 * @param y y position
	 */
	public CanvasBoxConst(Canvas canvas, int x, int y) {
		super(canvas);
		this.canvas = canvas;
		this.signal = new ConstSignal();
		this.setLayout(null);
		this.setBounds(x, y, 130, 110);
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Gleichspannung"));
		title.setBounds(5, 5, 110, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(110, 1, 19, 19);
		this.status.setStatusDefined();
		this.add(status);
		this.lbl_cb_u1 = new JLabel("<html>U<sub>1</sub> = 0 V</html>");
		this.lbl_cb_u1.setBounds(5, 20, 90, 20);
		this.add(lbl_cb_u1);
		this.lbl_cb_u2 = new JLabel("<html>U<sub>2</sub> = 0 V</html>");
		this.lbl_cb_u2.setBounds(5, 40, 90, 20);
		this.add(lbl_cb_u2);
		this.lbl_cb_u3 = new JLabel("<html>U<sub>3</sub> = 0 V</html>");
		this.lbl_cb_u3.setBounds(5, 60, 90, 20);
		this.add(lbl_cb_u3);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		this.btn_edit.setBounds(1, 80, 128, 29);
		this.btn_edit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}			
		});
		this.add(btn_edit);
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Eigenschaften Gleichspannung"));
		dialog.setLayout(null);
		dialog.setSize(255, 200);
		this.edt_u1 = new JTextField("0");
		this.edt_u1.setBounds(40, 60, 100, 20);
		this.edt_u2 = new JTextField("0");
		this.edt_u2.setBounds(40, 85, 100, 20);
		this.edt_u2.setVisible(false);
		this.edt_u3 = new JTextField("0");
		this.edt_u3.setBounds(40, 110, 100, 20);
		this.edt_u3.setVisible(false);
		this.lbl_u1 = new JLabel("<html>U = </html>");
		this.lbl_u1.setBounds(5, 60, 35, 20);
		this.lbl_u2 = new JLabel("<html>U<sub>2</sub> =</html>");
		this.lbl_u2.setBounds(5, 85, 35, 20);
		this.lbl_u2.setVisible(false);
		this.lbl_u3 = new JLabel("<html>U<sub>3</sub> =</html>");
		this.lbl_u3.setBounds(5, 110, 35, 20);
		this.lbl_u3.setVisible(false);
		this.lbl_v1 = new JLabel("V");
		this.lbl_v1.setBounds(140, 60, 10, 20);
		this.lbl_v2 = new JLabel("V");
		this.lbl_v2.setBounds(140, 85, 10, 20);
		this.lbl_v2.setVisible(false);
		this.lbl_v3 = new JLabel("V");
		this.lbl_v3.setBounds(140, 110, 10, 20);
		this.lbl_v3.setVisible(false);
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(null);
		optionPanel.setBounds(5, 5, 245, 50);
		optionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eigenschaften")));
		dialog.add(optionPanel);
		ButtonGroup btnGroup_phases = new ButtonGroup();
		this.radio_symmetric = new JRadioButton(LanguageTranslator.getTranslation("symmetrisch"));
		radio_symmetric.setSelected(true);
		radio_symmetric.setBounds(10, 20, 110, 20);
		radio_symmetric.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setSymmetric();
			}		
		});
		btnGroup_phases.add(radio_symmetric);
		optionPanel.add(radio_symmetric);
		this.radio_asymmetric = new JRadioButton(LanguageTranslator.getTranslation("asymmetrisch"));
		radio_asymmetric.setSelected(false);
		radio_asymmetric.setBounds(120, 20, 118, 20);
		radio_asymmetric.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setAsymmetric();
				edt_u2.setText(edt_u1.getText()); //use symmetric u as default
				edt_u3.setText(edt_u1.getText());
			}
			
		});
		btnGroup_phases.add(radio_asymmetric);
		optionPanel.add(radio_asymmetric);
		dialog.add(lbl_u1);
		dialog.add(edt_u1);
		dialog.add(lbl_v1);
		dialog.add(lbl_u2);
		dialog.add(edt_u2);
		dialog.add(lbl_v2);
		dialog.add(lbl_u3);
		dialog.add(edt_u3);
		dialog.add(lbl_v3);
		JButton btn_ok = new JButton(LanguageTranslator.getTranslation("OK"));
		btn_ok.setBounds(5, 135, 245, 25);
		btn_ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btn_okClick();
			}
			
		});
		dialog.add(btn_ok);
	}
	
	/**
	 * display the form elements for a symmetric definition
	 */
	private void setSymmetric() {
		this.updateVoltageValues();
		lbl_u1.setText("<html>U =</html>");
		edt_u2.setVisible(false);
		lbl_u2.setVisible(false);
		lbl_v2.setVisible(false);
		edt_u3.setVisible(false);
		lbl_u3.setVisible(false);
		lbl_v3.setVisible(false);
	}
	
	/**
	 * display the form elements for an asymmetric definition
	 */
	private void setAsymmetric() {
		this.updateVoltageValues();
		lbl_u1.setText("<html>U<sub>1</sub> =</html>");
		lbl_u2.setVisible(true);
		lbl_v2.setVisible(true);

		lbl_u3.setVisible(true);
		edt_u2.setVisible(true);
		edt_u3.setVisible(true);
		lbl_v3.setVisible(true);
	}
	
	/**
	 * update the voltages in the form elements
	 */
	private void updateVoltageValues() {
		this.edt_u1.setText(GUIMath.roundDecimals(this.signal.getValue()[0], 5));
		this.edt_u2.setText(GUIMath.roundDecimals(this.signal.getValue()[1], 5));
		this.edt_u3.setText(GUIMath.roundDecimals(this.signal.getValue()[2], 5));
	}
	
	/**
	 * show the edit dialog
	 */
	private void showDialog() {
		this.updateVoltageValues();
		this.dialog.setVisible(true);
	}
	
	/**
	 * constant voltage is full defined
	 */
	public boolean fullDefined() {
		return true;
	}
	
	/**
	 * get the signal
	 */
	public Signal getSignal() {
		return this.signal;
	}
	
	/**
	 * set the signal voltages
	 * @param voltage voltages in the three phases
	 */
	public void setVoltage(double[] voltage) {
		this.signal.updateValues(voltage);
		if (voltage[0] == voltage[1] && voltage[0] == voltage[2]) {
			this.setSymmetric();
			this.radio_symmetric.setSelected(true);
		} else {
			this.setAsymmetric();
			this.radio_asymmetric.setSelected(true);
		}
	}

	@Override
	/**
	 * deleting a phasor is ok
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true;
	}
	
	/**
	 * get the voltage values
	 * @return voltages of the three phases
	 */
	public double[] getVoltages() {
		return this.signal.getValue();
	}

	/**
	 * action of the ok button
	 */
	public void btn_okClick() {
		try {	
			double u1 = Double.parseDouble(edt_u1.getText().replace(',', '.'));
			if (radio_symmetric.isSelected()) {
				double[] voltages = {u1, u1, u1};
				signal.updateValues(voltages);
				lbl_cb_u1.setText("<html>U = " + GUIMath.roundDecimals(u1, 5) + " V</html>");
				lbl_cb_u2.setVisible(false);
				lbl_cb_u3.setVisible(false);
			} else {
				double u2 = Double.parseDouble(edt_u2.getText().replace(',', '.'));
				double u3 = Double.parseDouble(edt_u3.getText().replace(',', '.'));
				double[] voltages = {u1, u2, u3};
				signal.updateValues(voltages);
				lbl_cb_u1.setText("<html>U<sub>1</sub> = " + GUIMath.roundDecimals(u1, 5) + " V</html>");
				lbl_cb_u2.setText("<html>U<sub>2</sub> = " + GUIMath.roundDecimals(u2, 5) + " V</html>");
				lbl_cb_u3.setText("<html>U<sub>3</sub> = " + GUIMath.roundDecimals(u3, 5) + " V</html>");
				lbl_cb_u2.setVisible(true);
				lbl_cb_u3.setVisible(true);
			}
			dialog.dispose();
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur g�ltige Zahlenwerte als Spannungswerte angeben"));
		}
	}
}
