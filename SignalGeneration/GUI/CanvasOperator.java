package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import Data.Phasor;

public abstract class CanvasOperator extends JPanel implements MouseListener, MouseMotionListener, CanvasTreeElement {
	private static final long serialVersionUID = 26L;
	private int mousePressX;
	private int mousePressY;
	protected Canvas canvas;
	protected CanvasConnector input0;
	protected CanvasConnector input1;
	protected CanvasConnector output;
	private boolean mousePressedActive;
	protected boolean mouseOver;
	protected CanvasTreeElement parent;
	protected CanvasTreeElement child0;
	protected CanvasTreeElement child1;
	public int id;
	public CanvasOperator(Canvas canvas) {
		super();
		this.setOpaque(false);
		this.setLayout(null);
		this.canvas = canvas;
		this.mouseOver = false;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);

	}
		
	private CanvasTreeElement getHighestRelevantNode() {
		CanvasTreeElement relevant = this;
		while (relevant.getParentNode() != null && relevant.getParentNode().isVariable()) {
			relevant = relevant.getParentNode();
		}
		return relevant;
	}
	
	public CanvasConnector getConnector0() {
		return input0;
	}
	
	public CanvasConnector getConnector1() {
		return input1;
	}
	
	public CanvasConnector getOutputConnector() {
		return this.output;
	}
	
	public boolean fullDefined() {
		if (child0 != null && child1 != null) {
			return child0.fullDefined() && child1.fullDefined();
		}
		return false;
	}
	
	public void removeConnection(CanvasConnector myConnector) {
		if (input0 == myConnector) {
			this.child0 = null;
			if (this.isVariable()) {
				this.getHighestRelevantNode().resetSubTree(true);
				this.getHighestRelevantNode().retypeSubTree();
			}
			this.updateSignalOrAmplitudeModulation();
		} else if (input1 == myConnector) {
			this.child1 = null;
			if (this.isVariable()) {
				this.getHighestRelevantNode().resetSubTree(true);
				this.getHighestRelevantNode().retypeSubTree();
			}
			this.updateSignalOrAmplitudeModulation();
		} else if (output == myConnector) {
			this.parent = null;
			if (this.isVariable()) {
				this.resetSubTree(true);
				this.retypeSubTree();
			}
		}
	}
	
	public void setMousePressedActive(boolean active) {
		this.mousePressedActive = active;
	}
	
	public boolean getMousePressedActive() {
		return this.mousePressedActive;
	}
	
	public ConnectorType getConnectorTypeTo(CanvasOperator op) {
		if (parent == op) {
			return output.getType();
		}
		if (child0 == op) {
			return input0.getType();
		}
		if (child1 == op) {
			return input1.getType();
		}
		return ConnectorType.nothing;
	}
	
	public ConnectorType getAlternativeConnectorTypeTo(CanvasOperator op) {
		if (parent == op) {
			return output.getAlternativeType();
		}
		if (child0 == op) {
			return input0.getAlternativeType();
		}
		if (child1 == op) {
			return input1.getAlternativeType();
		}
		return ConnectorType.nothing;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		if (this.output.getAlternativeType() == ConnectorType.nothing) {
			if (this.output.getType() == ConnectorType.SignalOutput) {
				g.setColor(Color.RED);
			} else if (this.output.getType() == ConnectorType.AMOutput) {
				g.setColor(Color.BLUE);
			}
		}
		g.drawOval(0, 9, 50, 50);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (canvas.getMode() == CanvasMode.DELETE) {
			canvas.removeTreeElement(this);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.mouseOver = true;
		this.updateUI();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.mouseOver = false;
		this.updateUI();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.mousePressX = e.getX();
		this.mousePressY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (canvas.getMode() == CanvasMode.MOUSE && canvas.getMousePosition() != null && canvas.isEnabled()) { //case null: Mouse is out of canvas
			this.setLocation(canvas.getMousePosition().x - this.mousePressX, canvas.getMousePosition().y - this.mousePressY);	
			canvas.updateUI();
		}

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}


	public void highlightFor(CanvasConnector cc) {
		input0.highlightFor(cc);
		input1.highlightFor(cc);
		output.highlightFor(cc);
	}
	
	public void stopHighlight() {
		input0.stopHighlight();
		input1.stopHighlight();
		output.stopHighlight();
	}

	public CanvasTreeElement getParentNode() {
		return this.parent;
	}
		
	public CanvasTreeElement getToplevelElementInTreeSegment() {
		CanvasTreeElement node = this.parent;
		while(node != null && node.getParentNode() != null) {
			node = node.getParentNode();
		}
		return node;
	}
	public CanvasConnector getConnectorFor(CanvasTreeElement te) {
		if (this.child0 == te) {
			return input0;
		} else if (this.child1 == te) {
			return input1;
		} else if (this.parent == te) {
			return this.output;
		} else {
			return null;
		}
	}

	protected abstract void updateNeighbors(CanvasOperator updateSource);
	
	public void setConnected(CanvasConnector myConnector, CanvasConnector otherConnector) {
		if (otherConnector.getAlternativeType() == ConnectorType.nothing) {
			myConnector.setAlternativeType(ConnectorType.nothing);
			myConnector.setType(otherConnector.getType().opposite());
		}
			if (myConnector == output) {
				this.parent = otherConnector.getTreeElement();
			} else if (myConnector == input0) {
				this.child0 = otherConnector.getTreeElement();
				this.updateSignalOrAmplitudeModulation();
			} else if (myConnector == input1) {
				this.child1 = otherConnector.getTreeElement();
				this.updateSignalOrAmplitudeModulation();
			} 
			updateNeighbors(this);

	}
	

	
	public void retypeSubTree() {
		if (this.output.getAlternativeType() != ConnectorType.nothing) {
			//my output is variable
			if (this.getParentNode() != null && this.getParentNode().getConnectorFor(this).getAlternativeType() == ConnectorType.nothing) {
				//parent input defined
				if (this.getParentNode().getConnectorFor(this).getType().opposite() == this.output.getType() || this.getParentNode().getConnectorFor(this).getType().opposite() == this.output.getAlternativeType()) {
					//set type as determined by parent
					this.output.setAlternativeType(ConnectorType.nothing);
					this.output.setType(this.getParentNode().getConnectorFor(this).getType().opposite());
				}
			}
			if ((this.child0 != null && this.child0.getConnectorFor(this).getAlternativeType() == ConnectorType.nothing) || (this.child1 != null && this.child1.getConnectorFor(this).getAlternativeType() == ConnectorType.nothing)) {
				this.updateNeighbors(null);
			}
		}
		if (this.output.getAlternativeType() == ConnectorType.nothing) {
			//my output is defined (not variable)
			if (this.parent instanceof CanvasOperator) {
				//tell it to my parent
				((CanvasOperator)this.parent).updateNeighbors(this);
			}
		}
		if (this.isVariable()) {
			//I'm variable, so maybe something changed => Tell it to the children
			if (this.child0 != null) {
				this.child0.retypeSubTree();
			}
			if (this.child1 != null) {
				this.child1.retypeSubTree();
			}
		}
	}
	
	public boolean hasParent() {
		if (this.parent == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true;
	}

}
