package GUI;

public class Connection {
	private CanvasConnector input;
	private CanvasConnector output;
	private boolean highlightForDelete;
	
	public boolean isHighlightForDelete() {
		return highlightForDelete;
	}

	public void setHighlightForDelete(boolean highlightForDelete) {
		this.highlightForDelete = highlightForDelete;
	}

	public Connection(CanvasConnector output, CanvasConnector input) {
		this.input = input; 
		this.output = output;
		input.setConnected(true, output);
		output.setConnected(true, input);
	}

	public CanvasConnector getInput() {
		return input;
	}

	public CanvasConnector getOutput() {
		return output;
	}
	
	public void removeConnection() {
		this.input.removeConnection();
		this.output.removeConnection();
	}	
}
