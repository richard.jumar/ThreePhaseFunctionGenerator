package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToolBar;

import javax.swing.filechooser.FileNameExtensionFilter;
import Controller.DataControl;
import Controller.FileControl;
import Controller.HardwareControl;
import Data.Phasor;


/**
 * Main Window of the GUI
 */
public class MainWindow extends JFrame {
	private static final long serialVersionUID = 32L;
	private JMenuBar menu;
	private JToolBar toolbar;
	private JPanel mainPanel;
	private Canvas canvas;
	private PhasorPanel grp_fm;
	private WorkspaceTree treePane;
	private AbstractAction mouse;
	private AbstractAction delete;
	private OutputPanel signalOutput;
	private JPanel grp_output;
	private OutputPanel amOutput;
	private JPanel grp_operators;
	private JPanel grp_signal;
	private InputPanel harmonic;
	private InputPanel pointInterpolation;
	private InputPanel function;
	private InputPanel constSignal;
	private JPanel grp_am;
	private InputPanel gain;
	private InputPanel step;
	private InputPanel ramp;
	private InputPanel pointinterpolation_am;
	private InputPanel function_am;
	private JDialog dialogAbout;
	private String version = "1.0";
	private String date = "5. April 2019";
	
	/**
	 * constructor
	 */
	public MainWindow() {

		//================
		/*
		 * Do your font configuration here
		 * 
		 */		
		//=================
		
		this.setSize(1920, 1080); //full HD
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setUndecorated(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		createMenu();
		this.toolbar = createToolbar();
		this.add(toolbar, BorderLayout.PAGE_START);
		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(null);
		this.add(mainPanel);
		setCanvas(new Canvas());
		grp_output = new JPanel();
		grp_output.setBounds(1615, 5, 280, 130);
		grp_output.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Ausgaben")));
		grp_output.setLayout(null);
		signalOutput = new OutputPanel(LanguageTranslator.getTranslation("Signalausgabe"));
		signalOutput.setBounds(5, 20, 270, 50);
		signalOutput.setBorder(BorderFactory.createLineBorder(Color.RED));
		grp_output.add(signalOutput);
		amOutput = new OutputPanel(LanguageTranslator.getTranslation("Amplitudenmodulationsausgabe"));
		amOutput.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		amOutput.setBounds(5, 75, 270, 50);
		grp_output.add(amOutput);
		this.mainPanel.add(grp_output);
		grp_operators = new JPanel();
		grp_operators.setBounds(1615, 140, 280, 75);
		grp_operators.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Operatoren")));
		grp_operators.setLayout(null);
		OperatorSymbol addOp = new OperatorSymbol(8, 18);
		addOp.setPlus();
		grp_operators.add(addOp);
		OperatorSymbol multOp = new OperatorSymbol(65, 18);
		multOp.setDot();
		grp_operators.add(multOp);
		this.mainPanel.add(grp_operators);
		grp_signal = new JPanel();
		grp_signal.setBounds(1615, 220, 280, 240);
		grp_signal.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Signale")));
		grp_signal.setLayout(null);
		harmonic = new InputPanel(LanguageTranslator.getTranslation("Harmonische"));
		harmonic.setBorder(BorderFactory.createLineBorder(Color.RED));
		harmonic.setBounds(5, 20, 270, 50);
		grp_signal.add(harmonic);
		pointInterpolation = new InputPanel(LanguageTranslator.getTranslation("Punktinterpolation"));
		pointInterpolation.setBorder(BorderFactory.createLineBorder(Color.RED));
		pointInterpolation.setBounds(5, 75, 270, 50);
		grp_signal.add(pointInterpolation);
		function = new InputPanel(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion"));
		function.setBorder(BorderFactory.createLineBorder(Color.RED));
		function.setBounds(5, 130, 270, 50);
		grp_signal.add(function);
		constSignal = new InputPanel(LanguageTranslator.getTranslation("Gleichspannung"));
		constSignal.setBorder(BorderFactory.createLineBorder(Color.RED));
		constSignal.setBounds(5, 185, 270, 50);
		grp_signal.add(constSignal);
		this.mainPanel.add(grp_signal);
		grp_am = new JPanel();
		grp_am.setBounds(1615, 465, 280, 295);
		grp_am.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Amplitudenmodulationsfunktionen")));
		grp_am.setLayout(null);
		gain = new InputPanel(LanguageTranslator.getTranslation("Verst�rkung"));
		gain.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		gain.setBounds(5, 20, 270, 50);
		grp_am.add(gain);
		this.mainPanel.add(grp_am);
		step = new InputPanel(LanguageTranslator.getTranslation("Sprung"));
		step.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		step.setBounds(5, 75, 270, 50);
		grp_am.add(step);
		ramp = new InputPanel(LanguageTranslator.getTranslation("Rampe"));
		ramp.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		ramp.setBounds(5, 130, 270, 50);
		grp_am.add(ramp);
		pointinterpolation_am = new InputPanel(LanguageTranslator.getTranslation("Punktinterpolation_am"));
		pointinterpolation_am.changeLabelText(LanguageTranslator.getTranslation("Punktinterpolation"));
		pointinterpolation_am.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		pointinterpolation_am.setBounds(5, 185, 270, 50);
		grp_am.add(pointinterpolation_am);
		function_am = new InputPanel(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion_am"));
		function_am.changeLabelText(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion"));
		function_am.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		function_am.setBounds(5, 240, 270, 50);
		grp_am.add(function_am);
		grp_fm = new PhasorPanel();
		grp_fm.setBounds(1615, 765, 280, 192);
		this.mainPanel.add(grp_fm);
		HardwareControl.lbl_status.setBounds(5, 952, 1895, 20);
		this.mainPanel.add(HardwareControl.lbl_status);
		HardwareControl.lbl_status.setText(LanguageTranslator.getTranslation("Eingabemodus"));
		String workspace = FileControl.ini.read("GUI", "workspace");
		if (!workspace.isEmpty()) {
			loadWorkspace(new File(workspace));
		}
	}
	
	public void enableSignalAndAmPanels(boolean enabled) {
		signalOutput.setEnabled(enabled);
		grp_output.setEnabled(enabled);
		amOutput.setEnabled(enabled);
		grp_operators.setEnabled(enabled);
		grp_signal.setEnabled(enabled);
		harmonic.setEnabled(enabled);
		pointInterpolation.setEnabled(enabled);
		function.setEnabled(enabled);
		constSignal.setEnabled(enabled);
		grp_am.setEnabled(enabled);
		gain.setEnabled(enabled);
		step.setEnabled(enabled);
		ramp.setEnabled(enabled);
		pointinterpolation_am.setEnabled(enabled);
		function_am.setEnabled(enabled);
	}

	/**
	 * set the canvas of this window
	 * @param canvas
	 */
	public void setCanvas(Canvas canvas) {
		if (this.canvas != null) {
			this.mainPanel.remove(this.canvas);
		}
		this.canvas = canvas;
		this.canvas.setBounds(310, 5, 1300, 950);
		this.canvas.setLayout(null);
		this.canvas.setBackground(Color.WHITE);
		this.mainPanel.add(canvas);
	}
	
	/**
	 * returns the phasor panel
	 * @return
	 */
	public PhasorPanel getPhasorPanel() {
		return this.grp_fm;
	}
	
	/**
	 * menu bar
	 */
	private void createMenu() {
		this.menu = new JMenuBar();
		JMenu file = new JMenu(LanguageTranslator.getTranslation("Datei"));
		Action newAction = new AbstractAction(LanguageTranslator.getTranslation("Neu")) {
			private static final long serialVersionUID = 33L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DataControl.resetClock();
				canvas.clearAll();
			}			
		};
		file.add(newAction);
		
		Action openAction = new AbstractAction(LanguageTranslator.getTranslation("�ffnen")) {

			private static final long serialVersionUID = 34L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (canvas.clearAll()) {
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(LanguageTranslator.getTranslation("Signale und Amplitudenmodulationen"), "s", "am", "undef"));
					fc.setCurrentDirectory(FileControl.workspace);
					int dialogState = fc.showOpenDialog(null);
					if (dialogState == JFileChooser.APPROVE_OPTION) {
						FileControl.loadCanvasFromFile(fc.getSelectedFile().getAbsolutePath());
					}
				}
			}			
		};
		file.add(openAction);
		
		Action saveAction = new AbstractAction(LanguageTranslator.getTranslation("Speichern")) {

			private static final long serialVersionUID = 35L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if(FileControl.saveAction(canvas)) {
					canvas.setChanged(false);
				}
			}			
		};
		file.add(saveAction);
		
		
		Action saveAsAction = new AbstractAction(LanguageTranslator.getTranslation("Speichern unter")) {
			private static final long serialVersionUID = 36L;

			public void actionPerformed(ActionEvent arg0) {
				if (FileControl.saveAsAction(canvas)) {
					canvas.setChanged(false);
				}
			}		
		};

		file.add(saveAsAction);
		Action exitAction = new AbstractAction(LanguageTranslator.getTranslation("Beenden")) {

			private static final long serialVersionUID = 37L;

			public void actionPerformed(ActionEvent arg0) {
				close();
			}			
		};
		file.add(exitAction);
		this.menu.add(file);
		JMenu edit = new JMenu(LanguageTranslator.getTranslation("Bearbeiten"));
		Action workspaceAction = new AbstractAction("Workspace") {

			private static final long serialVersionUID = 38L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int dialogState = fc.showOpenDialog(null);
				if (dialogState == JFileChooser.APPROVE_OPTION) {
					loadWorkspace(fc.getSelectedFile());
					FileControl.ini.save("GUI", "workspace", fc.getSelectedFile().getPath());
				}
			}
		};
		edit.add(workspaceAction);
		this.menu.add(edit);
		JMenu help = new JMenu(LanguageTranslator.getTranslation("Hilfe"));
		this.dialogAbout = new JDialog();
		this.dialogAbout.setModal(true);
		this.dialogAbout.setTitle(LanguageTranslator.getTranslation(LanguageTranslator.getTranslation("�ber")));
		this.dialogAbout.setLayout(null);
		this.dialogAbout.setBounds(800, 300, 250, 160);
		JLabel lbl_title = new JLabel(LanguageTranslator.getTranslation("KIT 3-Phase Function Generator GUI"));
		lbl_title.setBounds(5,10,240,20);
		JLabel lbl_version = new JLabel(LanguageTranslator.getTranslation("Version") + ": " + this.version);
		lbl_version.setBounds(5,30,100,20);
		JLabel lbl_date = new JLabel(this.date);
		lbl_date.setBounds(5, 50, 150, 20);
		JLabel lbl_auth = new JLabel(LanguageTranslator.getTranslation("Entwickelt von") + " Anselm Erdmann");
		lbl_auth.setBounds(5, 70, 200, 20);
		this.dialogAbout.add(lbl_title);
		this.dialogAbout.add(lbl_version);
		this.dialogAbout.add(lbl_date);
		this.dialogAbout.add(lbl_auth);
		JButton btn_closeAbout = new JButton(LanguageTranslator.getTranslation("Schlie�en"));
		btn_closeAbout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dialogAbout.setVisible(false);
			}
			
		});
		btn_closeAbout.setBounds(70, 90, 100, 25);
		this.dialogAbout.add(btn_closeAbout);
		Action aboutAction = new AbstractAction(LanguageTranslator.getTranslation("�ber")) {

			private static final long serialVersionUID = 39L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialogAbout.setVisible(true);
			}
			
		};
		help.add(aboutAction);
		this.menu.add(help);
		this.setJMenuBar(menu);
	}


	
	/**
	 * loading FIR-Filters from the filter directory (see preferences.ini)
	 */
	public void loadFilters() {
		HardwareControl.cbo_filter.removeAllItems();
		if (Main.getWorkspaceDir() != null) {
			File[] filterFolders = Main.getWorkspaceDir().listFiles(new FileFilter() {

				@Override
				public boolean accept(File arg0) {
					if (arg0.getName().equals(FileControl.ini.read("GUI", "filterSubDir"))) {
						return true;
					}
					return false;
				}
				
			});
			if (filterFolders.length  > 0) {
				File filterFolder = filterFolders[0];		
				FileFilter filterFileFilter = new FileFilter() {					
					@Override
					public boolean accept(File file) {
						if (file.getName().endsWith(".fcf")) { //recording
							return true;
						}
						return false;
					}					
				};
				
				for (int i = 0; i < filterFolder.listFiles(filterFileFilter).length; i++) {
					HardwareControl.cbo_filter.addItem(filterFolder.listFiles(filterFileFilter)[i].getName());
				}
			}
			
		}
	}
	
	/**
	 * load workspace into the workspace tree on the left
	 * @param workspace
	 */
	private void loadWorkspace(File workspace) {
		Main.setWorkspaceDir(workspace);
		if (treePane != null) {
			mainPanel.remove(treePane); //remove old workspace tree
		}
		treePane = new WorkspaceTree();
		treePane.setBounds(5, 5, 300, 950);
		treePane.setMinimumSize(new Dimension(200, 800));
		mainPanel.add(treePane);
		mainPanel.updateUI();
		FileControl.workspace = workspace;
	}
	
	/**
	 * enable the workspace tree
	 * @param enabled
	 */
	public void setWorkspaceTreeEnabled(boolean enabled) {
		this.treePane.enableDrag(enabled);
	}
	
	
	/**
	 * creates the toolbar
	 * @return
	 */
	private JToolBar createToolbar() {
		JToolBar toolbar = new JToolBar();
		this.mouse = new AbstractAction("mouse", new ImageIcon("GUI/mouse.png")) {
			private static final long serialVersionUID = 40L;

			public void actionPerformed(ActionEvent e) {
				canvas.setMode(CanvasMode.MOUSE);
				canvas.updateUI();
				setWorkspaceTreeEnabled(true);
				enableSignalAndAmPanels(true);
				HardwareControl.lbl_status.setText(LanguageTranslator.getTranslation("Eingabemodus"));
				
			}
		};
		this.delete = new AbstractAction("delete", new ImageIcon("GUI/delete.png")) {
			private static final long serialVersionUID = 41L;

			public void actionPerformed(ActionEvent arg0) {
				canvas.setMode(CanvasMode.DELETE);
				canvas.updateUI();
				setWorkspaceTreeEnabled(false);
				enableSignalAndAmPanels(false);
				HardwareControl.lbl_status.setText(LanguageTranslator.getTranslation("L�schmodus"));
			}
		};
		toolbar.add(this.mouse);
		toolbar.add(this.delete);
		PlayAction pa = new PlayAction();
		StopAction sa = new StopAction(pa);
		pa.setStopAction(sa);
		sa.setEnabled(false);
		toolbar.add(pa);
		toolbar.add(sa);
		HardwareControl.pbr_buffer = new JProgressBar();
		HardwareControl.pbr_buffer.setBounds(0, 0, 100, 20);
		HardwareControl.pbr_buffer.setMinimum(0);
		HardwareControl.pbr_buffer.setMaximum(100);
		toolbar.add(HardwareControl.pbr_buffer);
		HardwareControl.createComboAndCheckBoxes();
		toolbar.add(HardwareControl.cbx_monitor);
		toolbar.add(HardwareControl.cbo_technologie);
		toolbar.add(HardwareControl.cbo_soundCard);
		HardwareControl.reloadSoundCards();
		toolbar.add(HardwareControl.cbx_filter);
		toolbar.add(HardwareControl.cbo_filter);
		this.loadFilters();
		return toolbar;
		
	}
	
	/**
	 * enable the canvas (for change of mode)
	 * @param enable
	 */
	public void enableCanvas(boolean enable) {
		this.mouse.setEnabled(enable);
		this.delete.setEnabled(enable);
		this.canvas.setEnabled(enable);
	}
	
	/**
	 * check whether deleting of the given phasor is ok
	 * @param phasor to delete
	 * @return
	 */
	public  boolean isDeletePhasorForCanvasOk(Phasor p) {
		return this.canvas.isDeletePhasorOk(p);
		//It's not possible to tell the phasor panel canvas, because the canvas object could change while runtime. 
	}
	
	/**
	 * close the main window
	 */
	private void close() {
		if (canvas.clearAll()) {
			this.dispose();
		}
	}
	
	/**
	 * set the value for the bar diagram of the buffer
	 * @param percent
	 */
	public static void setBufferValue(int percent) {
		HardwareControl.pbr_buffer.setValue(percent);
	}
}
