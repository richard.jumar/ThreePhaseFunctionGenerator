package GUI;

import java.io.File;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

public class WorkspaceTreeLeaf implements TreeNode {

	private TreeNode parent;
	private File file;
	private WorkspaceTreeLeafType leafType;
	
	public WorkspaceTreeLeaf(TreeNode parent, File file) {
		this.parent = parent;
		this.file = file;
		if (file.getPath().endsWith(".s")) {
			this.leafType = WorkspaceTreeLeafType.SIGNAL;
		} else if (file.getPath().endsWith(".am")) {
			this.leafType = WorkspaceTreeLeafType.AMPLITUDEMODULATION;
		} else {
			this.leafType = WorkspaceTreeLeafType.RECORDED;
		}
	}
	public String toString() {
		return this.file.getName();
	}
	
	public String getPath() {
		return file.getAbsolutePath();
	}
	
	@Override
	public Enumeration<WorkspaceTreeLeafType> children() {
		return null;
	}

	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	@Override
	public TreeNode getChildAt(int arg0) {
		return null;
	}

	@Override
	public int getChildCount() {
		return 0;
	}

	@Override
	public int getIndex(TreeNode arg0) {
		return 0;
	}

	@Override
	public TreeNode getParent() {
		return this.parent;
	}

	public boolean isLeaf() {
		return true;
	}
	
	public WorkspaceTreeLeafType getLeafType() {
		return this.leafType;
	}


}
