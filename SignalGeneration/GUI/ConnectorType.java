package GUI;

public enum ConnectorType {
	SignalInput, SignalOutput, AMInput, AMOutput, nothing;
	public ConnectorType opposite() {
		if (this == SignalInput) {
			return SignalOutput;
		}
		if (this == SignalOutput) {
			return SignalInput;
		}
		if (this == AMInput) {
			return AMOutput;
		}
		if (this == AMOutput) {
			return AMInput;
		}
		return null;
		
	}
	
	public boolean isInput() {
		if (this == SignalInput || this == AMInput) {
			return true;
		}
		return false;
	}
	
	public boolean isOutput() {
		if (this == SignalOutput || this == AMOutput) {
			return true;
		}
		return false;
	}
	
	public boolean isSignal() {
		if (this == SignalInput || this == SignalOutput) {
			return true;
		}
		return false;
	}
	
	public boolean isAM() {
		if (this == AMInput || this == AMOutput) {
			return true;
		}
		return false;
	}
}
