package GUI;

import java.awt.Color;
import java.awt.Graphics;

import Data.AddAmplitudeModulation;
import Data.AddSignal;
import Data.AmplitudeModulation;
import Data.Signal;

public class CanvasOperatorAdd extends CanvasOperator {

	private static final long serialVersionUID = 27L;
	private AmplitudeModulation amplitudeModulation;
	private AddSignal signal;

	public CanvasOperatorAdd(Canvas canvas, int x, int y) {
		super(canvas);
		this.setBounds(x, y, 51, 60);
		this.output = new CanvasConnector(canvas, this, ConnectorType.SignalOutput);
		this.output.setAlternativeType(ConnectorType.AMOutput);
		this.output.setBounds(21, 1, 9, 9);
		this.output.setConnectionPointOffset(4, 8);
		this.add(this.output);		
		this.input0 = new CanvasConnector(canvas, this, ConnectorType.SignalInput);
		this.input0.setAlternativeType(ConnectorType.AMInput);
		this.input0.setBounds(1, 51, 9, 9);
		this.input0.setConnectionPointOffset(8, 2);
		this.add(this.input0);
		this.input1 = new CanvasConnector(canvas, this, ConnectorType.SignalInput);
		this.input1.setAlternativeType(ConnectorType.AMInput);
		this.input1.setBounds(41, 51, 9, 9);
		this.input1.setConnectionPointOffset(0, 2);
		this.add(this.input1);
	}

	@Override
	protected void updateNeighbors(CanvasOperator updateSource) {
		if (updateSource == this.parent) {
			//update information from the parent operator
			if (parent instanceof CanvasOperator) {
				if (((CanvasOperator) parent).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) parent).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.output.setType(((CanvasOperator) parent).getConnectorTypeTo(this).opposite());
						this.output.setAlternativeType(ConnectorType.nothing);
					}
				}
			}
		}
		if (updateSource == this.child0 || updateSource == null) {
			if (child0 instanceof CanvasOperator) {
				if (((CanvasOperator) child0).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) child0).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.input0.setType(((CanvasOperator) child0).getConnectorTypeTo(this).opposite());
						this.input0.setAlternativeType(ConnectorType.nothing);
					}
				}
			} else {
				if (child0 != null) {
					this.input0.setType(child0.getConnectorFor(this).getType().opposite());
					this.input0.setAlternativeType(ConnectorType.nothing);
				}
			}
		}
		if (updateSource == this.child1 || updateSource == null) {
			if (child1 instanceof CanvasOperator) { //case child 1 is canvas operator
				if (((CanvasOperator) child1).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) child1).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.input1.setType(((CanvasOperator) child1).getConnectorTypeTo(this).opposite());
						this.input1.setAlternativeType(ConnectorType.nothing);
					}
				}
			} else { //case child 1 is no canvas operator (type is not changeable)
				if (child1 != null) {
					this.input1.setType(child1.getConnectorFor(this).getType().opposite());
					this.input1.setAlternativeType(ConnectorType.nothing);
				}
			}
		}
		
		//update own type
		if (this.output.getAlternativeType() == ConnectorType.nothing) {
			//case output type is defined
			if (this.output.getType() == ConnectorType.SignalOutput) {
				//output is signal => both inputs are of type signal
				this.input1.setType(ConnectorType.SignalInput);
				this.input1.setAlternativeType(ConnectorType.nothing);
				this.input0.setType(ConnectorType.SignalInput);
				this.input0.setAlternativeType(ConnectorType.nothing);
			} else if (this.output.getType() == ConnectorType.AMOutput) {
				//two AM-Inputs
				this.input0.setType(ConnectorType.AMInput);
				this.input0.setAlternativeType(ConnectorType.nothing);
				this.input1.setType(ConnectorType.AMInput);
				this.input1.setAlternativeType(ConnectorType.nothing);
			}
		} else if (this.input0.getAlternativeType() == ConnectorType.nothing) {
			if (this.input0.getType() == ConnectorType.SignalInput) {
				this.input1.setType(ConnectorType.SignalInput);
				this.input1.setAlternativeType(ConnectorType.nothing);
				this.output.setType(ConnectorType.SignalOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			} else {//input0 type is am
				this.input1.setType(ConnectorType.AMInput);
				this.input1.setAlternativeType(ConnectorType.nothing);
				this.output.setType(ConnectorType.AMOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			}
		} else if (this.input1.getAlternativeType() == ConnectorType.nothing) {
			if (this.input1.getType() == ConnectorType.SignalInput) {
				this.input0.setType(ConnectorType.SignalInput);
				this.input0.setAlternativeType(ConnectorType.nothing);
				this.output.setType(ConnectorType.SignalOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			} else {//input1 type is am
				this.input0.setType(ConnectorType.AMInput);
				this.input0.setAlternativeType(ConnectorType.nothing);
				this.output.setType(ConnectorType.AMOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			}
		}

		if (updateSource != parent) {
			if (parent instanceof CanvasOperator) {
				((CanvasOperator) parent).updateNeighbors(this);
			}
		}
		if (updateSource != child0) {
			if (child0 instanceof CanvasOperator) {
				((CanvasOperator) child0).updateNeighbors(this);
			}
		}
		if (updateSource != child1) {
			if (child1 instanceof CanvasOperator) {
				((CanvasOperator) child1).updateNeighbors(this);
			}
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (canvas.getMode() == CanvasMode.DELETE && this.mouseOver) {
			g.setColor(Color.RED);
			g.fillOval(0, 9, 50, 50);
			g.setColor(Color.WHITE);
			g.fillRect(11, 32, 29, 5);
			g.fillRect(23, 20, 5, 29);
		} else {
			g.drawLine(22, 19, 28, 19);
			g.drawLine(28, 19, 28, 31);
			g.drawLine(28, 31, 40, 31);
			g.drawLine(40, 31, 40, 37);
			g.drawLine(40, 37, 28, 37);
			g.drawLine(28, 37, 28, 49);
			g.drawLine(28, 49, 22, 49);
			g.drawLine(22, 49, 22, 37);
			g.drawLine(22, 37, 10, 37);
			g.drawLine(10, 37, 10, 31);
			g.drawLine(10, 31, 22, 31);
			g.drawLine(22, 31, 22, 19);
		}
	}

	@Override
	public void resetSubTree(boolean startHere) {
		//reset inputs and output to variable inputs/output
		this.output.setType(ConnectorType.SignalOutput);
		this.output.setAlternativeType(ConnectorType.AMOutput);
		this.input0.setType(ConnectorType.SignalInput);
		this.input0.setAlternativeType(ConnectorType.AMInput);
		this.input1.setType(ConnectorType.SignalInput);
		this.input1.setAlternativeType(ConnectorType.AMInput);
		if (this.child0 != null) {
			child0.resetSubTree(false);
		}
		if (this.child1 != null) {
			child1.resetSubTree(false);
		}
	}

	public boolean isVariable() {
		return true;
	}

	@Override
	public Signal getSignal() {
		return this.signal;
	}

	@Override
	public void updateSignalOrAmplitudeModulation() {
		if (this.input0.getAlternativeType() == ConnectorType.nothing && this.input1.getAlternativeType() == ConnectorType.nothing && this.child0 != null && this.child1 != null) { //both inputs are not variable and not empty
			if (this.input0.getType() == ConnectorType.AMInput && this.input1.getType() == ConnectorType.AMInput) { //case am
				this.signal = null; 
				if (this.child0.getAmplitudeModulation() != null && this.child1.getAmplitudeModulation() != null) {
					this.amplitudeModulation = new AddAmplitudeModulation(this.child0.getAmplitudeModulation(), this.child1.getAmplitudeModulation());
				} else {
					this.amplitudeModulation = null;
				}
			} else { //case both are of type signal
				this.amplitudeModulation = null;
				if (this.child0.getSignal() != null && this.child1.getSignal() != null) { 
					this.signal = new AddSignal(this.child1.getSignal(), this.child0.getSignal());				
				} else {
					this.signal = null;
				}
			}
		}
		if (this.parent != null) {
			this.parent.updateSignalOrAmplitudeModulation();
		}
	}

	@Override
	public AmplitudeModulation getAmplitudeModulation() {
		return this.amplitudeModulation;
	}

}