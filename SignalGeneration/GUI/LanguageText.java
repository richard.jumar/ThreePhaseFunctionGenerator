package GUI;

import java.util.HashMap;
import java.util.Map;

public class LanguageText {
	private Map<Language, String> textTranslations;
	public LanguageText(Language[] l, String[] text) {
		assert l.length == text.length;
		this.textTranslations = new HashMap<Language, String>();
		for (int i = 0; i < l.length; i++) {
			this.textTranslations.put(l[i], text[i]);
		}
	}
	
	public String getTranslation(Language l) {
		return this.textTranslations.get(l);
	}
}
