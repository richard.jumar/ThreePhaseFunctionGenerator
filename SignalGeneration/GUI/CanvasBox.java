package GUI;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

/**
 * canvas boxes are a part of the tree on the canvas. A canvas box could be a leaf or the root. 
 */
public abstract class CanvasBox extends JPanel implements MouseListener, MouseMotionListener, CanvasTreeElement {
	private static final long serialVersionUID = 8L;
	protected CanvasConnector input;
	protected CanvasConnector output;
	protected CanvasConnector connectedWith; 
	protected SymbolStatus status;
	private int mousePressX;
	private int mousePressY;
	protected Canvas canvas;
	private Color defaultColor;
	public CanvasBox(Canvas canvas) {
		super();
		this.canvas = canvas;
		this.setLayout(null);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.defaultColor = this.getBackground();
	}
	
	public abstract CanvasTreeElement getParentNode();
	
	/**
	 * canvas box has a parent in the tree
	 */
	public boolean hasParent() {
		if (getParentNode() == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public void updateSignalOrAmplitudeModulation() {
		//nothing, because signals or ams are independent here
	}
	
	/**
	 * canvas box has a child in the tree (only signal or am output)
	 * @return
	 */
	public boolean hasChild() {
		if (this.input != null && canvas.getConnectionOutputConnector(this.input) != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * bounds of the box
	 */
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		int xConnector = (width / 2) - 4;
		if (this.output != null) {
			this.output.setBounds(xConnector, 0, 9, 9);
		}
	}
	
	/**
	 * set the output connector
	 * @param output
	 */
	protected void setOutput(CanvasConnector output) {
		this.output = output;
		this.output.setConnectionPointOffset(4,0);
		this.add(this.output);
	}
	
	/**
	 * set the input connector
	 * @param input
	 */
	protected void setInput(CanvasConnector input) {
		this.input = input;
		this.input.setBounds(55, 41, 9, 9);
		this.input.setConnectionPointOffset(4, 8);
		this.add(this.input);
	}

	/**
	 * get the input connector
	 * @return
	 */
	public CanvasConnector getInputConnector() {
		return this.input;
	}
	
	/**
	 * highlight fitting connectors for the given connector
	 * @param cc connector
	 */
	public void highlightFor(CanvasConnector cc) {
		if (input != null) {
			input.highlightFor(cc);
		}
		if (output != null) {
			output.highlightFor(cc);
		}
	}
	
	/**
	 * stop connector highlighting
	 */
	public void stopHighlight() {
		if (input != null) {
			input.stopHighlight();
		}
		if (output != null) {
			output.stopHighlight();
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (canvas.getMode() == CanvasMode.DELETE) {
			canvas.removeTreeElement(this);
			canvas.updateUI();
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (canvas.getMode() == CanvasMode.DELETE) {
			this.setBackground(Color.RED);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setBackground(this.defaultColor);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.mousePressX = e.getX();
		this.mousePressY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (canvas.getMode() == CanvasMode.MOUSE && canvas.isEnabled()) {
			this.setLocation(canvas.getMousePosition().x - this.mousePressX, canvas.getMousePosition().y - this.mousePressY);	
			canvas.updateUI();
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}
	
	@Override
	public void setMousePressedActive(boolean active) {
		
	}
	
	public boolean getMousePressedActive() {
		return false;
	}
	
	/**
	 * connect this canvas box
	 */
	public void setConnected(CanvasConnector myConnector, CanvasConnector otherConnector) {
		this.connectedWith = otherConnector;		
		if (this.fullDefined()) {
			this.status.setStatusDefindedAndConnected();
		}
		canvas.updateTopLevelElement();
	}
	
	/**
	 * remove a connection from this canvas box
	 */
	public void removeConnection(CanvasConnector myConnector) {
		this.connectedWith = null;
		if (this.fullDefined()) {
			this.status.setStatusDefined();
		} else {
			this.status.setStatusUndefined();
		}
		canvas.updateTopLevelElement();
	}
	
	/**
	 * retype the subtree
	 */
	public void retypeSubTree() {
		if (this.getParentNode() != null) {
			if (this.getParentNode() instanceof CanvasOperator) {
				((CanvasOperator)this.getParentNode()).updateNeighbors(null);
			}
		}
	}

	/**
	 * overwrite setEnabled method to enable all child components
	 */
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for (int i = 0; i < this.getComponentCount(); i++) {
			this.getComponent(i).setEnabled(enabled);
		}
	}
}
