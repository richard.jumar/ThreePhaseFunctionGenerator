package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdom2.Element;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Phasor;
import File.XmlFileReaderData;

public class CanvasBoxAmplitudeModulationImport extends CanvasBoxAmplitudeModulationInput {
	private static final long serialVersionUID = 9L;
	private JLabel lbl_embedded;
	private boolean embedded;
	private AmplitudeModulation am;
	private XmlFileReaderData xml;
	private String path;
	private List<Phasor> usedPhasors;
	private List<Phasor> unknownPhasors;
	private Element phasorImportElement;
	private JTextField edt_newName;
	private JComboBox<Phasor> cbo_phasor;
	private JLabel lbl_replace;
	private JLabel lbl_rename;
	private JButton btn_next;
	private JPanel panel_option;
	private JRadioButton radio_transfer;
	private JRadioButton radio_rename;
	private JDialog phasorDialog;
	private JLabel lbl_phasorName;
	private JButton btn_newPhasor;
	private JButton btn_newPhasorInfo;
	private int phasorCounter;
	private JLabel lbl_title;
	
	public CanvasBoxAmplitudeModulationImport(Canvas canvas, int x, int y, boolean embedded, String path, Element subRootElement, Element phasorImportElement, List<Phasor> availablePhasors) {
		super(canvas);
		this.usedPhasors = new ArrayList<Phasor>();
		if (phasorImportElement == null) {
			this.phasorImportElement = new Element("phasorimport");
		} else {
			this.phasorImportElement = phasorImportElement;
		}
		this.lbl_title = new JLabel();
		this.lbl_title.setBounds(5, 15, 190, 15);
		this.add(this.lbl_title);
		this.setBounds(x, y, 150, 50);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		this.status = new SymbolStatus();
		this.status.setBounds(130, 1, 19, 19);
		this.add(status);
		this.lbl_embedded = new JLabel();
		this.lbl_embedded.setBounds(5, 30, 190, 15);
		this.add(lbl_embedded);
		this.setEmbedded(embedded); //important for saving.
		if (!path.equals("")) { //case import from file (not embedded or first use)
			this.path = path;
			this.xml = new XmlFileReaderData(path);
			xml.builtData();
		} else { //case import from xml-Element
			this.path = "";
			this.xml = new XmlFileReaderData(subRootElement, null, null, null); 
		}
		this.am = xml.getAmplitudeModulation();
		if (this.am == null) {
			this.status.setStatusUndefined();
		} else {
			//import phasors
			this.unknownPhasors = new ArrayList<Phasor>();
			for (int i = 0; i < xml.getUsedPhasors().size(); i++) {
				if (phasorImportElement == null || (! isPhasorImported(xml.getUsedPhasors().get(i), phasorImportElement))) {
					if (availablePhasors == null || (!availablePhasors.contains(xml.getUsedPhasors().get(i)))) {
						this.unknownPhasors.add(xml.getUsedPhasors().get(i));
					} else {
						this.usedPhasors.add(xml.getUsedPhasors().get(i));
					}
				}
			}
			for (int i = 0; i < xml.getUsedPhasors().size(); i++) {
				if (phasorImportElement != null && isPhasorImported(xml.getUsedPhasors().get(i), phasorImportElement)) {
					//Don't do that in the for loop before! (problems in the case of name swapping)
					xml.getAmplitudeModulation().replacePhasor(xml.getUsedPhasors().get(i), substitutePhasor(xml.getUsedPhasors().get(i)));
				}
			}
			if (phasorImportElement != null && this.unknownPhasors.size() > 0) {
				JOptionPane.showMessageDialog(Main.getMainwindow(), "In einer nicht eingebetteten Datei wurden Drehzeiger�nderungen vorgenommen.");
			}
			if (unknownPhasors.size() > 0) {
				phasorImport();
			}
			if (this.connectedWith != null) {
				this.status.setStatusDefindedAndConnected();
			} else {
				this.status.setStatusDefined();
			}
		}

		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				JPopupMenu m = new JPopupMenu();
				JMenuItem phasorInfo = new JMenuItem("Drehzeigerinfo");
				phasorInfo.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						DefaultListModel<Phasor> usedPhasorListModel = new DefaultListModel<Phasor>();
						for (int i = 0; i < usedPhasors.size(); i++) {
							usedPhasorListModel.addElement(usedPhasors.get(i));							
						}
						JDialog dialog = new JDialog();
						dialog.setBounds(e.getX(), e.getY(), 200, 200);
						dialog.setLayout(null);
						JList<Phasor> list = new JList<Phasor>();
						list.setModel(usedPhasorListModel);
						list.setBounds(5, 0, 175, 135);
						dialog.add(list);
						JButton btn_edit = new JButton("Details");
						btn_edit.setBounds(5, 140, 100, 20);
						btn_edit.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (list.getSelectedIndex() >= 0) {
									Main.getMainwindow().getPhasorPanel().showPhasorInfo(list.getSelectedValue());
								}
							}							
						});
						dialog.add(btn_edit);
						dialog.setVisible(true);
					}		
				});
				m.add(phasorInfo);
				m.show(getThis(), e.getX(), e.getY());
			}
		});

	}
	
	private void phasorImport() {
		
		phasorDialog = new JDialog();
		phasorDialog.setBounds(500, 300, 400, 280);
		phasorDialog.setLayout(null);
		phasorDialog.setModal(true);
		JLabel lbl_info; 
		if (this.unknownPhasors.size() == 1) {
			lbl_info = new JLabel ("Es wurde ein Drehzeiger gefunden.");
		} else {
			lbl_info = new JLabel("Es wurden " + this.unknownPhasors.size() + " Drehzeiger gefunden.");
		}
		lbl_info.setBounds(5, 10, 350, 15);
		phasorDialog.add(lbl_info);
		phasorCounter = -1;
		panel_option = new JPanel();
		panel_option.setLayout(null);
		panel_option.setBounds(5, 40, 380, 160);
		phasorDialog.add(panel_option);
		lbl_phasorName = new JLabel();
		lbl_phasorName.setBounds(10, 25, 190, 25);
		panel_option.add(lbl_phasorName);
		JButton btn_phasorInfo = new JButton("Details");
		btn_phasorInfo.setBounds(200, 25, 80, 25);
		btn_phasorInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.getMainwindow().getPhasorPanel().showPhasorInfo(unknownPhasors.get(phasorCounter));
			}			
		});
		panel_option.add(btn_phasorInfo);
		ButtonGroup btnGroup_phasor = new ButtonGroup();
		this.radio_transfer = new JRadioButton("Drehzeiger �bernehmen");
		radio_transfer.setBounds(10, 50, 200, 20);
		radio_transfer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_replace.setVisible(false);
				lbl_rename.setVisible(false);
				edt_newName.setVisible(false);
				cbo_phasor.setVisible(false);
				btn_newPhasorInfo.setVisible(false);
				btn_newPhasor.setVisible(false);
			}
		});
		btnGroup_phasor.add(radio_transfer);
		panel_option.add(radio_transfer);
		radio_rename = new JRadioButton("Drehzeiger umbenennen");
		radio_rename.setBounds(10, 70, 200, 20);
		radio_rename.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_replace.setVisible(false);
				lbl_rename.setVisible(true);
				edt_newName.setVisible(true);
				cbo_phasor.setVisible(false);
				btn_newPhasorInfo.setVisible(false);
				btn_newPhasor.setVisible(false);
			}
		});
		btnGroup_phasor.add(radio_rename);
		panel_option.add(radio_rename);
		JRadioButton radio_replace = new JRadioButton("Drehzeiger durch anderen Drehzeiger ersetzen");
		radio_replace.setBounds(10, 90, 300, 20);
		radio_replace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_replace.setVisible(true);
				lbl_rename.setVisible(false);
				edt_newName.setVisible(false);
				cbo_phasor.setVisible(true);
				btn_newPhasorInfo.setVisible(true);
				btn_newPhasor.setVisible(true);
			}
		});
		btnGroup_phasor.add(radio_replace);
		panel_option.add(radio_replace);
		this.lbl_rename = new JLabel("Neue Bezeichnung");
		this.lbl_rename.setBounds(10, 120, 105, 20);
		panel_option.add(this.lbl_rename);
		this.lbl_replace = new JLabel("Ersatz-Drehzeiger");
		this.lbl_replace.setBounds(10, 120, 105, 20);
		panel_option.add(this.lbl_replace);
		this.edt_newName = new JTextField();
		this.edt_newName.setBounds(120, 120, 100, 20);
		panel_option.add(this.edt_newName);
		this.cbo_phasor = new JComboBox<Phasor>();
		updateCbo_phasor();
		this.cbo_phasor.setBounds(120,  120, 100, 20);
		panel_option.add(this.cbo_phasor);
		btn_newPhasorInfo = new JButton("Details");
		btn_newPhasorInfo.setBounds(225, 120, 75, 20);
		btn_newPhasorInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cbo_phasor.getSelectedIndex() >= 0) {
					Main.getMainwindow().getPhasorPanel().showPhasorInfo((Phasor)cbo_phasor.getSelectedItem());
				}
			}
		});
		btn_newPhasorInfo.setVisible(false);
		panel_option.add(btn_newPhasorInfo);	
		btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu..."));
		btn_newPhasor.setBounds(305, 120, 70, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
			}			
		});
		btn_newPhasor.setVisible(false);
		panel_option.add(btn_newPhasor);
		this.btn_next = new JButton(LanguageTranslator.getTranslation("weiter"));
		this.btn_next.setBounds(300, 215, 80, 20);
		this.btn_next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radio_transfer.isSelected()) {
					usedPhasors.add(unknownPhasors.get(phasorCounter));
					Element phasorTransferElement = new Element("phasortransfer").addContent(unknownPhasors.get(phasorCounter).getName());
					phasorImportElement.addContent(phasorTransferElement);
					nextPhasor();
				} else if (radio_rename.isSelected()) {
					if (DataControl.getClock().getPhasor(edt_newName.getText()) == null) {
						if (usedPhasorExists(edt_newName.getText())) {
							JOptionPane.showMessageDialog(phasorDialog, "Ein anderer zu importierender Drehzeiger tr�gt diesen Namen bereits.");
						} else {
							if (edt_newName.getText().trim().equals("")) {
								JOptionPane.showMessageDialog(phasorDialog, "Die Bezeichnung f�r den neuen Drehzeiger ist nicht zul�ssig.");
							} else {
								Element oldPhasorNameElement = new Element("oldname").addContent(unknownPhasors.get(phasorCounter).getName());
								Element newPhasorNameElement = new Element("newname").addContent(edt_newName.getText());
								unknownPhasors.get(phasorCounter).setName(edt_newName.getText());
								usedPhasors.add(unknownPhasors.get(phasorCounter));
								
								Element phasorRenameElement = new Element("phasorrename");
								phasorRenameElement.addContent(oldPhasorNameElement);
								phasorRenameElement.addContent(newPhasorNameElement);
								phasorImportElement.addContent(phasorRenameElement);
								nextPhasor();
							}
						}
					} else {
						JOptionPane.showMessageDialog(phasorDialog, LanguageTranslator.getTranslation("Die gew�nschte Bezeichnung f�r den Drehzeiger ist bereits vergeben."));
					}
				} else if (radio_replace.isSelected()) {
					if (cbo_phasor.getSelectedIndex() >= 0) {
						xml.getSignal().replacePhasor(unknownPhasors.get(phasorCounter), (Phasor) cbo_phasor.getSelectedItem());
						usedPhasors.add((Phasor) cbo_phasor.getSelectedItem());
						Element phasorReplaceElement = new Element("phasorreplace");
						Element oldPhasorElement = new Element("oldphasor").addContent(unknownPhasors.get(phasorCounter).getName());
						Element newPhasorElement = new Element("newphasor").addContent(((Phasor) cbo_phasor.getSelectedItem()).getName());
						phasorReplaceElement.addContent(oldPhasorElement);
						phasorReplaceElement.addContent(newPhasorElement);
						phasorImportElement.addContent(phasorReplaceElement);
						nextPhasor();
					} else {
						JOptionPane.showMessageDialog(phasorDialog, LanguageTranslator.getTranslation("Es wurde kein Drehzeiger ausgew�hlt"));
					}
				}
			}			
		});
		phasorDialog.add(btn_next);
		phasorDialog.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				
			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				
			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				canvas.removeTreeElement(getThis());
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		nextPhasor();
		phasorDialog.setVisible(true);
}

	private Phasor substitutePhasor(Phasor p) {
		for (int i = 0; i < phasorImportElement.getChildren("phasortransfer").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasortransfer").get(i).getText())) {
				return DataControl.getClock().getPhasor(p.getName());
			}
		}
		for (int i = 0; i < phasorImportElement.getChildren("phasorrename").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasorrename").get(i).getChildText("oldname"))) {
				return DataControl.getClock().getPhasor(phasorImportElement.getChildren("phasorrename").get(i).getChildText("newname"));
			}
		}
		for (int i = 0; i < phasorImportElement.getChildren("phasorreplace").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasorreplace").get(i).getChildText("oldphasor"))) {
				return DataControl.getClock().getPhasor(phasorImportElement.getChildren("phasorreplace").get(i).getChildText("newphasor"));
			}
		}
		return null;
	}

	
	private boolean isPhasorImported(Phasor p, Element phasorImportElement) {
		for (int i = 0; i < phasorImportElement.getChildren("phasortransfer").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasortransfer").get(i).getText())) {
				return true;
			}
		}
		for (int i = 0; i < phasorImportElement.getChildren("phasorrename").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasorrename").get(i).getChildText("oldname"))) {
				return true;
			}
		}
		for (int i = 0; i < phasorImportElement.getChildren("phasorreplace").size(); i++) {
			if (p.getName().equals(phasorImportElement.getChildren("phasorreplace").get(i).getChildText("oldphasor"))) {
				return true;
			}
		}
		return false;
	}
	
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		cbo_phasor.updateUI();
	}
	
	private void nextPhasor() {
		if (phasorCounter >= this.xml.getUsedPhasors().size() - 1) { // last iteration
			for (int i = 0; i < this.usedPhasors.size(); i++) {
				DataControl.addPhasor(this.usedPhasors.get(i));
			}
			this.phasorDialog.dispose();
		} else {
			this.phasorCounter++;
			phasorDialogLabeling();
		}
	}
	
	private void phasorDialogLabeling() {
		this.panel_option.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger") + " " + (phasorCounter + 1) + " " + LanguageTranslator.getTranslation("von") + " " + xml.getUsedPhasors().size()));
		this.lbl_phasorName.setText("Bezeichnung: " + xml.getUsedPhasors().get(phasorCounter));
		if (DataControl.getClock().getPhasor(xml.getUsedPhasors().get(phasorCounter).getName()) == null) { //no phasor with this name exists => phasor is able to be imported
			this.radio_transfer.setEnabled(true);
			this.radio_transfer.setSelected(true);
			this.edt_newName.setVisible(false);
			this.lbl_rename.setVisible(false);
		} else {
			this.radio_transfer.setEnabled(false);
			this.radio_rename.setSelected(true);
			this.edt_newName.setVisible(true);
			this.lbl_rename.setVisible(true);
		}
		this.cbo_phasor.setVisible(false);
		this.lbl_replace.setVisible(false);
		this.edt_newName.setText("");
	}
	
	private CanvasBoxAmplitudeModulationInput getThis() {
		return this;
	}
	

	public boolean usedPhasorExists(String name) {
		for (int i = 0; i < this.usedPhasors.size(); i++) {
			if (this.usedPhasors.get(i).getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * set mode embedded (true) or linked (false)
	 * @param embedded 
	 */
	private void setEmbedded(boolean embedded) {
		this.embedded = embedded;
		if (embedded) {
			this.lbl_embedded.setText("Datei eingebettet");
		} else {
			this.lbl_embedded.setText("Datei verlinkt");
		}
	}

	/**
	 * amplitude modulation is full defined
	 * @return full defined
	 */
	public boolean fullDefined() {
		return (this.am != null);
	}

	/**
	 * get the root element of the child tree
	 * @return
	 */
	public Element getSubRootElement() {
		return xml.getRoot();
	}
	
	/**
	 * is the mode embedded? 
	 * @return true for embedded, false for linked
	 */
	public boolean isEmbedded() {
		return this.embedded;
	}
	
	/**
	 * get the path of the (linked) file of the child tree
	 * @return
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * get the resulting amplitude modulation of the sub tree
	 */
	public AmplitudeModulation getAmplitudeModulation() {
		return this.am;
	}

	/**
	 * check whether the deleting of a phasor is ok for the subtree
	 * @param phasorToDelete
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		for (int i = 0; i < this.usedPhasors.size(); i++) {
			if (this.usedPhasors.get(i) == phasorToDelete) {
				return false;
			}
		} 
		return true;
	}
	
	public void setTitle(String title) {
		this.lbl_title.setText(title);
	}
	
	public String getTitle() {
		return this.lbl_title.getText();
	}
}
