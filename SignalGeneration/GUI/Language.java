package GUI;

public enum Language {
	Deutsch("de"), English("en");
	
	private final String abbr;
	
	Language(String abbr) {
		this.abbr = abbr;
	}
	
	public static Language getLanguage(String abbr) {
		for (Language l : Language.values()) {
			if (l.abbr.equals(abbr)) {
				return l;
			}
		}
		return null;
	}
}
