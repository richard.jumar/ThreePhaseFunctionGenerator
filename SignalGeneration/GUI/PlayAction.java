package GUI;

import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import Controller.DataControl;
import Controller.FileControl;
import Controller.HardwareControl;
import Data.Filter;
import Monitor.MonitorWindow;
import Player.SignalPlayer;
import Player.HardwareConfiguration;


/**
 * Action for the playback of a signal
 */
public class PlayAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 45L;

	/**
	 * constructor
	 */
	public PlayAction() {
		super(LanguageTranslator.getTranslation("Abspielen"), new ImageIcon("GUI/start.png"));
	}

	private SignalPlayer player;
	private MonitorWindow mw;
	private StopAction stopAction;
	private String status_msg_before_playing;
	
	/**
	 * start playback action
	 */
	public void actionPerformed(ActionEvent arg0) {

		if (DataControl.getTopLevelSignal() != null) {
			HardwareConfiguration hwConfig = HardwareControl.getHardwareConfiguration();
			if (hwConfig != null) {
				Filter filter = null;
				if (HardwareControl.cbx_filter.isSelected()) {
					String filterPath = FileControl.ini.read("GUI", "workspace") + "/" + FileControl.ini.read("GUI", "filterSubDir") + "/" + HardwareControl.cbo_filter.getSelectedItem();
					filter = new Filter(new File(filterPath));
					int readFilterFile = filter.readFile();
					//1: ok, 0: file error, -1: no/wrong filter type (Discrete-Time FIR Filter expected), -2: no/wrong filter structure (expected Direct-Form FIR), -3: Filter Length is missing, -4: wrong coefficient format (expected Decimal), -5: Missing "Numerator" Line, -6: Not enough/invalid values
					if (readFilterFile == -1) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Der Filtertyp muss \"Discrete-Time FIR Filter\" sein."));
						return;
					} else if (readFilterFile == -2) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Die Filterstruktur muss \"Direkt-Form FIR\" sein."));
						return;
					} else if (readFilterFile == -3) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Es konnte keine L�ngenangabe in der Filterdatei gefunden werden"));
						return;
					} else if (readFilterFile == -4) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Die Koeffizienten m�ssen im Dezimalformat angegeben werden."));
						return;
					} else if (readFilterFile == -5) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Die \"Numerator\"-Zeile konnte nicht in der Filterdatei gefunden werden."));
						return;
					} else if (readFilterFile == -6) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Es konnten nicht gen�gend g�ltige Filterkoeffizienten gefunden werden."));
						return;
					} else if (readFilterFile == 0) {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Die Filterdatei " + filterPath + " konnte nicht ge�ffnet werden."));
						return;
					} else { //readFilterFile == 1
						//all ok
					}
				} else {
					filter = new Filter();
				}
				status_msg_before_playing = HardwareControl.lbl_status.getText();
				this.player = new SignalPlayer(DataControl.getTopLevelSignal(), hwConfig, filter);
				if (HardwareControl.cbx_monitor.isSelected()) {
					this.mw = new MonitorWindow();
					Thread t1 = new Thread(mw);
					t1.start();
				}
				SignalPlayer.log("Start playing");
				DataControl.getClock().reset();
				Thread t2 = new Thread(player);
				t2.start();
				this.setEnabled(false);
				HardwareControl.cbx_monitor.setEnabled(false);
				HardwareControl.cbo_technologie.setEnabled(false);
				HardwareControl.cbo_soundCard.setEnabled(false);
				HardwareControl.cbx_filter.setEnabled(false);
				HardwareControl.cbo_filter.setEnabled(false);
				HardwareControl.pbr_buffer.setEnabled(true);
				Main.getMainwindow().getPhasorPanel().setEnabled(false);
				Main.getMainwindow().setWorkspaceTreeEnabled(false);
				Main.getMainwindow().enableSignalAndAmPanels(false);
				if (this.stopAction != null) {
					this.stopAction.setEnabled(true);
					Main.getMainwindow().enableCanvas(false);
				}
				
			} else {
				JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Bitte w�hlen Sie eine g�ltige Hardwarekonfiguration"));
			}
		} else if (DataControl.getTopLevelAmplitudeModulation() != null) {
			JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Es k�nnen nur Signale wiedergegeben werden."));
		} else {
			JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Das Signal ist nicht vollst�ndig definiert."));
		}
	}
	
	/**
	 * actions for the stop of the playback
	 */
	public void stop() {
		if (this.mw != null) {
			this.mw.activateStop();
			this.mw = null;
		} 
		this.player.stop();
		HardwareControl.lbl_status.setText(status_msg_before_playing);
		if (status_msg_before_playing.equals(LanguageTranslator.getTranslation("L�schmodus"))) {
			Main.getMainwindow().setWorkspaceTreeEnabled(false);
			Main.getMainwindow().enableSignalAndAmPanels(false);
		} else {
			Main.getMainwindow().setWorkspaceTreeEnabled(true);
			Main.getMainwindow().enableSignalAndAmPanels(true);
		}
		Main.getMainwindow().getPhasorPanel().setEnabled(true);
	}

	/**
	 * set the stop action for this play action
	 * @param stopAction
	 */
	public void setStopAction(StopAction stopAction) {
		this.stopAction = stopAction;
	}
}
