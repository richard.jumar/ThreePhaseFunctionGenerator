package GUI;

public class GUIMath {
	/**
	 * Round a double value to a number of decimals
	 * @param value
	 * @param decimals
	 * @return
	 */
	public static String roundDecimals(double value, int decimals) {
		String value_str = value + "";
		String[] parts = value_str.split("\\.");
		if (parts.length == 1) {
			return parts[0];
		} else if (parts.length == 2) {
			if (parts[1].length() < decimals) {
				decimals = parts[1].length(); //decimals only if necessary
			}
			return parts[0] + "," + parts[1].substring(0, decimals);
		}
		return "ERROR";
	}
	
	/**
	 * Compute the angle phi (IEEE Std C37.118.1-2011)
	 * format is [0�; 360�)
	 * @param sin
	 * @param cos
	 * @return
	 */
	public static String phiInDegreeFromSinCos(double sin, double cos) {
		if (cos >= 0) {
			int phiMilliDegree = (int) Math.round(((Math.atan(-sin / cos) / (2 * Math.PI)) * 360000));
			if (phiMilliDegree < 0) {
				phiMilliDegree += 360000;
			}
			return ((phiMilliDegree / 1000) + "," + (phiMilliDegree % 1000) + "�");
		} else {
			int phiMilliDegree = 180000 + (int) Math.round(((Math.atan(-sin / cos) / (2 * Math.PI)) * 360000));
			return ((phiMilliDegree / 1000) + "," + (phiMilliDegree % 1000) + "�");
		}
	}
	

	
}
