package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Frequency;
import Data.NonPeriodicInterpolation;
import Data.NonPeriodicInterpolationPoint;
import Data.NonPeriodicLinearInterpolation;
import Data.NonPeriodicPointInterpolationAmplitudeModulation;
import Data.NonPeriodicSplineInterpolation;
import Data.PeriodicLinearInterpolation;
import Data.PeriodicPointInterpolationAmplitudeModulation;
import Data.PeriodicSplineInterpolation;
import Data.Phasor;

/**
 * Canvas box for a point interpolation amplitude modulation
 */
public class CanvasBoxAmplitudeModulationPointInterpolation extends CanvasBoxAmplitudeModulationInput implements CanvasBoxPointInterpolation {
	private static final long serialVersionUID = 12L;
	private JLabel lbl_periodic;
	private JButton btn_edit;
	private JComboBox<Phasor> cbo_phasor;
	private JComboBox<String> cbo_interpolationType;
	private JLabel lbl_max;
	private JLabel lbl_zero;
	private JLabel lbl_min;
	private PointInterpolationCanvas pi_canvas;
	private Canvas canvas;
	private JDialog mainDialog;
	private JPanel phasorPanel;
	private JPanel startEndPanel;
	private JPanel amplitudePanelPeriodic;
	private JPanel amplitudePanelNonPeriodic;
	private JTextField edt_maxValue;
	private JTextField edt_minValue;
	private JTextField edt_NPValue; //NP: non periodic
	private PeriodicPointInterpolationAmplitudeModulation periodicInterpolation;
	private NonPeriodicPointInterpolationAmplitudeModulation nonPeriodicInterpolation;
	private JLabel lbl_phasor;
	private SymbolPhasorMini phasorSymbol;
	private JLabel lbl_start_end;
	private JLabel lbl_intType;
	private JLabel lbl_points;
	private JLabel lbl_minMaxValue;
	private GraphPanel gp_box;
	private JTextField edt_startTime;
	private JTextField edt_endTime;
	private JRadioButton radio_zeroMeanMax;
	private JRadioButton radio_zeroMeanMin;
	private JRadioButton radio_min;
	private JRadioButton radio_max;
	private JRadioButton radio_periodic;
	private JRadioButton radio_nonPeriodic;
	
	/**
	 * constructor
	 * @param canvas canvas
	 * @param x x position
	 * @param y y position
	 */
	public CanvasBoxAmplitudeModulationPointInterpolation(Canvas canvas, int x, int y) {
		super(canvas);
		this.canvas = canvas;
		pi_canvas = new PointInterpolationCanvas(this);
		pi_canvas.setBounds(5, 105, 1080, 400);		
		this.periodicInterpolation = new PeriodicPointInterpolationAmplitudeModulation(null); // doesn't work. Set phasor before using...
		this.nonPeriodicInterpolation = new NonPeriodicPointInterpolationAmplitudeModulation(DataControl.getClock(), -1, pi_canvas.getWidth());
		this.setLayout(null);
		this.setBounds(x, y, 200, 200);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Punktinterpolation"));
		title.setBounds(5, 10, 1050, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.add(status);
		this.lbl_periodic = new JLabel();
		this.lbl_periodic.setBounds(105, 0, 95, 15);
		this.add(lbl_periodic);
		this.lbl_periodic.setVisible(false);
		phasorSymbol = new SymbolPhasorMini();
		phasorSymbol.setBounds(6, 27, 13, 13);
		this.add(phasorSymbol);
		lbl_phasor = new JLabel("<" + LanguageTranslator.getTranslation("nicht ausgew�hlt") + ">");
		lbl_phasor.setBounds(23, 25, 175, 15);
		this.add(lbl_phasor);
		this.lbl_start_end = new JLabel();
		this.lbl_start_end.setVisible(false);
		this.lbl_start_end.setBounds(5, 25, 175, 15);
		this.add(lbl_start_end);
		this.lbl_intType = new JLabel("");
		this.lbl_intType.setBounds(5, 40, 190, 15);
		this.add(lbl_intType);
		this.lbl_points = new JLabel("0 " + LanguageTranslator.getTranslation("St�tzpunkte"));
		this.lbl_points.setBounds(5, 55, 190, 15);
		this.add(lbl_points);
		this.lbl_minMaxValue = new JLabel("");
		this.lbl_minMaxValue.setBounds(5, 70, 195, 15);
		this.add(lbl_minMaxValue);
		this.gp_box = new GraphPanel(this.periodicInterpolation);
		this.gp_box.setBounds(1, 90, 198, 80);
		this.add(gp_box);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		this.btn_edit.setBounds(1, 170, 198, 29);
		this.btn_edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
				
			}

		});
		this.add(btn_edit);
		this.lbl_max = new JLabel(LanguageTranslator.getTranslation("max."));
		this.lbl_zero = new JLabel("0");
		this.lbl_min = new JLabel(LanguageTranslator.getTranslation("min."));
		this.mainDialog = new JDialog();
		this.mainDialog.setTitle(LanguageTranslator.getTranslation("Punktinterpolation"));
		this.mainDialog.setModal(true);
		this.mainDialog.setLayout(null);
		this.mainDialog.setSize(1200, 600);
		JPanel intOptionPanel = new JPanel();
		intOptionPanel.setLayout(null);
		intOptionPanel.setBounds(5, 20, 250, 80);
		intOptionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Interpolationseinstellungen")));
		this.mainDialog.add(intOptionPanel);
		ButtonGroup btnGroup_periodic = new ButtonGroup();
		radio_periodic = new JRadioButton(LanguageTranslator.getTranslation("periodisch"));
		radio_periodic.setSelected(true);
		radio_periodic.setBounds(10, 20, 95, 20);
		radio_periodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setPeriodicMode();
			}
			
		});
		btnGroup_periodic.add(radio_periodic);
		intOptionPanel.add(radio_periodic);
		radio_nonPeriodic = new JRadioButton(LanguageTranslator.getTranslation("nichtperiodisch"));
		radio_nonPeriodic.setBounds(105, 20, 130, 20);
		radio_nonPeriodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setNonPeriodicMode();
			}
			
		});
		btnGroup_periodic.add(radio_nonPeriodic);
		intOptionPanel.add(radio_nonPeriodic);
		cbo_interpolationType = new JComboBox<String>();
		cbo_interpolationType.addItem(new String(LanguageTranslator.getTranslation("linear")));
		cbo_interpolationType.addItem(new String(LanguageTranslator.getTranslation("kubische Splines")));
		cbo_interpolationType.setBounds(10, 50, 150, 20);
		cbo_interpolationType.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pi_canvas.setInterpolationType(cbo_interpolationType.getSelectedIndex());
				pi_canvas.reinterpolate();
				lbl_intType.setText(cbo_interpolationType.getSelectedItem().toString());
			}
			
		});
		intOptionPanel.add(cbo_interpolationType);
		this.phasorPanel = new JPanel();
		this.phasorPanel.setLayout(null);
		this.phasorPanel.setBounds(260, 20, 250, 80);
		this.phasorPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger")));
		cbo_phasor = new JComboBox<Phasor>();
		updateCbo_phasor();
		cbo_phasor.setBounds(10, 20, 100, 20);
		this.phasorPanel.add(cbo_phasor);
		JButton btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu..."));
		btn_newPhasor.setBounds(120, 20, 80, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
				Phasor lastCreatedPhasor = Main.getMainwindow().getPhasorPanel().getLastCreatedPhasor();
				if (lastCreatedPhasor != null) {
					cbo_phasor.setSelectedItem(lastCreatedPhasor);
				}
			}
		});
		this.phasorPanel.add(btn_newPhasor);
		this.phasorPanel.setVisible(true);
		this.mainDialog.add(this.phasorPanel);
		this.startEndPanel = new JPanel();
		this.startEndPanel.setBounds(260, 20, 250, 80);
		this.startEndPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Zeitrahmen")));
		this.startEndPanel.setLayout(null);
		this.startEndPanel.setVisible(false);
		JLabel lbl_startTime = new JLabel(LanguageTranslator.getTranslation("Startzeit"));
		lbl_startTime.setBounds(10, 15, 100, 20);
		this.startEndPanel.add(lbl_startTime);
		JLabel lbl_endTime = new JLabel(LanguageTranslator.getTranslation("Endzeit"));
		lbl_endTime.setBounds(10, 40, 100, 20);
		this.startEndPanel.add(lbl_endTime);
		this.edt_startTime = new JTextField();
		this.edt_startTime.setBounds(80, 15, 100, 20);
		this.startEndPanel.add(this.edt_startTime);
		this.edt_endTime = new JTextField();
		this.edt_endTime.setBounds(80, 40, 100, 20);
		this.startEndPanel.add(this.edt_endTime);
		JLabel lbl_startUnit = new JLabel("s");
		lbl_startUnit.setBounds(180, 15, 10, 20);
		this.startEndPanel.add(lbl_startUnit);
		JLabel lbl_endUnit = new JLabel("s");
		lbl_endUnit.setBounds(180, 40, 100, 20);
		this.startEndPanel.add(lbl_endUnit);
		this.mainDialog.add(this.startEndPanel);
		this.amplitudePanelPeriodic = new JPanel();
		this.amplitudePanelPeriodic.setBounds(515, 20, 400, 80);
		this.amplitudePanelPeriodic.setLayout(null);
		this.amplitudePanelPeriodic.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Amplitude")));
		JLabel lbl_maxValue = new JLabel(LanguageTranslator.getTranslation("Maximaler Wert"));
		lbl_maxValue.setBounds(10, 20, 130, 20);
		this.amplitudePanelPeriodic.add(lbl_maxValue);
		this.edt_maxValue = new JTextField("0");
		this.edt_maxValue.setBounds(140, 20, 50, 20);
		this.amplitudePanelPeriodic.add(this.edt_maxValue);
		JButton btn_setMaxDCFree = new JButton(LanguageTranslator.getTranslation("Mittelwertfrei setzen"));
		btn_setMaxDCFree.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (edt_minValue.getText().equals("auto")) {
						edt_minValue.setText("0");
					}
					edt_maxValue.setText("auto");
				} catch (NumberFormatException e2) {
					e2.printStackTrace();
				}
			}
			
			
		});
		btn_setMaxDCFree.setBounds(195, 20, 175, 20);
		this.amplitudePanelPeriodic.add(btn_setMaxDCFree);
		JLabel lbl_minV = new JLabel(LanguageTranslator.getTranslation("Minimaler Wert"));
		lbl_minV.setBounds(10, 50, 130, 20);
		this.amplitudePanelPeriodic.add(lbl_minV);
		this.edt_minValue = new JTextField("0");
		this.edt_minValue.setBounds(140, 50, 50, 20);
		this.amplitudePanelPeriodic.add(this.edt_minValue);
		
		JButton btn_setMinDCFree = new JButton(LanguageTranslator.getTranslation("Mittelwertfrei setzen"));
		btn_setMinDCFree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (edt_maxValue.getText().equals("auto")) {
						edt_maxValue.setText("0");
					}
					edt_minValue.setText("auto");
				} catch (NumberFormatException e2) {
					e2.printStackTrace();
				}
			}
		});
		btn_setMinDCFree.setBounds(195, 50, 175, 20);
		this.amplitudePanelPeriodic.add(btn_setMinDCFree);
		this.mainDialog.add(this.amplitudePanelPeriodic);
		this.amplitudePanelNonPeriodic = new JPanel();
		this.amplitudePanelNonPeriodic.setBounds(515, 20, 430, 80);
		this.amplitudePanelNonPeriodic.setLayout(null);
		this.amplitudePanelNonPeriodic.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Amplitude")));
		ButtonGroup btnGroup_amplitude = new ButtonGroup();
		radio_zeroMeanMax = new JRadioButton(LanguageTranslator.getTranslation("Mittelwertfrei, Maximum angeben"));
		radio_zeroMeanMax.setSelected(true);
		radio_zeroMeanMax.setBounds(10, 15, 240, 20);
		radio_zeroMeanMax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radio_zeroMeanMaxAction();
			}
		});
		btnGroup_amplitude.add(radio_zeroMeanMax);
		this.amplitudePanelNonPeriodic.add(radio_zeroMeanMax);
		radio_zeroMeanMin = new JRadioButton(LanguageTranslator.getTranslation("Mittelwertfrei, Minimum angeben"));
		radio_zeroMeanMin.setBounds(10, 35, 240, 20);
		radio_zeroMeanMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radio_zeroMeanMinAction();
			}
		});
		btnGroup_amplitude.add(radio_zeroMeanMin);
		this.amplitudePanelNonPeriodic.add(radio_zeroMeanMin);
		radio_min = new JRadioButton(LanguageTranslator.getTranslation("Minimum angeben"));
		radio_min.setBounds(250, 15, 150, 20);
		radio_min.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radio_maxAction();
			}
		});
		btnGroup_amplitude.add(radio_min);
		this.amplitudePanelNonPeriodic.add(radio_min);
		radio_max = new JRadioButton(LanguageTranslator.getTranslation("Maximum angeben"));
		radio_max.setBounds(250, 35, 150, 20);
		radio_max.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radio_maxAction();

			}
		});
		btnGroup_amplitude.add(radio_max);
		this.amplitudePanelNonPeriodic.add(radio_max);
		this.amplitudePanelNonPeriodic.setVisible(false);
		this.edt_NPValue = new JTextField("0");
		this.edt_NPValue.setBounds(10, 55, 50, 20);
		this.amplitudePanelNonPeriodic.add(this.edt_NPValue);
		
		this.mainDialog.add(this.amplitudePanelNonPeriodic);
		pi_canvas.setBackground(Color.WHITE);
		this.mainDialog.add(pi_canvas);
		this.mainDialog.add(this.lbl_min);
		this.mainDialog.add(this.lbl_max);
		this.mainDialog.add(this.lbl_zero);
		JButton btn_ok = new JButton(LanguageTranslator.getTranslation("OK"));
		btn_ok.setBounds(985, 510, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radio_periodic.isSelected()) {
					btn_ok_periodic_click();
				} else {
					btn_ok_nonperiodic_click();
				}
			}
		});
		this.mainDialog.add(btn_ok);
	}

	/**
	 * action of radio button max
	 */
	public void radio_maxAction() {
		pi_canvas.setModeZeroLine();
		pi_canvas.reinterpolate();
		pi_canvas.updateUI();
		updateMarkers();
	}
	
	/**
	 * action of radio button min
	 */
	private void radio_minAction() {
		pi_canvas.setModeZeroLine();
		pi_canvas.reinterpolate();
		pi_canvas.updateUI();
		updateMarkers();
	}
	
	/**
	 * action of radio button zero mean min
	 */
	private void radio_zeroMeanMinAction() {
		pi_canvas.setModeZeroMean();
		pi_canvas.reinterpolate();
		pi_canvas.updateUI();
		updateMarkers();
	}
	
	/**
	 * action of radio button zero mean max
	 */
	private void radio_zeroMeanMaxAction() {
		pi_canvas.setModeZeroMean();
		pi_canvas.reinterpolate();
		pi_canvas.updateUI();
		updateMarkers();
	}
	
	/**
	 * ok button action (case nonperiodic)
	 */
	public void btn_ok_nonperiodic_click() {
		nonPeriodicInterpolation.clearPoints();
		long startTime = 0;
		long endTime = 0;
		double microSecondsPerPixel = 0;
		try {
			startTime = Math.round(Double.parseDouble(this.edt_startTime.getText().replace(",", ".")) * 1000000);
			endTime = Math.round(Double.parseDouble(this.edt_endTime.getText().replace(",", ".")) * 1000000);
			microSecondsPerPixel = (double) (endTime - startTime) / (pi_canvas.getWidth() + 2); //connection pixels are not displayed (should not be set by the user)
			if (startTime < 0 || startTime >= endTime) {
				throw new IllegalArgumentException("no valid time period");
			}
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Bitte geben Sie einen g�ltigen Zeitraum an."));
			return;
		}
		double value = 0;
		double valuesPerPixel = 0;
		try {
			value = Double.parseDouble(this.edt_NPValue.getText().replace(",", "."));
			if (radio_zeroMeanMax.isSelected()) {
				valuesPerPixel = value / (pi_canvas.getMeanValue() - (double) pi_canvas.getMax());
			} else if (radio_zeroMeanMin.isSelected()) {
				valuesPerPixel = value / ((double) pi_canvas.getMeanValue() - pi_canvas.getMin()); //neg min value => pos valuesPerPixel
			} else if (radio_max.isSelected()) {
				valuesPerPixel = value / ((double) (pi_canvas.getZeroLinePosition() - pi_canvas.getMax()));
			} else { //radio_min
				valuesPerPixel = value / ((double) (pi_canvas.getZeroLinePosition() - pi_canvas.getMin()));
			}
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Bitte geben Sie einen g�ltigen Spannungswert an."));
			return;
		}
		this.periodicInterpolation = null;
		this.nonPeriodicInterpolation = new NonPeriodicPointInterpolationAmplitudeModulation(DataControl.getClock(), startTime, endTime);
		NonPeriodicInterpolation npi;
		if (this.cbo_interpolationType.getSelectedIndex() == 1) {
			npi = new NonPeriodicSplineInterpolation(startTime, endTime);
		} else {
			npi = new NonPeriodicLinearInterpolation(startTime, endTime);
		}
		if (radio_min.isSelected() || radio_max.isSelected()) {
			npi.setAbsoluteZeroMode(0);
		}
		this.nonPeriodicInterpolation.setInterpolation(npi);
		for (int i = 0; i < this.pi_canvas.getPoints().size(); i++) {
			if (this.radio_max.isSelected() || this.radio_min.isSelected()) {
				this.nonPeriodicInterpolation.addPoint(new NonPeriodicInterpolationPoint(Math.round((pi_canvas.getPoints().get(i).getX() + 1) * microSecondsPerPixel), (pi_canvas.getZeroLinePosition() - pi_canvas.getPoints().get(i).getY()) * valuesPerPixel));
			} else {
				this.nonPeriodicInterpolation.addPoint(new NonPeriodicInterpolationPoint(Math.round((pi_canvas.getPoints().get(i).getX() + 1) * microSecondsPerPixel), (pi_canvas.getMeanValue() - (double) pi_canvas.getPoints().get(i).getY()) * valuesPerPixel));
			}
		}
		if (this.nonPeriodicInterpolation.interpolate()) {		
			if (fullDefined()) {
				if (this.connectedWith != null) {
					this.status.setStatusDefindedAndConnected();
				} else {
					this.status.setStatusDefined();
				}
			}
			gp_box.setNonPeriodic(nonPeriodicInterpolation, startTime, endTime);
			gp_box.computeThumbnail();
			lbl_periodic.setText("");
			lbl_phasor.setVisible(false);
			phasorSymbol.setVisible(false);
			lbl_start_end.setText(GUIMath.roundDecimals((((double) startTime)/1000000),3) + "s " + LanguageTranslator.getTranslation("bis") + " " + GUIMath.roundDecimals((((double) endTime)/1000000),3) + "s");
			this.lbl_start_end.setVisible(true);
			if (this.radio_max.isSelected() || this.radio_min.isSelected()) {
				lbl_minMaxValue.setText("min=" + DisplayFunctions.roundDecimals(((pi_canvas.getZeroLinePosition() - pi_canvas.getMin()) * valuesPerPixel), 3) + "   max="+ DisplayFunctions.roundDecimals(((pi_canvas.getZeroLinePosition() - pi_canvas.getMax()) * valuesPerPixel), 3));
			} else {
				lbl_minMaxValue.setText("min=" + DisplayFunctions.roundDecimals(((pi_canvas.getMeanValue() - pi_canvas.getMin()) * valuesPerPixel), 3) + "   max="+ DisplayFunctions.roundDecimals(((pi_canvas.getMeanValue() - pi_canvas.getMax()) * valuesPerPixel), 3));
			}
			updateUI();
			mainDialog.dispose();
			canvas.updateTopLevelElement();
		} else {
			JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Fehler bei der Interpolation"));
		}
		if (this.getParentNode() != null) {
			this.getParentNode().updateSignalOrAmplitudeModulation();
		}
	}
	
	/**
	 * ok button action (periodic case)
	 */
	public void btn_ok_periodic_click() {
		if (cbo_phasor.getSelectedItem() instanceof Phasor) {
			periodicInterpolation.clearPoints();
			double valuesPerPixel = 0;
			double minValue = 0;
			double maxValue = 0;
			pi_canvas.reinterpolate();
			pi_canvas.computeLineMinMax();
			if (edt_maxValue.getText().equals("auto") || edt_minValue.getText().equals("auto")) {
				if (edt_maxValue.getText().equals("auto")) {
					minValue = Double.parseDouble(edt_minValue.getText().replace(',', '.'));
					if (minValue < 0) {
						valuesPerPixel = (0 - minValue) / (pi_canvas.getMin() - pi_canvas.getMeanValue());
					} else {
						valuesPerPixel = minValue / (pi_canvas.getMin() - pi_canvas.getMeanValue());
					}
					
				} else if (edt_minValue.getText().equals("auto")) {
					maxValue = Double.parseDouble(edt_maxValue.getText().replace(',', '.'));
					if (maxValue < 0) {
						valuesPerPixel = (0 - maxValue) / (pi_canvas.getMeanValue() - pi_canvas.getMax());
					} else {
						valuesPerPixel = maxValue / (pi_canvas.getMeanValue() - pi_canvas.getMax());
					}
					
				}
				minValue = (pi_canvas.getMeanValue() - pi_canvas.getMin()) * valuesPerPixel;
				maxValue = (pi_canvas.getMeanValue() - pi_canvas.getMax()) * valuesPerPixel;

			} else {
				//min and max is given
				minValue = Double.parseDouble(edt_minValue.getText().replace(',', '.'));
				maxValue = Double.parseDouble(edt_maxValue.getText().replace(',', '.'));
								
			}
			if (pi_canvas.getInterpolationType() == 1) {
				periodicInterpolation.setInterpolation(new PeriodicSplineInterpolation());
			} else {
				periodicInterpolation.setInterpolation(new PeriodicLinearInterpolation());
			}
			
			periodicInterpolation.setPointList(DisplayFunctions.canvasValuesToDoubleValues(pi_canvas.getPoints(), edt_minValue.getText(), edt_maxValue.getText(), pi_canvas.getMin(), pi_canvas.getMax(), pi_canvas.getMeanValue(), pi_canvas.getWidth()));
			if (periodicInterpolation.interpolate()) {
				Canvas.canvasLog("Interpolation successfull");				
			} else {
				Canvas.canvasLog("Interpolation failed");
			}
			if (fullDefined()) {
				if (this.connectedWith != null) {
					this.status.setStatusDefindedAndConnected();
				} else {
					this.status.setStatusDefined();
				}
			}
			setPhasor();
			gp_box.setPeriodic(periodicInterpolation);
			gp_box.computeThumbnail();
			if (cbo_phasor.getSelectedIndex() != -1) {
				lbl_phasor.setText(cbo_phasor.getSelectedItem().toString() + " (" + Frequency.microHz_valueToHzString(((Phasor)cbo_phasor.getSelectedItem()).getFrequency().getF0_phase1(), 4) + ")");
				lbl_phasor.setVisible(true);
				phasorSymbol.setVisible(true);
			}
			this.lbl_start_end.setVisible(false);
			lbl_minMaxValue.setText("min=" + DisplayFunctions.roundDecimals(minValue, 3) + "   max="+ DisplayFunctions.roundDecimals(maxValue, 3));
			updateUI();
			mainDialog.dispose();
			
		} else {
			JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Ein Drehzeiger muss ausgew�hlt sein!"));
		}
		if (this.getParentNode() != null) {
			this.getParentNode().updateSignalOrAmplitudeModulation();
		}
		canvas.updateTopLevelElement();
	}
	
	/**
	 * set the phasor from the combobox
	 */
	public void setPhasor() {
		setPhasor((Phasor) cbo_phasor.getSelectedItem());
	}
	
	public void setEdt_NPValueText(String text) {
		this.edt_NPValue.setText(text);
	}

	/**
	 * set the phasor
	 * @param phasor
	 */
	public void setPhasor(Phasor phasor) {
		periodicInterpolation.setPhasor(phasor);
		lbl_phasor.setText(phasor + " (" + Frequency.microHz_valueToHzString(((Phasor)cbo_phasor.getSelectedItem()).getFrequency().getF0_phase1(), 4) + ")"); 
		lbl_phasor.updateUI();
		cbo_phasor.setSelectedItem(phasor);
	}
	
	/**
	 * get the selected phasor
	 * @return
	 */
	public Phasor getSelectedPhasor() {
		return periodicInterpolation.getPhasor();
	}
	
	/**
	 * show the edit dialog
	 */
	private void showDialog() {
		updateCbo_phasor();
		this.mainDialog.setVisible(true);
	}

	/**
	 * Chooses the selected interpolation type (for external selection from the xml file)
	 * @param type 0 is linear 1 is cubic spline
	 */
	public void chooseInterpolationType(int type) {
		this.cbo_interpolationType.setSelectedIndex(type);
		this.pi_canvas.setInterpolationType(type);
	}
	
	/**
	 * update the phasor combo box
	 */
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		if (this.periodicInterpolation != null && this.periodicInterpolation.getPhasor() != null) {
			cbo_phasor.setSelectedItem(this.periodicInterpolation.getPhasor());
			Canvas.canvasLog("Pointinterpolation: Set phasor to " + this.periodicInterpolation.getPhasor());
		} else {
			Canvas.canvasLog("Pointinterpolation (Am): Phasor not found");
		}
	}
	
	/**
	 * update the markers
	 */
	public void updateMarkers() {
		if (pi_canvas != null) {
			if (pi_canvas.getMin() >= 0) {
				this.lbl_min.setBounds(8 + pi_canvas.getWidth(), pi_canvas.getMin() - 5 + pi_canvas.getBounds().y, 35, 15);
				this.lbl_min.setVisible(true);
			} else {
				this.lbl_min.setVisible(false);
			}
			if (pi_canvas.getMax() >= 0) {
				this.lbl_max.setBounds(8 + pi_canvas.getWidth(), pi_canvas.getMax() - 5 + pi_canvas.getBounds().y, 35, 15);
				this.lbl_max.setVisible(true);
			} else {
				this.lbl_max.setVisible(false);
			}
			if (pi_canvas.getZeroLinePosition() >= 0 && pi_canvas.getZeroLinePosition() < pi_canvas.getHeight()) {
				this.lbl_zero.setBounds(8 + pi_canvas.getWidth(), (int) Math.round(pi_canvas.getZeroLinePosition()) - 5 + pi_canvas.getBounds().y, 10, 15);
			}
		}	
	}
	
	/**
	 * set the text for the number of points on the canvas box
	 * @param text
	 */
	public void setLbl_pointsText(String text) {
		this.lbl_points.setText(text);
	}
	
	/**
	 * add a point to the point interpolation canvas
	 * @param p point
	 * @return
	 */
	public boolean addPointToPiCanvas(Point2D p) {
		return pi_canvas.addPoint(p);
	}
	
	/**
	 * set the text of the max and min edits
	 * @param edt_max
	 * @param edt_min
	 */
	public void setAmplitudeEdits(String edt_max, String edt_min) {
		this.edt_maxValue.setText(edt_max);
		this.edt_minValue.setText(edt_min);
	}
	
	public String getEdt_maxVoltageText() { //voltage is value (implementation of an abstract method)
		return this.edt_maxValue.getText();
	}
	
	public String getEdt_minVoltageText() { //voltage is value (implementation of an abstract method)
		return this.edt_minValue.getText();
	}

	/**
	 * get the amplitude modulation
	 */
	public AmplitudeModulation getAmplitudeModulation() {
		if (fullDefined()) {
			if (this.periodicInterpolation != null) {
				return this.periodicInterpolation;
			} else {
				return this.nonPeriodicInterpolation;
			}
		}
		return null;
	}
	
	public int getPeriodicInterpolationNumber() {
		return this.pi_canvas.getInterpolationType();
	}
	
	public List<Point2D> getPointInterpolationCanvasPointList() {
		return this.pi_canvas.getPoints();
	}
	
	/**
	 * get the minimum value of the point interpolation canvas
	 * @return
	 */
	public int getCanvasMin() {
		return this.pi_canvas.getMin();
	}
	
	/**
 	* get the minimum value of the point interpolation canvas
 	* @return
 	*/
	public int getCanvasMax() {
		return this.pi_canvas.getMax();
	}
	
	/**
	 * get the mean value of the point interpolation canvas
	 * @return
	 */
	public double getCanvasMean() {
		return this.pi_canvas.getMeanValue();
	}
	
	/**
	 * get the width of the point interpolation canvas
	 * @return
	 */
	public int getCanvasWidth() {
		return this.pi_canvas.getWidth();
	}
	
	public boolean isPeriodicSelected() {
		return this.radio_periodic.isSelected();
	}
	
	public String getEdt_NPVoltageText() { //voltage is value (implementation of an abstract method)
		return this.edt_NPValue.getText();
	}
	
	public String getEdt_startTimeText() {
		return this.edt_startTime.getText();
	}
	
	public String getEdt_endTimeText() {
		return this.edt_endTime.getText();
	}
	
	/**
	 * get the type of amplitude definition
	 * possible results: zeromeanmax, zeromeanmin, max, min
	 * @return
	 */
	public String getAmplitudeDefinitionType() {
		if (this.radio_zeroMeanMax.isSelected()) {
			return "zeromeanmax";
		} else if (this.radio_zeroMeanMin.isSelected()) {
			return "zeromeanmin";
		} else if (this.radio_max.isSelected()) {
			return "max";
		} else {
			return "min";
		}
	}

	
	public void setTimeEdits(String start, String end) {
		this.edt_startTime.setText(start);
		this.edt_endTime.setText(end);
	}
	
	/**
	 * activate the periodic mode
	 */
	public void setPeriodicMode() {
		startEndPanel.setVisible(false);
		phasorPanel.setVisible(true);
		pi_canvas.setPeriodic(true);
		
		pi_canvas.reinterpolate();
		amplitudePanelNonPeriodic.setVisible(false);
		amplitudePanelPeriodic.setVisible(true);
		periodicInterpolation = new PeriodicPointInterpolationAmplitudeModulation(null); // doesn't work. Set phasor before using...
		this.radio_periodic.setSelected(true);
	}
	
	/**
	 * activate the nonperiodic mode
	 */
	public void setNonPeriodicMode() {
		phasorPanel.setVisible(false);
		startEndPanel.setVisible(true);
		pi_canvas.setPeriodic(false);
		pi_canvas.reinterpolate();
		amplitudePanelPeriodic.setVisible(false);
		amplitudePanelNonPeriodic.setVisible(true);
		this.radio_nonPeriodic.setSelected(true);
	}
	
	/**
	 * set the amplitude definition type
	 * @param type zeromeanmax or zeromeanmin or max or min
	 */
	public void setAmplitudeDefinitionType(String type) {
		if (type.equals("zeromeanmax")) {
			this.radio_zeroMeanMax.setSelected(true);
			this.radio_zeroMeanMaxAction();
		} else if (type.equals("zeromeanmin")) {
			this.radio_zeroMeanMin.setSelected(true);
			this.radio_zeroMeanMinAction();
		} else if (type.equals("max")) {
			this.radio_max.setSelected(true);
			this.radio_maxAction();
		} else if (type.equals("min")) {
			this.radio_min.setSelected(true);
			this.radio_minAction();
		} else {
			Canvas.canvasLog("unknown amplitude definition type " + type);
		}
	}
	
	/**
	 * set the parameters for the horizontal lines on the point interpolation canvas
	 * @param min
	 * @param max
	 * @param zeroLine
	 */
	public void setLineParams(int min, int max, int zeroLine) {
		this.pi_canvas.setLineParams(min, max, zeroLine);
	}
	
	/**
	 * is the point interpolation full defined?
	 */
	public boolean fullDefined() {
		if (this.periodicInterpolation != null && this.periodicInterpolation.getPhasor() != null && this.periodicInterpolation.getPointList().size() > 2 || this.nonPeriodicInterpolation != null) {
			return true;
		}
		return false;
	}

	/**
	 * can the given phasor be deleted
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		if (this.periodicInterpolation.getPhasor() == phasorToDelete) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * get the y position of the zero line 
	 * @return
	 */
	public int getZeroLinePosition() {
		return pi_canvas.getZeroLinePosition();
	}

}
