package GUI;

import Data.AmplitudeModulation;
import Data.Phasor;
import Data.Signal;

public interface CanvasTreeElement {
	public CanvasTreeElement getParentNode();
	public void setMousePressedActive(boolean active);
	public boolean getMousePressedActive();
	public void setConnected(CanvasConnector myConnector, CanvasConnector otherConnector);
	public void removeConnection(CanvasConnector myConnector);
	public CanvasTreeElement getToplevelElementInTreeSegment();
	public void highlightFor(CanvasConnector cc);
	public void stopHighlight();
	public void resetSubTree(boolean startHere);
	public void retypeSubTree();
	public boolean fullDefined();
	public void updateSignalOrAmplitudeModulation();
	public Signal getSignal(); //null if it is an amplitude modulation
	public AmplitudeModulation getAmplitudeModulation(); //null if it is a signal
	public boolean isVariable();
	public CanvasConnector getConnectorFor(CanvasTreeElement te);
	public CanvasConnector getOutputConnector();
	public boolean hasParent();
	public boolean isDeletePhasorOk(Phasor phasorToDelete);
}
