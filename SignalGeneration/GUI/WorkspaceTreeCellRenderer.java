package GUI;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class WorkspaceTreeCellRenderer extends DefaultTreeCellRenderer {


	private static final long serialVersionUID = 52L;

	@Override
	public Component getTreeCellRendererComponent(JTree arg0, Object arg1, boolean arg2, boolean arg3, boolean arg4,
			int arg5, boolean arg6) {
		if (arg1 instanceof WorkspaceTreeNodeDir) {
			return super.getTreeCellRendererComponent(arg0, arg1, arg2, arg3, arg4, arg5, arg6);		
		} else if (arg1 instanceof WorkspaceTreeLeaf) {
			JLabel label = new JLabel(arg1.toString());
			if (((WorkspaceTreeLeaf) arg1).getLeafType() == WorkspaceTreeLeafType.SIGNAL) {
				label.setIcon(new ImageIcon("GUI/signal.png"));	
			} else if (((WorkspaceTreeLeaf) arg1).getLeafType() == WorkspaceTreeLeafType.AMPLITUDEMODULATION) {
				label.setIcon(new ImageIcon("GUI/am.png"));
			} else {
				label.setIcon(new ImageIcon("GUI/recorded.png"));
			}
			return label;	
		} else {
			return new JLabel(LanguageTranslator.getTranslation("unbekanntes Objekt"));
		}
		
	}

}
