package GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * bar diagram for harmonic waves
 *
 */
public class BarDiagramPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;
	private double[] values;
	private int[] orders;
	/**
	 * constructor
	 * @param values values of the bars
	 * @param orders orders of the bars
	 */
	public BarDiagramPanel(double[] values, int[] orders) {
		super();
		this.values = values; 
		this.orders = orders;
		this.setBackground(Color.WHITE);
		this.setLayout(null);
	}
	
	/**
	 * update the bars
	 * @param values
	 * @param orders
	 */
	public void updateBars(double[] values, int[] orders) {
		this.values = values;
		this.orders = orders;
	}
	
	/**
	 * repaint the diagram
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.removeAll();
		
		if (values.length == orders.length && orders.length > 0) {
			g.setColor(Color.RED);
			int numberOfBars = this.orders[this.orders.length - 1];
			int barWidth = this.getWidth() / numberOfBars;
			double maxValue = 0;
			for (int i = 0; i < values.length; i++) {
				if (values[i] > maxValue) {
					maxValue = values[i];
				}
			}
			double perPixel = maxValue / this.getHeight();
			int orderCtr = 0;
			for (int i = 1; i <= numberOfBars; i++) {
				if (i == orders[orderCtr]) {
					int height = (int) Math.round(values[orderCtr] / perPixel);
					g.fillRect((i - 1) * barWidth, this.getHeight() - height, barWidth, height);
					orderCtr++;
				}
				JLabel lbl_order = new JLabel(i + "");
				int x = ((i - 1) * barWidth) + (barWidth / 2) - 4;
				int w = 8;
				if (i > 9) {
					x -= 4;
					w = 16;
				}
				
				lbl_order.setBounds(x, this.getHeight() - 12, w, 10);
				this.add(lbl_order);
			}
		}
	}
}
