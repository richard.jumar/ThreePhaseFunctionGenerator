package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

public class OperatorSymbol extends JPanel implements MouseMotionListener {

	private static final long serialVersionUID = 42L;
	private boolean plus;
	private boolean dot;
	public OperatorSymbol(int x, int y) {
		super();
		this.setBounds(x, y, 50, 50);
		this.setOpaque(false);
		this.plus = false;
		this.dot = false;
		this.addMouseMotionListener(this);
		this.setTransferHandler(new TransferHandler() {
			private static final long serialVersionUID = 43L;
			public int getSourceActions(JComponent c) {
				return TransferHandler.COPY;
			}
			protected Transferable createTransferable(JComponent c) {
				if (plus) {
					return new StringSelection("add");
				} 
				if (dot) {
					return new StringSelection("mult");
				}
				return new StringSelection("unknown");
			}
		});
	}
	
	public void setDot() {
		this.dot = true;
		this.plus = false;
	}
	
	public void setPlus() {
		this.plus = true;
		this.dot = false;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.drawOval(0, 0, 49, 49);
		if (this.plus) {
			g.drawLine(22, 10, 27, 10);
			g.drawLine(27, 10, 27, 22);
			g.drawLine(27, 22, 39, 22);
			g.drawLine(39, 22, 39, 27);
			g.drawLine(39, 27, 27, 27);
			g.drawLine(27, 27, 27, 39);
			g.drawLine(27, 39, 22, 39);
			g.drawLine(22, 39, 22, 27);
			g.drawLine(22, 27, 10, 27);
			g.drawLine(10, 27, 10, 22);
			g.drawLine(10, 22, 22, 22);
			g.drawLine(22, 22, 22, 10);
		}
		if (this.dot) {
			g.fillOval(15, 15, 19, 19);
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		TransferHandler h = this.getTransferHandler();
		h.exportAsDrag(this, arg0, TransferHandler.COPY);		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}

}
