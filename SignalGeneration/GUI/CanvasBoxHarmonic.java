package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import Controller.DataControl;
import Data.Frequency;
import Data.Harmonic;
import Data.Phasor;
import Data.Signal;
import Data.Wave;

/**
 * Canvas box for harmonics
 */
public class CanvasBoxHarmonic extends CanvasBoxSignalInput {
	private static final long serialVersionUID = 14L;
	private Harmonic harmonic;
	private JLabel lbl_abs_value;
	private JLabel lbl_phi_value;
	private JLabel lbl_phasor;
	private JButton btn_edit;
	private JTextField edt_amplitude_sin;
	private JTextField edt_amplitude_cos;
	private JTextField edt_phi;
	private JTextField edt_abs;
	private JComboBox<Phasor> cbo_phasor;
	private JTable tbl_waves;
	private DefaultTableModel tableModel;
	private JPanel gbx_sinCosInput;
	private JPanel gbx_phiAbsInput;
	private GraphPanel gp;
	private GraphPanel gp_box;
	private BarDiagramPanel bd;
	private JDialog dialog;


	/**
	 * constructor
	 * @param canvas
	 * @param x x position
	 * @param y y position
	 * @param h harmonic
	 */
	public CanvasBoxHarmonic(Canvas canvas, int x, int y, Harmonic h) {
		super(canvas);
		this.harmonic = h; // doesn't work. Set phasor before using...
		this.setLayout(null);
		this.setBounds(x, y, 200, 200);
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Harmonische"));
		title.setBounds(5, 5, 100, 10);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.add(status);
		SymbolPhasorMini phasorSymbol = new SymbolPhasorMini();
		phasorSymbol.setBounds(6, 22, 13, 13);
		this.add(phasorSymbol);
		lbl_phasor = new JLabel("<" + LanguageTranslator.getTranslation("nicht ausgew�hlt") + ">");
		lbl_phasor.setBounds(24, 20, 175, 15);
		this.add(lbl_phasor);
		JLabel amp = new JLabel(LanguageTranslator.getTranslation("Amplitude (1. Ordnung)"));
		amp.setBounds(5, 35, 150, 15);
		this.add(amp);
		JLabel amp_abs = new JLabel("- " + LanguageTranslator.getTranslation("Spitze") + ":");
		amp_abs.setBounds(5, 50, 80, 15);
		this.add(amp_abs);
		JLabel phi = new JLabel("- \u03C6:");
		phi.setBounds(5, 65, 80, 15);
		this.add(phi);
		this.lbl_abs_value = new JLabel("0 V");
		this.lbl_abs_value.setBounds(100, 50, 100, 15);
		this.add(lbl_abs_value);
		this.lbl_phi_value = new JLabel("0�");
		this.lbl_phi_value.setBounds(100, 65, 100, 15);
		this.add(lbl_phi_value);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		this.btn_edit.setBounds(1, 170, 198, 29);
		this.btn_edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}

		});
		this.add(btn_edit);
		edt_amplitude_sin = new JTextField();
		edt_amplitude_cos = new JTextField();
		edt_abs = new JTextField();
		edt_phi = new JTextField();
		this.gp_box = new GraphPanel(this.harmonic);
		this.gp_box.setBounds(1, 80, 198, 80);
		this.add(gp_box);
		
		//dialog
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Einstellungen Harmonische"));
		dialog.setLayout(null);
		dialog.setSize(500, 400);
		JLabel lbl_phasorL = new JLabel(LanguageTranslator.getTranslation("Drehzeiger"));
		lbl_phasorL.setBounds(5, 25, 70, 15);
		dialog.add(lbl_phasorL);
		cbo_phasor = new JComboBox<Phasor>();
		updateCbo_phasor();
		cbo_phasor.setBounds(75, 23, 100, 20);
		dialog.add(cbo_phasor);
		JButton btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu") + "...");
		btn_newPhasor.setBounds(190, 23, 80, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
				Phasor lastCreatedPhasor = Main.getMainwindow().getPhasorPanel().getLastCreatedPhasor();
				if (lastCreatedPhasor != null) {
					cbo_phasor.setSelectedItem(lastCreatedPhasor);
					resetTableAndBarDiagram();
				}
			}
		});
		dialog.add(btn_newPhasor);
		String[] colums = {LanguageTranslator.getTranslation("Ordnung"), LanguageTranslator.getTranslation("Frequenz"), "Sin.", "Cos.", LanguageTranslator.getTranslation("Abs."), "\u03C6", LanguageTranslator.getTranslation("Verh")};
		tbl_waves = new JTable(new DefaultTableModel(colums, 0));
		DefaultTableCellRenderer rightAlignRenderer = new DefaultTableCellRenderer();
		rightAlignRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		tbl_waves.getColumnModel().getColumn(0).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(1).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(2).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(3).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(4).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(5).setCellRenderer(rightAlignRenderer);
		tbl_waves.getColumnModel().getColumn(6).setCellRenderer(rightAlignRenderer);
		tableModel = (DefaultTableModel) tbl_waves.getModel();
		JScrollPane table = new JScrollPane(tbl_waves);
		table.setBounds(5, 50, 470, 100);
		dialog.add(table);
		JButton btn_new = new JButton(LanguageTranslator.getTranslation("Hinzuf�gen"));
		btn_new.setBounds(5, 155, 110, 20);
		btn_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editWave(null);
			}
		});
		dialog.add(btn_new);
		JButton btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		btn_edit.setBounds(120, 155, 110, 20);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_waves.getSelectedRow() > -1) { //-1 is default (if no row is selected)
					editWave(harmonic.getWaves().get(tbl_waves.getSelectedRow()));
				}
			}
		});
		dialog.add(btn_edit);
		JButton btn_delete = new JButton(LanguageTranslator.getTranslation("Entfernen"));
		btn_delete.setBounds(235, 155, 110, 20);
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_waves.getSelectedRow() > -1) { //-1 is default (if no row is selected)
					harmonic.removeWave(tbl_waves.getSelectedRow());
					resetTableAndBarDiagram();
					gp.computeThumbnail();
				}
			}
		});
		dialog.add(btn_delete);
		bd = new BarDiagramPanel(new double[]{}, new int[]{});
		bd.setBounds(5, 180, 200, 150);
		dialog.add(bd);
		resetTableAndBarDiagram();
		gp = new GraphPanel(this.harmonic);
		gp.setBounds(215, 180, 260, 150);
		dialog.add(gp);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(380, 335, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newHarmonic();
				updateUI();
				gp_box.computeThumbnail();
				canvas.updateTopLevelElement();
				dialog.dispose();
			}
		});
		dialog.add(btn_ok);
	}
	
	/**
	 * set the harmonic
	 * @param h harmonic
	 */
	public void setHarmonic(Harmonic h) {
		this.harmonic = h; 
		this.gp_box.setPeriodic(h);
	}
	
	/**
	 * get the harmonic
	 * @return
	 */
	public Harmonic getHarmonic() {
		return this.harmonic;
	}
	
	/**
	 * draw the canvas box
	 */
	public void paintComponent(Graphics g) {
		if (this.harmonic != null) {
			if (this.harmonic.getWaves().size() > 0 && this.harmonic.getWaves().get(0).getOrder() == 1) {
				this.lbl_abs_value.setText(this.displayAbs(0));
				this.lbl_phi_value.setText(this.displayPhi(0));
			}
		}
		super.paintComponent(g);
	}

	/**
	 * show the edit dialog
	 */
	private void showDialog() {
		this.updateCbo_phasor();
		dialog.setVisible(true);
	}

	/**
	 * update the selectable phasor list
	 */
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		if (this.harmonic.getPhasor() != null) {
			cbo_phasor.setSelectedItem(this.harmonic.getPhasor());
		}
		cbo_phasor.updateUI();
	}
	
	/**
	 * recompute the table and bar diagramm of the harmonic waves
	 */
	private void resetTableAndBarDiagram() {
		//remove all rows
		while (tableModel.getRowCount() > 0) {
			tableModel.removeRow(0);
		}
		//create the rows from the WaveList
		long f_phasor = 0; 
		if (this.cbo_phasor.getSelectedItem() != null) { // null is the case if a new harmonic should be created
			f_phasor = ((Phasor) this.cbo_phasor.getSelectedItem()).getFrequency().getF0_phase1(); 
		}
		double[] values = new double[this.harmonic.getWaves().size()];
		int[] orders = new int[this.harmonic.getWaves().size()];
		for (int i = 0; i < this.harmonic.getWaves().size(); i++) {
			double abs = Math.sqrt(Math.pow(this.harmonic.getWaves().get(i).getSinAmplitude(), 2) + Math.pow(this.harmonic.getWaves().get(i).getCosAmplitude(), 2));
			String part = "";
			if (this.harmonic.getWaves().get(0).getOrder() == 1) {
				double part_value = ((abs / Math.sqrt(Math.pow(this.harmonic.getWaves().get(0).getSinAmplitude(), 2) + Math.pow(this.harmonic.getWaves().get(0).getCosAmplitude(), 2))) * 100 );
				part_value = ((double) Math.round(part_value * 1000)) / 1000d;
				part = (part_value  + "%").replace('.', ',');
			}
			tableModel.addRow(new String[] {this.harmonic.getWaves().get(i).getOrder() + "", Frequency.microHz_valueToHzString(this.harmonic.getWaves().get(i).getOrder() * f_phasor, 4), GUIMath.roundDecimals(this.harmonic.getWaves().get(i).getSinAmplitude(), 3) + " V", GUIMath.roundDecimals(this.harmonic.getWaves().get(i).getCosAmplitude(), 3) + " V", displayAbs(i), displayPhi(i),  part});
			//bar diagram:
			orders[i] = this.harmonic.getWaves().get(i).getOrder();
			values[i] = abs;
			bd.updateBars(values, orders);
		}
		if (this.harmonic.getWaves().size() == 0) {
			bd.updateBars(new double[]{}, new int[]{});
		}
		this.tbl_waves.updateUI();
		this.bd.updateUI();
	}

	/**
	 * create a harmonic with the parameters from the dialog
	 */
	private void newHarmonic() {
		if (cbo_phasor.getSelectedIndex() > -1) {
			this.harmonic.setPhasor((Phasor) cbo_phasor.getSelectedItem());
			lbl_phasor.setText(cbo_phasor.getSelectedItem().toString() + " (" + Frequency.microHz_valueToHzString(((Phasor)cbo_phasor.getSelectedItem()).getFrequency().getF0_phase1(), 4) + ")"); 
			lbl_phasor.updateUI();
			if (connectedWith != null) {
				status.setStatusDefindedAndConnected();
			} else {
				status.setStatusDefined();
			}
		} else {
			status.setStatusUndefined();
		}
		status.updateUI();
	}

	/**
	 * Fills the gui with the content from the data (set by the xml reader)
	 */
	public void fillGuiFromData() {
		if (this.harmonic.getPhasor() != null) {
			this.cbo_phasor.setSelectedItem(this.harmonic.getPhasor());
		}
		resetTableAndBarDiagram();
		gp.computeThumbnail();
		gp_box.computeThumbnail();
		newHarmonic();
	}
	
	/**
	 * dialog for wave edit
	 * @param wave
	 */
	private void editWave(Wave wave) {
		JDialog dialog = new JDialog();
		dialog.setModal(true);
		String order; 
		if (wave == null) {
			dialog.setTitle(LanguageTranslator.getTranslation("Neue Welle"));
			order = "0";
			edt_amplitude_sin.setText("0"); 
			edt_amplitude_cos.setText("0");
			edt_phi.setText("0");
			edt_abs.setText("0");
		} else {
			dialog.setTitle(LanguageTranslator.getTranslation("Welle bearbeiten"));
			order = wave.getOrder() + "";
			edt_amplitude_sin.setText((wave.getSinAmplitude() + "").replace('.', ','));
			edt_amplitude_cos.setText((wave.getCosAmplitude() + "").replace('.', ','));
			double abs = Math.sqrt(Math.pow(wave.getSinAmplitude(), 2) + Math.pow(wave.getCosAmplitude(), 2));
			edt_abs.setText((abs + "").replace('.', ','));
			edt_phi.setText(GUIMath.phiInDegreeFromSinCos(wave.getSinAmplitude(), wave.getCosAmplitude()));
		}
		dialog.setLayout(null);
		dialog.setSize(250, 265);
		JLabel lbl_order = new JLabel(LanguageTranslator.getTranslation("Ordnung"));
		lbl_order.setBounds(5, 5, 60, 15);
		dialog.add(lbl_order);
		JTextField edt_order = new JTextField(order + "");
		edt_order.setBounds(70, 3, 20, 20);
		dialog.add(edt_order);
		JPanel gbx_inputTypeSelect = new JPanel();
		gbx_inputTypeSelect.setLayout(null);
		gbx_inputTypeSelect.setBounds(5, 30, 240, 50);
		gbx_inputTypeSelect.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eingabeart")));
		ButtonGroup btnGroup_input = new ButtonGroup();
		JRadioButton radio_cosSinInput = new JRadioButton(LanguageTranslator.getTranslation("Sinus- und Cosinusamplitude"));
		radio_cosSinInput.setBounds(5, 15, 215, 15);
		radio_cosSinInput.setSelected(true);
		radio_cosSinInput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gbx_phiAbsInput.setVisible(false);
				gbx_sinCosInput.setVisible(true);
			}
		
		});
		btnGroup_input.add(radio_cosSinInput);
		gbx_inputTypeSelect.add(radio_cosSinInput);
		
		JRadioButton radio_phiAbs = new JRadioButton(LanguageTranslator.getTranslation("Phasenverschiebung und Betrag"));
		radio_phiAbs.setBounds(5, 30, 230, 15);
		
		radio_phiAbs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gbx_sinCosInput.setVisible(false);
				gbx_phiAbsInput.setVisible(true);
			}			
		});
		btnGroup_input.add(radio_phiAbs);
		gbx_inputTypeSelect.add(radio_phiAbs);
		
		dialog.add(gbx_inputTypeSelect);
		initEditInputPanels();
		dialog.add(gbx_sinCosInput);
		dialog.add(gbx_phiAbsInput);
		gbx_phiAbsInput.setVisible(false);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(5, 200, 240, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int order = -1;
				double sin_amplitude = Double.MIN_VALUE;
				double cos_amplitude = Double.MIN_VALUE;
				double phi = Double.MIN_VALUE;
				double abs = Double.MIN_VALUE;
				try {
					order = Integer.parseInt(edt_order.getText());
					if (gbx_sinCosInput.isVisible()) {
						String[] sin_amplitude_parts = edt_amplitude_sin.getText().split(",");
						String[] cos_amplitude_parts = edt_amplitude_cos.getText().split(",");
						if (sin_amplitude_parts.length > 0 && sin_amplitude_parts.length <= 2 && cos_amplitude_parts.length > 0 && cos_amplitude_parts.length <= 2) {
				
							sin_amplitude = ((double) Integer.parseInt(sin_amplitude_parts[0]));
							if (sin_amplitude_parts.length == 2) {
								sin_amplitude += Double.parseDouble("0." + sin_amplitude_parts[1]);
							}
							cos_amplitude = ((double) Integer.parseInt(cos_amplitude_parts[0]));
							if (cos_amplitude_parts.length == 2) {
								cos_amplitude += Double.parseDouble("0." + cos_amplitude_parts[1]);
							}
							if (wave != null) {
								harmonic.getWaves().remove(wave);
							}
							if (sin_amplitude == 0 && cos_amplitude == 0) {
								JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Mindestens eine Amplitude muss von Null verschieden sein!"));
							} else {
								if (harmonic.addWave(new Wave(order, sin_amplitude, cos_amplitude))) {
									resetTableAndBarDiagram();
									gp.computeThumbnail();
									dialog.dispose();
								} else {
									JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte eine g�ltige und noch nicht vorhandene Ordnung angeben"));
								}
							}
						}
					} else if (gbx_phiAbsInput.isVisible()) {
						String[] phi_parts = edt_phi.getText().split(",");
						String[] abs_parts = edt_abs.getText().split(",");
						
						phi = ((double) Integer.parseInt(phi_parts[0]));
						if (phi_parts.length == 2) {
							phi += Double.parseDouble("0." + phi_parts[1]);
						}
					
						abs = ((double) Integer.parseInt(abs_parts[0]));
						if (abs_parts.length == 2) {
							abs += Double.parseDouble("0." + abs_parts[1]);
						}
						if (wave != null) {
							harmonic.getWaves().remove(wave);
						}	
						sin_amplitude = Math.sin(-(phi * Math.PI / 180)) * abs;
						cos_amplitude = Math.cos((phi * Math.PI / 180)) * abs;
						if (abs != 0) {
							if (harmonic.addWave(new Wave(order, sin_amplitude, cos_amplitude))) {
								resetTableAndBarDiagram();
								gp.computeThumbnail();
								gp.updateUI();
								dialog.dispose();
							} else {
								JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte eine g�ltige und noch nicht vorhandene Ordnung angeben"));	
							}
						} else {
							JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Die Amplitude muss von Null verschieden sein!"));
						}
					} 
				} catch (NumberFormatException e) {
					Canvas.canvasLog("NumberFormatException");
					if (order == -1 || sin_amplitude == Double.MIN_VALUE || cos_amplitude == Double.MIN_VALUE || phi == Double.MIN_VALUE || abs == Double.MIN_VALUE) {
						String text = "";
						if (order == -1) {
							text += LanguageTranslator.getTranslation("Die Ordnung muss eine nat�rliche Zahl sein") + ".\n";
						}
						if (sin_amplitude == Double.MIN_VALUE || cos_amplitude == Double.MIN_VALUE) {
							text += LanguageTranslator.getTranslation("Die Amplitudengr��en m�ssen als Zahlenwert angegeben werden") + ".\n";
						}
						if (phi == Double.MIN_VALUE || abs == Double.MIN_VALUE) {
							text += LanguageTranslator.getTranslation("Der Winkel \u03C6 und der Betragswert muss als Zahlenwert angegeben werden") + ".\n";
						}

						JOptionPane.showMessageDialog(dialog, text);
					
					} else {
						e.printStackTrace();
					}
				}
			}
		});
		dialog.add(btn_ok);
		dialog.setVisible(true);
	}
	
	/**
	 * initialize the dialog components for waves
	 */
	private void initEditInputPanels() {
		gbx_sinCosInput = new JPanel();
		gbx_sinCosInput.setLayout(null);
		gbx_sinCosInput.setBounds(5, 80, 240, 110);
		JPanel gbx_sinus = new JPanel();
		gbx_sinus.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Sinus-Komponente")));
		gbx_sinus.setBounds(0, 5, 240, 50);
		gbx_sinus.setLayout(null);
		JLabel lbl_amplitude_sin = new JLabel(LanguageTranslator.getTranslation("Amplitude"));
		lbl_amplitude_sin.setBounds(5, 20, 80, 15);
		gbx_sinus.add(lbl_amplitude_sin);

		edt_amplitude_sin.setBounds(85, 18, 50, 20);
		gbx_sinus.add(edt_amplitude_sin);
		JLabel lbl_volt_sin = new JLabel("V");
		lbl_volt_sin.setBounds(140, 20, 10, 15);
		gbx_sinus.add(lbl_volt_sin);
		gbx_sinCosInput.add(gbx_sinus);
		JPanel gbx_cosinus = new JPanel();
		gbx_cosinus.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Cosinus-Komponente")));
		gbx_cosinus.setBounds(0, 60, 240, 50);
		gbx_cosinus.setLayout(null);
		JLabel lbl_amplitude_cos = new JLabel("Amplitude");
		lbl_amplitude_cos.setBounds(5, 20, 80, 15);
		gbx_cosinus.add(lbl_amplitude_cos);
		edt_amplitude_cos.setBounds(85, 18, 50, 20);
		gbx_cosinus.add(edt_amplitude_cos);
		JLabel lbl_volt_cos = new JLabel("V");
		lbl_volt_cos.setBounds(140, 20, 10, 15);
		gbx_cosinus.add(lbl_volt_cos);
		gbx_sinCosInput.add(gbx_cosinus);
		gbx_phiAbsInput = new JPanel();
		gbx_phiAbsInput.setLayout(null);
		gbx_phiAbsInput.setBounds(5, 80, 220, 110);
		JPanel gbx_phi = new JPanel();
		gbx_phi.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phasenverschiebung")));
		gbx_phi.setBounds(0, 5, 220, 50);
		gbx_phi.setLayout(null);
		JLabel lbl_phi = new JLabel("\u03C6");
		lbl_phi.setBounds(5, 20, 60, 15);
		gbx_phi.add(lbl_phi);
		edt_phi.setBounds(65, 18, 50, 20);
		gbx_phi.add(edt_phi);
		JLabel lbl_degree = new JLabel("�");
		lbl_degree.setBounds(115, 20, 10, 15);
		gbx_phi.add(lbl_degree);
		gbx_phiAbsInput.add(gbx_phi);
		JPanel gbx_abs = new JPanel();
		gbx_abs.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Amplitude")));
		gbx_abs.setBounds(0, 60, 220, 50);
		gbx_abs.setLayout(null);
		JLabel lbl_abs = new JLabel(LanguageTranslator.getTranslation("Betrag"));
		lbl_abs.setBounds(5, 20, 60, 15);
		gbx_abs.add(lbl_abs);

		edt_abs.setBounds(65, 18, 50, 20);
		gbx_abs.add(edt_abs);
		JLabel lbl_absV = new JLabel("V");
		lbl_absV.setBounds(115, 20, 10, 15);
		gbx_abs.add(lbl_absV);
		gbx_phiAbsInput.add(gbx_abs);
	}
	
	/**
	 * absolute amplitude from a composition out of sin and cos
	 * @param i
	 * @return
	 */
	private String displayAbs(int i) {
		double abs = Math.sqrt(Math.pow(this.harmonic.getWaves().get(i).getSinAmplitude(), 2) + Math.pow(this.harmonic.getWaves().get(i).getCosAmplitude(), 2));
		return GUIMath.roundDecimals(abs, 3) + " V";
	}
	
	/**
	 * compute phi
	 * @param i
	 * @return
	 */
	private String displayPhi(int i) {
		String phi_degree = GUIMath.phiInDegreeFromSinCos(this.harmonic.getWaves().get(i).getSinAmplitude(), this.harmonic.getWaves().get(i).getCosAmplitude());//(((double) Math.round(phi * 180000 / Math.PI)) / 1000d) + "�";
		return phi_degree;
	}
	
	/**
	 * check whether the harmonic is full defined
	 */
	public boolean fullDefined() {
		if (this.harmonic != null && this.harmonic.getPhasor() != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * get the signal
	 * @return signal
	 */
	public Signal getSignal() {
		return this.harmonic;
	}

	/**
	 * check whether the phasor the phasor is unused and can be deleted
	 * @param phasorToDelete
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		if (this.harmonic.getPhasor() == phasorToDelete) {
			return false;
		} else {
			return true;
		}
	}

}
