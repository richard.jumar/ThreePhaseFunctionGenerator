package GUI;

import java.util.HashMap;
import java.util.Map;

public class LanguageTranslator {
	
	public static Language currentLanguage = Language.Deutsch;
	private static Map<String, LanguageText> translations = new HashMap<String, LanguageText>();
	
	public static void generateTranslations() {
		translations.put("Datei", new LanguageText(new Language[] {Language.Deutsch, Language.English}, new String[] {"Datei", "File"}));
		translations.put("Drehzeiger2", new LanguageText(new Language[] {Language.Deutsch, Language.English}, new String[] {"Drehzeiger", "Phasors"}));
		addEnglishToGermanKey("Bearbeiten", "Edit");
		addEnglishToGermanKey("Speichern", "Save");
		addEnglishToGermanKey("Speichern unter", "Save as");
		addEnglishToGermanKey("Beenden", "Exit");
		addEnglishToGermanKey("Ausgaben", "Outputs");
		addEnglishToGermanKey("Signalausgabe", "Signal Output");
		addEnglishToGermanKey("Amplitudenmodulationsausgabe", "Amplitude Modulation Output");
		addEnglishToGermanKey("Operatoren", "Operators");
		addEnglishToGermanKey("Signale", "Signals");
		addEnglishToGermanKey("Harmonische", "Harmonic");
		addEnglishToGermanKey("Punktinterpolation", "Point Interpolation");
		addEnglishToGermanKey("Abschnittsweise definierte Funktion", "Piecewise Defined Function");
		addEnglishToGermanKey("Gleichspannung", "Direct Voltage");
		addEnglishToGermanKey("Amplitudenmodulationsfunktionen", "Amplitude Modulation Functions");
		addEnglishToGermanKey("Verst�rkung", "Gain");
		addEnglishToGermanKey("Sprung", "Step");
		addEnglishToGermanKey("Rampe", "Ramp");
		addEnglishToGermanKey("Punktinterpolation", "Point Interpolation");
		addEnglishToGermanKey("Abschnittsweise definierte Funktion", "Piecewise Defined Function");
		addEnglishToGermanKey("Drehzeiger", "Phasor");
		addEnglishToGermanKey("Neu", "New");
		addEnglishToGermanKey("Details", "Details");
		addEnglishToGermanKey("L�schen", "Delete");
		addEnglishToGermanKey("Amplitude (1. Ordnung)", "Amplitude (1st order)");
		addEnglishToGermanKey("Spitze", "peak");
		addEnglishToGermanKey("Ordnung", "Order");
		addEnglishToGermanKey("Frequenz", "Frequency");
		addEnglishToGermanKey("Verh", "Prop");
		addEnglishToGermanKey("Hinzuf�gen", "Add");
		addEnglishToGermanKey("Entfernen", "Remove");
		addEnglishToGermanKey("Neue Welle", "New wave");
		addEnglishToGermanKey("Welle bearbeiten", "Edit wave");
		addEnglishToGermanKey("Phasenverschiebung", "phase shift");
		addEnglishToGermanKey("Einstellungen Harmonische", "Properties: Harmonic");
		addEnglishToGermanKey("Ausgangsfilter", "Output Filter");
		addEnglishToGermanKey("Eingabemodus", "Input mode");
		addEnglishToGermanKey("Rampe", "Ramp");
		addEnglishToGermanKey("Signalausgabe", "Signal Output");
		addEnglishToGermanKey("Eigenschaften Rampe", "Properties: Ramp");
		addEnglishToGermanKey("periodisch", "Periodic");
		addEnglishToGermanKey("nichtperiodisch", "Non periodic");
		addEnglishToGermanKey("symmetrisch", "Symmetric");
		addEnglishToGermanKey("asymmetrisch", "Asymmetric");
		addEnglishToGermanKey("Alle Phasen", "All Phases");
		addEnglishToGermanKey("Phasen nicht symmetrisch", "non symmetric phases");
		addEnglishToGermanKey("Phasen symmetrisch", "symmetric phases");
		addEnglishToGermanKey("ja", "true");
		addEnglishToGermanKey("nein", "false");
		addEnglishToGermanKey("�ber", "About");
		addEnglishToGermanKey("Hilfe", "Help");
		addEnglishToGermanKey("Entwickelt von", "Developed by");
		addEnglishToGermanKey("Schlie�en", "Close");
		addEnglishToGermanKey("Am.-Ausgabe", "Am. Output");
		addEnglishToGermanKey("Bezeichnung", "Identifier");
		addEnglishToGermanKey("Frequenzmodulation", "Frequency Modulation");
		addEnglishToGermanKey("Eigenschaften", "Properties");
		addEnglishToGermanKey("Startzeit", "Start time");
		addEnglishToGermanKey("Endzeit", "End time");
		addEnglishToGermanKey("Endfrequenz", "End frequency");
		addEnglishToGermanKey("Neue Frequenzmodulation", "New Frequency Modulation");
		addEnglishToGermanKey("Bitte nur g�ltige Zahlenwerte eingeben!", "Please insert only valid numbers!");
		addEnglishToGermanKey("Eigenschaften Verst�rkung", "Properties: Gain");
		addEnglishToGermanKey("nicht ausgew�hlt", "not selected");
		addEnglishToGermanKey("Grundwert", "Base value");
		addEnglishToGermanKey("Alternativwert", "Alterning value");
		addEnglishToGermanKey("R�cksprung", "inverseStep");
		addEnglishToGermanKey("Es ist kein Drehzeiger ausgew�hlt", "There is no phasor selected");
		addEnglishToGermanKey("Eigenschaften Sprung", "Properties: Step");
		addEnglishToGermanKey("Rampenstart", "Start of ramp");
		addEnglishToGermanKey("Rampenende", "End or ramp");
		addEnglishToGermanKey("Sollen die durchgef�hrten �nderungen gespeichert werden?", "Do you want to save changes?");
		addEnglishToGermanKey("�nderungen speichern", "Save changes");
		addEnglishToGermanKey("Bitte nur g�ltige Zahlenwerte als Spannungswerte angeben", "Please insert only valid voltages");
		addEnglishToGermanKey("Eigenschaften Gleichspannung", "Properties: Direct Voltage");
		addEnglishToGermanKey("St�tzpunkte", "points");
		addEnglishToGermanKey("Abschnittsw. def. Funktion", "Piecewise def. Function");
		addEnglishToGermanKey("Polynomabschnitte", "polynomial sections");
		addEnglishToGermanKey("Maximaler Wert", "Maximum value");
		addEnglishToGermanKey("Minimaler Wert", "Minimum value");
		addEnglishToGermanKey("Mittelwertfrei setzen", "Set without mean");
		addEnglishToGermanKey("Ein Drehzeiger muss ausgew�hlt sein!", "A phasor has to be selected!");
		addEnglishToGermanKey("Das Signal ist nicht vollst�ndig definiert", "The signal definition is not completed");
		addEnglishToGermanKey("L�schmodus", "Deletion mode");
		addEnglishToGermanKey("Temperatur in �C", "Temperature in �C");
		addEnglishToGermanKey("CPU-Takt", "CPU frequency");
		addEnglishToGermanKey("CPU-Auslastung", "CPU utilization");
		addEnglishToGermanKey("Speicher", "Memory");
		addEnglishToGermanKey("Lesezeiger", "read pointer");
		addEnglishToGermanKey("Schreibzeiger", "write pointer");
		addEnglishToGermanKey("Polynom", "Polynomial");
		addEnglishToGermanKey("Start bei", "Start at");
		addEnglishToGermanKey("Faktor", "Coefficient");
		addEnglishToGermanKey("Potenz", "Power");
		addEnglishToGermanKey("<html>Hinweis: Der Ursprung befindet<br>sich bei 0�</html>", "<html>Note: The origin is<br>at 0�</html>");
		addEnglishToGermanKey("<html>Hinweis: Der Ursprung befindet<br>sich beim angegebenen Startzeitpunkt<br>des Polynomsegments</html>", "<html>Note: The origin is at the<br>specified begin of<br>the polynomial segment</html>");
		addEnglishToGermanKey("�ffnen", "Open");
		addEnglishToGermanKey("Maximale Spannung", "Maximum voltage");
		addEnglishToGermanKey("Minimale Spannung", "Minimum voltage");
		addEnglishToGermanKey("kubische Splines", "cubic splines");
		addEnglishToGermanKey("Interpolationseinstellungen", "Interpolation properties");
		addEnglishToGermanKey("Aktualisieren", "Refresh");
	}
	
	public static void addEnglishToGermanKey(String germanKey, String english) {
		translations.put(germanKey, new LanguageText(new Language[] {Language.Deutsch, Language.English}, new String[] {germanKey, english}));
	}
	
	public static String getTranslation(String key) {
		try {
			return translations.get(key).getTranslation(currentLanguage);
		} catch (NullPointerException e) {
			return key;
		}
	}
	
}
