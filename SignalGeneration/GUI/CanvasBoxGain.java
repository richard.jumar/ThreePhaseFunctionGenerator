package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.NonPeriodicAmplitudeModulationGain;
import Data.Phasor;

/**
 * canvas box for the amplitude modulation type gain
 */
public class CanvasBoxGain extends CanvasBoxAmplitudeModulationInput {
	private static final long serialVersionUID = 14L;
	private JButton btn_edit;
	private JLabel lbl_k1;
	private JLabel lbl_k2;
	private JLabel lbl_k3;
	private JLabel lbl_cb_k1;
	private JLabel lbl_cb_k2;
	private JLabel lbl_cb_k3;
	private JTextField edt_k1;
	private JTextField edt_k2;
	private JTextField edt_k3;
	private JRadioButton radio_symmetric;
	private JRadioButton radio_asymmetric;
	private NonPeriodicAmplitudeModulationGain gain;
	private JDialog dialog;
	
	/**
	 * constructor
	 * @param canvas canvas
	 * @param x x position
	 * @param y y position
	 */
	public CanvasBoxGain(Canvas canvas, int x, int y) {
		super(canvas); 
		this.gain = new NonPeriodicAmplitudeModulationGain(DataControl.getClock());
		this.setLayout(null);
		this.setBounds(x, y, 110, 110);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Verstärkung"));
		title.setBounds(5, 5, 100, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(90, 1, 19, 19);
		this.status.setStatusDefined();
		this.add(status);
		this.lbl_cb_k1 = new JLabel("k = 1,0");
		this.lbl_cb_k1.setBounds(5, 20, 100, 20);
		this.add(lbl_cb_k1);
		this.lbl_cb_k2 = new JLabel();		
		this.lbl_cb_k2.setBounds(5, 40, 100, 20);
		this.add(lbl_cb_k2);
		this.lbl_cb_k3 = new JLabel();		
		this.lbl_cb_k3.setBounds(5, 60, 100, 20);
		this.add(lbl_cb_k3);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		this.btn_edit.setBounds(1, 80, 108, 29);
		this.btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}
		});
		this.add(btn_edit);
		
		lbl_k1 = new JLabel("k=");
		lbl_k1.setBounds(10, 60, 25, 20);
		lbl_k2 = new JLabel("<html>k<sub>2</sub>=</html>");
		lbl_k2.setBounds(10, 85, 25, 20);
		lbl_k2.setVisible(false);
		lbl_k3 = new JLabel("<html>k<sub>3</sub>=</html>");
		lbl_k3.setBounds(10, 110, 25, 20);
		lbl_k3.setVisible(false);
		
		edt_k1 = new JTextField();
		edt_k1.setBounds(35, 60, 50, 20);
		edt_k2 = new JTextField();
		edt_k2.setBounds(35, 85, 50, 20);
		edt_k2.setVisible(false);
		edt_k3 = new JTextField();
		edt_k3.setBounds(35, 110, 50, 20);
		edt_k3.setVisible(false);
		
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Eigenschaften Verstärkung"));
		dialog.setLayout(null);
		dialog.setSize(280, 205);
		
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(null);
		optionPanel.setBounds(5, 5, 250, 50);
		optionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eigenschaften")));
		dialog.add(optionPanel);
		ButtonGroup btnGroup_phases = new ButtonGroup();
		this.radio_symmetric = new JRadioButton(LanguageTranslator.getTranslation("symmetrisch"));
		radio_symmetric.setSelected(true);
		radio_symmetric.setBounds(10, 20, 110, 20);
		radio_symmetric.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setSymmetric();
			}		
		});
		btnGroup_phases.add(radio_symmetric);
		optionPanel.add(radio_symmetric);
		this.radio_asymmetric = new JRadioButton(LanguageTranslator.getTranslation("asymmetrisch"));
		radio_asymmetric.setSelected(false);
		radio_asymmetric.setBounds(118, 20, 120, 20);
		radio_asymmetric.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setAsymmetric();
				edt_k2.setText(edt_k1.getText()); //use symmetric k as default
				edt_k3.setText(edt_k1.getText());
			}
			
		});
		btnGroup_phases.add(radio_asymmetric);
		optionPanel.add(radio_asymmetric);
		dialog.add(lbl_k1);
		dialog.add(edt_k1);
		dialog.add(lbl_k2);
		dialog.add(edt_k2);
		dialog.add(lbl_k3);
		dialog.add(edt_k3);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(5, 135, 250, 25);
		btn_ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btn_okClick();
			}
			
		});
		dialog.add(btn_ok);
		
		
		
		
		canvas.updateTopLevelElement(); //gain is full defined (everytime), so it can called here
	}

	/**
	 * ok button action
	 */
	public void btn_okClick() {
		try {	
			double k1 = Double.parseDouble(edt_k1.getText().replace(',', '.'));
			if (radio_symmetric.isSelected()) {
				double[] gains = {k1, k1, k1};
				gain.setGain(gains);
				lbl_cb_k1.setText("k = " + GUIMath.roundDecimals(k1, 5));
				lbl_cb_k2.setVisible(false);
				lbl_cb_k3.setVisible(false);
			} else {
				double k2 = Double.parseDouble(edt_k2.getText().replace(',', '.'));
				double k3 = Double.parseDouble(edt_k3.getText().replace(',', '.'));
				double[] gains = {k1, k2, k3};
				gain.setGain(gains);
				lbl_cb_k1.setText("<html>k<sub>1</sub> = " + GUIMath.roundDecimals(k1, 5) + "</html>");
				lbl_cb_k2.setText("<html>k<sub>2</sub> = " + GUIMath.roundDecimals(k2, 5) + "</html>");
				lbl_cb_k3.setText("<html>k<sub>3</sub> = " + GUIMath.roundDecimals(k3, 5) + "</html>");
				lbl_cb_k2.setVisible(true);
				lbl_cb_k3.setVisible(true);
			}
			dialog.dispose();
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur gültige Zahlenwerte als Verstärkungsfaktor angeben"));
		}
	}
	
	/**
	 * display asymmetric for elements
	 */
	private void setAsymmetric() {
		updateGainValues();
		lbl_k1.setText("<html>k<sub>1</sub>=</html>");
		lbl_k2.setVisible(true);
		lbl_k3.setVisible(true);
		edt_k2.setVisible(true);
		edt_k3.setVisible(true);
	}
	
	/**
	 * display symmetric form elements
	 */
	private void setSymmetric() {
		updateGainValues();
		lbl_k1.setText("k=");
		lbl_k2.setVisible(false);
		lbl_k3.setVisible(false);
		edt_k2.setVisible(false);
		edt_k3.setVisible(false);
	}
	
	/**
	 * update the gain values in the form elements
	 */
	private void updateGainValues() {
		this.edt_k1.setText(GUIMath.roundDecimals(this.gain.getGain()[0], 5));
		this.edt_k2.setText(GUIMath.roundDecimals(this.gain.getGain()[1], 5));
		this.edt_k3.setText(GUIMath.roundDecimals(this.gain.getGain()[2], 5));
	}
	
	/**
	 * show the edit dialog
	 */
	private void showDialog() {
		updateGainValues();
		if (gain.getGain()[0] == gain.getGain()[1] && gain.getGain()[0] == gain.getGain()[2]) {
			setSymmetric();
			this.radio_symmetric.setSelected(true);
		} else {
			setAsymmetric();
			radio_asymmetric.setSelected(true);
			
		}
		
		dialog.setVisible(true);
	}



	/**
	 * gain is full defined (default is k=1)
	 */
	public boolean fullDefined() {
		return true;
	}

	/**
	 * get the amplitude modulation
	 */
	public AmplitudeModulation getAmplitudeModulation() {
		return this.gain;
	}
	
/**
 * get the gains
 * @return gains (three phases)
 */
	public NonPeriodicAmplitudeModulationGain getGain() {
		return this.gain;
	}
	
	/**
	 * set the gains
	 * @param gains gain (three phases)
	 */
	public void setGain(double[] gains) {
		gain.setGain(gains);
		if (gains[0] == gains[1] && gains[0] == gains[2]) {
			this.setSymmetric();
			this.radio_symmetric.setSelected(true);
		} else {
			this.setAsymmetric();
			this.radio_asymmetric.setSelected(true);
		}
	}

	/**
	 * deleting of phasors is ok
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true;
	}
	
}
