package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import Controller.DataControl;
import Controller.FileControl;
import Data.NonPeriodicSignalRecorded;
import Data.Phasor;
import Data.Signal;

public class CanvasBoxSignalInputRecorded extends CanvasBoxSignalInput {
	private static final long serialVersionUID = 20L;
	private NonPeriodicSignalRecorded signal;
	private String calibrationFilePath;
	private String path;
	private JLabel lbl_calibration;
	private JDialog dialog;
	private JTextField edt_startTime;
	private JCheckBox cbx_nextFile;
	private JCheckBox cbx_calibrationFile;
	private JPanel panel_calibFile;
	private JLabel lbl_calibFileName;
	private JLabel lbl_cb_startTime;
	private boolean loadFollowingFiles;
	
	public CanvasBoxSignalInputRecorded(Canvas canvas, int x, int y, String path) {
		super(canvas);
		this.path = path;
		this.signal = new NonPeriodicSignalRecorded(DataControl.getClock(), path);
		this.loadFollowingFiles = false;
		this.setBounds(x, y, 200, 80);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		this.status.setStatusDefined();
		this.add(status);
		JLabel lbl_title = new JLabel(LanguageTranslator.getTranslation("Aufgezeichnetes Signal"));
		lbl_title.setBounds(5, 5, 150, 15);
		this.add(lbl_title);
		lbl_cb_startTime = new JLabel(LanguageTranslator.getTranslation("Startzeit") + " 0,0s");
		lbl_cb_startTime.setBounds(5, 20, 190, 15);
		this.add(lbl_cb_startTime);
		this.lbl_calibration = new JLabel(LanguageTranslator.getTranslation("interne Kalibrierung"));
		this.lbl_calibration.setBounds(5, 35, 190, 15);
		this.add(lbl_calibration);
		JButton btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		btn_edit.setBounds(1, 55, 198, 24);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}			
		});
		this.add(btn_edit);
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Einstellungen"));
		dialog.setLayout(null);
		dialog.setSize(275, 200);
		JLabel lbl_startTime = new JLabel(LanguageTranslator.getTranslation("Startzeit"));
		lbl_startTime.setBounds(5, 5, 50, 20);
		dialog.add(lbl_startTime);
		this.edt_startTime = new JTextField("0");
		this.edt_startTime.setBounds(55, 5, 50, 20);
		dialog.add(edt_startTime);
		JLabel lbl_s = new JLabel("s");
		lbl_s.setBounds(105, 5, 10, 20);
		dialog.add(lbl_s);
		this.cbx_nextFile = new JCheckBox(LanguageTranslator.getTranslation("Folgedateien im Anschluss wiedergeben"));
		this.cbx_nextFile.setBounds(5, 25, 260, 20);
		this.cbx_nextFile.setSelected(true);
		dialog.add(this.cbx_nextFile);
		cbx_calibrationFile = new JCheckBox(LanguageTranslator.getTranslation("Externe Kalibrierung"));
		cbx_calibrationFile.setBounds(5, 45, 260, 20);
		cbx_calibrationFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (cbx_calibrationFile.isSelected()) {
					panel_calibFile.setVisible(true);
				} else {
					panel_calibFile.setVisible(false);
				}
			}
			
			
		});
		dialog.add(cbx_calibrationFile);
		this.panel_calibFile = new JPanel();
		this.panel_calibFile.setBounds(5, 65, 250, 70);
		this.panel_calibFile.setLayout(null);
		this.panel_calibFile.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("externe Kalibrierungsdatei")));
		this.lbl_calibFileName = new JLabel(LanguageTranslator.getTranslation("Keine Kalibrierungsdatei ausgew�hlt"));
		this.lbl_calibFileName.setBounds(5, 20, 250, 15);
		if (this.calibrationFilePath != null) {
			this.lbl_calibFileName.setText(this.calibrationFilePath);
		}
		this.panel_calibFile.add(this.lbl_calibFileName);
		JButton btn_calibration = new JButton(LanguageTranslator.getTranslation("Kalibrierungsdatei ausw�hlen"));		
		btn_calibration.setBounds(5, 40, 210, 25);
		btn_calibration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(FileControl.workspace);
				fc.setFileFilter(new FileNameExtensionFilter(LanguageTranslator.getTranslation("Kalibrierungsdatei"), "ini"));
				int dialogState = fc.showOpenDialog(null);
				
				if (dialogState == JFileChooser.APPROVE_OPTION) {			
					calibrationFilePath = fc.getSelectedFile().getAbsolutePath();
					lbl_calibFileName.setText(fc.getSelectedFile().getName());
				}
			}
			
		});
		this.panel_calibFile.add(btn_calibration);
		this.panel_calibFile.setVisible(false);
		dialog.add(panel_calibFile);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(5, 135, 100, 25);
		btn_ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (cbx_calibrationFile.isSelected() && calibrationFilePath == null) {
						throw new IllegalArgumentException(LanguageTranslator.getTranslation("No callibration file is selected"));
					}
					double startTime_double = Double.parseDouble(edt_startTime.getText().replace(",", "."));
					long startTime = Math.round(startTime_double * 1000000);
					if (startTime < 0) {
						throw new NumberFormatException(LanguageTranslator.getTranslation("negative values are not allowed here"));
					}
					signal.setStartTime(startTime);
					signal.setLoadNextFile(cbx_nextFile.isSelected());
					loadFollowingFiles = cbx_nextFile.isSelected();
					if (cbx_calibrationFile.isSelected()) {
						signal.setCalibrationFilePath(calibrationFilePath);
					} else {
						signal.setCalibrationFilePath(null);
						calibrationFilePath = null;
						lbl_calibFileName.setText("");
					}
					lbl_cb_startTime.setText(LanguageTranslator.getTranslation("Startzeit") + ": " + GUIMath.roundDecimals(startTime_double, 3) + " s");
					if (calibrationFilePath == null || !cbx_calibrationFile.isSelected()) {
						lbl_calibration.setText(LanguageTranslator.getTranslation("interne Kalibrierung"));
					} else {
						lbl_calibration.setText(LanguageTranslator.getTranslation("Kalibrierung") + ": " + lbl_calibFileName.getText());
					}
					dialog.dispose();
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur positive Zahlenwerte als Startzeit angeben"));
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte w�hlen Sie eine Kalibrierungsdatei aus"));
				}
				
			}
			
		});
		dialog.add(btn_ok);
	}

	private void showDialog() {
		this.edt_startTime.setText(GUIMath.roundDecimals((double)signal.getStartTime() / 1000000d, 6));
		dialog.setVisible(true);
	}
	
	@Override
	public boolean fullDefined() {
		return true;
	}

	@Override
	public Signal getSignal() {
		return this.signal;
	}


	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true;
	}

	public String getPath() {
		return this.path;
	}
	
	public String getCalibrationFilePath() {
		return this.calibrationFilePath;
	}
	
	public void setCalibrationFilePath(String calibrationFilePath) {
		this.signal.setCalibrationFilePath(calibrationFilePath);
		String[] fileName = calibrationFilePath.split("\\\\");
		String[] fileName2 = fileName[fileName.length - 1].split("/");
		lbl_calibFileName.setText(fileName2[fileName2.length - 1]);
		this.cbx_calibrationFile.setSelected(true);
		this.panel_calibFile.setVisible(true);
		lbl_calibration.setText("Kalibrierung: " + fileName2[fileName2.length - 1]);
		this.calibrationFilePath = calibrationFilePath;
	}
	
	public boolean isLoadFollowingFiles() {
		return this.loadFollowingFiles;
	}
	
	public void setLoadFollowingFiles(boolean loadFollowingFiles) {
		this.signal.setLoadNextFile(loadFollowingFiles);
		this.cbx_nextFile.setSelected(loadFollowingFiles);		
	}
	
	public long getStart() {
		return this.signal.getStartTime();
	}
	
	public void setStart(long start) {
		this.signal.setStartTime(start);
		edt_startTime.setText(GUIMath.roundDecimals(((double)start) / 1000000, 6));
		lbl_cb_startTime.setText("Startzeit: " + GUIMath.roundDecimals(((double)start) / 1000000, 3) + " s");
	}
}
