package GUI;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import Controller.HardwareControl;

/**
 * Stop action for the playback of a signal
 */
public class StopAction extends AbstractAction {
	private static final long serialVersionUID = 47L;
	private PlayAction playAction;
	
	/**
	 * constructor
	 * @param playAction playback start action
	 */
	public StopAction(PlayAction playAction) {
		super(LanguageTranslator.getTranslation("Stop"), new ImageIcon("GUI/stop.png"));
		this.playAction = playAction;
	}
	
	/**
	 * action of the stop button
	 */
	public void actionPerformed(ActionEvent arg0) {
		this.playAction.stop();
		this.setEnabled(false);
		playAction.setEnabled(true);
		HardwareControl.cbx_monitor.setEnabled(true);
		HardwareControl.cbo_technologie.setEnabled(true);
		HardwareControl.cbo_soundCard.setEnabled(true);
		HardwareControl.cbx_filter.setEnabled(true);
		HardwareControl.cbo_filter.setEnabled(true);
		HardwareControl.pbr_buffer.setValue(0);
		HardwareControl.pbr_buffer.setEnabled(false);
		Main.getMainwindow().enableCanvas(true);
	}

}
