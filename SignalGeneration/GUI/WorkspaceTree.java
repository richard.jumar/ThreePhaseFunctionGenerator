package GUI;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultTreeModel;

public class WorkspaceTree extends JScrollPane {
	private static final long serialVersionUID = 50L;
	private String path;
	private JTree tree; 
	private WorkspaceTreeNodeDir root;
	private DefaultTreeModel treeModel;
	
	public WorkspaceTree() {
		this.path = Main.getWorkspaceDir().getAbsolutePath();
		this.root = new WorkspaceTreeNodeDir(null, new File(this.path));
		this.tree = new JTree(root);
		WorkspaceTreeCellRenderer wtcr = new WorkspaceTreeCellRenderer();
		this.tree.setCellRenderer(wtcr);
		this.tree.setRootVisible(false);
		this.treeModel = (DefaultTreeModel) this.tree.getModel();
		this.tree.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (tree.getPathForLocation(e.getX(), e.getY()) != null) {
					if (tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent() instanceof WorkspaceTreeNodeDir) {
						if (SwingUtilities.isRightMouseButton(e)) {
							folderContextMenu(e, (WorkspaceTreeNodeDir) tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent());
						}
					}
				}
			}
			public void mousePressed(MouseEvent e) {
				if (tree.getPathForLocation(e.getX(), e.getY()) != null && tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent() instanceof WorkspaceTreeLeaf) {
					setDraggedLeaf(((WorkspaceTreeLeaf) tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent()));
				}
			}
			public void mouseExited(MouseEvent e) {
				setDraggedLeaf(null);
			}
			
		});
		this.setTransferHandler(new TransferHandler() {
			private static final long serialVersionUID = 51L;
			public int getSourceActions(JComponent c) {
				return TransferHandler.COPY;
			}
			protected Transferable createTransferable(JComponent c) {
				Transferable t = new StringSelection("leaf");
				return t;
			}
		});
		this.tree.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				if (tree.getPathForLocation(arg0.getX(), arg0.getY()) != null && tree.getPathForLocation(arg0.getX(), arg0.getY()).getLastPathComponent() instanceof WorkspaceTreeLeaf) {
					TransferHandler h = getTransferHandler();
					h.exportAsDrag(getThis(), arg0, TransferHandler.COPY);
				}
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				
			}
			
		});
		this.setViewportView(this.tree);
	}
	
	public void enableDrag(boolean enabled) {
		this.tree.setDragEnabled(enabled);

		this.tree.setEnabled(enabled);
		this.tree.updateUI();
	}

	public void setDraggedLeaf(WorkspaceTreeLeaf leaf) {
		DataTransfer.setDraggedLeaf(leaf);
	}
	
	private void folderContextMenu(MouseEvent e, WorkspaceTreeNodeDir node) {
		JPopupMenu m = new JPopupMenu();
		JMenuItem refresh = new JMenuItem(LanguageTranslator.getTranslation("Aktualisieren"));
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				treeModel.reload(node);
			}		
		});
		m.add(refresh);
		m.show(this.tree, e.getX(), e.getY());
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	private WorkspaceTree getThis() {
		return this;
	}
	
}
