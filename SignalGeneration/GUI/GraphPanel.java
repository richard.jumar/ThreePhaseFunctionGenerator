package GUI;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import Controller.DataControl;
import Data.DoubleValueFunction;
import Data.DummyPhasor;
import Data.Periodic;
import Data.Phasor;

/**
 * Panel which is able to show DoubleVauleFunctions
 * (periodic or nonperiodic)
 *
 */
public class GraphPanel extends JPanel {
	private static final long serialVersionUID = 29L;
	private int steps; 
	private Periodic p; 
	private DoubleValueFunction f;
	private boolean symmetric;
	private Color phaseColors[] = {Color.orange, Color.blue, Color.green};
	private long start;
	private long end;
	private int[][] displayValues;
	private int center;
	
	/**
	 * constructor 
	 * 
	 * @param p periodic (set null for non periodic functions and use setNonPeriodic)
	 */
	public GraphPanel(Periodic p) {
		this.p = p;
		this.setBackground(Color.WHITE);
		this.symmetric = true;

	}

	/**
	 * set the periodic function for visualization 
	 * Redundancy with the constructor!
	 * @param p
	 */
	public void setPeriodic(Periodic p) {
		this.f = null;
		this.p = p;
	}
	
	/**
	 * set an non periodic function for visualization
	 * @param f function
	 * @param start start time of visualization
	 * @param end end time of visualization 
	 */
	public void setNonPeriodic(DoubleValueFunction f, long start, long end) {
		this.p = null;
		this.f = f;
		this.start = start;
		this.end = end;
	}
	
	/**
	 * set the symmetry parameter
	 * symmetric functions are visualized by one graph, non symmetric functions by three graphs. 
	 * @param symmetric
	 */
	public void setSymmetric(boolean symmetric) {
		this.symmetric = symmetric;
	}
	
	/**
	 * position and dimension of the panel
	 */
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		this.steps = this.getWidth() - 1;
		if (this.getHeight() % 2 == 1) {
			this.center = (this.getHeight() + 1) / 2;
		} else {
			this.center = (this.getHeight()) / 2;
		}
		this.displayValues = new int[this.getWidth()][1];
		for (int i = 0; i < this.getWidth(); i++) {
			this.displayValues[i][0] = center;
		}
	}
	
	/**
	 * draw the graphs
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawLine(0, center, this.getWidth() - 1, center);
		for (int j = 0; j < this.displayValues[0].length; j++) {
			if (!this.symmetric) {
				g.setColor(this.phaseColors[j]);
			}
			for (int i = 0; i < this.displayValues.length - 1; i++) {
				g.drawLine(i, this.displayValues[i][j], i + 1, this.displayValues[i + 1][j]);
			}
		}
	}
	
	/**
	 * calculate the graphs
	 */
	public void computeThumbnail() {
		if (p != null) { //periodic
			Phasor originalPhasor = p.getPhasor();
			DummyPhasor tmpPhasor = new DummyPhasor(steps);
			p.setPhasor(tmpPhasor);
			double max_abs = 0; 
			int maxPhases = 1;
			if (!this.symmetric) {
				maxPhases = 3;
			}
			displayValues = new int[this.getWidth()][maxPhases];
			double[][] allValues = new double[steps + 1][maxPhases];
			//find maximum absolute value
			for (int i = 0; i < steps + 1; i++) {
				double[] value = ((DoubleValueFunction) p).getValue();
				for (int j = 0; j < maxPhases; j++) {
					allValues[i][j] = value[j];
					if (value[j] < 0) {
						if (0 - value[j] > max_abs) {
							max_abs = 0 - value[j];
						}
					} else {
						if (value[j] > max_abs) {
							max_abs = value[j];
						}
					}
				}
				tmpPhasor.nextStep();
			}
			double perPixel = max_abs / ((this.getHeight() - 1) / 2);
			for (int i = 0; i < steps + 1; i++) {
				for (int j = 0; j < maxPhases; j++) {
					this.displayValues[i][j] = center - (int) (allValues[i][j] / perPixel);
				}
			}
			p.setPhasor(originalPhasor);
		} else if (this.f != null) { //non periodic
			DataControl.getClock().reset();
			DataControl.getClock().goToTimeWithoutPhasorWithoutCaching(start);
			long timeStepSize = (end - start) / (this.getWidth() - 1); 
			long time = start;
			int maxPhases = 1;
			if (!this.symmetric) {
				maxPhases = 3;
			}
			displayValues = new int[this.getWidth()][maxPhases];
			double max_abs = 0; 
			double[][] allValues = new double[steps + 1][maxPhases];
			for (int i = 0; i < this.getWidth(); i++) {
				double[] value = (f).getValue();
				for (int j = 0; j < maxPhases; j++) {
					allValues[i][j] = value[j];
					if (value[j] < 0) {
						if (0 - value[j] > max_abs) {
							max_abs = 0 - value[j];
						}
					} else {
						if (value[j] > max_abs) {
							max_abs = value[j];
						}
					}
					
				}
				time += timeStepSize;
				DataControl.getClock().goToTimeWithoutPhasorWithoutCaching(time);
			}
			DataControl.getClock().reset();
			double perPixel = max_abs / ((this.getHeight() - 1) / 2);			
			for (int i = 0; i < steps + 1; i++) {
				for (int j = 0; j < maxPhases; j++) {
					this.displayValues[i][j] = center - (int) (allValues[i][j] / perPixel);

				}

			}
		} else {
			Canvas.canvasLog("No function for visualization found");
		}
		this.updateUI();
	}
}
