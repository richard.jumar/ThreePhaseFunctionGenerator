package GUI;

public class DataTransfer {

	private static WorkspaceTreeLeaf draggedLeaf;
	
	public static void setDraggedLeaf(WorkspaceTreeLeaf draggedLeaf) {
		DataTransfer.draggedLeaf = draggedLeaf;
	}
	
	public static WorkspaceTreeLeaf getDraggedLeaf() {
		return DataTransfer.draggedLeaf;
	}
}
