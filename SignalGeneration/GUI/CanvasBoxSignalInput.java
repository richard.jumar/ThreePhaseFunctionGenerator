package GUI;

import Data.AmplitudeModulation;

public abstract class CanvasBoxSignalInput extends CanvasBoxInput {
	private static final long serialVersionUID = 19L;


	public CanvasBoxSignalInput(Canvas canvas) {
		super(canvas, ConnectorType.SignalOutput);
	}


	public AmplitudeModulation getAmplitudeModulation() {
		return null;
	}

}
