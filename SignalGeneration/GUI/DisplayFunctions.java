package GUI;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import Data.Angle;
import Data.PeriodicInterpolationPoint;

public class DisplayFunctions {

	public static String roundDecimals(double value, int decimals) {
		String value_str = value + "";
		String[] parts = value_str.split("\\.");
		if (parts.length == 1) {
			return parts[0];
		} else if (parts.length == 2) {
			if (parts[1].length() < decimals) {
				decimals = parts[1].length(); //decimals only if necessary
			}
			return parts[0] + "," + parts[1].substring(0, decimals);
		}
		return "ERROR";
	}
	
	public static Angle periodicDisplayXToAngle(int x, int width) {
		double angle_double = (double) x / ((double) width) * Math.pow(2, 64);
		if (angle_double > (double) Long.MAX_VALUE) {
			angle_double = angle_double - Math.pow(2, 64);
		}
		return new Angle(Math.round(angle_double));
	}
		
	public static List<PeriodicInterpolationPoint> canvasValuesToDoubleValues(List<Point2D> canvasPoints, String min, String max, int canvasMin, int canvasMax, double canvasMean, int width) {
		List<PeriodicInterpolationPoint> piPoints = new ArrayList<PeriodicInterpolationPoint>();
		if (max.equals("auto") || min.equals("auto")) {
			//mean voltage = 0
			double voltPerPixel = 0;
			if (max.equals("auto")) {
				double minVoltage = Double.parseDouble(min.replace(',', '.'));
				if (minVoltage < 0) {
					voltPerPixel = (0 - minVoltage) / (canvasMin - canvasMean);
				} else {
					voltPerPixel = minVoltage / (canvasMin - canvasMean);
				}
				for (int i = 0; i < canvasPoints.size(); i++) {
					piPoints.add(new PeriodicInterpolationPoint(DisplayFunctions.periodicDisplayXToAngle((int) Math.round(canvasPoints.get(i).getX()), width), (canvasMean - canvasPoints.get(i).getY()) * voltPerPixel));
				}
			} else if (min.equals("auto")) {
				double maxVoltage = Double.parseDouble(max.replace(',', '.'));
				if (maxVoltage < 0) {
					voltPerPixel = (0 - maxVoltage) / (canvasMean - canvasMax);
				} else {
					voltPerPixel = maxVoltage / (canvasMean - canvasMax);
				}
				for (int i = 0; i < canvasPoints.size(); i++) {
					piPoints.add(new PeriodicInterpolationPoint(DisplayFunctions.periodicDisplayXToAngle((int) Math.round(canvasPoints.get(i).getX()), width), (canvasMean - canvasPoints.get(i).getY()) * voltPerPixel));
				}
			}		
		} else {
			//min and max is given
			double minVoltage = Double.parseDouble(min.replace(',', '.'));
			double maxVoltage = Double.parseDouble(max.replace(',', '.'));
			double voltPerPixel = (maxVoltage - minVoltage) / (canvasMin - canvasMax);
			for (int i = 0; i < canvasPoints.size(); i++) {
				piPoints.add(new PeriodicInterpolationPoint(DisplayFunctions.periodicDisplayXToAngle((int) Math.round(canvasPoints.get(i).getX()), width), (minVoltage + ((canvasMin - canvasPoints.get(i).getY()) * voltPerPixel))));
			}
				
		}
		return piPoints;
	}

}
