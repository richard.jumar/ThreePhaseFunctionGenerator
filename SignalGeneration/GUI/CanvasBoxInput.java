package GUI;

/**
 * Canvas boxes which are input elements for the tree (leafs). 
 *
 */
public abstract class CanvasBoxInput extends CanvasBox {
	private static final long serialVersionUID = 15L;

	public CanvasBoxInput(Canvas canvas, ConnectorType outputType) {
		super(canvas);
		setOutput(new CanvasConnector(canvas, this, outputType));
		this.input = null; 
	}
	
	/**
	 * returns the parent node in the tree or null, if this box is not connected
	 */
	public CanvasTreeElement getParentNode() {
		if (this.connectedWith == null) {
			return null;
		}
		return this.connectedWith.getTreeElement();
	}

	/**
	 * returns the highest ranked element in the tree that can be reached over the connections from this box
	 */
	public CanvasTreeElement getToplevelElementInTreeSegment() {
		CanvasTreeElement node = this.connectedWith.getTreeElement();
		while(node != null) {
			node = node.getParentNode();
		}
		return node;
	}


	public int getFreeInputs(ConnectorType type) {
		return 0; //only 1 output
	}

	@Override
	public void resetSubTree(boolean startHere) {
		//no subtree
	}

	@Override
	public boolean isVariable() {
		return false;
	}

	@Override
	public CanvasConnector getConnectorFor(CanvasTreeElement te) {
		return this.output; //only 1 output => only 1 connector
	}

	/**
	 * get the output connector
	 */
	public CanvasConnector getOutputConnector() {		
		return this.output;
	}

}
