package GUI;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

public class WorkspaceTreeNodeDir implements TreeNode {
	private File dir;
	private TreeNode parent;
	private FileFilter filter;
	public WorkspaceTreeNodeDir(TreeNode parent, File dir) {
		this.dir = dir;
		this.parent = parent;
		this.filter = new WorkspaceTreeFileFilter();
	}
	
	
	public String toString() {
		return this.dir.getName();
	}
	
	@Override
	public Enumeration<WorkspaceTreeLeafType> children() {
		return null;
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * comparator for files. 
	 * directories first, then files
	 * the order in a category is alphabetically
	 */
	Comparator<File> dirAndFileComparator = new Comparator<File>() {
		@Override
		public int compare(File arg0, File arg1) {
			if (arg0.isDirectory() & !arg1.isDirectory()) {
				return -1;
			} else if (!arg0.isDirectory() & arg1.isDirectory()) {
				return 1;
			} else {
				return arg0.compareTo(arg1);
			}
		}

	};
	
	@Override
	public TreeNode getChildAt(int childIndex) {
		File[] files = this.dir.listFiles(this.filter);
		Arrays.sort(files, dirAndFileComparator);
		if (files[childIndex].isDirectory()) {
			return new WorkspaceTreeNodeDir(this, files[childIndex]);
		} else {
			return new WorkspaceTreeLeaf(this, files[childIndex]);
		}
	}


	
	@Override
	public int getChildCount() {
		return this.dir.listFiles(this.filter).length;
	}

	@Override
	public int getIndex(TreeNode node) {
		return 0;
	}

	@Override
	public TreeNode getParent() {
		return this.parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
