package GUI;

import java.awt.Color;
import java.awt.Graphics;

import Data.AmplitudeModulation;
import Data.MultAmplitudeModulation;
import Data.MultSignalAmplitudeModulation;
import Data.Signal;

public class CanvasOperatorMult extends CanvasOperator {

	private static final long serialVersionUID = 28L;
	private AmplitudeModulation amplitudeModulation;
	private Signal signal;
	
	public CanvasOperatorMult(Canvas canvas, int x, int y) {
		super(canvas);
		this.setBounds(x, y, 51, 60);
		this.output = new CanvasConnector(canvas, this, ConnectorType.SignalOutput);
		this.output.setAlternativeType(ConnectorType.AMOutput);
		this.output.setBounds(21, 1, 9, 9);
		this.output.setConnectionPointOffset(4, 8);
		this.add(this.output);		
		this.input0 = new CanvasConnector(canvas, this, ConnectorType.SignalInput);
		this.input0.setAlternativeType(ConnectorType.AMInput);
		this.input0.setBounds(1, 51, 9, 9);
		this.input0.setConnectionPointOffset(8, 2);
		this.add(this.input0);
		this.input1 = new CanvasConnector(canvas, this, ConnectorType.SignalInput);
		this.input1.setAlternativeType(ConnectorType.AMInput);
		this.input1.setBounds(41, 51, 9, 9);
		this.input1.setConnectionPointOffset(0, 2);
		this.add(this.input1);
	}

	/**
	 * Tell the neighbours that the output/input type could be changed
	 */
	protected void updateNeighbors(CanvasOperator updateSource) {
		if (updateSource == this.parent) {
			//update information from the parent operator
			if (parent instanceof CanvasOperator) {
				if (((CanvasOperator) parent).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) parent).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.output.setType(((CanvasOperator) parent).getConnectorTypeTo(this).opposite());
						this.output.setAlternativeType(ConnectorType.nothing);
					}
				}
			}
		}
		if (updateSource == this.child0 || updateSource == null) {
			if (child0 instanceof CanvasOperator) {
				if (((CanvasOperator) child0).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) child0).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.input0.setType(((CanvasOperator) child0).getConnectorTypeTo(this).opposite());
						this.input0.setAlternativeType(ConnectorType.nothing);
					}
				}
			} else {
				if (child0 != null) {
					this.input0.setType(child0.getConnectorFor(this).getType().opposite());
					this.input0.setAlternativeType(ConnectorType.nothing);
				}
			}
		}
		if (updateSource == this.child1 || updateSource == null) {
			if (child1 instanceof CanvasOperator) {
				if (((CanvasOperator) child1).getAlternativeConnectorTypeTo(this) == ConnectorType.nothing) {
					if (((CanvasOperator) child1).getConnectorTypeTo(this) != ConnectorType.nothing) {
						this.input1.setType(((CanvasOperator) child1).getConnectorTypeTo(this).opposite());
						this.input1.setAlternativeType(ConnectorType.nothing);
					}
				}
			} else {
				if (child1 != null) {
					this.input1.setType(child1.getConnectorFor(this).getType().opposite());
					this.input1.setAlternativeType(ConnectorType.nothing);
				}
			}
		}
		
		if (this.output.getAlternativeType() == ConnectorType.nothing) {
			//case output type is defined
			if (this.output.getType() == ConnectorType.SignalOutput) {
				//one input is signal, one input is amplitude modulation
				if (this.input0.getAlternativeType() == ConnectorType.nothing) {
					//input0 is defined
					if (this.input0.getType() == ConnectorType.SignalInput) {
						//define input 1 as AMInput
						this.input1.setType(ConnectorType.AMInput);
						this.input1.setAlternativeType(ConnectorType.nothing);
					} else if (this.input0.getType() == ConnectorType.AMInput) {
						//define input1 as SignalInput
						this.input1.setType(ConnectorType.SignalInput);
						this.input1.setAlternativeType(ConnectorType.nothing);
					}
				} else if (this.input1.getAlternativeType() == ConnectorType.nothing) {
					//input1 is defined
					if (this.input1.getType() == ConnectorType.SignalInput) {
						//define input1 as SignalInput
						this.input0.setType(ConnectorType.AMInput);
						this.input0.setAlternativeType(ConnectorType.nothing);
					} else if (this.input1.getType() == ConnectorType.AMInput) {
						//define input0 as AMInput
						this.input0.setType(ConnectorType.SignalInput);
						this.input0.setAlternativeType(ConnectorType.nothing);
					}
				}
			} else if (this.output.getType() == ConnectorType.AMOutput) {
				//two AM-Inputs
				this.input0.setType(ConnectorType.AMInput);
				this.input0.setAlternativeType(ConnectorType.nothing);
				this.input1.setType(ConnectorType.AMInput);
				this.input1.setAlternativeType(ConnectorType.nothing);
			}
		} 
		if (this.input0.getAlternativeType() == ConnectorType.nothing && this.input0.getType() == ConnectorType.SignalInput) {
			//input0 is defined as SignalInput => Output is signal, input1 is AM
			this.input1.setType(ConnectorType.AMInput);
			this.input1.setAlternativeType(ConnectorType.nothing);
			this.output.setType(ConnectorType.SignalOutput);
			this.output.setAlternativeType(ConnectorType.nothing);
		}
		if (this.input1.getAlternativeType() == ConnectorType.nothing && this.input1.getType() == ConnectorType.SignalInput) {
			//input1 is defined as SignalInput => output is a signal, input0 is am
			this.input0.setType(ConnectorType.AMInput);
			this.input0.setAlternativeType(ConnectorType.nothing);
			this.output.setType(ConnectorType.SignalOutput);
			this.output.setType(ConnectorType.nothing);
		}
		if (this.input0.getAlternativeType() == ConnectorType.nothing && this.input1.getAlternativeType() == ConnectorType.nothing) {
			//both inputs are defined
			if (this.input0.getType() == ConnectorType.SignalInput || this.input1.getType() == ConnectorType.SignalInput) {
				//one input is a signal => define output as signal
				this.output.setType(ConnectorType.SignalOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			} else {
				//both inputs are am => define output as am
				this.output.setType(ConnectorType.AMOutput);
				this.output.setAlternativeType(ConnectorType.nothing);
			}
		}
		if (updateSource != parent) {
			if (parent instanceof CanvasOperator) {
				((CanvasOperator) parent).updateNeighbors(this);
			}
		}
		if (updateSource != child0) {
			if (child0 instanceof CanvasOperator) {
				((CanvasOperator) child0).updateNeighbors(this);
			}
		}
		if (updateSource != child1) {
			if (child1 instanceof CanvasOperator) {
				((CanvasOperator) child1).updateNeighbors(this);
			}
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (canvas.getMode() == CanvasMode.DELETE && this.mouseOver) {
			g.setColor(Color.RED);
			g.fillOval(0, 9, 50, 50);
			g.setColor(Color.WHITE);
			g.fillOval(16, 25, 19, 19);
		} else {
			g.fillOval(16, 25, 19, 19);
		}
	}

	@Override
	public void resetSubTree(boolean startHere) {
		//reset inputs and output to variable inputs/output
		this.output.setType(ConnectorType.SignalOutput);
		this.output.setAlternativeType(ConnectorType.AMOutput);
		this.input0.setType(ConnectorType.SignalInput);
		this.input0.setAlternativeType(ConnectorType.AMInput);
		this.input1.setType(ConnectorType.SignalInput);
		this.input1.setAlternativeType(ConnectorType.AMInput);
		if (this.child0 != null) {
			child0.resetSubTree(false);
		}
		if (this.child1 != null) {
			child1.resetSubTree(false);
		}
 	}


	public boolean isVariable() {
		return true;
	}


	@Override
	public Signal getSignal() {
		return this.signal;
	}



	@Override
	public void updateSignalOrAmplitudeModulation() {
		if (this.input0.getAlternativeType() == ConnectorType.nothing && this.input1.getAlternativeType() == ConnectorType.nothing && this.child0 != null && this.child1 != null) { //both inputs are not variable and not empty
			if (this.input0.getType() == ConnectorType.AMInput && this.input1.getType() == ConnectorType.AMInput) { //case am
				this.signal = null; 
				if (this.child0.getAmplitudeModulation() != null && this.child1.getAmplitudeModulation() != null) {
					this.amplitudeModulation = new MultAmplitudeModulation(this.child0.getAmplitudeModulation(), this.child1.getAmplitudeModulation());
				} else {
					this.amplitudeModulation = null;
				}
			} else { //case signal

				this.amplitudeModulation = null;
				if (this.child0.getAmplitudeModulation() != null && this.child1.getSignal() != null) { //child0 is am and child1 signal
					this.signal = new MultSignalAmplitudeModulation(this.child1.getSignal(), this.child0.getAmplitudeModulation());
				} else if (this.child0.getSignal() != null && this.child1.getAmplitudeModulation() != null) {//child0 is signal and child1 am
					this.signal = new MultSignalAmplitudeModulation(this.child0.getSignal(), this.child1.getAmplitudeModulation());
				} else {
					this.signal = null;
				}
			}
		}
		if (this.parent != null) {
			this.parent.updateSignalOrAmplitudeModulation();
		}

	}

	@Override
	public AmplitudeModulation getAmplitudeModulation() {
		return this.amplitudeModulation;
	}

}
