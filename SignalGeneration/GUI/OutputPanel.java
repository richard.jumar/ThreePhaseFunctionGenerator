package GUI;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

public class OutputPanel extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 44L;
	private JLabel label;
	
	/**
	 * 
	 * @param text displayed on the panel
	 * @param signal true for signal, false for amplitude modulation
	 */
	public OutputPanel(String text) {
		super();
		this.label = new JLabel(text);
		this.add(this.label);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setTransferHandler(new TransferHandler() {

			private static final long serialVersionUID = 45L;
			public int getSourceActions(JComponent c) {
				return TransferHandler.COPY;
			}
			protected Transferable createTransferable(JComponent c) {
				Transferable t = new StringSelection(text);
				return t;
			}
		});
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		TransferHandler h = this.getTransferHandler();
		h.exportAsDrag(this, arg0, TransferHandler.COPY);
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}
	

	public void setEnabled(boolean enabled) {
		this.label.setEnabled(enabled);
	}

}
