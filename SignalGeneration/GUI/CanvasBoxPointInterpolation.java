package GUI;

import java.awt.geom.Point2D;
import java.util.List;

import Data.Phasor;

public interface CanvasBoxPointInterpolation {
	public void setLbl_pointsText(String text);
	public void updateMarkers();
	public void updateUI();
	public boolean isPeriodicSelected();
	public Phasor getSelectedPhasor();
	public String getEdt_minVoltageText();
	public String getEdt_maxVoltageText();
	public int getCanvasMin();
	public int getCanvasMax();
	public double getCanvasMean();
	public int getCanvasWidth();
	public String getEdt_NPVoltageText();
	public String getEdt_startTimeText();
	public String getEdt_endTimeText();
	public String getAmplitudeDefinitionType();
	public int getZeroLinePosition();
	public List<Point2D> getPointInterpolationCanvasPointList();
	public int getPeriodicInterpolationNumber();
	public void chooseInterpolationType(int type);
	public boolean addPointToPiCanvas(Point2D p);
	public void setAmplitudeEdits(String edt_max, String edt_min);	
	public void setPeriodicMode();
	public void setNonPeriodicMode();
	public void setLineParams(int min, int max, int zeroLine);
	public void setAmplitudeDefinitionType(String type);
	public void setTimeEdits(String start, String end);
	public void btn_ok_periodic_click();
	public void btn_ok_nonperiodic_click();
	public void setPhasor(Phasor phasor);
}