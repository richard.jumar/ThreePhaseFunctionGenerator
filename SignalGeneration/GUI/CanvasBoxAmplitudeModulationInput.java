package GUI;

import Data.Signal;

/**
 * Canvas box for amplitude modulation input -- leaf of the tree on the canvas
 *
 */
public abstract class CanvasBoxAmplitudeModulationInput extends CanvasBoxInput {
	private static final long serialVersionUID = 10L;

	public CanvasBoxAmplitudeModulationInput(Canvas canvas) {
		super(canvas, ConnectorType.AMOutput);
	}

	/**
	 * signal is null
	 */
	public Signal getSignal() {
		return null; //amplitude modulation is no signal
	}
}
