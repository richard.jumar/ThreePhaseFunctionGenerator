package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controller.DataControl;
import Data.Angle;
import Data.Frequency;
import Data.NonPeriodicPartwiseDefinedSignal;
import Data.PeriodicPartwiseDefinedSignal;
import Data.Phasor;
import Data.PolynomSegment;
import Data.Signal;

/**
 * canvas box for piecewise defined functions with polynomials 
 */
public class CanvasBoxSignalFunction extends CanvasBoxSignalInput {
	private static final long serialVersionUID = 17L;
	private JButton btn_edit;
	private JDialog mainDialog;
	private JPanel phasorPanel;
	private JPanel timePanel;
	private JComboBox<Phasor> cbo_phasor;
	private JTextField edt_startTime;
	private JLabel lbl_phasor;
	private JLabel lbl_startTime_cb;
	private JTable tbl_parts;
	private DefaultTableModel polynomTableModel;
	private List<PolynomSegment> polynoms_periodic;
	private List<PolynomSegment> polynoms_nonperiodic;
	private PeriodicPartwiseDefinedSignal signal_periodic;
	private NonPeriodicPartwiseDefinedSignal signal_nonperiodic;
	private GraphPanel gp;
	private GraphPanel gp_box;
	private SymbolPhasorMini phasorSymbol;
	private JLabel lbl_polySections;
	JRadioButton radio_periodic;
	JRadioButton radio_nonPeriodic;
	private boolean periodic;

	/**
	 * Constructor
	 * @param canvas canvas
	 * @param x x position
	 * @param y y position
	 */
	public CanvasBoxSignalFunction(Canvas canvas, int x, int y) {
		super(canvas);
		this.periodic = true;
		this.canvas = canvas;
		this.polynoms_periodic = new ArrayList<PolynomSegment>();
		this.polynoms_nonperiodic = new ArrayList<PolynomSegment>();
		this.signal_periodic = new PeriodicPartwiseDefinedSignal(null);
		this.signal_nonperiodic = new NonPeriodicPartwiseDefinedSignal(DataControl.getClock());
		this.setLayout(null);
		this.setBounds(x, y, 200, 200);
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Abschnittsw. def. Funktion"));
		title.setBounds(5, 5, 195, 10);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.add(status);
		this.phasorSymbol = new SymbolPhasorMini();
		phasorSymbol.setBounds(6, 22, 13, 13);
		this.add(phasorSymbol);
		lbl_phasor = new JLabel("<" + LanguageTranslator.getTranslation("nicht ausgew�hlt") + ">");
		lbl_phasor.setBounds(23, 20, 140, 15);
		this.add(lbl_phasor);
		this.lbl_startTime_cb = new JLabel();
		this.lbl_startTime_cb.setBounds(5, 20, 190, 15);
		this.lbl_startTime_cb.setVisible(false);
		this.add(this.lbl_startTime_cb);
		this.lbl_polySections = new JLabel("0 " + LanguageTranslator.getTranslation("Polynomabschnitte"));
		this.lbl_polySections.setBounds(5, 50, 190, 15);
		this.add(lbl_polySections);
		this.gp_box = new GraphPanel(this.signal_periodic);
		this.gp_box.setBounds(1, 85, 198, 80);
		this.add(gp_box);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		this.btn_edit.setBounds(1, 170, 198, 29);
		this.btn_edit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}
		});
		this.add(btn_edit);


		this.mainDialog = new JDialog();
		this.mainDialog.setTitle(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion"));
		this.mainDialog.setModal(true);
		this.mainDialog.setLayout(null);
		this.mainDialog.setSize(530, 500);
		JPanel intOptionPanel = new JPanel();
		intOptionPanel.setLayout(null);
		intOptionPanel.setBounds(5, 20, 250, 50);
		intOptionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Interpolationseinstellungen")));
		this.mainDialog.add(intOptionPanel);
		ButtonGroup btnGroup_periodic = new ButtonGroup();
		radio_periodic = new JRadioButton(LanguageTranslator.getTranslation("periodisch"));
		radio_periodic.setSelected(true);
		radio_periodic.setBounds(10, 20, 100, 20);
		radio_periodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setPeriodic();
			}
			
		});
		btnGroup_periodic.add(radio_periodic);
		intOptionPanel.add(radio_periodic);
		radio_nonPeriodic = new JRadioButton(LanguageTranslator.getTranslation("nichtperiodisch"));
		radio_nonPeriodic.setBounds(110, 20, 125, 20);
		radio_nonPeriodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setNonPeriodic();
			}

		});
		btnGroup_periodic.add(radio_nonPeriodic);
		intOptionPanel.add(radio_nonPeriodic);
		this.phasorPanel = new JPanel();
		this.phasorPanel.setLayout(null);
		this.phasorPanel.setBounds(260, 20, 250, 50);
		this.phasorPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger")));
		cbo_phasor = new JComboBox<Phasor>();
		updateCbo_phasor();
		cbo_phasor.setBounds(10, 20, 100, 20);
		this.phasorPanel.add(cbo_phasor);
		JButton btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu") + "...");
		btn_newPhasor.setBounds(120, 20, 80, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
			}
		});
		this.phasorPanel.add(btn_newPhasor);
		this.phasorPanel.setVisible(true);
		this.mainDialog.add(this.phasorPanel);
		this.timePanel = new JPanel();
		this.timePanel.setLayout(null);
		this.timePanel.setBounds(260, 20, 250, 50);
		this.timePanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Wiedergabe")));
		JLabel lbl_startTime = new JLabel(LanguageTranslator.getTranslation("Wiedergabestartzeit"));
		lbl_startTime.setBounds(10, 20, 140, 20);
		this.timePanel.add(lbl_startTime);
		this.edt_startTime = new JTextField();
		this.edt_startTime.setBounds(150, 20, 80, 20);
		this.timePanel.add(this.edt_startTime);
		JLabel lbl_startTimeUnit = new JLabel("s");
		lbl_startTimeUnit.setBounds(230, 20, 10, 20);
		this.timePanel.add(lbl_startTimeUnit);
		this.timePanel.setVisible(false);
		this.mainDialog.add(this.timePanel);
		String[] colums = {LanguageTranslator.getTranslation("Start"), LanguageTranslator.getTranslation("Polynom")};
		tbl_parts = new JTable(new DefaultTableModel(colums, 0));
		DefaultTableCellRenderer rightAlignRenderer = new DefaultTableCellRenderer();
		rightAlignRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		tbl_parts.getColumnModel().getColumn(0).setCellRenderer(rightAlignRenderer);
		tbl_parts.getColumnModel().getColumn(1).setCellRenderer(rightAlignRenderer);
		polynomTableModel = (DefaultTableModel) tbl_parts.getModel();
		JScrollPane table = new JScrollPane(tbl_parts);
		table.setBounds(5, 80, 500, 100);
		mainDialog.add(table);
		JButton btn_new = new JButton(LanguageTranslator.getTranslation("Hinzuf�gen"));
		btn_new.setBounds(5, 185, 120, 20);
		btn_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editParts(null, periodic);
			}	
		});
		mainDialog.add(btn_new);
		JButton btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		btn_edit.setEnabled(false);
		btn_edit.setBounds(130, 185, 120, 20);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_parts.getSelectedRow() > -1) { //-1 is default (if no row is selected)
					//TODO 
				}
			}
		});
		mainDialog.add(btn_edit);
		JButton btn_delete = new JButton(LanguageTranslator.getTranslation("Entfernen"));
		btn_delete.setBounds(255, 185, 120, 20);
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_parts.getSelectedRow() > -1) { //-1 is default (if no row is selected)
					if (periodic) {
						signal_periodic.removePart0to360(tbl_parts.getSelectedRow());
						updatePolynomTable(true);
					} else {
						signal_nonperiodic.removePart(tbl_parts.getSelectedRow());
						updatePolynomTable(false);
					}

				}
			}
		});
		mainDialog.add(btn_delete);
		this.gp = new GraphPanel(this.signal_periodic);
		this.gp.setBounds(5, 210, 500, 200);
		mainDialog.add(gp);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(405, 420, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				click_ok();	
			}
		});
		this.mainDialog.add(btn_ok);
	}
	
	/**
	 * click the ok button
	 */
	public void click_ok() {
		if (periodic) {
			if (cbo_phasor.getSelectedItem() instanceof Phasor) {
				setPhasor((Phasor) cbo_phasor.getSelectedItem());						
				if (fullDefined()) {
					if (connectedWith != null) {
						status.setStatusDefindedAndConnected();
					} else {
						status.setStatusDefined();
					}
				}
				gp_box.setPeriodic(signal_periodic);
				if (cbo_phasor.getSelectedIndex() != -1) {
					lbl_phasor.setText(cbo_phasor.getSelectedItem().toString() + " (" + Frequency.microHz_valueToHzString(((Phasor)cbo_phasor.getSelectedItem()).getFrequency().getF0_phase1(), 4) + ")");
				}
				this.lbl_phasor.setVisible(true);
				this.phasorSymbol.setVisible(true);
				this.lbl_startTime_cb.setVisible(false);
				this.lbl_polySections.setText(signal_periodic.getNumberOfSegments() + " " + LanguageTranslator.getTranslation("Polynomsegmente"));
				gp_box.computeThumbnail();
				updateUI();
				mainDialog.dispose();
			} else {
				JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Ein Drehzeiger muss ausgew�hlt sein!"));
			}
		} else { //non periodic
			try {
				long startTime = Math.round(Double.parseDouble(edt_startTime.getText().replace(',', '.')) * 1000000);
				if (startTime < 0) {
					throw new NumberFormatException("start time must be greater or equal 0");
				}
				if (fullDefined()) {
					if (connectedWith != null) {
						status.setStatusDefindedAndConnected();
					} else {
						status.setStatusDefined();
					}
				}
				long gp_box_startTime = signal_nonperiodic.getFirstSegmentStart();
				long gp_box_endTime = (long) (signal_nonperiodic.getLastSegmentStart() + 0.1 * (signal_nonperiodic.getLastSegmentStart() - signal_nonperiodic.getFirstSegmentStart()));
				if (gp_box_endTime == gp_box_startTime) { //case: only one segment
					gp_box_endTime = gp_box_startTime + 1000000; // 1s
				}
				
				gp_box.setNonPeriodic(signal_nonperiodic, gp_box_startTime, gp_box_endTime);
				this.lbl_polySections.setText(signal_nonperiodic.getNumberOfSegments() + " " + LanguageTranslator.getTranslation("Polynomsegmente"));
				this.lbl_phasor.setVisible(false);
				this.phasorSymbol.setVisible(false);
				this.lbl_startTime_cb.setText(LanguageTranslator.getTranslation("Startzeit") + " " + GUIMath.roundDecimals(((double) startTime)/1000000 , 3) + "s");
				this.lbl_startTime_cb.setVisible(true);
				gp_box.computeThumbnail();
				signal_nonperiodic.setStartTime(startTime);
				updateUI();
				mainDialog.dispose();
				
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(mainDialog, LanguageTranslator.getTranslation("Bitte einen g�ltigen Startzeitpunkt angeben"));	
			}

		}
		if (this.getParentNode() != null) {
			this.getParentNode().updateSignalOrAmplitudeModulation();
		}
		canvas.updateTopLevelElement();
	}
	
	/**
	 * get the polynomial list
	 * @return
	 */
	public List<PolynomSegment> getPolynoms() {
		if (this.periodic) {
			return this.polynoms_periodic; 
		} else {
			return polynoms_nonperiodic;
		}
	}
	
	/**
	 * set the phasor
	 * @param phasor
	 */
	public void setPhasor(Phasor phasor) {
		signal_periodic.setPhasor(phasor); 
		lbl_phasor.setText(phasor.toString() + " (" + Frequency.microHz_valueToHzString(phasor.getFrequency().getF0_phase1(), 4) + ")");
		this.cbo_phasor.setSelectedItem(phasor);
		this.setPeriodic();
	}
	
	/**
	 * get the selected phasor
	 * please check yourself whether the mode is periodic
	 * @return
	 */
	public Phasor getSelectedPhasor() {
		return signal_periodic.getPhasor();
	}

	/**
	 * edit a part of the piecewise defined function
	 * @param part
	 * @param periodic 
	 */
	private void editParts(Object part, boolean periodic) {
		PolynomSegment polynom = new PolynomSegment(0l, new ArrayList<Double>()); //startTime 0 must be updated later!
		JDialog dialog = new JDialog();
		dialog.setModal(true);
		dialog.setLayout(null);
		dialog.setSize(260, 290);
		if (part == null) {
			dialog.setTitle(LanguageTranslator.getTranslation("Neuer Abschnitt"));
		} else {
			dialog.setTitle(LanguageTranslator.getTranslation("Abschnitt bearbeiten"));
		}
		JLabel lbl_start = new JLabel(LanguageTranslator.getTranslation("Start bei"));
		lbl_start.setBounds(5, 5, 60, 20);
		dialog.add(lbl_start);
		JTextField edt_start = new JTextField();
		edt_start.setBounds(65, 5, 60, 20);
		dialog.add(edt_start);
		JLabel lbl_degree;
		if (periodic) {
			lbl_degree = new JLabel("�");
		} else {
			lbl_degree = new JLabel("s");
		}
		lbl_degree.setBounds(126, 5, 10, 20);
		dialog.add(lbl_degree);
		String[] colums = {LanguageTranslator.getTranslation("Faktor"), LanguageTranslator.getTranslation("Potenz")};
		JTable tbl_polynom = new JTable(new DefaultTableModel(colums, 0));
		DefaultTableCellRenderer rightAlignRenderer = new DefaultTableCellRenderer();
		rightAlignRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		tbl_polynom.getColumnModel().getColumn(0).setCellRenderer(rightAlignRenderer);
		tbl_polynom.getColumnModel().getColumn(1).setCellRenderer(rightAlignRenderer);
		DefaultTableModel tableModel = (DefaultTableModel) tbl_polynom.getModel();
		JScrollPane table = new JScrollPane(tbl_polynom);
		table.setBounds(5, 30, 245, 100);
		dialog.add(table);
		JTextField edt_factor = new JTextField("0");
		edt_factor.setBounds(5, 135, 50, 20);
		dialog.add(edt_factor);
		JLabel lbl_dot = new JLabel("\u00B7 x^");
		lbl_dot.setBounds(60, 135, 25, 20);
		dialog.add(lbl_dot);
		JTextField edt_exponent = new JTextField("0");
		edt_exponent.setBounds(85, 135, 50, 20);
		dialog.add(edt_exponent);
		JButton btn_add = new JButton(LanguageTranslator.getTranslation("Hinzuf�gen"));
		btn_add.setBounds(140, 135, 110, 20);
		btn_add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					double factor = Double.parseDouble(edt_factor.getText().replace(',', '.'));
					int exponent = Integer.parseInt(edt_exponent.getText().replace(',', '.'));
					if (exponent < 0) {
						JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Der Exponent darf nicht negativ sein."));
					} else {
						double factor_corr;
						if (periodic) {
							factor_corr = factor * Math.pow(360, exponent) / Math.pow(Math.pow(2, 64), exponent);
						} else {
							factor_corr = factor / Math.pow(1000000, exponent); 
						}
						if (polynom.addParameter(exponent, factor_corr)) {
							while(tableModel.getRowCount() > 0) {	//clear table
								tableModel.removeRow(0);
							}
							for (int i = 0; i < polynom.getParameters().size(); i++) {
								if (polynom.getParameters().get(i) != 0) {
									String[] data = new String[2];
									data[1] = i + "";
									if (periodic) {
										data[0] = GUIMath.roundDecimals(polynom.getParameters().get(i) * Math.pow(Math.pow(2, 64), i) / Math.pow(360, i), 3);
									} else {
										data[0] = GUIMath.roundDecimals(polynom.getParameters().get(i) * Math.pow(1000000, i), 3);
									}
									tableModel.addRow(data);
								}
							}
							//make textfields ready for the next input
							edt_factor.setText("0");
							edt_exponent.setText((exponent + 1) + "");
						} else {
							JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Dieser Exponent ist bereits vorhanden"));
						}
					}
					
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur Zahlenwerte eingeben"));	
				}

			}
		});
		dialog.add(btn_add);
		JLabel lbl_origin = new JLabel();
		if (periodic) {
			lbl_origin.setText(LanguageTranslator.getTranslation("<html>Hinweis: Der Ursprung befindet<br>sich bei 0�</html>"));
		} else {
			lbl_origin.setText(LanguageTranslator.getTranslation("<html>Hinweis: Der Ursprung befindet<br>sich beim angegebenen Startzeitpunkt<br>des Polynomsegments</html>"));
		}
		lbl_origin.setBounds(5, 160, 250, 60);
		dialog.add(lbl_origin);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(5, 225, 245, 25);
		btn_ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (periodic) {
					try {
						double angle_double = Double.parseDouble(edt_start.getText().replace(',', '.'));
						polynom.setStart(Angle.DegreeToAngle(angle_double));
						while (angle_double >= 180) {
							angle_double -= 360;
							polynom.shiftThis(-Math.pow(2, 64));
						}
						while (angle_double < -180) {
							angle_double += 360;
							polynom.shiftThis(Math.pow(2, 64));
						}
						addPolynomSegment(polynom, true);
						
						//polynom.shift(0d);
						dialog.dispose();
					} catch (NumberFormatException e) {
						JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte einen g�ltigen Startwinkel angeben"));	
					}
				} else {
					try {
						double startTime_double = Double.parseDouble(edt_start.getText().replace(',', '.'));
						long startTime = Math.round(startTime_double * 1000000);
						if (startTime < 0) {
							throw new NumberFormatException("negative start time");
						}
						polynom.setStart(startTime); //�s
						addPolynomSegment(polynom, false);
						dialog.dispose();
					} catch (NumberFormatException e) {
						JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte einen g�ltigen Startzeitpunkt angeben"));	
					}
				}
			}			
		});
		dialog.add(btn_ok);
		dialog.setVisible(true);
	}
	
	/**
	 * add a part with a polynomial segment
	 * @param ps polynomial segment
	 * @param periodic
	 */
	public void addPolynomSegment(PolynomSegment ps, boolean periodic) {
		if (this.periodic) {
			this.polynoms_periodic.add(ps);
		} else {
			this.polynoms_nonperiodic.add(ps);
		}
		updatePolynomTable(periodic);
	}
	
	/**
	 * update the table by reloading all entries
	 * @param periodic
	 */
	private void updatePolynomTable(boolean periodic) {
		//clear table
		while(polynomTableModel.getRowCount() > 0) {
			polynomTableModel.removeRow(0);
		}
		List<PolynomSegment> polynoms;
		if (periodic) {
			polynoms = this.polynoms_periodic;
		} else {
			polynoms = this.polynoms_nonperiodic;
		}
		
		//sort polynomials... 
		for (int i = 0; i < polynoms.size(); i++) {
			if (periodic) {
				//...from 0� to 360�
				polynoms.get(i).setSigned(false);
			} else {
				//...from "-infinity to infinity"
				polynoms.get(i).setSigned(true);
			}
		}
		Collections.sort(polynoms);

		//load new items 
		for (int i = 0; i < polynoms.size(); i++) {
			String polynomString = "";
			for (int j = polynoms.get(i).getParameters().size() - 1; j >= 0 ; j--) { //start with the highest exponent
				if (periodic) {
					List<Double> params0to360 = polynoms.get(i).getParameters();
					if (polynoms.get(i).getStart() < 0) { //show 0� to 360� instead of internal representation
						params0to360 = polynoms.get(i).shift(Math.pow(2, 64)); //shift +360� if the start angle is lower than 0�
					}
					if (params0to360.get(j) != 0) {
						double factor = params0to360.get(j) * Math.pow(Math.pow(2, 64), j) / Math.pow(360, j);
						if (! polynomString.equals("")) {
							if (factor < 0) {
								polynomString += " - ";
							} else {
								polynomString += " + ";
							}
						} else {
							polynomString = "<html>";
							if (factor < 0) {
								polynomString += "-";
							}
						}
						if (factor < 0) {
							polynomString += GUIMath.roundDecimals(0 - factor, 5);
						} else {
							polynomString += GUIMath.roundDecimals(factor, 5);
						}
						if (j > 0) {
							 polynomString += " \u00B7 x<sup>" + j + "</sup>";
						}
					}
				} else { // nonperiodic
					if (polynoms.get(i).getParameters().get(j) != 0) {
						double factor = polynoms.get(i).getParameters().get(j) * Math.pow(1000000, j);
						if (! polynomString.equals("")) {
							if (factor < 0) {
								polynomString += " - ";
							} else {
								polynomString += " + ";
							}
						} else { //first element
							polynomString = "<html>";
							if (factor < 0) {
								polynomString += "-";
							}
						}
						if (factor < 0) {
							polynomString += GUIMath.roundDecimals(0 - factor, 5);
						} else {
							polynomString += GUIMath.roundDecimals(factor, 5);
						}
						if (j > 0) {
							 polynomString += " \u00B7 x<sup>" + j + "</sup>";
						}
					}
				}
			}
			polynomString += "</html>";
			if (polynomString.equals("</html>")) {
				polynomString = "0";
			}
			String start = "";
			if (periodic) {
				start = new Angle(polynoms.get(i).getStart()).getDegreeWithDecimals(3);
			} else {
				start = GUIMath.roundDecimals(((double)polynoms.get(i).getStart())/1000000, 3);
			}

			String[] row = {start, polynomString};
			polynomTableModel.addRow(row);
		}
		if (periodic) {
			this.signal_periodic.setPolynoms(polynoms);
			this.signal_periodic.setReadyToPlay();
			this.gp.setPeriodic(this.signal_periodic);
		} else {
			this.signal_nonperiodic.setPolynoms(polynoms);
			this.signal_nonperiodic.setReadyToPlay();
			long gp_startTime =  this.signal_nonperiodic.getFirstSegmentStart();
			long gp_endTime = (long) (this.signal_nonperiodic.getLastSegmentStart() + 0.1 * (this.signal_nonperiodic.getLastSegmentStart() - this.signal_nonperiodic.getFirstSegmentStart()));
			if (gp_endTime == gp_startTime) { //case: only one segment
				gp_endTime = gp_startTime + 1000000; // 1s
			}
			this.gp.setNonPeriodic(this.signal_nonperiodic, gp_startTime, gp_endTime);
		}
		this.gp.computeThumbnail();
	}
	
	/**
	 * reload the combobox with phasors
	 */
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		if (this.signal_periodic.getPhasor() != null) {
			cbo_phasor.setSelectedItem(this.signal_periodic.getPhasor());
		}
		cbo_phasor.updateUI();
	}
	
	/**
	 * show the edit (main) dialog
	 */
	private void showDialog() {
		this.updateCbo_phasor();
		this.mainDialog.setVisible(true);
	}

	/**
	 * signal is full defined
	 */
	public boolean fullDefined() {
		if (!this.periodic || this.signal_periodic.getPhasor() != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * return the signal
	 */
	public Signal getSignal() {
		if (this.periodic) {
			return this.signal_periodic;
		} else {
			return this.signal_nonperiodic;
		}
	}

	/**
	 * check whether the phasor can be deleted
	 * @param phasorToDelete 
	 */
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		if (this.periodic && this.signal_periodic != null && this.signal_periodic.getPhasor() == phasorToDelete) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * is periodic mode active?
	 * @return
	 */
	public boolean isPeriodic() {
		return this.periodic;
	}
	
	/**
	 * set the start time (non periodic mode)
	 * @param startTime
	 */
	public void setStartTime(long startTime) {
		this.edt_startTime.setText(GUIMath.roundDecimals(((double) startTime) / 1000000, 6)); 
		this.setNonPeriodic();
	}
	
	/**
	 * set periodic mode
	 */
	private void setPeriodic() {
		this.radio_periodic.setSelected(true);
		periodic = true;
		timePanel.setVisible(false);
		phasorPanel.setVisible(true);
		updateGUI();
	}
	
	/**
	 * set non periodic mode
	 */
	private void setNonPeriodic() {
		this.radio_nonPeriodic.setSelected(true);
		periodic = false;
		phasorPanel.setVisible(false);
		timePanel.setVisible(true);
		updateGUI();
	}
	/**
	 * update polynomial table and canvas box
	 */
	
	public void updateGUI() {
		updateUI();
		updatePolynomTable(this.periodic);
	}
}
