package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

import Controller.DataControl;
import Controller.FileControl;
import Data.Harmonic;
import Data.Phasor;
import File.LogFileWriter;

/**
 * canvas for drawing a signal tree
 *
 */
public class Canvas extends JPanel implements MouseMotionListener, MouseListener {
	private static final long serialVersionUID = 7L;
	private int startX;
	private int startY;
	private int diffX;
	private int diffY;
	private boolean lineActive;
	private List<CanvasTreeElement> treeElements;
	private CanvasConnector tmpConnStart; 
	private CanvasConnector tmpConnAim;
	private List<Connection> connections;
	private CanvasBox topLevelElement;
	private CanvasMode mode;
	private int opId = 0;
	private String filePath; //path of the opened file of empty, if no file has been opened. 
	private boolean changed; //unsaved changes
	
	/**
	 * constructor
	 */
	public Canvas() {
		super();
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.changed = false;
		this.filePath = "";
		this.startX = 0; 
		this.startY = 0;
		this.lineActive = false;
		this.mode = CanvasMode.MOUSE;
		this.treeElements = new ArrayList<CanvasTreeElement>();
		this.connections = new ArrayList<Connection>();
		
		this.setTransferHandler(new TransferHandler() {
			private static final long serialVersionUID = 8L;

			public boolean canImport(TransferHandler.TransferSupport support) {
				if (mode == CanvasMode.MOUSE && getThis().isEnabled()) {
					Transferable t = support.getTransferable();
					try {
						if (t.getTransferData(DataFlavor.stringFlavor).toString().equals("mult") || t.getTransferData(DataFlavor.stringFlavor).toString().equals("add") || t.getTransferData(DataFlavor.stringFlavor).toString().equals("leaf") || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Harmonische")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Punktinterpolation")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Gleichspannung")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Verstärkung")) || t.getTransferData(DataFlavor.stringFlavor).equals(LanguageTranslator.getTranslation("Sprung")) || t.getTransferData(DataFlavor.stringFlavor).equals(LanguageTranslator.getTranslation("Rampe")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Punktinterpolation_am")) || t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion_am")) ||  topLevelElement == null) {
							return true;		
						}
					} catch (UnsupportedFlavorException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				return false;
			}
			
			public boolean importData(TransferHandler.TransferSupport support) {
				
				Transferable t = support.getTransferable();

				try {
					canvasLog("Transfer handler " + t.getTransferData(DataFlavor.stringFlavor).toString());
					changed = true;
					if(t.getTransferData(DataFlavor.stringFlavor).toString().equals("mult")) {
						CanvasOperator mult = new CanvasOperatorMult(getThis(), mouseX(), mouseY());
						mult.id = opId;
						opId++;
						getThis().add(mult);
						getThis().addTreeElement(mult);
						getThis().updateUI();	
						return true;
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals("add")) {
						CanvasOperator add = new CanvasOperatorAdd(getThis(), mouseX() , mouseY());
						add.id = opId;
						opId++;
						getThis().add(add);
						getThis().addTreeElement(add);
						getThis().updateUI();
						return true;
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals("leaf")) {
						CanvasBox p;
						if (DataTransfer.getDraggedLeaf().getLeafType() == WorkspaceTreeLeafType.SIGNAL) {
							int answer = JOptionPane.showOptionDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Soll die Datei eingebettet werden?"), LanguageTranslator.getTranslation("Signal importieren"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {LanguageTranslator.getTranslation("Ja"), LanguageTranslator.getTranslation("nein")}, LanguageTranslator.getTranslation("ja"));
							p = new CanvasBoxSignalImport(getThis(), mouseX(), mouseY(), (answer == 0), DataTransfer.getDraggedLeaf().getPath(), null, null, null);
							((CanvasBoxSignalImport) p).setTitle(DataTransfer.getDraggedLeaf().toString());
						} else if (DataTransfer.getDraggedLeaf().getLeafType() == WorkspaceTreeLeafType.AMPLITUDEMODULATION) {
							int answer = JOptionPane.showOptionDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Soll die Amplitudenmodulation eingebettet werden?"), LanguageTranslator.getTranslation("Amplitduenmodulation importieren"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {LanguageTranslator.getTranslation("Ja"), LanguageTranslator.getTranslation("nein")}, LanguageTranslator.getTranslation("ja"));
							p = new CanvasBoxAmplitudeModulationImport(getThis(), mouseX(), mouseY(), (answer == 0), DataTransfer.getDraggedLeaf().getPath(), null, null, null);
							((CanvasBoxAmplitudeModulationImport) p).setTitle(DataTransfer.getDraggedLeaf().toString());
						} else if (DataTransfer.getDraggedLeaf().getLeafType() == WorkspaceTreeLeafType.RECORDED) {
							p = new CanvasBoxSignalInputRecorded(getThis(), mouseX(), mouseY(), DataTransfer.getDraggedLeaf().getPath());
						} else {
							p = null;
						}
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Harmonische"))) {
						CanvasBox p = new CanvasBoxHarmonic(getThis(), mouseX(), mouseY(), new Harmonic(null));
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Punktinterpolation"))) {
						CanvasBox p = new CanvasBoxSignalPointInterpolation(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();						
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion"))) {
						CanvasBox p = new CanvasBoxSignalFunction(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Gleichspannung"))) {
						CanvasBox p = new CanvasBoxConst(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Verstärkung"))) {
						CanvasBox p = new CanvasBoxGain(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Sprung"))) {
						CanvasBox p = new CanvasBoxStep(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Rampe"))) {
						CanvasBox p = new CanvasBoxRamp(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Punktinterpolation_am"))) {
						CanvasBox p = new CanvasBoxAmplitudeModulationPointInterpolation(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Abschnittsweise definierte Funktion_am"))) {
						CanvasBox p = new CanvasBoxAmplitudeModulationFunction(getThis(), mouseX(), mouseY());
						getThis().addTreeElement(p);
						getThis().add(p);
						getThis().updateUI();	
					} else if (topLevelElement == null) {
						canvasLog("Import ToplevelElement");
						CanvasBox p;
						if(t.getTransferData(DataFlavor.stringFlavor).toString().equals(LanguageTranslator.getTranslation("Signalausgabe"))) {
							p = new CanvasBoxSignalOutput(getThis(), mouseX(), mouseY());
						} else {
							p = new CanvasBoxAmplitudeModulationOutput(getThis(), mouseX(), mouseY());
						}


						treeElements.add(p);
						getThis().add(p);
						setTopLevelElement(p);
						getThis().updateUI();
					} else {
						
					}
				} catch (UnsupportedFlavorException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return true;
			}
		});
	}

	
	/**
	 * set the top level canvas box (CanvasBoxSignalOutput or CanvasBoxAmplitudeModulationOutput)
	 * @param topLevelElement
	 */
	public void setTopLevelElement(CanvasBox topLevelElement) {
		this.topLevelElement = topLevelElement;
	}
	
	/**
	 * get the toplevel type
	 * @return s for signal, am for amplitudemodulation, undef for undefined
	 */
	public String getTopLevelType() {
		if (this.topLevelElement instanceof CanvasBoxSignalOutput) {
			return "s";
		} else if (this.topLevelElement instanceof CanvasBoxAmplitudeModulationOutput) {
			return "am";
		} else {
			return "undef";
		}
	}
	
	/**
	 * update the top level element
	 */
	public void updateTopLevelElement() {	
		if (this.topLevelElement != null && this.topLevelElement.fullDefined()) {
			if (this.topLevelElement instanceof CanvasBoxSignalOutput) {
				DataControl.setTopLevelSignal(((CanvasBoxSignalOutput) this.topLevelElement).getSignal());
				canvasLog("updateTopLevelElement: Top level signal is set");
				if (((CanvasBoxSignalOutput) this.topLevelElement).getSignal() == null) {
					Canvas.canvasLog("updateTopLevelElement: Top level signal is null! Why?");
					this.topLevelElement.status.setStatusUndefined();
				}
				this.topLevelElement.status.setStatusDefindedAndConnected();
			} else if (this.topLevelElement instanceof CanvasBoxAmplitudeModulationOutput) {
				DataControl.setTopLevelAmplitudeModulation(((CanvasBoxAmplitudeModulationOutput) this.topLevelElement).getAmplitudeModulation());
				canvasLog("updateTopLevelElement: Top level amplitude modulation is set");
				this.topLevelElement.status.setStatusDefindedAndConnected();
			} else {
				DataControl.resetTopLevel();
				canvasLog("updateTopLevelElement: No top level element");
			}
		} else {
			DataControl.resetTopLevel();
			if (this.topLevelElement != null) {
				this.topLevelElement.status.setStatusUndefined();
			}
		}
		if (this.topLevelElement != null) {
			this.topLevelElement.status.updateUI();
		}
	}
	
	/**
	 * add a element to the signal or am tree on the canvas
	 * @param te
	 */
	public void addTreeElement(CanvasTreeElement te) {
		this.changed = true;
		this.treeElements.add(te);
		this.updateTopLevelElement();
	}
	
	/**
	 * get all elements from the tree on the canvas
	 * @return
	 */
	public List<CanvasTreeElement> getTreeElements() {
		return this.treeElements;
	}
	
	/**
	 * remove a element from the tree on the canvas
	 * @param treeElement CanvasTreeElement and instanceof JPanel
	 */
	public void removeTreeElement(CanvasTreeElement treeElement) {
		this.changed = true;
		for (int i = 0; i < this.connections.size(); i++) {
			if (connections.get(i).getInput().getTreeElement() == treeElement || connections.get(i).getOutput().getTreeElement() == treeElement) {
				connections.get(i).removeConnection();
				connections.remove(i);
				i--; // next element will at position i after deleting the element at position i
			}
		}
		this.treeElements.remove(treeElement);
		this.remove((JPanel) treeElement);
		if (treeElement == this.topLevelElement) {
			this.topLevelElement = null;
			canvasLog("Toplevelelement is set to null");
		}
		this.updateTopLevelElement();
		updateUI();
	}
	
	/**
	 * highlight available canvas connectors for a connection
	 * @param cc first canvas connector of the connection
	 */
	public void highlightFor(CanvasConnector cc) {
		for (int i = 0; i < this.treeElements.size(); i++) {
			this.treeElements.get(i).highlightFor(cc);
		}
	}
	
	/**
	 * stop highlighting canvas connectors
	 */
	public void stopHighlight() {
		for (int i = 0; i < this.treeElements.size(); i++) {
			this.treeElements.get(i).stopHighlight();
		}
	}
	
	/**
	 * start the visualized connection from a connector to the mouse
	 * @param bc
	 */
	public void startConnection(CanvasConnector bc) {
		this.tmpConnStart = bc;
	}
	
	/**
	 * set the end of a virtual visualized connection from a connector to the mouse
	 * @param bc
	 */
	public void setTmpConnAim(CanvasConnector bc) {
		this.tmpConnAim = bc;
	}
	
	/**
	 * The connection must be valid!
	 * @param c
	 */
	public void addConnection(Connection c) {
		this.connections.add(c);
	}
	
	/**
	 * check a connection whether it is allowed and add it if it is allowed
	 */
	public void checkConnection() {
		if (tmpConnStart != null && tmpConnAim != null) {
			if (tmpConnStart.canConnect(tmpConnAim)) {
				if (tmpConnStart.getType().isInput()) {
					connections.add(new Connection(tmpConnAim, tmpConnStart));
				} else {
					connections.add(new Connection(tmpConnStart, tmpConnAim));
				}
				this.updateTopLevelElement();
			}
		}
		tmpConnStart = null;
		tmpConnAim = null;
	}
	
	/**
	 * this (canvas)
	 * @return
	 */
	private Canvas getThis() {
		return this;
	}
	
	/**
	 * get the mouse x position
	 * @return
	 */
	private int mouseX() {
		return this.getMousePosition().x;
	}
	
	/**
	 * get the mouse y position
	 * @return
	 */
	private int mouseY() {
		return this.getMousePosition().y;
	}
	
	/**
	 * redraw canvas with the connections between canvas boxes and operators
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawLine(4, 4, this.getWidth() - 25, 4);
		g.drawArc(this.getWidth() - 45, 4, 40, 40, 0, 90);
		g.drawLine(this.getWidth() - 5, this.getHeight() - 5, this.getWidth() - 5, 25);
		g.drawLine(this.getWidth() - 5, this.getHeight() - 5, 25, this.getHeight() - 5);	
		g.drawArc(4, this.getHeight() - 45, 40, 40, 180, 90);
		g.drawLine(4, this.getHeight() - 25, 4, 4);
		int circX = 10;
		int circY = 10;
		int circR = 31;
		g.fillArc(circX, circY, 2 * circR, 2 * circR +1, 90, 15);
		g.setColor(new Color(0, 150, 125));
		g.fillArc(circX, circY, 2 * circR, 2 * circR, 116, 14);
		g.fillArc(circX, circY, 2 * circR, 2 * circR, 141, 14);
		g.fillArc(circX, circY, 2 * circR, 2 * circR, 166, 14);
		g.setColor(Color.BLACK);
		int[] k_x = {circX + circR + (int) (0.43 * circR), circX + circR + (int) (0.73 * circR), circX + circR + (int) (0.33 * circR), circX + circR + (int) (0.73 * circR), circX + circR + (int) (0.43 * circR), circX + circR + (int) (0.07 * circR), circX + circR + (int) (0.07 * circR)};
		int[] k_y = {circY + circR, circY + circR, circY + circR - (int) (0.5 * circR), circY, circY, circY + circR - (int) (0.61 * circR), circY + circR - (int) (0.39 * circR)};
		g.fillPolygon(k_x, k_y, 7);
		g.fillRect(circX + circR + (int)(0.78*circR), circY, (int) (0.28 * circR), circR);
		g.fillRect(circX + circR + (int)(1.31*circR), circY, (int) (0.28 * circR), circR);	
		g.fillRect(circX + circR + (int)(1.12*circR), circY, (int) (0.66 * circR), (int) (0.23 * circR));
		if (lineActive) {
			g.drawLine(startX, startY, startX + diffX, startY + diffY);
		}
		for (int i = 0; i < connections.size(); i++) {
			if (this.mode == CanvasMode.DELETE && connections.get(i).isHighlightForDelete()) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.BLACK);
			}
			g.drawLine(connections.get(i).getOutput().getXonCanvas(), connections.get(i).getOutput().getYonCanvas(), connections.get(i).getInput().getXonCanvas(), connections.get(i).getInput().getYonCanvas());
		}
		
		
		
	}

	/**
	 * get the connected output connector to an input connector
	 * @param inputConnector
	 * @return connected output connector
	 */
	public CanvasConnector getConnectionOutputConnector(CanvasConnector inputConnector) {
		for (int i = 0; i < connections.size(); i++) {
			if (connections.get(i).getInput() == inputConnector) {
				return connections.get(i).getOutput();
			}
		}
		return null;
	}
	
	/**
	 * activate a temporary connection line
	 * @param startX
	 * @param startY
	 */
	public void setLineActive(int startX, int startY) {
		this.startX = startX;
		this.startY = startY;
		this.lineActive = true;
	}
	
	/**
	 * is the temporary connection line active?
	 * @return active
	 */
	public boolean isLineActive() {
		return this.lineActive;
	}
	
	/**
	 * set the difference of the end point of the temporary connection to the start point
	 * @param diffX
	 * @param diffY
	 */
	public void setDiff(int diffX, int diffY) {
		this.diffX = diffX;
		this.diffY = diffY;
	}
	
	/**
	 * deactivate temporary connection
	 */
	public void setLineInactive() {
		this.lineActive = false;
		this.updateUI();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (this.getMode() == CanvasMode.DELETE) {
			for (int i = 0; i < connections.size(); i++) {
				if (getDistConnectionPoint(this.connections.get(i), e.getX(), e.getY()) <= 3) {
					this.connections.get(i).setHighlightForDelete(true);
				} else {
					this.connections.get(i).setHighlightForDelete(false);
				}
			}
			this.updateUI();
		}
	}
	/**
	 * log messages in canvas context
	 * @param text message
	 */
	public static void canvasLog(String text) {
		if (!LogFileWriter.logToFile("canvas.log", text)) {
			System.out.println("Cannot write to canvas log file");
		}
		System.out.println("[CanvasLog] " + text);
	}

	/**
	 * set the mode (mouse or delete)
	 * @param mode
	 */
	public void setMode(CanvasMode mode) {
		this.mode = mode;
	}
	
	/**
	 * get the mode
	 * @return
	 */
	public CanvasMode getMode() {
		return this.mode;
	}

	/**
	 * Computes the shortest distance between the line of a connection and a given point.
	 * @param c Connection
	 * @param x x-coordinate
	 * @param y y-coordinate
	 * @return
	 */
	private int getDistConnectionPoint(Connection c, int x, int y) {
		if (c.getInput().getXonCanvas() == c.getOutput().getXonCanvas() && c.getInput().getYonCanvas() == c.getOutput().getYonCanvas()) {
			//distance between the connection points is 0. (...the determinant would be 0)
			return (int) Math.sqrt(Math.pow((x - c.getInput().getX()), 2) + Math.pow((y - c.getInput().getYonCanvas()), 2));
		}
		int[] g_support = {c.getInput().getXonCanvas(), c.getInput().getYonCanvas()};
		int[] g_direction = {c.getOutput().getXonCanvas() - c.getInput().getXonCanvas(), c.getOutput().getYonCanvas() - c.getInput().getYonCanvas()};
		int[] h_support = {x, y};
		int[] h_direction = {g_direction[1], 0 - g_direction[0]};
		int[] result_vector = {h_support[0] - g_support[0], h_support[1] - g_support[1]};
		int[][] matrix = new int[2][2];
		matrix[0][0] = g_direction[0];
		matrix[1][0] = g_direction[1];
		matrix[0][1] = 0-h_direction[0];
		matrix[1][1] = g_direction[0]; // = (-1) * h_diretion[1]
		//det must not be zero! Ok (see if condition).
		double invDet = 1 / ((double) ((matrix[0][0] * matrix[1][1]) - (matrix[1][0] * matrix[0][1])));
		double[][] invMatrix = new double[2][2];
		invMatrix[0][0] = invDet * (double) matrix[1][1];
		invMatrix[0][1] = invDet * (double) (0 - matrix[0][1]);
		invMatrix[1][0] = invDet * (double) (0 - matrix[1][0]);
		invMatrix[1][1] = invDet * (double) matrix[0][0];
		double scalar_g = invMatrix[0][0] * result_vector[0] + invMatrix[0][1] * result_vector[1];
		double scalar_h = invMatrix[1][0] * result_vector[0] + invMatrix[1][1] * result_vector[1];
		double dist_x;
		double dist_y;
		double dist_input_output = Math.sqrt(Math.pow(c.getOutput().getXonCanvas() - c.getInput().getXonCanvas(), 2) + Math.pow(c.getOutput().getYonCanvas() - c.getInput().getYonCanvas(), 2));
		if (scalar_g < 0) {
			//behind input point
			dist_x = x - c.getInput().getXonCanvas();
			dist_y = y - c.getInput().getYonCanvas();
		} else if (Math.sqrt(Math.pow(scalar_g * g_direction[0], 2) + Math.pow(scalar_g * g_direction[1], 2)) > dist_input_output) {
			//behind output point
			dist_x = x - c.getOutput().getXonCanvas();
			dist_y = y - c.getOutput().getYonCanvas();
		} else {
			//between input and output poinSignalausgabet
			dist_x = scalar_h * h_direction[0];
			dist_y = scalar_h * h_direction[1];
		}
		return (int) Math.sqrt(Math.pow(dist_x, 2) + Math.pow(dist_y, 2));
	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (this.mode == CanvasMode.DELETE) {
			for (int i = 0; i < connections.size(); i++) {
				if (connections.get(i).isHighlightForDelete()) {
					connections.get(i).removeConnection();
					connections.remove(i);
					updateUI();
					i--; //there will be another connection at the position of the removed connection in the next iteration
				}
			}
		}
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}


	@Override
	public void mousePressed(MouseEvent arg0) {
		
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		
	}

	/**
	 * check whether a phasor can be deleted (if it is unused)
	 * @param p phasor
	 * @return phasor can be deleted
	 */
	public boolean isDeletePhasorOk(Phasor p) {
		for (int i = 0; i < this.treeElements.size(); i++) {
			if (!this.treeElements.get(i).isDeletePhasorOk(p)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * set the path of the opened file
	 * @param path
	 */
	public void setFilePath(String path) {
		this.filePath = path;		
	}
	
	/**
	 * get the path of the opened file
	 * @return
	 */
	public String getFilePath() {
		return this.filePath;
	}
	
	/**
	 * clear the canvas
	 * @return
	 */
	public boolean clearAll() {
		boolean clear = false;
		if (this.changed) {
			int result = JOptionPane.showConfirmDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Sollen die durchgeführten Änderungen gespeichert werden?"), LanguageTranslator.getTranslation("Änderungen speichern"), JOptionPane.YES_NO_CANCEL_OPTION);
			if (result == 0) { //yes, save changes
				if (FileControl.saveAction(this)) {
					clear = true; 
				}
			} else if (result == 1) { //no, save no changes
				clear = true;
			} else if (result == 2) { //cancel
				// do nothing, clear = false
			}
		} else {
			clear = true;
		}
		if (clear) {
			while (this.connections.size() > 0) {
				this.connections.remove(0);
			}
			this.lineActive = false;
			this.tmpConnStart = null; 
			this.tmpConnAim = null;
			this.topLevelElement = null;
			this.mode = CanvasMode.MOUSE;
			this.opId = 0;
			this.filePath = ""; 
			while (this.treeElements.size() > 0) {
				this.treeElements.remove(0);	
			}
			while(this.getComponentCount() > 0) {
				this.remove(0);
			}
			this.updateUI();
			Main.getMainwindow().getPhasorPanel().updateData();
			return true;
		}
		return false;
	}
	
	/**
	 * overwrite setEnabled method to enable all child components
	 */
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for (int i = 0; i < this.getComponentCount(); i++) {
			this.getComponent(i).setEnabled(enabled);
		}
	}
	
	/**
	 * changes on canvas happend
	 * @param changed
	 */
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
}
