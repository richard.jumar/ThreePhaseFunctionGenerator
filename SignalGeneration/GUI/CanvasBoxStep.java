package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Angle;
import Data.Frequency;
import Data.NonPeriodicAmplitudeModulationStep;
import Data.PeriodicAmplitudeModulationStep;
import Data.Phasor;

public class CanvasBoxStep extends CanvasBoxAmplitudeModulationInput {

	private static final long serialVersionUID = 24L;
	private NonPeriodicAmplitudeModulationStep step_nonPeriodic;
	private PeriodicAmplitudeModulationStep step_periodic;
	private JDialog dialog;
	private JRadioButton radio_periodic;
	private JRadioButton radio_nonPeriodic;
	private JRadioButton radio_symmetric;
	private JRadioButton radio_asymmetric;
	private JComboBox<Phasor> cbo_phasor;
	private JTextField[] edt_stepBackTime;
	private JTextField[] edt_startValue;
	private JTextField[] edt_endValue;
	private JTextField[] edt_stepTime;
	private JPanel phasorPanel;
	private JPanel paramPanel;
	private JPanel[] phasePanels;
	private JLabel[] lbl_stepBackTime;
	private JLabel[] lbl_stepBackTimeUnit;
	private JLabel[] lbl_stepTimeUnit;
	private JLabel[] lbl_startValue;
	private JLabel[] lbl_endValue;
	private JLabel[] lbl_stepTime;
	private JLabel lbl_phasor;
	private JLabel lbl_symmetry;
	private JLabel lbl_min;
	private JLabel lbl_max;
	private SymbolPhasorMini phasorSymbol;
	private GraphPanel gp;
	
	public CanvasBoxStep(Canvas canvas, int x, int y) {
		super(canvas);
		this.setLayout(null);
		this.setBounds(x, y, 200, 200);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Sprung"));
		title.setBounds(5, 5, 100, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.add(status);
		this.gp = new GraphPanel(null);
		this.gp.setBounds(1, 90, 198, 80);
		this.add(gp);
		phasorSymbol = new SymbolPhasorMini();
		phasorSymbol.setBounds(6, 27, 13, 13);
		this.add(phasorSymbol);
		lbl_phasor = new JLabel("<" + LanguageTranslator.getTranslation("nicht ausgew�hlt") + ">");
		lbl_phasor.setBounds(23, 25, 140, 15);
		this.add(lbl_phasor);
		this.lbl_symmetry = new JLabel();
		this.lbl_symmetry.setBounds(5, 40, 190, 15);
		this.add(lbl_symmetry);
		this.lbl_min = new JLabel();
		this.lbl_min.setBounds(5, 55, 100, 15);
		this.add(this.lbl_min);
		this.lbl_max = new JLabel();
		this.lbl_max.setBounds(5, 70, 100, 15);
		this.add(this.lbl_max);
		JButton btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		btn_edit.setBounds(1, 170, 198, 29);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}
		});
		this.add(btn_edit);
		
		
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Eigenschaften Sprung"));
		dialog.setLayout(null);
		dialog.setSize(605, 260);
		
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(null);
		optionPanel.setBounds(5, 5, 245, 70);
		optionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eigenschaften")));
		dialog.add(optionPanel);
		ButtonGroup btnGroup_periodic = new ButtonGroup();
		this.radio_periodic = new JRadioButton(LanguageTranslator.getTranslation("periodisch"));
		radio_periodic.setSelected(true);
		radio_periodic.setBounds(5, 20, 90, 20);
		radio_periodic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setPeriodic();
			}		
		});
		btnGroup_periodic.add(radio_periodic);
		optionPanel.add(radio_periodic);
		this.radio_nonPeriodic = new JRadioButton(LanguageTranslator.getTranslation("nichtperiodisch"));
		radio_nonPeriodic.setSelected(false);
		radio_nonPeriodic.setBounds(115, 20, 125, 20);
		radio_nonPeriodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setNonPeriodic();
			}
			
		});
		btnGroup_periodic.add(radio_nonPeriodic);
		optionPanel.add(radio_nonPeriodic);
		this.radio_symmetric = new JRadioButton(LanguageTranslator.getTranslation("symmetrisch"));
		this.radio_symmetric.setSelected(true);
		this.radio_symmetric.setBounds(5, 40, 110, 20);
		this.radio_symmetric.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);				
			}
			
		});
		ButtonGroup btnGroup_symmetric = new ButtonGroup();
		btnGroup_symmetric.add(this.radio_symmetric);
		optionPanel.add(this.radio_symmetric);
		this.radio_asymmetric = new JRadioButton(LanguageTranslator.getTranslation("asymmetrisch"));
		this.radio_asymmetric.setSelected(false);
		this.radio_asymmetric.setBounds(115, 40, 125, 20);
		this.radio_asymmetric.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
				for(int i = 1; i < 3; i++) {
					edt_startValue[i].setText(edt_startValue[0].getText());
					edt_endValue[i].setText(edt_endValue[0].getText());
					edt_stepTime[i].setText(edt_stepTime[0].getText());
					edt_stepBackTime[i].setText(edt_stepBackTime[0].getText());
				}
			}
		});
		btnGroup_symmetric.add(this.radio_asymmetric);
		optionPanel.add(this.radio_asymmetric);
		
		this.phasorPanel = new JPanel();
		phasorPanel.setLayout(null);
		phasorPanel.setBounds(250, 5, 250, 50);
		phasorPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger")));
		cbo_phasor = new JComboBox<Phasor>();
		cbo_phasor.setBounds(10, 20, 100, 20);
		cbo_phasor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cbo_phasor.getSelectedIndex() != -1) {
					setPhasor((Phasor) cbo_phasor.getSelectedItem());
				}
			}
		});
		phasorPanel.add(cbo_phasor);
		JButton btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu") + "...");
		btn_newPhasor.setBounds(120, 20, 80, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
				Phasor lastCreatedPhasor = Main.getMainwindow().getPhasorPanel().getLastCreatedPhasor();
				if (lastCreatedPhasor != null) {
					cbo_phasor.setSelectedItem(lastCreatedPhasor);
				}
			}
		});
		phasorPanel.add(btn_newPhasor);
		phasorPanel.setVisible(true);
		this.dialog.add(phasorPanel);
		this.paramPanel = new JPanel();
		this.paramPanel.setBounds(5, 75, 580, 120);
		this.paramPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Parameter")));
		this.paramPanel.setLayout(null);
		this.dialog.add(this.paramPanel);
		this.phasePanels = new JPanel[3];
		this.lbl_stepBackTime = new JLabel[3];
		this.edt_stepBackTime = new JTextField[3];
		this.lbl_stepBackTimeUnit = new JLabel[3];
		this.lbl_stepTimeUnit = new JLabel[3];
		this.lbl_startValue = new JLabel[3];
		this.lbl_endValue = new JLabel[3];
		this.lbl_stepTime = new JLabel[3];
		this.edt_startValue = new JTextField[3];
		this.edt_endValue = new JTextField[3];
		this.edt_stepTime = new JTextField[3];
		for (int i = 0; i < 3; i++) {
			this.phasePanels[i] = new JPanel();
			this.phasePanels[i].setBounds((i * 190) + 5, 15, 190, 100);
			this.phasePanels[i].setLayout(null);
			this.phasePanels[i].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " " + (i + 1)));
			lbl_startValue[i] = new JLabel(LanguageTranslator.getTranslation("Grundwert"));
			lbl_startValue[i].setBounds(5, 15, 95, 20);
			this.phasePanels[i].add(lbl_startValue[i]);
			edt_startValue[i] = new JTextField();
			edt_startValue[i].setBounds(100, 15, 70, 20);
			this.phasePanels[i].add(edt_startValue[i]);			
			lbl_endValue[i] = new JLabel(LanguageTranslator.getTranslation("Alternativwert"));
			lbl_endValue[i].setBounds(5, 35, 95, 20);
			this.phasePanels[i].add(lbl_endValue[i]);
			this.edt_endValue[i] = new JTextField();
			edt_endValue[i].setBounds(100, 35, 70, 20);
			this.phasePanels[i].add(edt_endValue[i]);
			lbl_stepTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Sprung") + "</sub></html>");
			lbl_stepTime[i].setBounds(5, 55, 70, 20);
			this.phasePanels[i].add(lbl_stepTime[i]);
			lbl_stepTimeUnit[i] = new JLabel("�");
			lbl_stepTimeUnit[i].setBounds(170, 55, 10, 20);
			this.phasePanels[i].add(lbl_stepTimeUnit[i]);
			edt_stepTime[i] = new JTextField();
			edt_stepTime[i].setBounds(100, 55, 70, 20);
			this.phasePanels[i].add(edt_stepTime[i]);
			this.paramPanel.add(this.phasePanels[i]);
			this.lbl_stepBackTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("R�cksprung") + "</sub></html>");
			this.lbl_stepBackTime[i].setBounds(5, 75, 95, 20);
			this.phasePanels[i].add(this.lbl_stepBackTime[i]);
			edt_stepBackTime[i] = new JTextField();
			edt_stepBackTime[i].setBounds(100, 75, 70, 20);
			this.phasePanels[i].add(edt_stepBackTime[i]);
			this.lbl_stepBackTimeUnit[i] = new JLabel("�");
			this.lbl_stepBackTimeUnit[i].setBounds(170, 75, 70, 20);
			this.phasePanels[i].add(lbl_stepBackTimeUnit[i]);
		}
		this.phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
		this.phasePanels[1].setVisible(false);
		this.phasePanels[2].setVisible(false);
		JButton btn_ok = new JButton(LanguageTranslator.getTranslation("OK"));
		btn_ok.setBounds(5, 200, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (radio_periodic.isSelected()) {
						if (cbo_phasor.getSelectedIndex() > -1) {
							if (radio_symmetric.isSelected()) {
								step_periodic = new PeriodicAmplitudeModulationStep((Phasor) cbo_phasor.getSelectedItem(), Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[0].getText().replace(',', '.')), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepBackTime[0].getText().replace(',', '.')))));
								step_nonPeriodic = null;
							} else {
								double[] gndValue = {Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_startValue[1].getText().replace(',', '.')), Double.parseDouble(edt_startValue[2].getText().replace(',', '.'))};
								double[] altValue = {Double.parseDouble(edt_endValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[1].getText().replace(',', '.')), Double.parseDouble(edt_endValue[2].getText().replace(',', '.'))};
								Angle[] stepAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepTime[2].getText().replace(',', '.'))))};
								Angle[] stepBackAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepBackTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepBackTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_stepBackTime[2].getText().replace(',', '.'))))};
								step_periodic = new PeriodicAmplitudeModulationStep((Phasor) cbo_phasor.getSelectedItem(), gndValue, altValue, stepAngle, stepBackAngle);
								step_nonPeriodic = null;
							}
							if (connectedWith != null) {
								status.setStatusDefindedAndConnected();
								connectedWith.getTreeElement().updateSignalOrAmplitudeModulation();
							} else {
								status.setStatusDefined();
							}
							status.updateUI();
						} else {
							JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Es ist kein Drehzeiger ausgew�hlt"));
							return;
						}
					} else { //not periodic
						if (radio_symmetric.isSelected()) {
							step_nonPeriodic = new NonPeriodicAmplitudeModulationStep(DataControl.getClock(), Math.round(Double.parseDouble(edt_stepTime[0].getText().replace(',', '.')) * 1000000), Double.parseDouble(edt_startValue[0].getText().replace(',',  '.')), Double.parseDouble(edt_endValue[0].getText().replace(',', '.')));
						} else {
							long[] time = {Math.round(Double.parseDouble(edt_stepTime[0].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_stepTime[1].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_stepTime[2].getText().replace(',', '.')) * 1000000)};
							double[] startValue = {Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_startValue[1].getText().replace(',', '.')), Double.parseDouble(edt_startValue[2].getText().replace(',', '.'))};
							double[] endValue = {Double.parseDouble(edt_endValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[1].getText().replace(',', '.')), Double.parseDouble(edt_endValue[2].getText().replace(',', '.'))};
							step_nonPeriodic = new NonPeriodicAmplitudeModulationStep(DataControl.getClock(), time, startValue, endValue);
						}
						step_periodic = null;
						
						if (connectedWith != null) {
							status.setStatusDefindedAndConnected();
							connectedWith.getTreeElement().updateSignalOrAmplitudeModulation();
						} else {
							status.setStatusDefined();
						}
						status.updateUI();
					}
					canvas.updateTopLevelElement();
					updateBoxElements();
					dialog.dispose();
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur g�ltige Zahlenwerte eingeben!"));
				}
			}			
		});
		this.dialog.add(btn_ok);
	}
	
	public void updateBoxElements() {
		boolean symmetric = false;
		
		//check periodic
		if (this.step_periodic != null) {
			phasorSymbol.setVisible(true);
			lbl_phasor.setText(this.step_periodic.getPhasor().getName() + " (" + Frequency.microHz_valueToHzString(this.step_periodic.getPhasor().getFrequency().getF0_phase1(), 4) + ")");
			lbl_phasor.setVisible(true);
			gp.setNonPeriodic(null, 0, 0);
			gp.setPeriodic(this.step_periodic);
			if (this.step_periodic.isSymmetric()) {
				symmetric = true;

			}		

		} else if (this.step_nonPeriodic != null) {
			lbl_phasor.setVisible(false);
			phasorSymbol.setVisible(false);
			gp.setPeriodic(null);
			gp.setNonPeriodic(step_nonPeriodic, startAndEndForNonPeriodicGraphPanel()[0], startAndEndForNonPeriodicGraphPanel()[1]);
			if (this.step_nonPeriodic.isSymmetric()) {
				symmetric = true;
			}	

		} else {
			return;
		}
		
		//check symmetric
		if (symmetric) {
			lbl_symmetry.setText(LanguageTranslator.getTranslation("Phasen symmetrisch"));
			gp.setSymmetric(true);
		} else {
			lbl_symmetry.setText(LanguageTranslator.getTranslation("Phasen nicht symmetrisch"));
			gp.setSymmetric(false);
		}
		setMinMaxLabels();
		gp.computeThumbnail();
	}

	private long[] startAndEndForNonPeriodicGraphPanel() {
		if (this.step_nonPeriodic != null) {
			long min = Long.MAX_VALUE;
			long max = Long.MIN_VALUE;
			for (int i = 0; i < 3; i++) {
				if (this.step_nonPeriodic.getStepTime()[i] < min) {
					min = this.step_nonPeriodic.getStepTime()[i];
				}
				if (this.step_nonPeriodic.getStepTime()[i] > max) {
					max = this.step_nonPeriodic.getStepTime()[i];
				}
			}
			long diff = max - min;
			if (diff == 0) {
				long[] interval = {min - 1000, max + 1000};
				if (interval[0] < 0) {
					interval[0] = 0;
				}
				return interval;
			} else {
				//show 10% before the first step and 10% after the last step
				long[] interval = {min - (diff / 10), max + (diff / 10)};
				if (interval[0] < 0) {
					interval[0] = 0;
				}
				return interval;
			}
		}
		return null;
	}
	
	private void setMinMaxLabels() {
		double min = Double.MAX_VALUE; 
		double max = Double.MIN_VALUE;
		if (this.step_nonPeriodic != null) {

			for (int i = 0; i < 3; i++) {
				if (this.step_nonPeriodic.getStartValue()[i] < min) {
					min = this.step_nonPeriodic.getStartValue()[i];
				}
				if (this.step_nonPeriodic.getStartValue()[i] > max) {
					max = this.step_nonPeriodic.getStartValue()[i];
				}
				if (this.step_nonPeriodic.getEndValue()[i] < min) {
					min = this.step_nonPeriodic.getEndValue()[i];
				}
				if (this.step_nonPeriodic.getEndValue()[i] > max) {
					max = this.step_nonPeriodic.getEndValue()[i];
				}
			}

		} else {
			for (int i = 0; i < 3; i++) {
				if (this.step_periodic.getLevelA()[i] < min) {
					min = this.step_periodic.getLevelA()[i];
				}
				if (this.step_periodic.getLevelA()[i] > max) {
					max = this.step_periodic.getLevelA()[i];
				}
				if (this.step_periodic.getLevelB()[i] < min) {
					min = this.step_periodic.getLevelB()[i];
				}
				if (this.step_periodic.getLevelB()[i] > max) {
					max = this.step_periodic.getLevelB()[i];
				}
			}
		}
		this.lbl_min.setText("min = " + GUIMath.roundDecimals(min, 3));
		this.lbl_max.setText("max = " + GUIMath.roundDecimals(max, 3));
	}
	
	public void showDialog() {
		if (this.step_nonPeriodic != null) {
			this.setNonPeriodic();
			this.radio_nonPeriodic.setSelected(true);
			if (this.step_nonPeriodic.isSymmetric()) {
				this.radio_symmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);	
				this.edt_startValue[0].setText(GUIMath.roundDecimals(this.step_nonPeriodic.getStartValue()[0], 5));
				this.edt_endValue[0].setText(GUIMath.roundDecimals(this.step_nonPeriodic.getEndValue()[0], 5));
				this.edt_stepTime[0].setText(GUIMath.roundDecimals(((double) this.step_nonPeriodic.getStepTime()[0]) / 1000000d, 6));
			} else {
				this.radio_asymmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
				for (int i = 0; i < 3; i++) {
					this.edt_startValue[i].setText(GUIMath.roundDecimals(this.step_nonPeriodic.getStartValue()[i], 5));
					this.edt_endValue[i].setText(GUIMath.roundDecimals(this.step_nonPeriodic.getEndValue()[i], 5));
					this.edt_stepTime[i].setText(GUIMath.roundDecimals(((double) this.step_nonPeriodic.getStepTime()[i]) / 1000000d, 6));
				}
			}
		} else if (this.step_periodic != null) {
			this.setPeriodic();
			this.radio_periodic.setSelected(true);
			if (this.step_periodic.isSymmetric()) {
				this.radio_symmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);	
			} else {
				this.radio_asymmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	


			}
			for (int i = 0; i < 3; i++) {
				this.edt_startValue[i].setText(DisplayFunctions.roundDecimals(this.step_periodic.getLevelA()[i], 5));
				this.edt_endValue[i].setText(DisplayFunctions.roundDecimals(this.step_periodic.getLevelB()[i], 5));
				this.edt_stepTime[i].setText(this.step_periodic.getStepAB()[i].getDegreeWithDecimalsWithoutUnit(4));
				this.edt_stepBackTime[i].setText(this.step_periodic.getStepBA()[i].getDegreeWithDecimalsWithoutUnit(4));
			}
		} else {
			this.setPeriodic();
			this.radio_periodic.setSelected(true);
			this.radio_symmetric.setSelected(true);
			phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
			phasePanels[1].setVisible(false);
			phasePanels[2].setVisible(false);	
		}
		updateCbo_phasor();
		dialog.setVisible(true);
	}
	
	private void setPhasor(Phasor p) {
		
	}
	
	@Override
	public boolean fullDefined() {
		if (this.step_periodic != null || this.step_nonPeriodic != null) {
			return true;
		}
		return false;
	}

	@Override
	public AmplitudeModulation getAmplitudeModulation() {
		if (this.step_periodic != null) {
			return this.step_periodic;
		}
		if (this.step_nonPeriodic != null) {
			return this.step_nonPeriodic;
		}
		return null;
	}

	public PeriodicAmplitudeModulationStep getAmStepPeriodic() {
		return this.step_periodic;
	}
	
	public NonPeriodicAmplitudeModulationStep getAmStepNonPeriodic() {
		return this.step_nonPeriodic;
	}
	
	public void setPeriodicAmStep(PeriodicAmplitudeModulationStep step_periodic) {
		this.step_periodic = step_periodic;
	}
	
	public void setNonPeriodicAmStep(NonPeriodicAmplitudeModulationStep step_nonPeriodic) {
		this.step_nonPeriodic = step_nonPeriodic;
	}
	
	public void setPeriodic() {

		
		//gui dialog
		this.phasorPanel.setVisible(true);
		for (int i = 0; i < 3; i++) {
			this.lbl_startValue[i].setText(LanguageTranslator.getTranslation("Grundwert"));
			this.lbl_stepTime[i].setText("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Sprung") + "</sub></html>");
			this.lbl_endValue[i].setText(LanguageTranslator.getTranslation("Alternativwert"));
			this.lbl_stepBackTime[i].setVisible(true);
			this.edt_stepBackTime[i].setVisible(true);
			this.lbl_stepBackTimeUnit[i].setVisible(true);
			this.lbl_stepTimeUnit[i].setText("�");
		}
	}
	
	public void setNonPeriodic() {
		this.phasorPanel.setVisible(false);
		for (int i = 0; i < 3; i++) {
			this.lbl_startValue[i].setText(LanguageTranslator.getTranslation("Startwert"));
			this.lbl_endValue[i].setText(LanguageTranslator.getTranslation("Endwert"));
			this.lbl_stepTime[i].setText(LanguageTranslator.getTranslation("Sprungzeit"));
			this.lbl_stepBackTime[i].setVisible(false);
			this.edt_stepBackTime[i].setVisible(false);
			this.lbl_stepBackTimeUnit[i].setVisible(false);
			this.lbl_stepTimeUnit[i].setText("s");
		}
	}
	
	
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		cbo_phasor.updateUI();
	}



	@Override
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		if (this.step_periodic != null && this.step_periodic.getPhasor() == phasorToDelete) {
			return false;
		} else {
			return true;
		}
	}
}
