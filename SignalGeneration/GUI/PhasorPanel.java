package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controller.DataControl;
import Data.Angle;
import Data.Frequency;
import Data.FrequencyModulation;
import Data.Phasor;


/**
 * Panel with information of the created phasors and dialogs for creating or editing phasors an frequency modulations
 */
public class PhasorPanel extends JPanel {

	private static final long serialVersionUID = 45L;
	private JTable tbl_phasors;
	private JTable tbl_fMod;
	private DefaultTableModel tableModelPhasors;
	private DefaultTableModel tableModelFmod;
	private JScrollPane tablePhasors;
	private JScrollPane tableFmod;
	private JTextField edt_frequency;
	private JTextField[] edt_phi0;
	private JTextField edt_name;
	private JDialog dialog;
	private JDialog fModDialog;
	private JRadioButton radio_symmetric;
	private JRadioButton radio_asymmetric;
	private JPanel[] phasePanels;
	private JTextField[] edt_start;
	private JTextField[] edt_end;
	private JTextField[] edt_endFrequency;
	private List<FrequencyModulation> fModList;
	private Phasor editPhasor;
	private Phasor lastCreatedPhasor;
	private JButton btn_edit;
	private JButton btn_newPhasor;
	private JButton btn_delete;
	
	/**
	 * constructor
	 */
	public PhasorPanel() {
		
		this.lastCreatedPhasor = null;
		this.fModList = new ArrayList<FrequencyModulation>();
		this.setLayout(null);
		this.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger2")));
		String[] colums = {LanguageTranslator.getTranslation("Name"), LanguageTranslator.getTranslation("Frequenz"), LanguageTranslator.getTranslation("Fmod.") };
		tbl_phasors = new JTable(new DefaultTableModel(colums, 0));

		DefaultTableCellRenderer rightAlignRenderer = new DefaultTableCellRenderer();
		rightAlignRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		tbl_phasors.getColumnModel().getColumn(1).setCellRenderer(rightAlignRenderer);
		tableModelPhasors = (DefaultTableModel) tbl_phasors.getModel();
		this.tablePhasors = new JScrollPane(tbl_phasors);
		this.tablePhasors.setBounds(100, 10, 200, 100);
		this.add(tablePhasors);
		this.btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu"));
		btn_newPhasor.setBounds(10, 160, 75, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showNewPhasorDialog();
			}
		});
		this.add(btn_newPhasor);
		this.btn_edit = new JButton(LanguageTranslator.getTranslation("Details"));
		btn_edit.setBounds(85, 160, 90, 20);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_phasors.getSelectedRow() >= 0) {
					
					editPhasor = DataControl.getPhasors().get(tbl_phasors.getSelectedRow());
					
					edt_phi0[0].setText(editPhasor.getPhase1Start().getDegreeWithDecimals(3).replace('�', ' ').trim());
					edt_phi0[1].setText(editPhasor.getPhase2Start().getDegreeWithDecimals(3).replace('�', ' ').trim());
					edt_phi0[2].setText(editPhasor.getPhase3Start().getDegreeWithDecimals(3).replace('�', ' ').trim());
					edt_name.setText(editPhasor.getName());
					edt_frequency.setText(DisplayFunctions.roundDecimals((double) editPhasor.getFrequency().getF0_phase1() / 1000000, 4)); 
					fModList = new ArrayList<FrequencyModulation>(); 
					for (int i = 0; i < editPhasor.getFrequency().getFrequencyModulationList().size(); i++) {
						fModList.add(editPhasor.getFrequency().getFrequencyModulationList().get(i));
					}
					updateFmodList();
					dialog.setVisible(true);
				}
			}			
		});
		this.add(btn_edit);
		this.btn_delete = new JButton(LanguageTranslator.getTranslation("L�schen"));
		btn_delete.setBounds(175, 160, 95, 20);
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tbl_phasors.getSelectedRow() >= 0) {
					if (Main.getMainwindow().isDeletePhasorForCanvasOk(DataControl.getPhasors().get(tbl_phasors.getSelectedRow()))) {
						DataControl.removePhasor(DataControl.getPhasors().get(tbl_phasors.getSelectedRow()));
					} else {
						JOptionPane.showMessageDialog(Main.getMainwindow(), LanguageTranslator.getTranslation("Der gew�hlte Drehzeiger kann nicht gel�scht werden, da er noch in Verwendung ist.\n Entfernen oder �ndern sie erst alle Objekte, die diesen Drehzeiger verwenden."));
					}
				}
			}			
		});
		this.add(btn_delete);
		
		//new Phasor dialog
		this.dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Drehzeiger"));
		dialog.setLayout(null);
		dialog.setSize(1000, 350);
		JLabel lbl_frequency = new JLabel(LanguageTranslator.getTranslation("Frequenz"));
		lbl_frequency.setBounds(5, 5, 100, 20);
		dialog.add(lbl_frequency);
		edt_frequency = new JTextField("0");
		edt_frequency.setBounds(110, 5, 50, 20);
		dialog.add(edt_frequency);
		JLabel lbl_hz = new JLabel("Hz");
		lbl_hz.setBounds(165, 5, 20, 20);
		dialog.add(lbl_hz);
		JLabel lbl_name = new JLabel(LanguageTranslator.getTranslation("Bezeichnung"));
		lbl_name.setBounds(5, 30, 80, 15);
		dialog.add(lbl_name);
		edt_name = new JTextField();
		edt_name.setBounds(110, 30, 100, 20);
		dialog.add(edt_name);
		JPanel[] phase_phi_0 = new JPanel[3];
		edt_phi0 = new JTextField[3];
		for (int i = 0; i < 3; i++) {
			phase_phi_0[i] = new JPanel();
			phase_phi_0[i].setLayout(null);
			phase_phi_0[i].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " " + (i + 1)));
			JLabel lbl_phi = new JLabel("<html>\u03C6<sub>0</sub>=</html>");
			lbl_phi.setBounds(10, 27, 30, 15);
			phase_phi_0[i].add(lbl_phi);
			edt_phi0[i] = new JTextField();
			edt_phi0[i].setBounds(40, 20, 50, 20);
			phase_phi_0[i].add(edt_phi0[i]);
			JLabel degree = new JLabel("�");
			degree.setBounds(91, 20, 5, 15);
			phase_phi_0[i].add(degree);
			phase_phi_0[i].setBounds(5 + (i * 105), 50, 100, 50);
			dialog.add(phase_phi_0[i]);
		}
		JPanel panel_fmod = new JPanel();
		panel_fmod.setLayout(null);
		panel_fmod.setBounds(5, 105, 990, 180);
		panel_fmod.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Frequenzmodulation")));
		dialog.add(panel_fmod);

		String[] colums2 = {LanguageTranslator.getTranslation("Start") + " P1", LanguageTranslator.getTranslation("Ende") + " P1", LanguageTranslator.getTranslation("Endfrequenz") + " 1", "df1/dt", LanguageTranslator.getTranslation("Start") + " P2", LanguageTranslator.getTranslation("Ende") + " P2", LanguageTranslator.getTranslation("Endfrequenz") + " 2", "df2/dt", LanguageTranslator.getTranslation("Start") + " P3", LanguageTranslator.getTranslation("Ende") + " P3", LanguageTranslator.getTranslation("Endfrequenz") + " 3", "df3/dt"};
		this.tbl_fMod = new JTable(new DefaultTableModel(colums2, 0));
		tableModelFmod = (DefaultTableModel) tbl_fMod.getModel();
		this.tableFmod = new JScrollPane(tbl_fMod);
		this.tableFmod.setBounds(5, 15, 980, 130);
		panel_fmod.add(tableFmod);
		JButton btn_newFmod = new JButton(LanguageTranslator.getTranslation("Hinzuf�gen"));
		btn_newFmod.setBounds(5, 150, 110, 25);
		btn_newFmod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < 3; i++) {
					edt_start[i].setText("");
					edt_end[i].setText("");
					edt_endFrequency[i].setText("");
				}
				fModDialog.setVisible(true);;
			}
		});
		panel_fmod.add(btn_newFmod);
		JButton btn_deleteFmod = new JButton(LanguageTranslator.getTranslation("L�schen"));
		btn_deleteFmod.setBounds(120, 150, 110, 25);
		btn_deleteFmod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbl_fMod.getSelectedRow() >= 0) {
					fModList.remove(tbl_fMod.getSelectedRow());
					updateFmodList();
				}
			}
		});
		panel_fmod.add(btn_deleteFmod);
		JButton btn_ok = new JButton("OK");
		btn_ok.setBounds(5, 285, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (savePhasor()) {
					dialog.dispose();
				}
			}
		});
		dialog.add(btn_ok);
		
		//frequency modulation dialog
		this.fModDialog = new JDialog();
		this.fModDialog.setModal(true);
		this.fModDialog.setTitle(LanguageTranslator.getTranslation("Neue Frequenzmodulation"));
		this.fModDialog.setLayout(null);
		this.fModDialog.setSize(530, 220);
		
		
		
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(null);
		optionPanel.setBounds(5, 5, 235, 50);
		optionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eigenschaften")));
		this.fModDialog.add(optionPanel);
		ButtonGroup btnGroup_symmetric = new ButtonGroup();
		this.radio_symmetric = new JRadioButton(LanguageTranslator.getTranslation("symmetrisch"));
		this.radio_symmetric.setSelected(true);
		this.radio_symmetric.setBounds(5, 20, 110, 20);
		this.radio_symmetric.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);				
			}
			
		});
		btnGroup_symmetric.add(this.radio_symmetric);
		optionPanel.add(this.radio_symmetric);
		this.radio_asymmetric = new JRadioButton(LanguageTranslator.getTranslation("asymmetrisch"));
		this.radio_asymmetric.setSelected(false);
		this.radio_asymmetric.setBounds(115, 20, 115, 20);
		this.radio_asymmetric.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
				for(int i = 1; i < 3; i++) {
					edt_start[i].setText(edt_start[0].getText());
					edt_end[i].setText(edt_end[0].getText());
					edt_endFrequency[i].setText(edt_endFrequency[i].getText());
				}
			}
		});
		btnGroup_symmetric.add(this.radio_asymmetric);
		optionPanel.add(this.radio_asymmetric);
		
		
		
		
		this.phasePanels = new JPanel[3];
		this.edt_start = new JTextField[3];
		this.edt_end = new JTextField[3];
		this.edt_endFrequency = new JTextField[3];
		for (int i = 0; i < 3; i++) {
			this.phasePanels[i] = new JPanel();
			this.phasePanels[i].setLayout(null);
			this.phasePanels[i].setBounds(3 + (i * 170), 60, 170, 90);
			this.phasePanels[i].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " " + (i + 1)));
			
			JLabel lbl_start = new JLabel(LanguageTranslator.getTranslation("Startzeit"));
			lbl_start.setBounds(5, 15, 75, 20);
			this.phasePanels[i].add(lbl_start);
			edt_start[i] = new JTextField();
			edt_start[i].setBounds(90, 15, 60, 20);
			this.phasePanels[i].add(edt_start[i]);
			JLabel lbl_s1 = new JLabel("s");
			lbl_s1.setBounds(150, 15, 10, 20);
			this.phasePanels[i].add(lbl_s1);
			
			JLabel lbl_end = new JLabel(LanguageTranslator.getTranslation("Endzeit"));
			lbl_end.setBounds(5, 40, 75, 20);
			this.phasePanels[i].add(lbl_end);
			edt_end[i] = new JTextField();
			edt_end[i].setBounds(90, 40, 60, 20);
			this.phasePanels[i].add(edt_end[i]);
			JLabel lbl_s2 = new JLabel("s");
			lbl_s2.setBounds(150, 40, 10, 20);
			this.phasePanels[i].add(lbl_s2);
			
			JLabel lbl_endFrequency = new JLabel(LanguageTranslator.getTranslation("Endfrequenz"));
			lbl_endFrequency.setBounds(5, 65, 100, 20);
			this.phasePanels[i].add(lbl_endFrequency);
			edt_endFrequency[i] = new JTextField();
			edt_endFrequency[i].setBounds(90, 65, 60, 20);
			this.phasePanels[i].add(edt_endFrequency[i]);
			JLabel lbl_Hz = new JLabel("Hz");
			lbl_Hz.setBounds(150, 65, 15, 20);
			this.phasePanels[i].add(lbl_Hz);
			
			this.fModDialog.add(this.phasePanels[i]);
		}
		this.phasePanels[1].setVisible(false);
		this.phasePanels[2].setVisible(false);
		JButton btn_ok2 = new JButton("OK");
		btn_ok2.setBounds(5, 155, 100, 25);
		btn_ok2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (newFrequencyModulation()) {
					fModDialog.dispose();
				}
			}
		});
		this.fModDialog.add(btn_ok2);
	}
	
	/**
	 * show a dialog for creating a new phasor
	 */
	public void showNewPhasorDialog() {
		this.lastCreatedPhasor = null;
		editPhasor = null;
		fModList = new ArrayList<FrequencyModulation>();
		updateFmodList();
		edt_name.setText("");
		edt_frequency.setText("50");
		edt_phi0[0].setText("0");
		edt_phi0[1].setText("120");
		edt_phi0[2].setText("240");
		dialog.setVisible(true);
		updateUI();
	}
	
	/**
	 * create a new frequency modulation
	 * @return
	 */
	private boolean newFrequencyModulation() {
		try {
			FrequencyModulation fMod;
			if (this.radio_symmetric.isSelected()) {
				long start = Math.round(Double.parseDouble(edt_start[0].getText().replace(',', '.')) * 1000000);
				long end = Math.round(Double.parseDouble(edt_end[0].getText().replace(',', '.')) * 1000000);
				long fEnd = Math.round(Double.parseDouble(this.edt_endFrequency[0].getText().replace(',', '.')) * 1000000);
				fMod = new FrequencyModulation(start, end, fEnd);
			} else {
				long[] start = new long[3];
				long[] end = new long[3];
				long[] fEnd = new long[3];
				for (int i = 0; i < 3; i++) {
					start[i] = Math.round(Double.parseDouble(edt_start[i].getText().replace(',', '.')) * 1000000);
					end[i] = Math.round(Double.parseDouble(edt_end[i].getText().replace(',', '.')) * 1000000);
					fEnd[i] = Math.round(Double.parseDouble(this.edt_endFrequency[i].getText().replace(',', '.')) * 1000000);
				}
				fMod = new FrequencyModulation(start[0], end[0], fEnd[0], start[1], end[1], fEnd[1], start[2], end[2], fEnd[2]); 
			}
			int posP1 = positionToInsertInFmodList(fMod.gettMod1Start(), fMod.gettMod1End(), 0);
			int posP2 = positionToInsertInFmodList(fMod.gettMod2Start(), fMod.gettMod2End(), 1);
			int posP3 = positionToInsertInFmodList(fMod.gettMod3Start(), fMod.gettMod3End(), 2);
			if (posP1 == posP2 && posP1 == posP3 && posP1 >= 0) {
				fModList.add(posP1, fMod);
				updateFmodList();	
				return true;
			} else {
				JOptionPane.showMessageDialog(this.fModDialog, LanguageTranslator.getTranslation("Frequenzmodulationen d�rfen sich nicht �berschneiden!"));
			}			
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this.fModDialog, LanguageTranslator.getTranslation("Bitte nur g�ltige Zahlenwerte eingeben!"));
		}
		return false;
		
	}
	
	/**
	 * Computes the position in the list of frequency modulations
	 * overlappings are not allowed
	 * @param start start time of frequency modulation
	 * @param end end time of frequency modulation
	 * @param phaseIndex phase
	 * @return position index or -1 if there is no position (e. g. overlapping)
	 */
	private int positionToInsertInFmodList(long start, long end, int phaseIndex) {
		//check end >= start
		if (start >= end) {
			return -1;
		}
		//search for the position
		for (int i = 0; i < this.fModList.size(); i++) {
			if (end <= this.fModList.get(i).getStart()[phaseIndex]) {
				if (i == 0) {
					return 0;
				} else {
					if (start >= this.fModList.get(i - 1).getEnd()[phaseIndex]) {
						return i;
					} else {
						return -1; //no possible position found
					}
				}
			}
		}
		if (this.fModList.size() == 0) {
			return 0; //the new fmod ist the first element
		} else {
			if (start >= this.fModList.get(this.fModList.size() - 1).getEnd()[phaseIndex]) {
				return this.fModList.size(); //the new fmod is the last element
			} else {
				return -1; //no possible position found
			}
		}
	}
	
	/**
	 * update the frequency modulation table
	 */
	private void updateFmodList() {
		while(this.tableModelFmod.getRowCount() > 0) {
			this.tableModelFmod.removeRow(0);
		}
		for (int i = 0; i < this.fModList.size(); i++) {
			String[] start = {DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod1Start() / 1000000d, 3) + "s", DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod2Start() / 1000000d, 3) + "s", DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod3Start() / 1000000d, 3) + "s"};
			String[] end = {DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod1End() / 1000000d, 3) + "s", DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod2End() / 1000000d, 3) + "s", DisplayFunctions.roundDecimals((double) this.fModList.get(i).gettMod3End() / 1000000d, 3) + "s"};
			String[] fEnd = {Frequency.microHz_valueToHzString(this.fModList.get(i).getF1End(), 4), Frequency.microHz_valueToHzString(this.fModList.get(i).getF2End(), 4), Frequency.microHz_valueToHzString(this.fModList.get(i).getF3End(), 4)};
			
			String[] rocof = new String[3];
			if (i == 0) {
				long f_start = Frequency.stringHzTomicroHzLong(this.edt_frequency.getText());
				if (f_start > Long.MIN_VALUE) {
					rocof[0] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF1End() - f_start)) / (this.fModList.get(i).gettMod1End() - this.fModList.get(i).gettMod1Start()), 4) + " Hz/s" ;
					rocof[1] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF2End() - f_start)) / (this.fModList.get(i).gettMod2End() - this.fModList.get(i).gettMod2Start()), 4) + " Hz/s" ;
					rocof[2] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF3End() - f_start)) / (this.fModList.get(i).gettMod3End() - this.fModList.get(i).gettMod3Start()), 4) + " Hz/s" ;
				} else {
					rocof[0] = "undefiniert";
					rocof[2] = "undefiniert";
					rocof[3] = "undefiniert";
				}
			} else {
				rocof[0] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF1End() - this.fModList.get(i - 1).getF1End())) / (this.fModList.get(i).gettMod1End() - this.fModList.get(i).gettMod1Start()), 4) + " Hz/s" ;
				rocof[1] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF2End() - this.fModList.get(i - 1).getF2End())) / (this.fModList.get(i).gettMod2End() - this.fModList.get(i).gettMod2Start()), 4) + " Hz/s" ;
				rocof[2] = GUIMath.roundDecimals(((double)(this.fModList.get(i).getF3End() - this.fModList.get(i - 1).getF3End())) / (this.fModList.get(i).gettMod3End() - this.fModList.get(i).gettMod3Start()), 4) + " Hz/s" ;
			}
			String[] row = {start[0], end[0], fEnd[0], rocof[0], start[1], end[1], fEnd[1], rocof[1], start[2], end[2], fEnd[2], rocof[2]};
			this.tableModelFmod.addRow(row);
		}
	}
	
	/**
	 * save a phasor
	 * @return
	 */
	private boolean savePhasor() {
		//check frequency
		long f_long = Frequency.stringHzTomicroHzLong(this.edt_frequency.getText());
		if (f_long > Long.MIN_VALUE) {
			Frequency f = new Frequency(f_long);
			for (int i = 0; i < this.fModList.size(); i++) {
				f.addFrequencyModulation(this.fModList.get(i));
			}
			//check start angles
			Angle startAngle0;
			Angle startAngle1;
			Angle startAngle2;
			try {
				startAngle0 = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_phi0[0].getText().replace(',', '.'))));
				startAngle1 = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_phi0[1].getText().replace(',', '.'))));
				startAngle2 = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_phi0[2].getText().replace(',', '.'))));
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur g�ltige Zahlenwerte f�r die Startwinkel eingeben!"));
				return false;
			}
			
			//new phasor or edit phasor?
			if (this.editPhasor == null) { //case new phasor (no edit)
				Phasor p = new Phasor(DataControl.getClock(), f);
				p.setName(edt_name.getText().replace(" ", ""));
				p.setStartAngles(startAngle0, startAngle1, startAngle2);
				if (!DataControl.addPhasor(p)) {
					JOptionPane.showMessageDialog(this.dialog, "Ein Drehzeiger mit dieser Bezeichnung ist bereits vorhanden oder die Bezeichnung ist ung�ltig!\n Bitte w�hlen Sie eine noch nicht verwendete Bezeichnung.");
					return false;
				} else {
					this.lastCreatedPhasor = p;
				}
			} else { //case edit
				editPhasor.setName(edt_name.getText());
				editPhasor.setFrequency(f);
				editPhasor.setStartAngles(startAngle0, startAngle1, startAngle2);
				updateData();
			}
			return true;
		} else {
			JOptionPane.showMessageDialog(this.dialog, LanguageTranslator.getTranslation("Bitte eine g�ltige Startfrequenz eingeben!"));
			return false;
		}

	}
	
	/**
	 * bounds for this panel
	 */
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		this.tablePhasors.setBounds(10, 20, width - 20, 135);
	}
	
	/**
	 * update the phasor table on the panel
	 */
	public void updateData() {
		while (this.tableModelPhasors.getRowCount() > 0) {
			this.tableModelPhasors.removeRow(0);
		}
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			String fMod = LanguageTranslator.getTranslation("nein");
			if (DataControl.getPhasors().get(i).getFrequency().getFrequencyModulationList().size() > 0) {
				fMod = LanguageTranslator.getTranslation("ja");
			}
			String[] row = {DataControl.getPhasors().get(i).getName(), Frequency.microHz_valueToHzString(DataControl.getPhasors().get(i).getFrequency().getF0_phase1(), 4), fMod};
			tableModelPhasors.addRow(row);
		}
	}
	
	/**
	 * show a phasor info or edit dialog
	 * @param p phasor
	 */
	public void showPhasorInfo(Phasor p) {		
		this.lastCreatedPhasor = null;
		edt_phi0[0].setText(p.getPhase1().getDegreeWithDecimalsWithoutUnit(3));
		edt_phi0[1].setText(p.getPhase2().getDegreeWithDecimalsWithoutUnit(3));
		edt_phi0[2].setText(p.getPhase3().getDegreeWithDecimalsWithoutUnit(3));
		edt_name.setText(p.getName());
		edt_frequency.setText(DisplayFunctions.roundDecimals((double) p.getFrequency().getF0_phase1() / 1000000, 4)); 
		fModList = new ArrayList<FrequencyModulation>(); 
		for (int i = 0; i < p.getFrequency().getFrequencyModulationList().size(); i++) {
			fModList.add(p.getFrequency().getFrequencyModulationList().get(i));
		}
		updateFmodList();
		
		dialog.setVisible(true);
	}
	
	/**
	 * returns the last created phasor or null if no phasor has been created since the last time when dialog has been showed. 
	 * @return
	 */
	public Phasor getLastCreatedPhasor() {
		return this.lastCreatedPhasor;
	}
	
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.btn_newPhasor.setEnabled(enabled);
		this.btn_edit.setEnabled(enabled);
		this.btn_delete.setEnabled(enabled);
	}
 }
