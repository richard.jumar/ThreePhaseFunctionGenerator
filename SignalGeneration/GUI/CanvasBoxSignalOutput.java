package GUI;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.BorderFactory;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Phasor;
import Data.Signal;

public class CanvasBoxSignalOutput extends CanvasBox {
	private static final long serialVersionUID = 22L;
	private JLabel title;
	public CanvasBoxSignalOutput(Canvas canvas, int x, int y) {
		super(canvas);
		setInput(new CanvasConnector(canvas, this, ConnectorType.SignalInput));
		this.output = null;
		this.setBounds(x, y, 120, 50);
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		title = new JLabel(LanguageTranslator.getTranslation("Signalausgabe"));
		title.setBounds(5, 10, 100, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(100, 1, 19, 19);
		this.add(status);
	}
	
	/**
	 * returns the resulting signal of the tree
	 */
	public Signal getSignal() {
		return this.connectedWith.getTreeElement().getSignal();
	}
	
	/**
	 * returns null because 
	 */
	public CanvasTreeElement getParentNode() {
		return null;
	}

	/**
	 * 
	 */
	public CanvasTreeElement getToplevelElementInTreeSegment() {
		return this;
	}

	public int getFreeInputs(ConnectorType type) {
		if (type == this.input.getType() && !this.input.isConnected()) {
			return 1;
		}
		return 0;
	}

	@Override
	public void resetSubTree(boolean startHere) {
		if (this.input != null) {
			if (this.input.getTreeElement() instanceof CanvasOperator) {
				((CanvasOperator)this.input.getTreeElement()).resetSubTree(false);
			}
		}
	}

	@Override
	public boolean isVariable() {
		return false;
	}

	@Override
	public CanvasConnector getConnectorFor(CanvasTreeElement te) {
		return this.input;
	}

	@Override
	public boolean fullDefined() {
		if (this.connectedWith != null) {
			return this.connectedWith.getTreeElement().fullDefined();
		}
		return false;
	}

	@Override
	public CanvasConnector getOutputConnector() {
		return null;
	}

	@Override
	public AmplitudeModulation getAmplitudeModulation() {
		return null;
	}

	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		return true;
	}
	
	public void updateSignalOrAmplitudeModulation() {
		DataControl.setTopLevelSignal(this.getSignal());
	}
}
