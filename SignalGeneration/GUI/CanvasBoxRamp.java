package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controller.DataControl;
import Data.AmplitudeModulation;
import Data.Angle;
import Data.Frequency;
import Data.NonPeriodicAmplitudeModulationRamp;
import Data.PeriodicAmplitudeModulationRamp;
import Data.Phasor;

public class CanvasBoxRamp extends CanvasBoxAmplitudeModulationInput {

	private static final long serialVersionUID = 16L;
	private PeriodicAmplitudeModulationRamp ramp_periodic;
	private NonPeriodicAmplitudeModulationRamp ramp_nonPeriodic;
	
	private JDialog dialog;
	private JRadioButton radio_periodic;
	private JRadioButton radio_nonPeriodic;
	private JRadioButton radio_symmetric;
	private JRadioButton radio_asymmetric;
	private JComboBox<Phasor> cbo_phasor;
	private JTextField[] edt_ramp0StartTime;
	private JTextField[] edt_ramp0EndTime;
	private JTextField[] edt_ramp1StartTime;
	private JTextField[] edt_ramp1EndTime;
	private JTextField[] edt_startValue;
	private JTextField[] edt_endValue;
	private JPanel phasorPanel;
	private JPanel paramPanel;
	private JPanel[] phasePanels;
	private JLabel[] lbl_ramp0StartTime;
	private JLabel[] lbl_ramp0EndTime;
	private JLabel[] lbl_ramp1StartTime;
	private JLabel[] lbl_ramp1EndTime;
	private JLabel[] lbl_ramp0StartTimeUnit;
	private JLabel[] lbl_ramp0EndTimeUnit;
	private JLabel[] lbl_ramp1StartTimeUnit;
	private JLabel[] lbl_ramp1EndTimeUnit;
	private JLabel[] lbl_startValue;
	private JLabel[] lbl_endValue;
	private JLabel lbl_phasor;
	private JLabel lbl_symmetry;
	private JLabel lbl_min;
	private JLabel lbl_max;
	private SymbolPhasorMini phasorSymbol;
	private GraphPanel gp;
	
	public CanvasBoxRamp(Canvas canvas, int x, int y) {
		super(canvas);
		this.setLayout(null);
		this.setBounds(x, y, 200, 200);
		this.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		JLabel title = new JLabel(LanguageTranslator.getTranslation("Rampe"));
		title.setBounds(5, 5, 100, 15);
		this.add(title);
		this.status = new SymbolStatus();
		this.status.setBounds(180, 1, 19, 19);
		this.add(status);
		this.gp = new GraphPanel(null);
		this.gp.setBounds(1, 90, 198, 80);
		this.add(gp);
		phasorSymbol = new SymbolPhasorMini();
		phasorSymbol.setBounds(1, 22, 13, 13);
		this.add(phasorSymbol);
		lbl_phasor = new JLabel("<" + LanguageTranslator.getTranslation("nicht ausgewählt") + ">");
		lbl_phasor.setBounds(18, 20, 140, 15);
		this.add(lbl_phasor);
		this.lbl_symmetry = new JLabel();
		this.lbl_symmetry.setBounds(5, 40, 190, 15);
		this.add(lbl_symmetry);
		this.lbl_min = new JLabel();
		this.lbl_min.setBounds(5, 55, 190, 15);
		this.add(this.lbl_min);
		this.lbl_max = new JLabel();
		this.lbl_max.setBounds(5, 70, 190, 15);
		this.add(this.lbl_max);
		JButton btn_edit = new JButton(LanguageTranslator.getTranslation("Bearbeiten"));
		btn_edit.setBounds(1, 170, 198, 29);
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showDialog();
			}
		});
		this.add(btn_edit);
		
		
		dialog = new JDialog();
		dialog.setModal(true);
		dialog.setTitle(LanguageTranslator.getTranslation("Eigenschaften Rampe"));
		dialog.setLayout(null);
		dialog.setSize(605, 295);
		
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(null);
		optionPanel.setBounds(5, 5, 240, 70);
		optionPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Eigenschaften")));
		dialog.add(optionPanel);
		ButtonGroup btnGroup_periodic = new ButtonGroup();
		this.radio_periodic = new JRadioButton(LanguageTranslator.getTranslation("periodisch"));
		radio_periodic.setSelected(true);
		radio_periodic.setBounds(5, 20, 110, 20);
		radio_periodic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setPeriodic();
			}		
		});
		btnGroup_periodic.add(radio_periodic);
		optionPanel.add(radio_periodic);
		this.radio_nonPeriodic = new JRadioButton(LanguageTranslator.getTranslation("nichtperiodisch"));
		radio_nonPeriodic.setSelected(false);
		radio_nonPeriodic.setBounds(115, 20, 122, 20);
		radio_nonPeriodic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setNonPeriodic();
			}
			
		});
		btnGroup_periodic.add(radio_nonPeriodic);
		optionPanel.add(radio_nonPeriodic);
		this.radio_symmetric = new JRadioButton(LanguageTranslator.getTranslation("symmetrisch"));
		this.radio_symmetric.setSelected(true);
		this.radio_symmetric.setBounds(5, 40, 110, 20);
		this.radio_symmetric.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);		
	
			}
			
		});
		ButtonGroup btnGroup_symmetric = new ButtonGroup();
		btnGroup_symmetric.add(this.radio_symmetric);
		optionPanel.add(this.radio_symmetric);
		this.radio_asymmetric = new JRadioButton(LanguageTranslator.getTranslation("asymmetrisch"));
		this.radio_asymmetric.setSelected(false);
		this.radio_asymmetric.setBounds(115, 40, 120, 20);
		this.radio_asymmetric.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				phasePanels[0].setBorder(BorderFactory.createTitledBorder("Phase 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
				for(int i = 1; i < 3; i++) {
					edt_startValue[i].setText(edt_startValue[0].getText());
					edt_endValue[i].setText(edt_endValue[0].getText());
					edt_ramp0StartTime[i].setText(edt_ramp0StartTime[0].getText());
					edt_ramp0EndTime[i].setText(edt_ramp0EndTime[0].getText());
				}
			}
		});
		btnGroup_symmetric.add(this.radio_asymmetric);
		optionPanel.add(this.radio_asymmetric);
		
		this.phasorPanel = new JPanel();
		phasorPanel.setLayout(null);
		phasorPanel.setBounds(245, 5, 250, 50);
		phasorPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Drehzeiger")));
		cbo_phasor = new JComboBox<Phasor>();
		cbo_phasor.setBounds(10, 20, 100, 20);
		phasorPanel.add(cbo_phasor);
		JButton btn_newPhasor = new JButton(LanguageTranslator.getTranslation("Neu") + "...");
		btn_newPhasor.setBounds(120, 20, 80, 20);
		btn_newPhasor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.getMainwindow().getPhasorPanel().showNewPhasorDialog();
				updateCbo_phasor();
				Phasor lastCreatedPhasor = Main.getMainwindow().getPhasorPanel().getLastCreatedPhasor();
				if (lastCreatedPhasor != null) {
					cbo_phasor.setSelectedItem(lastCreatedPhasor);
				}
			}
		});
		phasorPanel.add(btn_newPhasor);
		phasorPanel.setVisible(true);
		this.dialog.add(phasorPanel);
		this.paramPanel = new JPanel();
		this.paramPanel.setBounds(5, 75, 580, 160);
		this.paramPanel.setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Parameter")));
		this.paramPanel.setLayout(null);
		this.dialog.add(this.paramPanel);
		this.phasePanels = new JPanel[3];
		this.lbl_ramp0StartTime = new JLabel[3];
		this.lbl_ramp0EndTime = new JLabel[3];
		this.edt_ramp0StartTime = new JTextField[3];
		this.edt_ramp0EndTime = new JTextField[3];
		this.lbl_ramp0StartTimeUnit = new JLabel[3];
		this.lbl_ramp0EndTimeUnit = new JLabel[3];
		this.lbl_ramp1StartTime = new JLabel[3];
		this.lbl_ramp1EndTime = new JLabel[3];
		this.edt_ramp1StartTime = new JTextField[3];
		this.edt_ramp1EndTime = new JTextField[3];
		this.lbl_ramp1StartTimeUnit = new JLabel[3];
		this.lbl_ramp1EndTimeUnit = new JLabel[3];
		this.lbl_startValue = new JLabel[3];
		this.lbl_endValue = new JLabel[3];
		this.edt_startValue = new JTextField[3];
		this.edt_endValue = new JTextField[3];
		for (int i = 0; i < 3; i++) {
			this.phasePanels[i] = new JPanel();
			this.phasePanels[i].setBounds((i * 190) + 5, 15, 190, 140);
			this.phasePanels[i].setLayout(null);
			this.phasePanels[i].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " " + (i + 1)));
			lbl_startValue[i] = new JLabel(LanguageTranslator.getTranslation("Grundwert"));
			lbl_startValue[i].setBounds(5, 15, 80, 20);
			this.phasePanels[i].add(lbl_startValue[i]);
			edt_startValue[i] = new JTextField();
			edt_startValue[i].setBounds(100, 15, 70, 20);
			this.phasePanels[i].add(edt_startValue[i]);			
			lbl_endValue[i] = new JLabel(LanguageTranslator.getTranslation("Alternativwert"));
			lbl_endValue[i].setBounds(5, 35, 95, 20);
			this.phasePanels[i].add(lbl_endValue[i]);
			this.edt_endValue[i] = new JTextField();
			this.edt_endValue[i].setBounds(100, 35, 70, 20);
			this.phasePanels[i].add(edt_endValue[i]);
			this.lbl_ramp0StartTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenstart") + " 1</sub></html>");
			this.lbl_ramp0StartTime[i].setBounds(5, 55, 95, 20);
			this.phasePanels[i].add(lbl_ramp0StartTime[i]);
			this.lbl_ramp0StartTimeUnit[i] = new JLabel("°");
			this.lbl_ramp0StartTimeUnit[i].setBounds(170, 55, 10, 20);
			this.phasePanels[i].add(lbl_ramp0StartTimeUnit[i]);
			this.edt_ramp0StartTime[i] = new JTextField();
			this.edt_ramp0StartTime[i].setBounds(100, 55, 70, 20);
			this.phasePanels[i].add(edt_ramp0StartTime[i]);
			this.lbl_ramp0EndTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenende") + " 1</sub></html>");
			this.lbl_ramp0EndTime[i].setBounds(5, 75, 95, 20);
			this.phasePanels[i].add(lbl_ramp0EndTime[i]);
			this.lbl_ramp0EndTimeUnit[i] = new JLabel("°");
			this.lbl_ramp0EndTimeUnit[i].setBounds(170, 75, 10, 20);
			this.phasePanels[i].add(lbl_ramp0EndTimeUnit[i]);
			this.edt_ramp0EndTime[i] = new JTextField();
			this.edt_ramp0EndTime[i].setBounds(100, 75, 70, 20);
			this.phasePanels[i].add(edt_ramp0EndTime[i]);
			this.lbl_ramp1StartTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenstart") + " 2</sub></html>");
			this.lbl_ramp1StartTime[i].setBounds(5, 95, 95, 20);
			this.phasePanels[i].add(this.lbl_ramp1StartTime[i]);
			this.edt_ramp1StartTime[i] = new JTextField();
			this.edt_ramp1StartTime[i].setBounds(100, 95, 70, 20);
			this.phasePanels[i].add(edt_ramp1StartTime[i]);
			this.lbl_ramp1StartTimeUnit[i] = new JLabel("°");
			this.lbl_ramp1StartTimeUnit[i].setBounds(170, 95, 70, 20);
			this.phasePanels[i].add(lbl_ramp1StartTimeUnit[i]);
			this.lbl_ramp1EndTime[i] = new JLabel("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenende") + " 2</sub></html>");
			this.lbl_ramp1EndTime[i].setBounds(5, 115, 95, 20);
			this.phasePanels[i].add(lbl_ramp1EndTime[i]);
			this.lbl_ramp1EndTimeUnit[i] = new JLabel("°");
			this.lbl_ramp1EndTimeUnit[i].setBounds(170, 115, 10, 20);
			this.phasePanels[i].add(lbl_ramp1EndTimeUnit[i]);
			this.edt_ramp1EndTime[i] = new JTextField();
			this.edt_ramp1EndTime[i].setBounds(100, 115, 70, 20);
			this.phasePanels[i].add(edt_ramp1EndTime[i]);
			this.paramPanel.add(this.phasePanels[i]);
		}
		this.phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
		this.phasePanels[1].setVisible(false);
		this.phasePanels[2].setVisible(false);
		JButton btn_ok = new JButton(LanguageTranslator.getTranslation("OK"));
		btn_ok.setBounds(5, 235, 100, 25);
		btn_ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (radio_periodic.isSelected()) {
						if (cbo_phasor.getSelectedIndex() > -1) {
							if (radio_symmetric.isSelected()) {
								Angle ramp0StartAngle = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0StartTime[0].getText().replace(',', '.'))));
								Angle ramp0EndAngle = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0EndTime[0].getText().replace(',', '.'))));
								Angle ramp1StartAngle = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1StartTime[0].getText().replace(',', '.'))));
								Angle ramp1EndAngle = new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1EndTime[0].getText().replace(',', '.'))));
								if (!(isNextAngle(ramp0StartAngle, ramp0EndAngle, ramp1StartAngle, ramp1EndAngle) && isNextAngle(ramp0EndAngle, ramp1StartAngle, ramp1EndAngle, ramp0StartAngle) && isNextAngle(ramp1StartAngle, ramp1EndAngle, ramp0StartAngle, ramp0EndAngle) && isNextAngle(ramp1EndAngle, ramp0StartAngle, ramp0EndAngle, ramp1StartAngle))) {
									JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Die Rampen dürfen sich nicht überschneiden"));
									return;
								}
								ramp_periodic = new PeriodicAmplitudeModulationRamp((Phasor) cbo_phasor.getSelectedItem(), ramp0StartAngle, ramp0EndAngle, ramp1StartAngle, ramp1EndAngle, Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[0].getText().replace(',', '.')));
								ramp_nonPeriodic = null;



							} else {
								double[] gndValue = {Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_startValue[1].getText().replace(',', '.')), Double.parseDouble(edt_startValue[2].getText().replace(',', '.'))};
								double[] altValue = {Double.parseDouble(edt_endValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[1].getText().replace(',', '.')), Double.parseDouble(edt_endValue[2].getText().replace(',', '.'))};
								Angle[] ramp0StartAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0StartTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0StartTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0StartTime[2].getText().replace(',', '.'))))};
								Angle[] ramp0EndAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0EndTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0EndTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp0EndTime[2].getText().replace(',', '.'))))};
								Angle[] ramp1StartAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1StartTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1StartTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1StartTime[2].getText().replace(',', '.'))))};
								Angle[] ramp1EndAngle = {new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1EndTime[0].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1EndTime[1].getText().replace(',', '.')))), new Angle(Angle.DegreeToAngle(Double.parseDouble(edt_ramp1EndTime[2].getText().replace(',', '.'))))};
								for (int i = 0; i < 3; i++) {
									if (!(isNextAngle(ramp0StartAngle[i], ramp0EndAngle[i], ramp1StartAngle[i], ramp1EndAngle[i]) && isNextAngle(ramp0EndAngle[i], ramp1StartAngle[i], ramp1EndAngle[i], ramp0StartAngle[i]) && isNextAngle(ramp1StartAngle[i], ramp1EndAngle[i], ramp0StartAngle[i], ramp0EndAngle[i]) && isNextAngle(ramp1EndAngle[i], ramp0StartAngle[i], ramp0EndAngle[i], ramp1StartAngle[i]))) {
										JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Die Rampen dürfen sich nicht überschneiden"));
										return;
									}
								}
								ramp_periodic = new PeriodicAmplitudeModulationRamp((Phasor) cbo_phasor.getSelectedItem(), ramp0StartAngle, ramp0EndAngle, ramp1StartAngle, ramp1EndAngle, gndValue, altValue);
								ramp_nonPeriodic = null;
							}
							if (connectedWith != null) {
								status.setStatusDefindedAndConnected();
								connectedWith.getTreeElement().updateSignalOrAmplitudeModulation();
							} else {
								status.setStatusDefined();
							}
							status.updateUI();
						} else {
							JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Es ist kein Drehzeiger ausgewählt"));
							return;
						}
					} else { //not periodic
						if (radio_symmetric.isSelected()) {
							if (Double.parseDouble(edt_ramp0EndTime[0].getText().replace(',', '.')) < Double.parseDouble(edt_ramp0StartTime[0].getText().replace(',', '.'))) {
								JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Das Rampenende darf nicht vor dem Rampenstart liegen"));
								return;
							}
							ramp_nonPeriodic = new NonPeriodicAmplitudeModulationRamp(DataControl.getClock(), Math.round(Double.parseDouble(edt_ramp0StartTime[0].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_ramp0EndTime[0].getText().replace(',', '.')) * 1000000), Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[0].getText().replace(',', '.')));
						} else {
							long[] startTime = {Math.round(Double.parseDouble(edt_ramp0StartTime[0].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_ramp0StartTime[1].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_ramp0StartTime[2].getText().replace(',', '.')) * 1000000)};
							long[] endTime = {Math.round(Double.parseDouble(edt_ramp0EndTime[0].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_ramp0EndTime[1].getText().replace(',', '.')) * 1000000), Math.round(Double.parseDouble(edt_ramp0EndTime[2].getText().replace(',', '.')) * 1000000)};
							double[] startValue = {Double.parseDouble(edt_startValue[0].getText().replace(',', '.')), Double.parseDouble(edt_startValue[1].getText().replace(',', '.')), Double.parseDouble(edt_startValue[2].getText().replace(',', '.'))};
							double[] endValue = {Double.parseDouble(edt_endValue[0].getText().replace(',', '.')), Double.parseDouble(edt_endValue[1].getText().replace(',', '.')), Double.parseDouble(edt_endValue[2].getText().replace(',', '.'))};
							for (int i = 0; i < 3; i++) {
							if (endTime[i] < startTime[i]) {
									JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Ein Rampenende darf nicht vor dem dazugehörigen Rampenstart liegen!"));
									return;
								}
							}
							ramp_nonPeriodic = new NonPeriodicAmplitudeModulationRamp(DataControl.getClock(), startTime, endTime, startValue, endValue);

						}
						ramp_periodic = null;
						if (connectedWith != null) {
							status.setStatusDefindedAndConnected();
							connectedWith.getTreeElement().updateSignalOrAmplitudeModulation();
						} else {
							status.setStatusDefined();
						}
						status.updateUI();
					}
					setMinMaxLabels();

					canvas.updateTopLevelElement();
					updateBoxElements();
					dialog.dispose();

				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(dialog, LanguageTranslator.getTranslation("Bitte nur gültige Zahlenwerte eingeben!"));
				}
			}			
		});
		this.dialog.add(btn_ok);
	}

	public void updateBoxElements() {
		boolean symmetric = false;;
		
		//check periodic
		if (this.ramp_periodic != null) {
			lbl_phasor.setText(this.ramp_periodic.getPhasor().getName() + " (" + Frequency.microHz_valueToHzString(this.ramp_periodic.getPhasor().getFrequency().getF0_phase1(), 4) + ")");
			lbl_phasor.setVisible(true);
			phasorSymbol.setVisible(true);
			gp.setNonPeriodic(null, 0, 0);
			gp.setPeriodic(this.ramp_periodic);
			if (this.ramp_periodic.isSymmetric()) {
				symmetric = true;
			}
			this.radio_periodic.setSelected(true);

		} else if (this.ramp_nonPeriodic != null) {
			this.radio_nonPeriodic.setSelected(true);
			lbl_phasor.setVisible(false);
			phasorSymbol.setVisible(false);
			gp.setPeriodic(null);
			gp.setNonPeriodic(ramp_nonPeriodic, startAndEndForNonPeriodicGraphPanel()[0], startAndEndForNonPeriodicGraphPanel()[1]);
			if (this.ramp_nonPeriodic.isSymmetric()) {
				symmetric = true;
			}
			
		} else {
			return;
		}
		
		//check symmetric
		if (symmetric) {
			lbl_symmetry.setText(LanguageTranslator.getTranslation("Phasen symmetrisch"));
			gp.setSymmetric(true);
			this.radio_symmetric.setSelected(true);
		} else {
			lbl_symmetry.setText(LanguageTranslator.getTranslation("Phasen nicht symmetrisch"));
			gp.setSymmetric(false);
			this.radio_asymmetric.setSelected(true);
			
		}
		setMinMaxLabels();
		gp.computeThumbnail();
	}
	
	private boolean isNextAngle(Angle thisAngle, Angle nextAngle, Angle notNext0, Angle notNext1) {
		if (nextAngle.getAngle() >= thisAngle.getAngle()) {
			if ((notNext0.getAngle() <= thisAngle.getAngle() || notNext0.getAngle() >= nextAngle.getAngle()) && (notNext1.getAngle() <= thisAngle.getAngle() || notNext1.getAngle() >= nextAngle.getAngle())) {
				return true;
			} else {
				return false;
			}
		} else {
			if ((notNext0.getAngle() <= thisAngle.getAngle()) && notNext1.getAngle() <= thisAngle.getAngle() && notNext0.getAngle() >= nextAngle.getAngle() && notNext1.getAngle() >= nextAngle.getAngle()) {
				return true;
			} else {
				return false; 
			}
		}
	}
	
	private long[] startAndEndForNonPeriodicGraphPanel() {
		if (this.ramp_nonPeriodic != null) {
			long min = Long.MAX_VALUE;
			long max = Long.MIN_VALUE;
			for (int i = 0; i < 3; i++) {
				if (this.ramp_nonPeriodic.getRampStartTime()[i] < min) {
					min = this.ramp_nonPeriodic.getRampStartTime()[i];
				}
				if (this.ramp_nonPeriodic.getRampEndTime()[i] > max) {
					max = this.ramp_nonPeriodic.getRampEndTime()[i];
				}
			}
			long diff = max - min;
			if (diff == 0) {
				long[] interval = {min - 1000, max + 1000};
				if (interval[0] < 0) {
					interval[0] = 0;
				}
				return interval;
			} else {
				//show 10% before the first step and 10% after the last step
				long[] interval = {min - (diff / 10), max + (diff / 10)};
				if (interval[0] < 0) {
					interval[0] = 0;
				}
				return interval;
			}
		}
		return null;
	}
	
	private void setMinMaxLabels() {
		double min = Double.MAX_VALUE; 
		double max = Double.MIN_VALUE;
		if (this.ramp_nonPeriodic != null) {

			for (int i = 0; i < 3; i++) {
				if (this.ramp_nonPeriodic.getStartValue()[i] < min) {
					min = this.ramp_nonPeriodic.getStartValue()[i];
				}
				if (this.ramp_nonPeriodic.getStartValue()[i] > max) {
					max = this.ramp_nonPeriodic.getStartValue()[i];
				}
				if (this.ramp_nonPeriodic.getEndValue()[i] < min) {
					min = this.ramp_nonPeriodic.getEndValue()[i];
				}
				if (this.ramp_nonPeriodic.getEndValue()[i] > max) {
					max = this.ramp_nonPeriodic.getEndValue()[i];
				}
			}

		} else {
			for (int i = 0; i < 3; i++) {
				if (this.ramp_periodic.getValue0()[i] < min) {
					min = this.ramp_periodic.getValue0()[i];
				}
				if (this.ramp_periodic.getValue0()[i] > max) {
					max = this.ramp_periodic.getValue0()[i];
				}
				if (this.ramp_periodic.getValue1()[i] < min) {
					min = this.ramp_periodic.getValue1()[i];
				}
				if (this.ramp_periodic.getValue1()[i] > max) {
					max = this.ramp_periodic.getValue1()[i];
				}
			}
		}
		this.lbl_min.setText("min = " + roundDecimals(min, 3));
		this.lbl_max.setText("max = " + roundDecimals(max, 3));
	}
	
	public void showDialog() {
		if (this.ramp_nonPeriodic != null) {
			this.setNonPeriodic();
			this.radio_nonPeriodic.setSelected(true);
			if (this.ramp_nonPeriodic.isSymmetric()) {
				this.radio_symmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);	
				this.edt_startValue[0].setText(roundDecimals(this.ramp_nonPeriodic.getStartValue()[0], 5));
				this.edt_endValue[0].setText(roundDecimals(this.ramp_nonPeriodic.getEndValue()[0], 5));
				this.edt_ramp0StartTime[0].setText(roundDecimals(((double) this.ramp_nonPeriodic.getRampStartTime()[0]) / 1000000d, 6));
				this.edt_ramp0EndTime[0].setText(roundDecimals(((double) this.ramp_nonPeriodic.getRampEndTime()[0]) / 1000000d, 6));
				
			} else {
				this.radio_asymmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
				for (int i = 0; i < 3; i++) {
					this.edt_startValue[i].setText(roundDecimals(this.ramp_nonPeriodic.getStartValue()[i], 5));
					this.edt_endValue[i].setText(roundDecimals(this.ramp_nonPeriodic.getEndValue()[i], 5));
					this.edt_ramp0StartTime[i].setText(roundDecimals(((double) this.ramp_nonPeriodic.getRampStartTime()[i]) / 1000000d, 6));
					this.edt_ramp0EndTime[i].setText(roundDecimals(((double) this.ramp_nonPeriodic.getRampEndTime()[i]) / 1000000d, 6));
				}
			}
		} else if (this.ramp_periodic != null) {
			this.setPeriodic();
			this.radio_periodic.setSelected(true);
			if (this.ramp_periodic.isSymmetric()) {
				this.radio_symmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
				phasePanels[1].setVisible(false);
				phasePanels[2].setVisible(false);	
			} else {
				this.radio_asymmetric.setSelected(true);
				phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Phase") + " 1"));
				phasePanels[1].setVisible(true);
				phasePanels[2].setVisible(true);	
			}
			for (int i = 0; i < 3; i++) {
				this.edt_startValue[i].setText(DisplayFunctions.roundDecimals(this.ramp_periodic.getValue0()[i], 5));
				this.edt_endValue[i].setText(DisplayFunctions.roundDecimals(this.ramp_periodic.getValue1()[i], 5));
				this.edt_ramp0StartTime[i].setText(this.ramp_periodic.getStartAngle0()[i].getDegreeWithDecimalsWithoutUnit(4));
				this.edt_ramp0EndTime[i].setText(this.ramp_periodic.getEndAngle0()[i].getDegreeWithDecimalsWithoutUnit(4));
				this.edt_ramp1StartTime[i].setText(this.ramp_periodic.getStartAngle1()[i].getDegreeWithDecimalsWithoutUnit(4));
				this.edt_ramp1EndTime[i].setText(this.ramp_periodic.getEndAngle1()[i].getDegreeWithDecimalsWithoutUnit(4));
			}
		} else {
			this.radio_periodic.setSelected(true);
			this.radio_symmetric.setSelected(true);
			phasePanels[0].setBorder(BorderFactory.createTitledBorder(LanguageTranslator.getTranslation("Alle Phasen")));
			phasePanels[1].setVisible(false);
			phasePanels[2].setVisible(false);	
		}
		updateCbo_phasor();
		dialog.setVisible(true);;
	}

	
	@Override
	public boolean fullDefined() {
		if (this.ramp_periodic != null || this.ramp_nonPeriodic != null) {
			return true;
		}
		return false;
	}

	@Override
	public AmplitudeModulation getAmplitudeModulation() {
		if (this.ramp_periodic != null) {
			return this.ramp_periodic;
		}
		if (this.ramp_nonPeriodic != null) {
			return this.ramp_nonPeriodic;
		}
		return null;
	}

	public PeriodicAmplitudeModulationRamp getAmRampPeriodic() {
		return this.ramp_periodic;
	}
	
	public NonPeriodicAmplitudeModulationRamp getAmRampNonPeriodic() {
		return this.ramp_nonPeriodic;
	}
	
	public void setPeriodic() {	
		//gui dialog
		this.phasorPanel.setVisible(true);
		for (int i = 0; i < 3; i++) {
			this.lbl_startValue[i].setText(LanguageTranslator.getTranslation("Grundwert"));
			
			this.lbl_endValue[i].setText("Alternativwert");
			this.lbl_ramp1StartTime[i].setVisible(true);
			this.edt_ramp1StartTime[i].setVisible(true);
			this.lbl_ramp1StartTimeUnit[i].setVisible(true);
			this.lbl_ramp1EndTime[i].setVisible(true);
			this.edt_ramp1EndTime[i].setVisible(true);
			this.lbl_ramp1EndTimeUnit[i].setVisible(true);
			this.lbl_ramp0StartTimeUnit[i].setText("°");
			this.lbl_ramp0EndTimeUnit[i].setText("°");
			this.lbl_ramp0StartTime[i].setText("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenstart") + " 1</sub></html>");
			this.lbl_ramp0EndTime[i].setText("<html>\u03C6<sub>" + LanguageTranslator.getTranslation("Rampenende") + " 1</sub></html>");
		}
	}
	
	public void setNonPeriodic() {
		this.phasorPanel.setVisible(false);
		for (int i = 0; i < 3; i++) {
			this.lbl_startValue[i].setText(LanguageTranslator.getTranslation("Startwert"));
			this.lbl_endValue[i].setText(LanguageTranslator.getTranslation("Endwert"));
			this.lbl_ramp0StartTime[i].setText(LanguageTranslator.getTranslation("Rampenstart"));
			this.lbl_ramp0EndTime[i].setText(LanguageTranslator.getTranslation("Rampenende"));
			this.lbl_ramp1StartTime[i].setVisible(false);
			this.edt_ramp1StartTime[i].setVisible(false);
			this.lbl_ramp1StartTimeUnit[i].setVisible(false);
			this.lbl_ramp1EndTime[i].setVisible(false);
			this.edt_ramp1EndTime[i].setVisible(false);
			this.lbl_ramp1EndTimeUnit[i].setVisible(false);
			this.lbl_ramp0StartTimeUnit[i].setText("s");
			this.lbl_ramp0EndTimeUnit[i].setText("s");
		}
	}
	
	
	private void updateCbo_phasor() {
		cbo_phasor.removeAllItems();
		for (int i = 0; i < DataControl.getPhasors().size(); i++) {
			cbo_phasor.addItem(DataControl.getPhasors().get(i));
		}
		if (this.ramp_periodic != null && this.ramp_periodic.getPhasor() != null) {
			cbo_phasor.setSelectedItem(this.ramp_periodic.getPhasor());
		}
		cbo_phasor.updateUI();
	}

	private String roundDecimals(double value, int decimals) {
		String value_str = value + "";
		String[] parts = value_str.split("\\.");
		if (parts.length == 1) {
			return parts[0];
		} else if (parts.length == 2) {
			if (parts[1].length() < decimals) {
				decimals = parts[1].length(); //decimals only if necessary
			}
			return parts[0] + "," + parts[1].substring(0, decimals);
		}
		return "ERROR";
	}

	public void setPeriodicAmRamp(PeriodicAmplitudeModulationRamp ramp) {
		this.ramp_periodic = ramp;		
	}
	
	public void setNonPeriodicAmRamp(NonPeriodicAmplitudeModulationRamp ramp) {
		this.ramp_nonPeriodic = ramp;		
	}

	@Override
	public boolean isDeletePhasorOk(Phasor phasorToDelete) {
		if (this.ramp_periodic != null && this.ramp_periodic.getPhasor() == phasorToDelete) {
			return false;
		} else {
			return true;
		}
	}
}
