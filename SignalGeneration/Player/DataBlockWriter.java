package Player;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import Controller.DataControl;
import Data.Filter;
import Data.Signal;

public class DataBlockWriter implements Runnable {
	private Signal s;
	private MemoryWriter mw;
	private CyclicBarrier barrier;
	private Filter filter;
	private boolean run;
	
	public void run() {
		this.run = true;
		int cacheSize = 0;
		while (run) {
			cacheSize = DataControl.getClock().getLastIterationLength();
			for (int i = 0; i < cacheSize; i++) {
				double[] values = filter.getFilteredValue(s.getCachedValue(i));
				mw.writeData(values);
			}
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
			
		}
	}

	public DataBlockWriter(Signal s, MemoryWriter mw, CyclicBarrier barrier, Filter filter) {
		this.s = s; 
		this.mw = mw;
		this.barrier = barrier;
		this.filter = filter;
	}

	/**
	 * stops writing after this iteration
	 */
	public void activateStop() {
		this.run = false;
		this.mw.setEmptySoundCard();
	}
}
