package Player;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import Controller.DataControl;
/**
 * Pipeline stage 1, calculates the values of the clock and all phasors for the next block. 
 *
 */
public class TimeSaver implements Runnable {
	private CyclicBarrier barrier; 
	private boolean run; 
	
	public TimeSaver(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	public void run() {
		this.run = true;
		while (run) {
			DataControl.getClock().incBlock(1000);
			try {
				this.barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * stops this thread
	 */
	public void activateStop() {
		SignalPlayer.log("Stop Timer");
		this.run = false;
	}

	
}
