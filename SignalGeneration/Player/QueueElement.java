package Player;

/**
 * Queue element for the xml recording queue
 */
public class QueueElement {
	private double[] value;
	private QueueElement next;
	
	/**
	 * Includes one sampling value of a xml recording
	 * @param value
	 */
	public QueueElement(double[] value) {
		this.value = value;
	}

	/**
	 * sets the following element in the queue
	 * @param next
	 */
	public void setNext(QueueElement next) {
		this.next = next;
	}
	
	/**
	 * returns the following queue element
	 * @return
	 */
	public QueueElement getNext() {
		return this.next;
	}
	
	/**
	 * returns the value
	 * @return
	 */
	public double[] getValue() {
		return this.value;
	}
}
