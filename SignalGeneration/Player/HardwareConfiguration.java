package Player;

import Controller.HardwareControl;
import Data.SamplingRate;

public class HardwareConfiguration {
	private int resolution; //in bit
	private int resolutionInByte;
	private SamplingRate samplingRate;
	private int channels; //necessary hardware channels
	private String[] command; //...to start JavaSound or ALSA
	private double[] stepsPerVolt;
	private int bytesPerFrame;
	
	public HardwareConfiguration(int resolution, SamplingRate samplingRate, int channels, String[] command, double[] stepsPerVolt) {
		this.resolution = resolution;
		this.resolutionInByte = resolution / 8;
		this.samplingRate = samplingRate;
		this.channels = channels;
		this.command = command;
		this.stepsPerVolt = stepsPerVolt;
		this.bytesPerFrame = this.resolutionInByte * this.channels;
		HardwareControl.log("Hardware configuration: resolution " + this.resolution + "bit, " + this.channels + " channels, frame size " + this.bytesPerFrame + " Byte, frequency " + this.samplingRate.samplingRate_int);
	}
	
	public double[] getStepsPerVolt() {
		return stepsPerVolt;
	}
	public void setStepsPerVolt(double[] stepsPerVolt) {
		this.stepsPerVolt = stepsPerVolt;
	}
	public int getResolution() {
		return resolution;
	}
	
	public int getResolutionInByte() {
		return this.resolutionInByte;
	}
	
	public void setResolution(int resolution) {
		this.resolution = resolution;
	}
	public SamplingRate getSamplingRate() {
		return samplingRate;
	}
	public void setSamplingRate(SamplingRate samplingRate) {
		this.samplingRate = samplingRate;
	}
	public int getChannels() {
		return channels;
	}
	public void setChannels(int channels) {
		this.channels = channels;
	}
	public String[] getCommand() {
		return command;
	}
	public void setCommand(String[] command) {
		this.command = command;
	}

	public int getBytesPerFrame() {
		return this.bytesPerFrame;
	}

}
