package Player;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import javax.imageio.ImageIO;

public class BitmapPlayer implements Runnable {

	@Override
	public void run() {
		SignalPlayer.log("Drawing bitmap");
		try {
			int width = 8000;
			int height = 256;
			MappedByteBuffer status;
			MappedByteBuffer data;
			RandomAccessFile dataFile;
		
			dataFile = new RandomAccessFile("../data", "rw");

			data = dataFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (256*256*256));

			RandomAccessFile statusFile;
			statusFile = new RandomAccessFile("../status", "rw");
			status = statusFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 10);
			
			while (status.get(1) == (byte) 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			int[] color = {0xFF0000, 0x00FF00, 0x0000FF};
	
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			//set all white
			for (int w = 0; w < width; w++) {
				for (int h = 0; h < height; h++) {
					if (h != 127) {
						img.setRGB(w, h, 0xFFFFFF);
					}
				}
			}
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < 3; j++) {
					int value = 255 - data.get((i * 3) + j) - 128;
					img.setRGB(i, value, color[j]);
				}	
			}
			File out = new File("play.bmp");
			try {
				ImageIO.write(img, "bmp", out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			SignalPlayer.log("Finished drawing bitmap");
			dataFile.close();
			statusFile.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
