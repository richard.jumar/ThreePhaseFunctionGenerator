package Player;

import java.util.concurrent.Semaphore;

/**
 * Buffer for recorded xml files. 
 * The queue consists of elements which include one sample (three phases) 
 */
public class RecordedSignalBuffer {
	private QueueElement first;
	private QueueElement last;
	private int elementCounter;
	private boolean finished;
	private Semaphore semaphore;
	
	public RecordedSignalBuffer() {
		this.elementCounter = 0;
		this.finished = false;
		this.semaphore = new Semaphore(1);
	}
	
	/**
	 * add a new element to the queue
	 * @param newElement
	 */
	public void push(QueueElement newElement) {
		try {
			this.semaphore.acquire();	
			if(first == null) {
				this.first = newElement;
				this.last = newElement;
			} else {
				this.last.setNext(newElement);
				this.last = newElement;
			}

			this.elementCounter++;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.semaphore.release();
	}
	
	/**
	 * removes the first element 
	 * (no return value, use getFirstElement)
	 */
	public void pull() {
		try {
			this.semaphore.acquire();
			if (elementCounter == 0) {
				if (finished) {
					SignalPlayer.log("RecordedSignalBuffer: Finished playing recorded signal");
					return;
				}
			}
			if (this.first == null) {
				SignalPlayer.log("RecordedSignalBuffer: First element is null, but " + elementCounter + " elements are expected in buffer");
			}

			while (this.elementCounter == 0 && !finished) {
				Thread.sleep(1000);
				SignalPlayer.log("RecordedSignalBuffer: Waiting for new sampling values");
			}
			this.first = this.first.getNext();
			this.elementCounter--;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.semaphore.release();
	}

	/**
	 * Is the queue empty (no elements in the queue)?
	 * @return
	 */
	public boolean isEmpty() {
		return (this.first == null);
	}
	
	public boolean hasFinished() {
		return this.finished;
	}
	
	/**
	 * sets a finished flag
	 * if the queue is empty and this flag is set, 
	 * all work is done
	 */
	public void setFinished() {
		this.finished = true;
		SignalPlayer.log("RecordedSignalBuffer: Finished flag is set");
	}
	
	/**
	 * number of the elements in the buffer
	 * @return
	 */
	public int getElementCount() {
		return this.elementCounter;
	}
	
	/**
	 * returns the first queue element
	 * @return
	 */
	public QueueElement getFirstElement() {
		return this.first;
	}
	
	/**
	 * remove all elements from the buffer and reset the buffer
	 */
	public void removeAll() {
		this.first = null;
		this.last = null;
		this.elementCounter = 0;
		this.finished = false;
	}
}
