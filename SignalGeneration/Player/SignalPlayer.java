package Player;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

import Controller.HardwareControl;
import Controller.DataControl;
import Data.AddAmplitudeModulation;
import Data.AddSignal;
import Data.BasicFunction;
import Data.DoubleValueFunction;
import Data.Filter;
import Data.MultAmplitudeModulation;
import Data.MultSignalAmplitudeModulation;
import Data.Signal;
import File.LogFileWriter;


public class SignalPlayer implements Runnable {
	private Signal s;
	private MemoryWriter mw;
	private List<Thread> basicFunctionThreads;
	private Thread thread_timeSaver;
	private Thread thread_dataBlockWriter;
	private List<BasicFunction> basicFunctions;
	private CyclicBarrier barrier;
	private Filter filter;
	private boolean stopAll;
	private TimeSaver timeSaver;
	private DataBlockWriter dataBlockWriter;

	public SignalPlayer (Signal s, HardwareConfiguration hwConfig, Filter filter) {
		this.stopAll = false;
		this.mw = new MemoryWriter(hwConfig);
		this.s = s;
		this.filter = filter;
		DataControl.getClock().setSamplingRate(hwConfig.getSamplingRate());
		HardwareControl.lbl_status.setText("Wiedergabe mit " + hwConfig.getSamplingRate().samplingRate_String + " Hz");
		this.basicFunctionThreads = new ArrayList<Thread>();
		this.basicFunctions = new ArrayList<BasicFunction>();
		
	}
	
	@Override
	public synchronized void run() {
		if (mw.initStatus()) {
			try {			
				if (HardwareControl.cbo_technologie.getSelectedIndex() == 0) {
					String[] command = {"nice", "-n", "-18", "../PlaybackUnit/ALSA/alsa.out"};
					new ProcessBuilder(command).start();
				/*} else if (HardwareControl.cbo_technologie.getSelectedIndex() == 1) {
					String[] command = {"java", "-classpath", "../PlaybackUnit/JavaSound", "Main"};
					new ProcessBuilder(command).start();*/
				} else {
					new Thread(new BitmapPlayer()).start();
				
				}
			
				runParallel();
			} catch (IOException e) {
				SignalPlayer.log("extarnal playing unit couldn't be start");
			}
		}
	}
	
	/**
	 * starts the parallel executed basic function threads, the data block writer thread (pipeline step 3) and the time saver thread (pipeline step 1)
	 */
	public void runParallel() {
		findBasicThreads(this.s);

		this.barrier = new CyclicBarrier(this.basicFunctionThreads.size() + 2, new Runnable() {
		
			@Override
			public void run() {
				DataControl.getClock().swapCaches();
				s.swapCaches();
				if (stopAll) {
					stopAndDeleteThreads();
					
				}
			}
			
		});
		for (int i = 0; i < this.basicFunctionThreads.size(); i++) {
			
			this.basicFunctions.get(i).setBarrier(this.barrier);
			this.basicFunctionThreads.get(i).start();
		}
		this.timeSaver = new TimeSaver(this.barrier);
		this.thread_timeSaver = new Thread(this.timeSaver);
		this.dataBlockWriter = new DataBlockWriter(s, mw, barrier, this.filter);
		this.thread_dataBlockWriter = new Thread(this.dataBlockWriter);
		this.thread_timeSaver.start();
		this.thread_dataBlockWriter.start();
		
	}
	
	/**
	 * searches for basic functions in the tree and adds them to the list
	 * @param f
	 */
	private void findBasicThreads(DoubleValueFunction f) {
		if (f instanceof AddSignal) {
			findBasicThreads(((AddSignal) f).getChild0());
			findBasicThreads(((AddSignal) f).getChild1());
		} else if (f instanceof AddAmplitudeModulation) {
			findBasicThreads(((AddAmplitudeModulation) f).getChild0());
			findBasicThreads(((AddAmplitudeModulation) f).getChild1());
		} else if (f instanceof MultSignalAmplitudeModulation) {
			findBasicThreads(((MultSignalAmplitudeModulation) f).getSignalChild());
			findBasicThreads(((MultSignalAmplitudeModulation) f).getAmplitudeModulationChild());
			
			
		} else if (f instanceof MultAmplitudeModulation) {
			findBasicThreads(((MultAmplitudeModulation) f).getChild0());
			findBasicThreads(((MultAmplitudeModulation) f).getChild1());			
		} else if (f instanceof BasicFunction) {
			this.basicFunctionThreads.add(new Thread((BasicFunction) f));
			this.basicFunctions.add((BasicFunction) f);
		} else {
			//nothing
		}
	}
	
	/**
	 * Only called by barrier!!!
	 */
	private void stopAndDeleteThreads() {
		while (this.basicFunctionThreads.size() > 0) {
			this.basicFunctions.get(0).activateStop();
			this.basicFunctions.remove(0);
			this.basicFunctionThreads.remove(0);
		}
		this.timeSaver.activateStop();
		this.dataBlockWriter.activateStop();
	}
	
	/**
	 * stops executing the threads after reaching the barrier. 
	 */
	public void stop() {
		this.stopAll = true;
	}
	
	public static void log(String log) {
		if (!LogFileWriter.logToFile("player.log", log)) {
			System.out.println("Cannot write to player log file");
		}
		System.out.println("[PlayerLog] " + log);
	}
}
