package Player;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import Controller.HardwareControl;
import GUI.MainWindow;
import Monitor.MonitorWindow;

public class MemoryWriter {
	private static MappedByteBuffer status;	
	private static MappedByteBuffer data; 
	private HardwareConfiguration hwConfig;
	private static int diff_disp = 0; 
	public long start = 0;
	
	/**
	 * constructor, inits the common used memory
	 * @param hwConfig hardware configuration
	 */
	public MemoryWriter(HardwareConfiguration hwConfig) {
		this.hwConfig = hwConfig;
		RandomAccessFile dataFile;
		try {
			dataFile = new RandomAccessFile("../data", "rw");
			data = dataFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 256*256*256);
		} catch (FileNotFoundException e) {
			File.FileLog.log("ERROR: Data file not found. Exspected it in the common parent folder of the main software and the other software parts");
		} catch (IOException e) {
			File.FileLog.log("ERROR: IOException in data file: " + e.getMessage());
		}
		
	}
	
	/**
	 * writes one frame to the common used memory
	 * @param values sampling values from 3 channels
	 * @return successfull
	 */
	public boolean writeData(double[] values) {	
		if (start == 0) {
			start = System.currentTimeMillis();
		}
		byte[] valuesByteArray;
		if (this.hwConfig.getResolution() == 16) {
			valuesByteArray = doubleValuesToS16_LE(values);
		} else if (this.hwConfig.getResolution() == 32) {
			valuesByteArray = doubleValuesToS32_LE(values);
		} else if (this.hwConfig.getResolution() == 8) {
			valuesByteArray = doubleValuesToS8(values);		
		} else {
			SignalPlayer.log("MemoryWriter: Not supported resolution " + this.hwConfig.getResolution() + "bit");
			valuesByteArray = allZero();
		}
		try {		
			//want to write from the next position write + 1 
			int writer = (status.get(0) & 0xFF) * 65536 + (status.get(1) & 0xFF) * 256 + (status.get(2) & 0xFF);
			int nextWriterPosition = (writer + 1) & 0xFFFFFF;
			if (nextWriterPosition == 1) {
				SignalPlayer.log("Jumping to the start of the common used memory");
				data.clear();
			}
			waitNext(nextWriterPosition, this.hwConfig.getBytesPerFrame());		
			data.put(valuesByteArray);
			writer = (writer + this.hwConfig.getBytesPerFrame()) & 0xFFFFFF;
			status.put(0, (byte) ((writer >> 16) & 0xFF));
			status.put(1, (byte) ((writer >> 8) & 0xFF));
			status.put(2, (byte) (writer & 0xFF));
			MonitorWindow.writer = writer;

		} catch (Exception e) {
			SignalPlayer.log("ERROR while writing data to the common used memory or updating status: " + e.getMessage());
			return false;
		}
		return true;
	}
	
	
	/**
	 * initializes the status file and maps it into memory
	 * @return
	 */
	public boolean initStatus() {
		RandomAccessFile statusFile;
		try {
			statusFile = new RandomAccessFile("../status", "rw");
		} catch (FileNotFoundException e) {
			File.FileLog.log("ERROR: Status file not found. Exspected it in the common parent folder of the main software and the other software parts");
			return false;
		}  
		try {
			status = statusFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 10);
		} catch (IOException e) {
			File.FileLog.log("ERROR: IOException while mapping status file to memory: " + e.getMessage());
			try {
				statusFile.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			return false;
		}
		status.put(0, (byte) 0); //writepointerH
		status.put(1, (byte) 0); //writepointerM
		status.put(2, (byte) 0); //writepointerL
		status.put(3, (byte) 255); //readpointerH
		status.put(4, (byte) 255); //readpointerM
		status.put(5, (byte) 255); //readpointerL
		status.put(6, (byte) hwConfig.getSamplingRate().code);
		status.put(7, (byte) (hwConfig.getResolution() / 8)); //from bit to byte -> division by 8
		status.put(8, (byte) HardwareControl.cbo_soundCard.getSelectedIndex());
		status.put(9, (byte) hwConfig.getChannels());
		try {
			statusFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	
	/**
	 * Converting into S8 format with calibration parameters from the hardware configuration
	 * @param values
	 * @return
	 */
	private byte[] doubleValuesToS8(double values[]) {
		byte[] ret = new byte[this.hwConfig.getBytesPerFrame()];
		for (int i = 0; i < 3; i++) {
			int value_int = (int) (values[i] * hwConfig.getStepsPerVolt()[i]);
			ret[i] = (byte) (value_int & 0xFF);
		}
		for (int i = 3; i < this.hwConfig.getBytesPerFrame(); i++) {
			ret[i] = (byte) 0;
		}
		return ret;
	}
	
	
	/**
	 * Converting into S16_LE format with calibration parameters from the hardware configuration
	 * @param values
	 * @return
	 */
	private byte[] doubleValuesToS16_LE(double values[]) {
		byte[] ret = new byte[this.hwConfig.getBytesPerFrame()];
		for (int i = 0; i < 3; i++) {
			int value_int = (int) (values[i] * hwConfig.getStepsPerVolt()[i]);
			ret[2 * i + 1] = (byte) ((value_int >> 8) & 0xFF);
			ret[2 * i] = (byte) (value_int & 0xFF);
		}
		for (int i = 6; i < this.hwConfig.getBytesPerFrame(); i++) {
			ret[i] = (byte) 0;
		}
		return ret;
	}

	/**
	 * Converting into S32_LE format with calibration parameters from the hardware configuration
	 * @param values
	 * @return
	 */
	private byte[] doubleValuesToS32_LE(double values[]) {
		byte[] ret = new byte[this.hwConfig.getBytesPerFrame()];
		for (int i = 0; i < 3; i++) {
			int value_int = (int) (values[i] * hwConfig.getStepsPerVolt()[i]);
			ret[4 * i] = (byte) (value_int & 0xFF);
			ret[4 * i + 1] = (byte) ((value_int >> 8) & 0xFF);
			ret[4 * i + 2] = (byte) ((value_int >> 16) & 0xFF);
			ret[4 * i + 3] = (byte) ((value_int >> 24) & 0xFF);
		}
		for (int i = 12; i < this.hwConfig.getBytesPerFrame(); i++) {
			ret[i] = (byte) 0;
		}
		return ret;
	}

	/**
	 * creates a zero frame
	 * @return
	 */
	private byte[] allZero() {
		byte[] ret = new byte[this.hwConfig.getBytesPerFrame()];
		for (int i = 0; i < this.hwConfig.getBytesPerFrame(); i++) {
			ret[i] = (byte) 0;
		}
		return ret;
	}
	
	
	/**
	 * Waits if the next address is blocked until it's unblocked. 
	 * @param write writePointer
	 * @return
	 * @throws Exception
	 */
	private static boolean waitNext(int writer, int size) {
		byte[] read = {status.get(3), status.get(4), status.get(5)};
		int reader = (read[0] & 0xFF) * 65536 + (read[1] & 0xFF) * 256 + (read[2] & 0xFF);
		MonitorWindow.reader = reader;
		while(!writeAccess(writer, reader, size)) {	
			try {
				for (int i = 0; i < 10; i++) {
					Thread.sleep(200);
					reader = ((status.get(3) & 0xFF) * 65536) + ((status.get(4) & 0xFF) * 256) + (status.get(5) & 0xFF);
					MonitorWindow.reader = reader;
				}
			} catch (InterruptedException e) {
				// do nothing, next loop iteration will come soon
			}

		}
		return true;
	}
	
	/**
	 * checks whether there is a write access to the common used memory
	 * @param writer writer address
	 * @param reader reader address
	 * @param writeLength length to write
	 * @return access
	 */
	private static boolean writeAccess(int writer, int reader, int writeLength) {
		int diff = (reader - writer) & 0xFFFFFF; //3 byte address
		int percentBufferFull = 100 - (diff/167772);
		if (percentBufferFull != MemoryWriter.diff_disp) {
			MainWindow.setBufferValue(percentBufferFull);
			MemoryWriter.diff_disp = percentBufferFull;
		}
		if (writeLength < diff) { // not "<="! Reader address is blocked!
			return true;
		}
		return false;
	}
	
	/**
	 * set the "empty soundcard" ID 255. The play process will stop soon. 
	 */
	public void setEmptySoundCard() {
		status.put(8, (byte) 255); //255 is no soundcard. Play process will stop soon. 
	}
	
}
