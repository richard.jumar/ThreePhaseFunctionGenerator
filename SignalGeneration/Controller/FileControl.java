package Controller;

import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import Data.Phasor;
import File.IniReaderWriter;
import File.XmlFileReader;
import File.XmlFileWriter;
import GUI.Canvas;
import GUI.LanguageTranslator;
import GUI.Main;

/**
 * Coordination of file actions 
 */
public class FileControl {
	
	public static IniReaderWriter ini;
	public static File workspace;
	
	/**
	 * open a canvas from a xml file
	 * @param path path of the xml file
	 */
	public static void loadCanvasFromFile(String path) {
		
		XmlFileReader xml = new XmlFileReader(path);
		//remove all phasors
		while(DataControl.getClock().getPhasors().size() > 0) {
			DataControl.getClock().removePhasor(DataControl.getClock().getPhasors().get(0));
		}
		//load phasors from xml file;
		List<Phasor> phasors = xml.getPhasors();
		for (int i = 0; i < phasors.size(); i++) {
			DataControl.getClock().addPhasor(phasors.get(i));
		}
		
		//load canvas
		Canvas newCanvas = new Canvas();
		xml.readTopLevelElements(newCanvas);
		newCanvas.updateTopLevelElement();
		Main.getMainwindow().setCanvas(newCanvas);
		newCanvas.setFilePath(path);
		newCanvas.updateUI();
		Main.getMainwindow().getPhasorPanel().updateData();
	}
	
	/**
	 * save action for the constructed tree on the canvas
	 */
	public static boolean saveAction(Canvas canvas) {
		if (canvas.getFilePath().isEmpty()) {
			return saveAsAction(canvas);
		} else {
			if ((canvas.getTopLevelType().equals("s") && canvas.getFilePath().endsWith(".s")) || (canvas.getTopLevelType().equals("am") && canvas.getFilePath().endsWith(".am")) || (canvas.getTopLevelType().equals("undef") && canvas.getFilePath().endsWith(".undef"))) {
				XmlFileWriter xfw = new XmlFileWriter(canvas.getFilePath(), canvas);
				return xfw.save();
			} else { //type changed
				return saveAsAction(canvas);
			}
		}
	}
	
	/**
	 * save as action for the constructed tree on the canvas
	 */
	public static boolean saveAsAction(Canvas canvas) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(FileControl.workspace);
		if (canvas.getTopLevelType().equals("s")) {
			fc.setFileFilter(new FileNameExtensionFilter(LanguageTranslator.getTranslation("Signal"), "s"));
		} else if(canvas.getTopLevelType().equals("am")) {
			fc.setFileFilter(new FileNameExtensionFilter(LanguageTranslator.getTranslation("Amplitudenmodulation"), "am"));
		} else {
			fc.setFileFilter(new FileNameExtensionFilter(LanguageTranslator.getTranslation("undefiniert"), "undef"));
		}
		fc.showSaveDialog(Main.getMainwindow());
		if (fc.getSelectedFile() != null) {
			String savePath = fc.getSelectedFile().getPath();
			if (!savePath.endsWith("." + canvas.getTopLevelType())) {
				savePath += "." + canvas.getTopLevelType();
			}
			XmlFileWriter xfw = new XmlFileWriter(savePath, canvas);
			return xfw.save();
		}
		return false;
	}

}
