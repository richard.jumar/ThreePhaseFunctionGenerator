package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JCheckBox;

import Data.SamplingRate;
import File.FileLog;
import File.LogFileWriter;
import GUI.LanguageTranslator;
import GUI.Main;
import Player.HardwareConfiguration;

/**
 * Coordination of the hardware preferences
 */
public class HardwareControl {
	public static JComboBox<String> cbo_technologie;
	public static JComboBox<String> cbo_soundCard;
	public static JComboBox<String> cbo_filter;
	public static JCheckBox cbx_monitor;
	public static JCheckBox cbx_filter;
	public static JProgressBar pbr_buffer;

	public static List<String> msg = new ArrayList<String>();
	public static JLabel lbl_status;

	public static void createComboAndCheckBoxes() {
		String[] technologie = {"ALSA", "Bitmap"};
		HardwareControl.cbo_technologie = new JComboBox<String>(technologie);
		HardwareControl.cbo_technologie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				HardwareControl.reloadSoundCards();				
			}			
		});
		HardwareControl.cbo_soundCard = new JComboBox<String>();
		HardwareControl.cbx_monitor = new JCheckBox(LanguageTranslator.getTranslation("Monitor"));
		HardwareControl.lbl_status = new JLabel();
		HardwareControl.cbx_filter = new JCheckBox(LanguageTranslator.getTranslation("Ausgangsfilter"));
		HardwareControl.cbo_filter = new JComboBox<String>();
		HardwareControl.cbx_filter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (HardwareControl.cbx_filter.isSelected()) {
					HardwareControl.cbo_filter.setEnabled(true);
					Main.getMainwindow().loadFilters();
				} else {
					HardwareControl.cbo_filter.setEnabled(false);
				}
			}
			
			
		});
	}
	
	/**
	 * reload the soundcards in the chosen sound architecture (ALSA or JavaSound) in an external process. 
	 */
	public static void reloadSoundCards() {
		HardwareControl.cbo_soundCard.removeAllItems();
		try {
			String[] removeDevicesFile = {"rm", "-f", "../devices.txt"};//Bei Windows auskommentieren
			Process remove = new ProcessBuilder(removeDevicesFile).start();
			remove.waitFor(); 
			String[] command;
			if (cbo_technologie.getSelectedIndex() == 0) { //ALSA
				String[] alsaCommand = {"../PlaybackUnit/ALSA/readdevices.out"};
				command = alsaCommand;
			/*} else if (cbo_technologie.getSelectedIndex() == 1){ //JavaSound
				String[] javaCommand = {"java", "-classpath", "../PlaybackUnit/JavaSound", "ReadDevices"};
				command = javaCommand;*/
			} else {
				String[] emptyCommand = {"touch", "../devices.txt"};
				command = emptyCommand;
			}
			Process readDevices = new ProcessBuilder(command).start();
			readDevices.waitFor();
			File file_devices = new File("../devices.txt");
			while(!file_devices.exists()) {
				//wait...
				HardwareControl.log("device file doesn't exist. Waiting...");
				Thread.sleep(1000);
			}
			FileReader devFileReader = new FileReader("../devices.txt");
			BufferedReader devReader = new BufferedReader(devFileReader);
			String line = devReader.readLine();
			while (line != null) {
				HardwareControl.cbo_soundCard.addItem(line);
				line = devReader.readLine();
			}
			devReader.close();
		} catch (IOException e) {
			log("problem while reloading soundcards");
		} catch (InterruptedException e) {
			log("interruptexception while loading soundcards");
		}
	}
	
	/**
	 * Get the chosen hardware configuration
	 * @return
	 */
	public static HardwareConfiguration getHardwareConfiguration() {
		if (HardwareControl.cbo_technologie.getSelectedIndex() == 0) { //ALSA
			return readDeviceSettingsFile("../PlaybackUnit/ALSA/devices.ini", HardwareControl.cbo_soundCard.getSelectedItem().toString().split(" \\(")[0]);
		/*} else if (HardwareControl.cbo_technologie.getSelectedIndex() == 1) { //JavaSound
			return readDeviceSettingsFile("../PlaybackUnit/JavaSound/devices.ini", HardwareControl.cbo_soundCard.getSelectedItem().toString().split(" \\[")[0]);*/
		} else { //Bitmap
			return new HardwareConfiguration(8, SamplingRate.samplingRate8000, 3,  null, new double[] {0.3, 0.3, 0.3});
		}
	}
	
	/**
	 * Get the hardware configuration for a given device and a given calibration file
	 * @param calibrationFilePath
	 * @param deviceName
	 * @return
	 */
	private static HardwareConfiguration readDeviceSettingsFile(String calibrationFilePath, String deviceName) {
		try {
			String format = "";
			String channels_string = "";
			String rate = "";
			String stepsPerVoltA_string = "";
			String stepsPerVoltB_string = "";
			String stepsPerVoltC_string = "";
			FileReader fr = new FileReader(calibrationFilePath);
			BufferedReader br = new BufferedReader(fr);
			String line;
			boolean deviceSection = false;
			while ((line = br.readLine()) != null) {
				if (deviceSection) {
					if (line.contains("format")) {
						format = line.split("=")[1];
					} else if (line.contains("channels")) {
						channels_string = line.split("=")[1];
					} else if (line.contains("rate")) {
						rate = line.split("=")[1];
					} else if (line.contains("stepspervoltA")) {
						stepsPerVoltA_string = line.split("=")[1];
					} else if (line.contains("stepspervoltB")) {
						stepsPerVoltB_string = line.split("=")[1];
					} else if (line.contains("stepspervoltC")) {
						stepsPerVoltC_string = line.split("=")[1];
					} else if (line.startsWith("[")){
						deviceSection = false;
					}					
				}
				if (line.equals("[" + deviceName + "]")) {
					deviceSection = true;
				}
			}
			br.close();
			int resolutionInByte = 0;
			SamplingRate samplingRate = null;
			int channels; 
			
			if (format.equals("S8_LE")) {
				resolutionInByte = 1;
			} else if (format.equals("S16_LE")) {
				resolutionInByte = 2;
			} else if (format.equals("S24_LE")) {
				resolutionInByte = 3;
			} else if (format.equals("S32_LE")) {
				resolutionInByte = 4;
			}
			
			if (rate.equals("8000")) {
				samplingRate = SamplingRate.samplingRate8000;
			} else if (rate.equals("12000")) {
				samplingRate = SamplingRate.samplingRate12000;
			} else if (rate.equals("16000")) {
				samplingRate = SamplingRate.samplingRate16000;
			} else if (rate.equals("22050")) {
				samplingRate = SamplingRate.samplingRate22050;
			} else if (rate.equals("24000")) {
				samplingRate = SamplingRate.samplingRate24000;
			} else if (rate.equals("32000")) {
				samplingRate = SamplingRate.samplingRate32000;
			} else if (rate.equals("44100")) {
				samplingRate = SamplingRate.samplingRate44100;
			} else if (rate.equals("48000")) {
				samplingRate = SamplingRate.samplingRate48000;
			} else if (rate.equals("88200")) {
				samplingRate = SamplingRate.samplingRate88200;
			} else if (rate.equals("96000")) {
				samplingRate = SamplingRate.samplingRate96000;
			} else if (rate.equals("176400")) {
				samplingRate = SamplingRate.samplingRate176400;
			} else if (rate.equals("192000")) {
				samplingRate = SamplingRate.samplingRate192000;		
			}
			
			channels = Integer.parseInt(channels_string);
			double stepsPerVoltA = Double.parseDouble(stepsPerVoltA_string);
			double stepsPerVoltB = Double.parseDouble(stepsPerVoltB_string);
			double stepsPerVoltC = Double.parseDouble(stepsPerVoltC_string);
			double[] stepsPerVolt = {stepsPerVoltA, stepsPerVoltB, stepsPerVoltC};
			return new HardwareConfiguration(resolutionInByte * 8, samplingRate, channels, null, stepsPerVolt);
		} catch (IOException e) {
			FileLog.log("Unable to read file " + calibrationFilePath);
			return null;
		} catch (NumberFormatException e) {
			FileLog.log("Unable to find all necessary information for device " + deviceName + ".");
			return null;
		}
	}
	
	/**
	 * hardware log
	 * @param msg
	 */
	public static void log(String msg) {
		if (!LogFileWriter.logToFile("hardware.log", msg)) {
			System.out.println("Cannot write to hardware log file");
		}
		System.out.println("[HardwareLog] " + msg);
	}
}
