package Controller;

import java.util.List;

import Data.AmplitudeModulation;
import Data.Clock;
import Data.Phasor;
import Data.SamplingRate;
import Data.Signal;
import GUI.Main;

public class DataControl {
	private static Clock clock = new Clock(SamplingRate.samplingRate48000); //default, signalplayer will update it
	private static Signal topLevelSignal;
	private static AmplitudeModulation topLevelAmplitudeModulation;

	
	public static void resetClock() {
		clock = new Clock(SamplingRate.samplingRate48000); //default, signalplayer will update it
	}
	
	public static boolean addPhasor(Phasor phasor) {
		if (!clock.addPhasor(phasor)) {
			return false;
		}
		Main.getMainwindow().getPhasorPanel().updateData();
		return true;
	}
	
	public static void removePhasor(Phasor phasor) {
		clock.removePhasor(phasor);
		Main.getMainwindow().getPhasorPanel().updateData();
	}
	
	/**
	 * Sets the top level signal. 
	 * Please set only full defined signals!
	 * @param signal
	 */
	public static void setTopLevelSignal(Signal signal) {
		topLevelAmplitudeModulation = null;
		topLevelSignal = signal;
	}
	
	/**
	 * Sets the top level amplitude modulation. 
	 * Please set only full defined amplitude modulations!
	 * @param signal
	 */
	public static void setTopLevelAmplitudeModulation(AmplitudeModulation am) {
		topLevelSignal = null;
		topLevelAmplitudeModulation = am;
	}
	
	public static void resetTopLevel() {
		topLevelSignal = null;
		topLevelAmplitudeModulation = null;
	}
	
	public static Signal getTopLevelSignal() {
		if (topLevelSignal == null) {
		}
		return topLevelSignal;
	}
	
	public static AmplitudeModulation getTopLevelAmplitudeModulation() {
		return topLevelAmplitudeModulation;
	}
	
	public static List<Phasor> getPhasors() {
		return clock.getPhasors();
	}
	
	public static Clock getClock() {
		return clock;
	}
	
	
}
