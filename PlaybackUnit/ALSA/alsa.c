#include <stdio.h>
#include <time.h>
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/time.h>
static char *device = "default";
unsigned char* data;
unsigned char* status;
unsigned char writePointer[3];
unsigned char readPointer[3];
unsigned char sampleRate;
unsigned char resolution;
unsigned char deviceID;
unsigned char channels;
struct timeval startTime; 
int errorHappend;
unsigned char openedDevice;
int bpi = 3; //2 is Banana Pi M2+, 3 is Banana Pi M3

void updateStatus() {
	writePointer[0] = *status;
	writePointer[1] = *(status + 1);
	writePointer[2] = *(status + 2);
	readPointer[0] = *(status + 3);
	readPointer[1] = *(status + 4);
	readPointer[2] = *(status + 5);
	sampleRate = *(status + 6);
	resolution = *(status + 7);
	deviceID = *(status + 8);
	channels = *(status + 9);
}

void printStatus() {
	printf("writePointer: %i %i %i, readPointer: %i %i %i, sampleRate: %i, resolution: %i, deviceID %i\n", writePointer[0], writePointer[1], writePointer[2], readPointer[0], readPointer[1], readPointer[2], sampleRate, resolution, deviceID, channels);
}

void findHardware() {
	int soundcard = -1;
	snd_ctl_t *handle;
	snd_ctl_card_info_t *info;
	snd_ctl_card_info_alloca(&info);
	if (snd_card_next(&soundcard) < 0 || soundcard < 0) {	
		printf("No soundcard");
		return;
	}
	FILE *devicesFile;
	devicesFile = fopen("../../devices.txt", "w");
	while (soundcard >= 0) {
		char alsaName[6]; //max 254 devices => 3 digits
		char soundcardInfo[256];
		sprintf(alsaName, "hw:%i", soundcard);
		snd_ctl_open(&handle, alsaName, 0);
		snd_ctl_card_info(handle, info);
		snprintf(soundcardInfo, 256, "[%i] %s (%s, %s)\n", soundcard, snd_ctl_card_info_get_name(info), snd_ctl_card_info_get_driver(info), snd_ctl_card_info_get_longname(info));
		int i;
		for (i = 0; i < 256; i++) {
			if ( soundcardInfo[i] == '\n') {
				i = 256;
			} else {
				fputc(soundcardInfo[i], devicesFile);
			}		
		}
		fputc(10, devicesFile); //newline
		printf("%s", soundcardInfo);
		snd_card_next(&soundcard);
		snd_ctl_close(handle);
	}
	fclose(devicesFile);


}

void incReader() {
	unsigned char *readH = (unsigned char*) (status + 3);
	unsigned char *readM = (unsigned char*) (status + 4);
	unsigned char *readL = (unsigned char*) (status + 5);
	unsigned char max = 255;
	if (*readL == max) {
		if (*readM == max) {
			*readH = *readH + 1;
		}
		*readM = *readM + 1;   
	}
	*readL = *readL + 1;
}

void messageLog(char message[], int length) {
	FILE *logFile;
	logFile = fopen("../PlaybackUnit/ALSA/log_alsa.txt", "a");
	struct timeval now, diffTime;
	gettimeofday(&now, (struct timezone *)0);
	timersub(&now, &startTime, &diffTime);
	long ms = ((diffTime.tv_sec * 1000) + (diffTime.tv_usec / 1000)); 
	char timer[32];
	int timeLength = snprintf(timer, 32, "[%d] ", ms);
	int i;
	for (i = 0; i < timeLength; i++) {
		fputc(timer[i], logFile);
	}
	for (i = 0; i < length; i++) {
		if (message[i] == '\n') {
			i = length; //stop writing
		} else {
			fputc(message[i], logFile);
		}
	}
	fputc(10, logFile); //newline
	fclose(logFile);
}

void alsaErrorLog(int error) {
	char error_msg[255];
	int error_msg_length = sprintf(error_msg, "ALSA Error: %s", snd_strerror(error));
	messageLog(error_msg, error_msg_length);
	if (bpi == 3) {
		system("gpio write 21 1"); //red led 1
		system("gpio write 22 0"); //green led 0
	} else {
		system("gpio -g write 5 1"); //red led 1
		system("gpio -g write 6 0"); //green led 0
	}
	errorHappend = 1;
}

int play() {
	//waiting for start...
	do {
		sleep(1);
		updateStatus();
	} while ((int) writePointer[0] == 0 && (int) writePointer[1] == 0 && (int) writePointer[2] == 0);
	errorHappend = 0;

	//set connection properties
	char alsaName[6]; //max 254 devices => 3 digits
	int soundcard = (int) deviceID; //selected by user in the GUI
	int alsaName_length = sprintf(alsaName, "hw:%i", soundcard);
	int error;
	openedDevice = deviceID;
	snd_pcm_t *handle;
	error = snd_pcm_open(&handle, alsaName, SND_PCM_STREAM_PLAYBACK, 0); //function returns a negativ error code or 0 in success case
	if (error < 0) { //error while opening
		char errormsg[256];
		snprintf(errormsg, 256, "ERROR Cannot open %s: %s\n", alsaName, snd_strerror(error));
		messageLog(errormsg, 256);
	} else { //opening was successfull 


		char msg_open[11];
		sprintf(msg_open, "Opened %s", alsaName);
		messageLog(msg_open, 7 + alsaName_length); //open msg in the log file

		char formatName[7] = "unknown";
		snd_pcm_format_t format = SND_PCM_FORMAT_UNKNOWN;
		int rate = 0; 
		if ((int) sampleRate == 1) {
			rate = 8000;
		} else if ((int) sampleRate == 2) {
			rate = 11025;
		} else if ((int) sampleRate == 3) {
			rate = 12000;
		} else if ((int) sampleRate == 4) {
			rate = 16000;
		} else if ((int) sampleRate == 5) {
			rate = 22050;
		} else if ((int) sampleRate == 6) {
			rate = 24000;
		} else if ((int) sampleRate == 7) {
			rate = 32000;
		} else if ((int) sampleRate == 8) {
			rate = 44100;
		} else if ((int) sampleRate == 9) {
			rate = 48000;
		} else if ((int) sampleRate == 10) {
			rate = 88200;
		} else if ((int) sampleRate == 11) {
			rate = 96000;
		} else if ((int) sampleRate == 12) {
			rate = 176400;
		} else if ((int) sampleRate == 13) {
			rate = 192000;
		}
		int channels_int = (int) channels;
		int framesPerBlock; 
		if ((int) resolution == 1) {
			format = SND_PCM_FORMAT_S8; //Unsigned 8 bit big endian
			framesPerBlock = 256 / (1 * channels_int);
			sprintf(formatName, "S8");
		} else if ((int) resolution == 2) {
			format = SND_PCM_FORMAT_S16_LE; //Unsigned 16 bit little endian
			framesPerBlock = 256 / (2 * channels_int);
			sprintf(formatName, "S16_LE");
		} else if ((int) resolution == 3) {
			format = SND_PCM_FORMAT_S24_LE; //unsigned 24 bit little endian
			framesPerBlock = 256 / (3 * channels_int);
			sprintf(formatName, "S24_LE");
		} else if ((int) resolution == 4) {
			format = SND_PCM_FORMAT_S32_LE; //unsigned 32 bit little Endian
			framesPerBlock = 256 / (4 * channels_int);
			sprintf(formatName, "S32_LE");
		}
		snd_pcm_access_t access = SND_PCM_ACCESS_RW_INTERLEAVED;

		snd_pcm_hw_params_t *hwparams;
		
		error = snd_pcm_hw_params_malloc(&hwparams);
		if (error < 0) {
			alsaErrorLog(error);
		}
		error = snd_pcm_hw_params_any(handle, hwparams);
		if (error < 0) {
			alsaErrorLog(error);
		}
		error = snd_pcm_hw_params_set_rate_resample(handle, hwparams, 1);
		if (error < 0) {
			alsaErrorLog(error);
		}
		error = snd_pcm_hw_params_set_access(handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED);
		if (error < 0) {
			alsaErrorLog(error);
		}
		error = snd_pcm_hw_params_set_format(handle, hwparams, format);
		if (error < 0) {
			alsaErrorLog(error);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "format is %s", formatName);
			messageLog(msg, msg_length);
		}
		error = snd_pcm_hw_params_set_channels(handle, hwparams, channels);
		if (error < 0) {
			alsaErrorLog(error);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "set %i channels", channels);
			messageLog(msg, msg_length);
		}
		
		unsigned int rate2 = rate;
		char msg[255];
		int msg_length = sprintf(msg, "Try to set rate to %i Hz", rate);
		messageLog(msg, msg_length);
		error = snd_pcm_hw_params_set_rate_near (handle, hwparams, &rate2, 0);
		if (error < 0) {
			alsaErrorLog(error);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "rate is %i Hz", rate2);
			messageLog(msg, msg_length);
		}

		int dir;
		unsigned int maxTime;
		error = snd_pcm_hw_params_get_buffer_time_max(hwparams, &maxTime, & dir);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read max Buffer time:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "maximum hardware buffer time is %i µs", maxTime);
			messageLog(msg, msg_length);
		}

		snd_pcm_uframes_t bSize;
		error = snd_pcm_hw_params_get_buffer_size(hwparams, &bSize);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read buffer size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char size_msg[255];
			int size_msg_length = sprintf(size_msg, "buffer size is %ld frames", bSize);
			messageLog(size_msg, size_msg_length);
		}

		error = snd_pcm_hw_params_get_buffer_size_min(hwparams, &bSize);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read minimum buffer size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char size_msg[255];
			int size_msg_length = sprintf(size_msg, "minimum buffer size is %ld frames", bSize);
			messageLog(size_msg, size_msg_length);
			printf("Buffersize=%ld\n", bSize);
		}

		error = snd_pcm_hw_params_get_buffer_size_max(hwparams, &bSize);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read maximum buffer size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char size_msg[255];
			int size_msg_length = sprintf(size_msg, "maximum buffer size is %ld frames", bSize);
			messageLog(size_msg, size_msg_length);
			printf("Buffersize=%ld\n", bSize);
		}

		snd_pcm_uframes_t periodSize;
		error = snd_pcm_hw_params_get_period_size(hwparams, &periodSize, &dir);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read period size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "period size is %ld frames", periodSize);
			messageLog(msg, msg_length);
		}

		error = snd_pcm_hw_params_get_period_size_min(hwparams, &periodSize, &dir);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read minimum period size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "minimum period size is %ld frames", periodSize);
			messageLog(msg, msg_length);
		}

		error = snd_pcm_hw_params_get_period_size_max(hwparams, &periodSize, &dir);
		if (error < 0) {
			//Only for information => don't call errormsg. 
			char msg[255];
			int msg_length = sprintf(msg, "Cannot read maximum period size:  %s", snd_strerror(error));
			messageLog(msg, msg_length);
		} else {
			char msg[255];
			int msg_length = sprintf(msg, "maximum period size is %ld frames", periodSize);
			messageLog(msg, msg_length);
		}

		error = snd_pcm_hw_params(handle, hwparams);
		if (error < 0) {
			alsaErrorLog(error);
		}
	
		if (errorHappend) {
			snd_pcm_close(handle);
			return 0;
		}

		char buffer[256];
		int framesWritten;
		int lastError;
		while(openedDevice == deviceID) {
			updateStatus();
		 	while (readPointer[0] == writePointer[0] && readPointer[1] == writePointer[1] && readPointer[2] == writePointer[2]) {
				printf("next address is locked. waiting...\n");
				char msg[17];
				sprintf(msg, "Reader is blocked");
				messageLog(msg, 17);
				printStatus();
				if (bpi == 3) {
					system("gpio -g write 6 0"); //clear green led
					system("gpio -g write 5 1"); //red led
				} else {
					system("gpio -g write 22 0"); //clear green led
					system("gpio -g write 21 1"); //red led
				}
				snd_pcm_close(handle);
				return 0;
			}
			
			incReader(); //increments the readPointer and waits if the writepointer is at the next position
			buffer[readPointer[2]] = *(data + readPointer[0] * 256 * 256 + readPointer[1] * 256 + readPointer[2]);		
			if (readPointer[2] == 255) {
				framesWritten = snd_pcm_writei(handle, buffer, framesPerBlock);
				if (framesWritten < 0) {
					if (framesWritten != lastError) { //avoid double messages
						printf("ERROR %s\n", snd_strerror(framesWritten));
						alsaErrorLog(framesWritten);
						lastError = framesWritten;
						printf ("Error nr %i\n", framesWritten);
						if (framesWritten == -32) {
							snd_pcm_close(handle);
							return 0;
						}
					}
				} else {
					lastError = 0;
				}
			}
			//do sth. 
		}
		if (bpi == 3) {
			system("gpio write 22 0"); //clear green led
		} else {
			system("gpio -g write 6 0"); //clear green led
		}
		snd_pcm_close(handle);
		char msg_close[20];
		sprintf(msg_close, "Connection is closed");
		messageLog(msg_close, 20); //close msg in the log file
		return 1;		
	}
}



int main (char *args[]) {
	gettimeofday(&startTime, (struct timezone *)0); //set start time
	char text[40];
	time_t dateTime = time(0); //datetime for welcome message in the log file
	sprintf(text, "ALSA started at %s", ctime(&dateTime)); 
	messageLog(text, 40);
	int fd = open("../status", O_RDWR); //open status file (Read and write)
	status = mmap(0, 10, PROT_WRITE, MAP_SHARED, fd, 0); 
	sprintf(text, "Opened status file"); 
	messageLog(text, 18);
	int data_file = open("../data", O_RDONLY); //open data file (read only)
	data = mmap(0, (256 * 256 * 256), PROT_READ, MAP_SHARED, data_file, 0);
	sprintf(text, "Opened data file"); 
	messageLog(text, 16);
	updateStatus(); 
	//findHardware();
	if (bpi == 3) {
		system("gpio mode 21 out"); //red led is at pin 29
		system("gpio mode 22 out"); //green led is at pin 31
		system("gpio mode 23 in"); //button in pin 33
		system("gpio mode 25 out"); //button power pin 35
		system("gpio write 21 1"); //set red led
		system("gpio write 22 1"); //set green led
		system("gpio write 25 0"); //button power 0V
		sleep(1);
		system("gpio write 21 0"); //clear red led
		system("gpio write 22 0"); //clear green led
	
	} else {
		system("gpio export 7 out"); //red led is at pin 29 (7, gpio 5)
		system("gpio export 8 out"); //green led is at pin 31 (8, gpio 6)
		system("gpio export 9 in"); //button in pin 33 (wi 23)
		system("gpio export 17 out"); //button power pin 35 (gpio 26)
		system("gpio -g write 5 1"); //set red led
		system("gpio -g write 6 1"); //set green led
		system("gpio -g write 26 1"); //button power 5V
		sleep(1);
		system("gpio -g write 5 0"); //clear red led
		system("gpio -g write 6 0"); //clear green led
	}
	
	//wait for button push (polling) //TODO replace by interrupt	
	int playStatus = 0;
	while(playStatus == 0) {
		char buttonState; 
		int buttonPushed;
		do {
			sleep(1);
			FILE *f ;
			if (bpi == 3) {
				f = popen("gpio read 23", "r");
			} else {
				f = popen("gpio read 23", "r");
			}
			if (f == NULL) {
				printf("start button error");
				return 0;
			} else {
				buttonState = fgetc(f);
				buttonPushed = 0;
				if ((buttonState == '1' && bpi == 2) || (buttonState == '0' && bpi == 3)) {
					buttonPushed = 1;
				}
				fclose(f);
			}
		} while (buttonPushed == 0);
		if (bpi == 3) {
			system("gpio write 21 0"); //clear red led
			system("gpio write 22 1"); //set green led			
		} else {
			system("gpio -g write 5 0"); //clear red led
			system("gpio -g write 6 1"); //set green led
		}
		playStatus = play();
	}
	return 0;
}
