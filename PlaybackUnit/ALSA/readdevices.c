#include <stdio.h>
#include <alsa/asoundlib.h>

void findHardware() {
	int soundcard = -1;
	snd_ctl_t *handle;
	snd_ctl_card_info_t *info;
	snd_ctl_card_info_alloca(&info);
	if (snd_card_next(&soundcard) < 0 || soundcard < 0) {	
		printf("No soundcard");
		return;
	}
	FILE *devicesFile;
	devicesFile = fopen("../devices.txt", "w");
	while (soundcard >= 0) {
		char alsaName[6]; //max 254 devices => 3 digits
		char soundcardInfo[256];
		sprintf(alsaName, "hw:%i", soundcard);
		snd_ctl_open(&handle, alsaName, 0);
		snd_ctl_card_info(handle, info);
		snprintf(soundcardInfo, 256, "%s (%s, %s)\n", snd_ctl_card_info_get_name(info), snd_ctl_card_info_get_driver(info), snd_ctl_card_info_get_longname(info));
		int i;
		for (i = 0; i < 256; i++) {
			if ( soundcardInfo[i] == '\n') {
				i = 256;
			} else {
				fputc(soundcardInfo[i], devicesFile);
			}		
		}
		fputc(10, devicesFile); //newline
		printf("%s", soundcardInfo);
		snd_card_next(&soundcard);
		snd_ctl_close(handle);
	}
	fclose(devicesFile);
}


int main (char *args[]) {
	findHardware();
	return 0;
}
