# Installation
The installation procedure is described in the manual. 

# License
Please note the license and the additional agreements in the license file. Translations of the EUPL v1.2 are available under [https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12](https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12).

# About
The operation mode has been presented on [SMAGRIMET](https://smagrimet.org/) 2019 on April 10th, 2019. The paper is available under [https://publikationen.bibliothek.kit.edu/1000095260](https://publikationen.bibliothek.kit.edu/1000095260). 

KIT-IAI group [Simulation and Visualization](https://www.iai.kit.edu/english/IAI-Workinggroup_1051.php)